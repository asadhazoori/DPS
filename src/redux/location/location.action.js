import {getCoordinatesServices} from '../../utilities/helpers/AccessLocation';
import {SET_USER_LOCATION} from './location.types';

export const setUserLocation = ({lat, lng}) => ({
  type: SET_USER_LOCATION,
  payload: {
    lat,
    lng,
  },
});

export const requestAndGetLocation = () => async dispatch => {
  try {
    let userLocation = null;

    try {
      const newCoordinate = await getCoordinatesServices();
      if (newCoordinate) {
        userLocation = {
          lat: newCoordinate.coords.latitude,
          lng: newCoordinate.coords.longitude,
        };
        dispatch(setUserLocation(userLocation));
      }
    } catch (error) {
      console.log(error, 'error while getting coordinate');
    }
    // console.log("userLocation", userLocation)
    return userLocation;
  } catch (error) {
    console.log('location.action.js', error, 'error while getting coordinate');
    return null;
  }
};
