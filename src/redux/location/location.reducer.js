import { SET_USER_LOCATION } from "./location.types";

const initialState = {
    lat: null,
    lng: null
}

export const locationReducer = (state = initialState, action) => {

    switch (action.type) {
        case SET_USER_LOCATION: {

            const { lat, lng } = action.payload;

            return {
                ...state,
                lat: lat,
                lng: lng
            }
        }
        default:
            return state;
    }




}