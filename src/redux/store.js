import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from "redux-thunk";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { persistStore, persistReducer } from "redux-persist";

import { UserReducer } from './users/user.reducer';
import { ProfileReducer } from './profile/profile.reducer';
import { LeavesReducer } from './leaves/leaves.reducer';
import { AttendanceReducer } from './attendance/attendance.reducer';
import { locationReducer } from './location/location.reducer';
import { ServerReducer } from './server/server.reducer';
import { settingsReducer } from './settings/settings.reducer';
import { LoanReducer } from './loans/loan.reducer';
import { OvertimeReducer } from './overtime/overtime.reducer';
import { LOGOUT_USER } from './users/user.types';
import { ChatReducer } from '../screens/chat/redux/chat.reducer';
import { RequestReducer } from './travel/request.reducer';

const allReducers = combineReducers({
    signin: UserReducer,
    employeeProfile: ProfileReducer,
    leaveStatus: LeavesReducer,
    attendance: AttendanceReducer,
    location: locationReducer,
    server: ServerReducer,
    settings: settingsReducer,
    leaves: LeavesReducer,
    loans: LoanReducer,
    overtime: OvertimeReducer,
    chat: ChatReducer,
    request: RequestReducer,
})

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['signin', 'employeeProfile', 'attendance', 'server', 'settings', 'chat', 'travel'],
    blacklist: ['location', 'leaves', 'loans', 'overtime'],
};

const rootReducer = (state, action) => {
    if (action.type === LOGOUT_USER) {
        const newState = Object.keys(state).reduce((acc, key) => {
            if (persistConfig.whitelist.includes(key)) {
                if (key == 'signin' || key == 'server' || key == 'settings') {
                    acc[key] = state[key];;
                }
                else {
                    acc[key] = undefined;
                }
            }
            return acc;
        }, {});

        state = newState;
    }
    return allReducers(state, action);
};


const persistedReducer = persistReducer(persistConfig, rootReducer);

export default store = createStore(
    persistedReducer,
    applyMiddleware(thunk));

export const persistedStore = persistStore(store);

