import { SET_EMPLOYEE_WISE_LOAN_REQ, SET_MANAGER_WISE_LOAN_REQ } from "./loan.types";

const initialState = {
    empWiseLoanReqs: [],
    mangrWiseLoanReqs: [],
}

export const LoanReducer = (state = initialState, action) => {

    switch (action.type) {
        case SET_EMPLOYEE_WISE_LOAN_REQ: {

            return {
                ...state,
                empWiseLoanReqs: action.payload,
            }
        }

        case SET_MANAGER_WISE_LOAN_REQ: {

            return {
                ...state,
                mangrWiseLoanReqs: action.payload,
            }
        }
        default:
            return state;
    }




}