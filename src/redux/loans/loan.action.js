import { SET_EMPLOYEE_WISE_LOAN_REQ, SET_MANAGER_WISE_LOAN_REQ } from "./loan.types"

export const setEmpWiseLoanReqs = (data) => ({
    type: SET_EMPLOYEE_WISE_LOAN_REQ,
    payload: data
})

export const setMangrWiseLoanReqs = (data) => ({
    type: SET_MANAGER_WISE_LOAN_REQ,
    payload: data
})
