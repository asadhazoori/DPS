import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { t } from 'i18next';
import { commonApi } from '../../../utilities/api/apiController';
import store from '../../store';
import { logout_user } from '../../users/user.actions';
import { setMangrWiseLoanReqs } from '../loan.action';

export const getEmployeeLoans = ({ navigation }) => {
  return async dispatch => {
    try {
      const employeeID = store.getState()?.employeeProfile?.employeeID;

      const body = {
        params: {
          model: 'loan.advance.wags',
          method: 'manager_wise_employee_loan',
          args: [employeeID],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        dispatch(setMangrWiseLoanReqs(response?.data?.result));
        return response?.data?.result;
      } else {
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: manager_wise_employee_loan\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.log(error, 'Error getEmployeeLoansAPI');
    }
  };
};
