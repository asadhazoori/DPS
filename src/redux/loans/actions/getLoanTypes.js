import { commonApi } from '../../../utilities/api/apiController';
import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../users/user.actions';
import { t } from 'i18next';
import store from '../../store';
import { setLoanTypes } from '../../profile/profile.actions';

export const getLoanTypes = ({ navigation }) => {
  return async dispatch => {
    try {
      const { employeeID, isManager } = store.getState()?.employeeProfile;

      const body = {
        params: {
          model: 'loan.advance.wags',
          method: 'get_loan_counts_type_wise',
          args: [employeeID],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        const data = response?.data?.result;

        const loanTypes = data?.loan_types;
        const employeeWiseCounts = data?.employee_wise_counts;
        const managerWiseCounts = data?.manager_wise_counts;
        var cards = [];
        if (isManager) {
          cards = [
            {
              id: 1,
              ...data?.employee_wise_counts,
            },
            {
              id: 2,
              ...data?.manager_wise_counts,
            },
          ];
        }
        await dispatch(
          setLoanTypes(loanTypes, managerWiseCounts, employeeWiseCounts, cards),
        );
      } else {
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: get_loan_counts_type_wise\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};
