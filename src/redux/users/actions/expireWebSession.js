
import { sessionExpire } from '../../../utilities/api/apiController';

export const expireWebSession = ({ navigation }) => {

    return async dispatch => {
        try {
            const body = {};

            const response = await sessionExpire({ body, navigation });
            const result = response?.data?.result?.success;

            if (result) {

                return result;
            }
        } catch (error) {
            console.log('Expire Session Api', error);
        }
    };
};
