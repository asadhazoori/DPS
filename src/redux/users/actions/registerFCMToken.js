import messaging from '@react-native-firebase/messaging';
import { commonApi } from '../../../utilities/api/apiController';
import store from '../../store';
import { logout_user } from '../user.actions';
import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';


export const registerFCMToken = async ({ dispatch, navigation }) => {
    const { uid } = store.getState()?.signin;

    try {
        const token = await messaging().getToken();

        const body = {
            params: {
                model: "res.users",
                method: "register_fcm_token",
                args: [{ "user_id": uid, "token": token }],
                kwargs: {}
            }
        };

        const response = await commonApi({ body, navigation });
        const result = response?.data?.result?.result;

        if (result) {

            return result;
        } else {
            if (response?.data?.error) {
                if (response?.data?.error?.message == 'Odoo Session Expired') {
                    setTimeout(() => { Toast.show(t('session-expired')); }, 500);
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Login' }],
                    });
                    dispatch(logout_user(false));
                } else {
                    setTimeout(() => {
                        Alert.alert(
                            response?.data?.error?.message,
                            `Method: register_fcm_token\n${response?.data?.error?.data?.message}`,
                        );
                    }, 300);
                }
            } else if (
                response == 'AxiosError: Request failed with status code 404'
            ) {
                setTimeout(() => { Toast.show(t('session-expired')); }, 500);
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Login' }],
                });
                dispatch(logout_user(false));
            }
        }
    } catch (error) {
        console.log('register_fcm_token', error);
    }
};
