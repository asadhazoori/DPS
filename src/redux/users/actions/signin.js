import { Alert } from 'react-native';
import { LoginApi } from '../../../utilities/api/apiController';
import { login_user } from '../user.actions';
import { getEmployeeProfile } from '../../profile/actions/getEmployeeProfile';
import Toast from 'react-native-simple-toast';
import { t } from 'i18next';

export const siginin = ({ navigation, inputs, DB, setLoading }) => {
  return async dispatch => {
    try {
      const body = {
        jsonrps: 2.0,
        params: {
          db: DB,
          login: inputs.username,
          password: inputs.password,
        },
      };

      const response = await LoginApi({ body, navigation });

      if (response?.data?.result?.uid) {
        const uid = response?.data?.result?.uid;
        await dispatch(login_user(response?.data?.result, inputs.loggedIn));
        return uid;
      } else if (response?.data?.error?.data?.message == 'Access Denied') {
        setTimeout(() => { Toast.show(t('invalid-credentials')); }, 500)

        setLoading(false);
      } else if (response == 'AxiosError: Network Error') {
        setLoading(false);
        // Toast.show(t('verify-ip-internet-msg'));
      } else if (response?.data?.error) {
        setTimeout(() => {
          Alert.alert(
            response?.data?.error?.message,
            `Method: authenticate\n${response?.data?.error?.data?.message}`,
          );
        }, 300);
        setLoading(false);
      } else {
        setLoading(false);
      }
    } catch (error) {
      console.log('SignInAPI', error);
    }
  };
};
