import { Alert } from 'react-native';
import { LogoutApi } from '../../../utilities/api/apiController';

export const siginOut = async ({ uid, navigation }) => {
  try {
    const body = {
      jsonrps: 2.0,
      params: {
        employee_id: uid,
      },
    };

    const response = await LogoutApi({ body, navigation });

    if (response?.data?.result == 'true') {
      navigation.navigate('Login');
    } else if (response?.data?.error) {
      setTimeout(() => {
        Alert.alert(
          response?.data?.error?.message,
          `${response?.data?.error?.data?.message}`,
        );
      }, 300);
    } else {
      setTimeout(() => {
        Alert.alert('Internet Connection Failed', `${response}`);
      }, 300);
    }
  } catch (error) {
    console.error(error);
  }
};
