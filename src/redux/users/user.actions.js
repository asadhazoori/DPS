import { CHANGE_PASSWORD, LOGGED_IN_GOT_EMPLOYEE, LOGIN_USER, LOGOUT_USER } from "./user.types";


export function login_user(data, loggedIn) {
    return {
        type: LOGIN_USER,
        payload: {
            data: data,
            loggedIn: loggedIn
        }
    }
}



export function loggedIn_gotEmployee(loggedIn) {
    return {
        type: LOGGED_IN_GOT_EMPLOYEE,
        payload: {
            loggedIn: loggedIn
        }
    }
}


export function logout_user(loggedIn) {
    return {
        type: LOGOUT_USER,
        payload: loggedIn
    }
}

export function set_change_Password(value) {

    return {
        type: CHANGE_PASSWORD,
        payload: value
    }
}