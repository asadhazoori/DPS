import {CHANGE_PASSWORD, LOGGED_IN_GOT_EMPLOYEE, LOGIN_USER, LOGOUT_USER} from './user.types';

const initialState = {
  uid: null,
  loggedIn: null,
  firstLogin: false,
  partnerId: null,
  isPasswordChanged: true,
  name: null,
  currentCompany: null
};

export const UserReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_USER:
      return {
        ...state,
        uid: action.payload.data.uid,
        partnerId: action.payload.data.partner_id,
        name: action.payload.data.name,
        currentCompany: action.payload.data.user_companies?.current_company,
        firstLogin: true,
      };

    case LOGGED_IN_GOT_EMPLOYEE:
      return {
        ...state,
        loggedIn: action.payload.loggedIn,
      };

    case LOGOUT_USER:
      return {
        ...state,
        loggedIn: action.payload,
      };

      case CHANGE_PASSWORD:
        return {
          ...state,
          isPasswordChanged: action.payload,
        };

    default:
      return state;
  }
};
