export const LOGIN_USER = 'login_user';
export const LOGOUT_USER = 'logout_user';
export const LOGGED_IN_GOT_EMPLOYEE = 'LoggedIn_GotEmployee';
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';