import { SET_EMPLOYEE_WISE_OVERTIME_REQ, SET_MANAGER_WISE_OVERTIME_REQ } from "./overtime.types";

const initialState = {
    mangrWiseOvertimeReqs: [],
    empWiseOvertimeReqs: [],
}

export const OvertimeReducer = (state = initialState, action) => {

    switch (action.type) {
        case SET_EMPLOYEE_WISE_OVERTIME_REQ: {

            return {
                ...state,
                empWiseOvertimeReqs: action.payload,
            }
        }

        case SET_MANAGER_WISE_OVERTIME_REQ: {

            return {
                ...state,
                mangrWiseOvertimeReqs: action.payload,
            }
        }
        default:
            return state;
    }




}