import { SET_EMPLOYEE_WISE_OVERTIME_REQ, SET_MANAGER_WISE_OVERTIME_REQ } from "./overtime.types";

export const setEmpWiseOvertimeReqs = (data) => ({
    type: SET_EMPLOYEE_WISE_OVERTIME_REQ,
    payload: data
})

export const setMangrWiseOvertimeReqs = (data) => ({
    type: SET_MANAGER_WISE_OVERTIME_REQ,
    payload: data
})
