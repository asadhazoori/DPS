import { commonApi } from '../../../utilities/api/apiController';
import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../users/user.actions';
import { t } from 'i18next';
import store from '../../store';
import { setOvertimeSummary } from '../../profile/profile.actions';

export const getOvertimeSummary = ({ navigation }) => {
  return async dispatch => {
    try {
      const { employeeID, isManager } = store.getState()?.employeeProfile;

      const body = {
        params: {
          model: 'hr.overtime.wags',
          method: 'get_overtime_summary',
          args: [employeeID],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        const data = response?.data?.result;

        const monthlyCounts = data?.monthly_counts;
        const employeeWiseCounts = data?.employee_wise_counts;
        const managerWiseCounts = data?.manager_wise_counts;
        var cards = [];
        if (isManager) {
          cards = [
            {
              id: 1,
              ...data?.monthly_counts,
            },
            {
              id: 2,
              ...data?.manager_wise_counts,
            },
          ];
        }
        await dispatch(
          setOvertimeSummary(
            managerWiseCounts,
            employeeWiseCounts,
            cards,
            monthlyCounts,
          ),
        );
      } else {
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: get_overtime_summary\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.log(error, 'getOvertimeSummary');
    }
  };
};
