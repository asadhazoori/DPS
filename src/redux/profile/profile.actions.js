import { GET_EMPLOYEE_PROFILE, SET_LEAVE_SUMMARY, SET_LOAN_TYPES, SET_OVERTIME_SUMMARY } from "./profile.types";

export function get_employee_profile(data,) {
    return {
        type: GET_EMPLOYEE_PROFILE,
        payload: {
            data: data,
            // familyInfo: family_info
        }

    }
}

export function setLeavesSummary(leaveTypes, cards) {
    return {
        type: SET_LEAVE_SUMMARY,
        payload: {
            leaveTypes,
            cards
        }
    }
}


export const setOvertimeSummary = (managerWiseCounts, employeeWiseCounts, cards, monthlyCounts) => ({
    type: SET_OVERTIME_SUMMARY,
    payload: {
        managerWiseCounts,
        employeeWiseCounts,
        cards,
        monthlyCounts
    }
})


export const setLoanTypes = (loanTypes, managerWiseCounts, employeeWiseCounts, cards) => ({
    type: SET_LOAN_TYPES,
    payload: {
        loanTypes,
        managerWiseCounts,
        employeeWiseCounts,
        cards
    }
})
