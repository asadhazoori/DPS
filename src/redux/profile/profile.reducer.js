import { GET_EMPLOYEE_PROFILE, SET_LEAVE_SUMMARY, SET_LOAN_TYPES, SET_OVERTIME_SUMMARY } from "./profile.types"


const initialState = {
    data: null,
    employeeID: null,
    name: null,
    employeeCode: null,
    department: null,
    isManager: null,

    overtimeCards: [],
    overtimeEmployeeWiseCounts: null,
    overtimeManagerWiseCounts: null,
    overtimeBalance: null,

    loanTypes: [],
    managerWiseLoanCounts: null,
    employeeWiseLoanCounts: null,
    loanCards: [],

    leave_types: [],
    leaveCards: [],

    // shifts: null,
    // holidayStatus: null,
    // loanTypeList: null,
    // familyInfo: null,

}

export const ProfileReducer = (state = initialState, action) => {

    switch (action.type) {
        case GET_EMPLOYEE_PROFILE:

            return {
                ...state,
                data: action.payload.data,
                employeeID: action.payload.data.id,
                name: action.payload.data.name,
                department: action.payload.data.department_id,
                employeeCode: action.payload.data.employee_code,
                isManager: action.payload.data.is_manager
                // shifts: action.payload.data.shifts,
                // loanTypeList: action.payload.data.loan_type_list,
                // holidayStatus: action.payload.data.holiday_status,
                // familyInfo: action.payload.familyInfo
            }

        case SET_LEAVE_SUMMARY:
            return {
                ...state,
                leave_types: action.payload?.leaveTypes,
                leaveCards: action.payload?.cards
            }

        case SET_OVERTIME_SUMMARY:

            return {
                ...state,
                overtimeCards: action.payload.cards,
                overtimeEmployeeWiseCounts: action.payload.employeeWiseCounts,
                overtimeManagerWiseCounts: action.payload.managerWiseCounts,
                overtimeBalance: action.payload.monthlyCounts,
            }


        case SET_LOAN_TYPES: {

            return {
                ...state,
                loanTypes: action.payload.loanTypes,
                managerWiseLoanCounts: action.payload.managerWiseCounts,
                employeeWiseLoanCounts: action.payload.employeeWiseCounts,
                loanCards: action.payload.cards,
            }
        }

        default:
            return state;
    }




}