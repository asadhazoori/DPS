import { commonApi } from '../../../utilities/api/apiController';
import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../users/user.actions';
import { t } from 'i18next';
import { getDailyAttendance } from './getDailyAttendance';

export const registerMacAddress = async ({ dispatch, uid, uniqueId, navigation }) => {
  try {
    const body = {
      params: {
        model: 'res.users',
        method: 'register_mac_address',
        args: [{ user_id: uid, mac_address: uniqueId }],
        kwargs: {},
      },
    };
    const response = await commonApi({ body, navigation });
    if (response?.data?.result?.status == 'success') {
      return true;
    } else {
      if (response?.data?.error) {
        if (response?.data?.error?.message == 'Odoo Session Expired') {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        } else {
          setTimeout(() => {
            Alert.alert(response?.data?.error?.message, `Method: register_mac_address\n${response?.data?.error?.data?.message}`,);
          }, 300);
        }
      } else if (
        response == 'AxiosError: Request failed with status code 404'
      ) {
        setTimeout(() => { Toast.show(t('session-expired')); }, 500);
        navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        });
        dispatch(logout_user(false));
      }
    }
  } catch (error) {
    console.log(error, 'error');
  }
};
