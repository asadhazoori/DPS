import { t } from 'i18next';
import { loggedIn_gotEmployee, login_user, logout_user } from '../../users/user.actions';
import { get_employee_profile } from '../profile.actions';
import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { getDailyAttendance } from '../../attendance/actions/getDailyAttendance';
import { getAllChangeRequests } from '../../attendance/actions/getAllChangeRequests';
import { getLoanTypes } from '../../loans/actions/getLoanTypes';
import { getAllocatedLeavesCount } from '../../leaves/actions/getAllocatedLeavesCount';
import { commonApi } from '../../../utilities/api/apiController';
import { getOvertimeSummary } from '../../overtime/actions/getOvertimeSummary';
import { getAllRequests } from '../../travel/actions/getAllRequests';
import { getChatGroups } from '../../../screens/chat/redux/apis/getChatGroups';
import { getEmployeeWarnings } from '../../travel/actions/getEmployeeWarnings';
import { getManualAttendCount_workType } from '../../attendance/actions/ManualAttendance/getManualAttendCount_workType';
import DeviceInfo from 'react-native-device-info';
import { registerMacAddress } from './registerMacAddress';
import store from '../../store';
import { getTickets } from '../../travel/actions/getTickets';

const callApis = async ({ profieData, navigation, dispatch, setLoading, callingFromProfileScreen }) => {
  const { isPasswordChanged } = store.getState()?.signin;

  await dispatch(get_employee_profile(profieData));
  await dispatch(loggedIn_gotEmployee(true));
  await dispatch(
    getDailyAttendance({
      navigation,
      employeeID: profieData.id,
      setLoading,
    }),
  );
  await dispatch(getAllChangeRequests({ navigation, employeeID: profieData?.id }));
  dispatch(getAllocatedLeavesCount({ navigation }));
  dispatch(getLoanTypes({ navigation }));
  dispatch(getOvertimeSummary({ navigation }));
  dispatch(getAllRequests({ navigation }));
  if (profieData?.enable_employee_warnings) {
    dispatch(getEmployeeWarnings({ navigation }));
  }
  if (profieData?.enable_notification) {
    getChatGroups({ navigation, dispatch });
  }
  if (profieData?.enable_manual_attendance) {
    dispatch(getManualAttendCount_workType({ navigation }));
  }
  if (profieData?.enable_internal_tickets) {
    dispatch(getTickets({ navigation }));
  }
  if (callingFromProfileScreen) {
    return;
  }

  if (setLoading) {
    setLoading(false);
    if (profieData?.allow_change_password && isPasswordChanged) {
      navigation.reset({
        index: 0,
        routes: [
          {
            name: 'ChangePassword',
            params: { isPasswordChanged: true },
          },
        ],
      });
    } else {

      navigation.reset({
        index: 0,
        routes: [
          {
            name: 'BottomTab',
          },
        ],
      });
    }
  } else {
    navigation.reset({
      index: 0,
      routes: [
        {
          name: 'BottomTab',
        },
      ],

    });
  }
};


export const getEmployeeProfile = ({ uid, navigation, setLoading, callingFromProfileScreen }) => {
  const id = store.getState()?.signin.uid;

  return async dispatch => {
    try {
      const body = {
        params: {
          model: 'hr.employee.wags',
          method: 'get_employee_details',
          args: [id ? id : uid],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        if (response?.data?.result?.[0]) {
          const profieData = response?.data?.result?.[0];

          const uniqueId = await DeviceInfo?.getUniqueId()
          const deviceName = await DeviceInfo?.getDeviceName()

          const combinedUniqueId = `${uniqueId} && ${deviceName}`;

          if (profieData?.mac_address) {

            const separatedStrings = profieData?.mac_address?.split(" && ");

            const deviceId = separatedStrings[0];
            const deviceeName = separatedStrings[1];

            if (deviceId === uniqueId) {
              await callApis({ navigation, profieData, dispatch, setLoading, callingFromProfileScreen });
            } else {
              if (setLoading) {
                setLoading(false)
                return {
                  message: "Already Logged In",
                  deviceeName,
                };
              } else {
                navigation.reset({
                  index: 0,
                  routes: [{
                    name: 'Login', params: {
                      status: {
                        message: "Already Logged In",
                        deviceeName,
                      }
                    },
                  }],
                });
                dispatch(logout_user(false));
              }
            }
          } else {

            const isRegistered = await registerMacAddress({
              dispatch,
              navigation,
              uid: id ? id : uid,
              uniqueId: combinedUniqueId,
            });

            if (isRegistered) {
              await callApis({ profieData, navigation, dispatch, setLoading, callingFromProfileScreen });
            }
          }

        } else {
          if (setLoading) {
            setLoading(false);
            setTimeout(() => { Toast.show(t('employee-not-found'), Toast.LONG); }, 500)
          } else {
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
            setTimeout(() => { Toast.show(t('employee-not-found'), Toast.LONG); }, 500)
          }
          // return 'not-found';
        }
      } else {
        if (setLoading) {
          setLoading(false);
        }
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500)
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: get_employee_details\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => {
            Alert.alert('Session Expired', 'Please Login Again', [
              {
                text: 'OK',
                onPress: () =>
                  navigation.reset({
                    index: 0,
                    routes: [{ name: 'Login' }],
                  }),
              },
            ]);
          }, 300);
        }
      }
    } catch (error) {
      if (setLoading) {
        setLoading(false);
      }
      console.log('EmployeeProfile', error);
    }
  };
};
