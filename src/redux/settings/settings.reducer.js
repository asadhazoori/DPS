import { SET_ACTIVE_SCREEN, SET_COMPANY_DETAILS, SET_APP_VERSION } from "./settings.types";

const initialState = {
  companyLogo: null,
  activeScreen: null,
};

export const settingsReducer = (state = initialState, action) => {
  switch (action.type) {

    case SET_COMPANY_DETAILS: {
      return {
        ...state,
        companyLogo: action.payload
      }
    }

    case SET_ACTIVE_SCREEN: {
      return {
        ...state,
        activeScreen: action.payload
      }
    }

    default:
      return state;
  }
};