import { Alert } from 'react-native';
import { commonApi } from '../../../utilities/api/apiController';
import Toast from 'react-native-simple-toast'
import { logout_user } from '../../users/user.actions';
import store from '../../store';

export const getAppVersion = async ({ navigation, dispatch }) => {
    try {
        const comnpanyId = store.getState().signin.currentCompany

        const body = {
            "params": {
                "model": "res.company",
                "method": "search_read",
                "args": [[["id", "=", comnpanyId ? comnpanyId : 1]]],
                "kwargs": { "fields": ["name", "hr_app_version"] }
            }
        }

        const resp = await commonApi({
            navigation,
            body,
            timeout: 9000
        })

        if (resp?.data?.result) {
            if (resp?.data?.result?.length > 0) {
                return resp?.data?.result?.[0]?.hr_app_version;
            }
        }
        else {
            if (resp?.data?.error) {
                if (resp?.data?.error?.message == "Odoo Session Expired") {
                    dispatch(logout_user(false))

                    navigation.reset({
                        index: 0,
                        routes: [
                            { name: 'Login' }
                        ],
                    })

                }
                else {
                    setTimeout(() => {
                        Alert.alert(resp?.data?.error?.message, `${resp?.data?.error?.data?.message}`);
                    }, 300);
                }
            }

            else if (resp == 'AxiosError: Request failed with status code 404') {
                dispatch(logout_user(false))

                navigation.reset({
                    index: 0,
                    routes: [
                        { name: 'Login' }
                    ],
                })
            } else if (resp == 'AxiosError: Slow Network Detected or Server Problem, Request Timeout!') {
                dispatch(logout_user(false))
            }

        }

    } catch (error) {
        console.log(error, "error in getAppVersion");
    }

}