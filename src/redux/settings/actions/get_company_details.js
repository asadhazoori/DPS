
import { getCompanyDetailsApi } from '../../../utilities/api/apiController';
import { setCompanyDetails, } from '../settings.actions'

export const getCompanyDetails = ({ navigation }) => async dispatch => {

    try {

        const body = {
            "jsonrpc": "2.0",
            "method": "call"
        }
        const resp = await getCompanyDetailsApi({
            navigation,
            body
        })
        // console.log("getCompanyLogoApi")

        if (resp?.data?.result) {
            dispatch(setCompanyDetails(resp?.data?.result))
        }

    } catch (error) {
        console.log(error, "error in getCompanyLogoApi");
    }

}