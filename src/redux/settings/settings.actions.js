import { SET_ACTIVE_SCREEN, SET_COMPANY_DETAILS } from "./settings.types"


export const setCompanyDetails = (data) => ({
  type: SET_COMPANY_DETAILS,
  payload: data
})

export const setActiveScreen = (data) => ({
  type: SET_ACTIVE_SCREEN,
  payload: data
})
