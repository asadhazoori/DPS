import { commonApi } from '../../../utilities/api/apiController';
import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../users/user.actions';
import { t } from 'i18next';
import store from '../../store';
import { setLeavesSummary } from '../../profile/profile.actions';

function filterLeaveTypes(leaveTypes, empGender, empMarital) {
  return leaveTypes.filter(leave => {
    const genderMatch =
      leave?.gender?.code === "both" ||
      leave?.gender?.code === empGender ||
      empGender === false || leave?.gender?.code === false;

    const maritalMatch =
      leave?.marital_status?.code === false ||
      leave?.marital_status?.code === empMarital ||
      empMarital === false;

    return genderMatch && maritalMatch;
  });
}

export const getAllocatedLeavesCount = ({ navigation }) => {
  return async dispatch => {
    try {
      const { employeeID, isManager, data } = store.getState()?.employeeProfile;
      const empGender = data?.gender;
      const empMarital = data?.marital

      const body = {
        params: {
          model: 'hr.holidays.wags',
          method: 'get_leave_counts_type_wise',
          args: [employeeID],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        const data = response?.data?.result;
        const filteredLeaveTypes = filterLeaveTypes(data?.leave_types, empGender, empMarital);
        let allocationData = data?.allocation_data;

        allocationData.forEach(item => {
          let typeCounts = data?.type_wise_counts[item.leave_type_name];
          item.pending = typeCounts?.pending ? typeCounts?.pending : 0;
          item.approved_by_manager = typeCounts?.approved_by_manager
            ? typeCounts?.approved_by_manager
            : 0;
          item.approved = typeCounts?.approved ? typeCounts?.approved : 0;
          item.rejected = typeCounts?.rejected ? typeCounts?.rejected : 0;
          item.total = typeCounts?.total ? typeCounts?.total : 0;
        });

        var cards = [];
        cards = [
          {
            leave_type_id: 11111,
            ...data?.employee_wise_counts,
          },
        ];

        cards = cards.concat(allocationData);

        if (isManager) {
          cards.push({
            isManager: true,
            leave_type_id: 99999,
            ...data?.manager_wise_counts,
          });
        }

        await dispatch(setLeavesSummary(filteredLeaveTypes, cards));
      } else {
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: get_leave_counts_type_wise\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.log(error, 'getAllocatedLeavesCountAPI');
    }
  };
};
