import { commonApi } from '../../../utilities/api/apiController';
import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../users/user.actions';
import { t } from 'i18next';

export const getMangrWiseLeaves = (navigation, employeeID, dispatch) => {
  return new Promise(async (resolve, reject) => {
    try {
      const body = {
        params: {
          model: 'hr.holidays.wags',
          method: 'manager_wise_leaves',
          args: [employeeID],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        resolve(response?.data?.result);
      } else {
        reject();
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: manager_wise_leaves\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.log(error, 'error');
      reject(error);
    }
  });
};
