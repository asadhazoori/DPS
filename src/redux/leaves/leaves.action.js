import { GET_ALLOCATED_LEAVES, GET_LEAVES_STATUS, GET_LEAVE_TYPES } from "./leaves.types";

export function get_leaves_status(data) {
    return {
        type: GET_LEAVES_STATUS,
        payload: data
    }
}

export function set_leave_types(data) {
    return {
        type: GET_LEAVE_TYPES,
        payload: data
    }
}

export function set_allocated_leaves(leaveTypes, cards) {
    return {
        type: GET_ALLOCATED_LEAVES,
        payload: {
            leaveTypes,
            cards
        }
    }
} 