
import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { t } from 'i18next';
import { commonApi } from '../../../utilities/api/apiController';
import { logout_user } from '../../users/user.actions';
import { setTicketTypes } from '../request.action';

export const getTicketTypes = ({ navigation }) => {
    return async dispatch => {
        try {

            const body = {
                "params": {
                    "model": "internal.ticket.wags",
                    "method": "get_ticket_types_categories",
                    "args": [],
                    "kwargs": {}
                }
            };

            const response = await commonApi({ body, navigation });

            if (response?.data?.result) {
                const data = response?.data?.result;

                await dispatch(
                    setTicketTypes(data?.ticket_types, data?.ticket_categories),
                );
            } else {
                if (response?.data?.error) {
                    if (response?.data?.error?.message == 'Odoo Session Expired') {
                        setTimeout(() => { Toast.show(t('session-expired')); }, 500);
                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Login' }],
                        });
                        dispatch(logout_user(false));
                    } else {
                        setTimeout(() => {
                            Alert.alert(
                                response?.data?.error?.message,
                                `Method: get_loan_counts_type_wise\n${response?.data?.error?.data?.message}`,
                            );
                        }, 300);
                    }
                } else if (
                    response == 'AxiosError: Request failed with status code 404'
                ) {
                    setTimeout(() => { Toast.show(t('session-expired')); }, 500);
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Login' }],
                    });
                    dispatch(logout_user(false));
                }
            }
        } catch (error) {
            console.log(error, 'error');
        }
    };
};
