import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { t } from 'i18next';
import { commonApi } from '../../../utilities/api/apiController';
import { logout_user } from '../../users/user.actions';
import { setStates } from '../request.action';

export const getStates = ({ navigation }) => {
  return async dispatch => {
    try {
      const body = {
        params: {
          model: 'res.country.state',
          method: 'search_read',
          args: [],
          kwargs: { fields: ['id', 'name'] },
        },
      };
      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        dispatch(setStates(response?.data?.result));
      } else {
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: search_read(res.country.state)\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};
