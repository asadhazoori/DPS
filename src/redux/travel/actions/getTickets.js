import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { t } from 'i18next';
import { commonApi } from '../../../utilities/api/apiController';
import store from '../../store';
import { logout_user } from '../../users/user.actions';
import { setTickets } from '../request.action';

export const getTickets = ({ navigation, }) => {
  return async dispatch => {
    try {
      const { uid } = store.getState()?.signin;

      const body = {
        params: {
          model: 'internal.ticket.wags',
          method: 'get_internal_tickets',
          args: [{ user_id: uid }],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        const data = response?.data?.result?.internal_tickets

        const counts = {
          "approved": data?.filter((item) => item?.state?.toLowerCase() === "approved").length,
          "assigned": data?.filter((item) => item?.state?.toLowerCase() === "assigned").length,
          "pending": data?.filter((item) => item?.state?.toLowerCase() === "draft").length,
          "closed": data?.filter((item) => item?.state?.toLowerCase() === "closed").length,
        };

        await dispatch(setTickets(data, counts))

      } else {
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: employee_wise_loan\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};
