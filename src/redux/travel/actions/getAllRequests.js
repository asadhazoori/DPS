import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { t } from 'i18next';
import { commonApi } from '../../../utilities/api/apiController';
import { logout_user } from '../../users/user.actions';
import { setAllRequests } from '../request.action';
import store from '../../store';

export const getAllRequests = ({ navigation }) => {
  return async dispatch => {
    try {
      const { employeeID } = store.getState()?.employeeProfile;

      const body = {
        params: {
          model: 'hr.employee.wags',
          method: 'get_employee_requests',
          args: [employeeID],
          kwargs: {},
        },
      };
      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        const data = response?.data?.result;
        const requestTypes = [
          { id: 2, name: t('Employment Letter') },
          { id: 3, name: t('Travel & Ticket') },
          { id: 4, name: t('Resignation') },
          { id: 5, name: t('Iqama Renewal') },
        ];

        const filteredRequestTypes = requestTypes?.filter(request => {
          switch (request.id) {
            case 2:
              return data.enable_identification_letters;
            case 3:
              return data.enable_travel_tickets;
            case 4:
              return data.enable_employee_resignations;
            case 5:
              return data.enable_employee_renewals;
            default:
              return false;
          }
        });

        dispatch(setAllRequests(data, filteredRequestTypes));
      } else {
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: get_employee_requests\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};
