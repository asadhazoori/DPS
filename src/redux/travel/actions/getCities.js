import { Alert } from "react-native";
import Toast from 'react-native-simple-toast'
import { t } from "i18next";
import { commonApi } from "../../../utilities/api/apiController";
import { logout_user } from "../../users/user.actions";
import { setCities } from "../request.action";

export const getCities = ({ navigation }) => {
    return async (dispatch) => {
        try {

            const body = {
                "params": {
                    "model": "res.city.wags",
                    "method": "search_read",
                    "args": [],
                    "kwargs": { "fields": ["id", "name"] }
                }
            }
            const response = await commonApi({ body, navigation });

            if (response?.data?.result) {
                dispatch(setCities(response?.data?.result))
            }
            else {
                if (response?.data?.error) {
                    if (response?.data?.error?.message == "Odoo Session Expired") {
                        dispatch(logout_user(false))
                        setTimeout(() => { Toast.show(t('session-expired')); }, 500)

                        navigation.reset({
                            index: 0,
                            routes: [
                                { name: 'Login' }
                            ],
                        })

                    }
                    else {
                        setTimeout(() => {
                            Alert.alert(response?.data?.error?.message, `Method: employee_wise_loan\n${response?.data?.error?.data?.message}`);
                        }, 300);
                    }
                }

                else if (response == 'AxiosError: Request failed with status code 404') {
                    dispatch(logout_user(false))
                    setTimeout(() => { Toast.show(t('session-expired')); }, 500)

                    navigation.reset({
                        index: 0,
                        routes: [
                            { name: 'Login' }
                        ],
                    })
                }
            }
        }

        catch (error) {
            console.log(error, "error");
        }
    }
}