import {
  SET_ALL_REQUESTS,
  SET_CITIES,
  SET_COUNTRIES,
  SET_EMPLOYEE_WARNING,
  SET_STATES,
  SET_TICKET,
  SET_TICKET_TYPES,
  SET_TRAVEL_MODES,
} from './request.types';

export const setCountries = data => ({
  type: SET_COUNTRIES,
  payload: data,
});

export const setCities = data => ({
  type: SET_CITIES,
  payload: data,
});

export const setStates = data => ({
  type: SET_STATES,
  payload: data,
});

export const setTravelModes = data => ({
  type: SET_TRAVEL_MODES,
  payload: data,
});

export const setAllRequests = (data, filteredRequestTypes) => ({
  type: SET_ALL_REQUESTS,
  payload: { data: data, filteredRequestTypes: filteredRequestTypes },
});

export const setEmployeeWarnings = data => ({
  type: SET_EMPLOYEE_WARNING,
  payload: data,
});

export const setTicketTypes = (types, categories) => ({
  type: SET_TICKET_TYPES,
  payload: { types: types, categories: categories },
});

export const setTickets = (tickets, counts) => ({
  type: SET_TICKET,
  payload: { tickets: tickets, counts: counts },
});
