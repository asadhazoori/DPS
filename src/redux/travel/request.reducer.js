import {t} from 'i18next';
import {
  SET_ALL_REQUESTS,
  SET_CITIES,
  SET_COUNTRIES,
  SET_EMPLOYEE_WARNING,
  SET_STATES,
  SET_TICKET,
  SET_TICKET_TYPES,
  SET_TRAVEL_MODES,
} from './request.types';

const initialState = {
  countries: [],
  cities: [],
  states: [],
  travelModes: [],
  allRequests: [],
  employeeWarning: [],
  isEmpLetterReqEnabled: false,
  isTravelReqEnabled: false,
  isIqamaReqEnabled: false,
  isResignationReqEnabled: false,
  requestTypes: [],
  requestTypeCards: [],
  tickets: [],
  ticketTypes: [],
  ticketCategories: [],
  ticketCounts: null,
};

export const RequestReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_COUNTRIES: {
      return {
        ...state,
        countries: action.payload,
      };
    }

    case SET_CITIES: {
      return {
        ...state,
        cities: action.payload,
      };
    }
    case SET_STATES: {
      return {
        ...state,
        states: action.payload,
      };
    }

    case SET_TRAVEL_MODES: {
      return {
        ...state,
        travelModes: action.payload,
      };
    }

    case SET_ALL_REQUESTS: {
      const cards = [
        {id: 1, name: t('all-requests')},
        ...action.payload?.filteredRequestTypes,
      ];
      return {
        ...state,
        allRequests: action.payload?.data?.requests,
        isEmpLetterReqEnabled:
          action.payload?.data?.enable_identification_letters,
        isTravelReqEnabled: action.payload?.data?.enable_travel_tickets,
        isIqamaReqEnabled: action.payload?.data?.enable_employee_renewals,
        isResignationReqEnabled:
          action.payload?.data?.enable_employee_resignations,
        requestTypes: action.payload?.filteredRequestTypes,
        requestTypeCards: cards,
      };
    }

    case SET_EMPLOYEE_WARNING: {
      return {
        ...state,
        employeeWarning: action.payload,
      };
    }

    case SET_TICKET_TYPES: {
      return {
        ...state,
        ticketTypes: action.payload?.types,
        ticketCategories: action.payload?.categories,
      };
    }

    case SET_TICKET: {
      return {
        ...state,
        tickets: action.payload?.tickets,
        ticketCounts: action.payload?.counts,
      };
    }

    default:
      return state;
  }
};
