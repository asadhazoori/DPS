import { commonApi } from '../../../utilities/api/apiController';
import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../users/user.actions';
import { t } from 'i18next';
import { getDailyAttendance } from './getDailyAttendance';
import { getFormattedTimeWithoutUTC } from '../../../utilities/helpers/CurretDate';

export const createPunchOut = ({
  uid,
  navigation,
  time,
  latitude,
  longitude,
  setTitle,
  setFreeze,
  employeeID,
  address,
  department,
  log_attendance_id,
  uniqueId,
}) => {
  return async dispatch => {
    try {
      const body = {
        params: {
          model: 'raw.attendance.wags',
          method: 'create_update_raw_attendance',
          args: [
            {
              user_id: uid,
              employee_id: employeeID,
              log_attendance_id: log_attendance_id,
              latitude: latitude,
              longitude: longitude,
              // "datetime": time.dateTime,
              department_id: department,
              location_address: address,
              device_id: uniqueId,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      console.log(response?.data)

      if (response?.data?.result?.message == 'Attendance Updated') {
        setFreeze(false);
        setTitle(`Punched Out at ${time?.time}`);
        dispatch(getDailyAttendance({ navigation, employeeID }));
        // navigation.navigate("Attendance");
        navigation.navigate('Attendance1', {
          screen: 'Attendance',
          params: { refreshDisabled: false },
        });

        // if (nav) {
        //     navigation.replace("Attendance");

        // } else {
        //     navigation.goBack();

        // }
      } else {
        if (response?.data?.result?.message && response?.data?.result?.time) {
          setTitle(`Punching not allowed at this time.`);
          setTimeout(() => {
            Alert.alert(
              'Too Early to Leave !',
              `Minimum time to punch-out is ${getFormattedTimeWithoutUTC(response?.data?.result?.time)}`,
              [{ text: 'OK', onPress: () => navigation.goBack() }],
            );
          }, 200);
        }
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTitle(`Punching Failed !`);
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: create_update_raw_attendance\n${response?.data?.error?.data?.message}`,
                [{ text: 'OK', onPress: () => navigation.goBack() }],
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.error(error, 'error');
    }
  };
};
