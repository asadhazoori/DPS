import { Alert } from 'react-native';
// import {set_change_requests} from '../attendance.action';
import Toast from 'react-native-simple-toast';
import { t } from 'i18next';
import { commonApi } from '../../../../utilities/api/apiController';
import { logout_user } from '../../../users/user.actions';
import { set_work_types } from '../../attendance.action';
import store from '../../../store';

export const getManualAttendCount_workType = ({ navigation }) => {
  const { employeeID, isManager } = store.getState()?.employeeProfile;

  return async dispatch => {
    try {
      const body = {
        params: {
          model: 'attendance.work.type.wags',
          method: 'fetch_work_types',
          args: [{ employee_id: employeeID }],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        const data = response?.data?.result;
        const work_types = data?.work_types;
        const employeeWiseCounts = data?.attendance_counts;
        const managerWiseCounts = data?.manager_attendance_counts;
        var cards = [];
        if (isManager) {
          cards = [
            {
              id: 1,
              ...data?.attendance_counts,
            },
            {
              id: 2,
              ...data?.manager_attendance_counts,
            },
          ];
        }
        await dispatch(
          set_work_types({
            workTypes: work_types,
            counts: employeeWiseCounts,
            cards: cards
          }),
        );
      } else {
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: get_change_request_details\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};
