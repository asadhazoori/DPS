import { Alert } from 'react-native';
// import {set_change_requests} from '../attendance.action';
import Toast from 'react-native-simple-toast';
import { t } from 'i18next';
import { commonApi } from '../../../../utilities/api/apiController';
import { logout_user } from '../../../users/user.actions';
import { set_manual_attendance_request } from '../../attendance.action';
import store from '../../../store';

export const getManualAttendReqs = ({ navigation, }) => {

  const employeeID = store.getState()?.employeeProfile?.employeeID;

  return async dispatch => {
    try {
      const body = {
        params: {
          model: 'manual.attendance.wags',
          method: 'fetch_manual_attendances',
          args: [
            {
              employee_id: employeeID,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        await dispatch(set_manual_attendance_request(response?.data?.result?.attendances));
      } else {
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: get_change_request_details\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};
