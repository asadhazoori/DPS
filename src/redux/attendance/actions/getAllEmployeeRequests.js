import { commonApi } from '../../../utilities/api/apiController';
import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { t } from 'i18next';
import { logout_user } from '../../users/user.actions';
import { set_emp_change_requests } from '../attendance.action';

export const getAllEmployeeRequests = ({ navigation, employeeID }) => {
  return async dispatch => {
    try {
      const body = {
        params: {
          model: 'raw.attendance.wags',
          method: 'manager_wise_change_requests',
          args: [employeeID],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        await dispatch(set_emp_change_requests(response?.data?.result));
      } else {
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: manager_wise_change_requests\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.log(error, 'getAllEmployeeRequests');
    }
  };
};
