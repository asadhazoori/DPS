import { commonApi } from '../../../utilities/api/apiController';
import { Alert } from 'react-native';
import { set_daily_attendance } from '../attendance.action';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../users/user.actions';
import { t } from 'i18next';
import { getTodayPunches } from './getTodayDetails';
import { getYesterdayPunchDetails } from './getYesterdayPunchDetails';

export const getDailyAttendance = ({
  navigation,
  employeeID,
  setLoading,
  employeeAttendance,
  setLoading1,
}) => {
  return async dispatch => {
    try {
      const body = {
        params: {
          model: 'daily.attendance.logs.wags',
          method: 'daily_attendance_details',
          args: [
            {
              employee_id: employeeID,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        if (employeeAttendance) {
          return response?.data?.result;
        } else {
          await dispatch(set_daily_attendance(response?.data?.result));

          if (setLoading1) {
            setLoading1(false);
          }

          const attendanceID =
            response?.data?.result?.active_attendance?.log_attendance_id;
          await dispatch(
            getTodayPunches(navigation, employeeID, attendanceID, setLoading),
          );
          if (response?.data?.result?.active_attendance?.overlapping) {
            const monthly_datewise_attendance =
              response?.data?.result?.monthly_datewise_attendance;
            const yesterdayLogID = Object.values(
              monthly_datewise_attendance,
            )?.[0]?.log_attendance_id;
            await dispatch(
              getYesterdayPunchDetails(
                navigation,
                employeeID,
                yesterdayLogID,
                setLoading,
              ),
            );
          }
        }
      } else {
        if (setLoading) {
          setLoading(false);
        }

        if (setLoading1) {
          setLoading1(false);
        }

        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: daily_attendance_details\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      if (setLoading) {
        setLoading(false);
      }

      if (setLoading1) {
        setLoading1(false);
      }
      console.log(error, 'error');
    }
  };
};
