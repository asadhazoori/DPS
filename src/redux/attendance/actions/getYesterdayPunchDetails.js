import { commonApi } from '../../../utilities/api/apiController';
import { Alert } from 'react-native';
import { set_yesterday_punches } from '../attendance.action';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../users/user.actions';
import { t } from 'i18next';

export const getYesterdayPunchDetails = (
  navigation,
  employeeID,
  attendanceID,
  setLoading,
  isManager,
) => {
  return async dispatch => {
    try {
      const body = {
        params: {
          model: 'daily.attendance.logs.wags',
          method: 'get_punch_details',
          args: [
            {
              employee_id: employeeID,
              log_attendance_id: attendanceID,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        if (isManager) {
          return response?.data?.result;
        } else {
          await dispatch(set_yesterday_punches(response?.data?.result));

          return response?.data?.result;
        }
      } else {
        if (setLoading) {
          setLoading(false);
        }

        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: get_punch_details\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      if (setLoading) {
        setLoading(false);
      }
      console.log(error, 'error');
    }
  };
};
