import { SET_CHANGE_REQUESTS, SET_DAILY_ATTENDANCE, SET_EMP_CHANGE_REQUESTS, SET_EMP_MANUAL_ATTEND_REQS, SET_MAN_WISE_MANUAL_ATTEND_REQS, SET_TODAY_PUNCHES, SET_WORK_TYPES, SET_YESTERDAY_PUNCHES } from "./attendance.types";

export function set_daily_attendance(data) {
    return {
        type: SET_DAILY_ATTENDANCE,
        payload: data
    }
}

export function set_today_punches(data) {
    return {
        type: SET_TODAY_PUNCHES,
        payload: data
    }
}

export function set_yesterday_punches(data) {
    return {
        type: SET_YESTERDAY_PUNCHES,
        payload: data
    }
}

export function set_change_requests(data) {
    return {
        type: SET_CHANGE_REQUESTS,
        payload: data
    }
}

export function set_emp_change_requests(data) {
    return {
        type: SET_EMP_CHANGE_REQUESTS,
        payload: data
    }
}

export function set_manual_attendance_request(data) {
    return {
        type: SET_EMP_MANUAL_ATTEND_REQS,
        payload: data
    }
}

export function set_manager_wise_manual_attendance_request(data) {
    return {
        type: SET_MAN_WISE_MANUAL_ATTEND_REQS,
        payload: data
    }
}

export function set_work_types(data) {
    return {
        type: SET_WORK_TYPES,
        payload: data
    }
} 