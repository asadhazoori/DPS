import {
  SET_CHANGE_REQUESTS,
  SET_DAILY_ATTENDANCE,
  SET_EMP_CHANGE_REQUESTS,
  SET_EMP_MANUAL_ATTEND_REQS,
  SET_MAN_WISE_MANUAL_ATTEND_REQS,
  SET_TODAY_PUNCHES,
  SET_WORK_TYPES,
  SET_YESTERDAY_PUNCHES,
} from './attendance.types';

const initialState = {
  activeAttendance: null,
  attendanceRecords: [],
  yesterdayAttendance: null,
  yesterdayAttendanceRecords: [],
  monthlyDatewiseAttendance: [],
  changeRequests: [],
  empChangeRequests: [],
  manualAttendRequests: [],
  mangrWiseManualAttendReqs: [],
  workTypes: [],
  manualAttendCounts: null,
  manualAttendCards: [],
  pendingReqs: null,
  rejectedReqs: null,
  approvedReqs: null,
};

export const AttendanceReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DAILY_ATTENDANCE:
      return {
        ...state,
        activeAttendance: action.payload.active_attendance,
        monthlyDatewiseAttendance: action.payload.monthly_datewise_attendance,
        yesterdayAttendance: Object.values(
          action.payload.monthly_datewise_attendance,
        )?.[0],
      };

    case SET_TODAY_PUNCHES:
      return {
        ...state,
        attendanceRecords: action.payload,
      };

    case SET_YESTERDAY_PUNCHES:
      return {
        ...state,
        yesterdayAttendanceRecords: action.payload,
      };

    case SET_CHANGE_REQUESTS:
      const requests = action.payload;
      const pendingReqs = requests?.filter(
        item => item?.state === 'change_request',
      ).length;
      const rejectedReqs = requests?.filter(
        item => item?.state === 'rejected',
      ).length;
      const approvedReqs = requests?.filter(
        item => item?.state === 'approved',
      ).length;

      return {
        ...state,
        changeRequests: action.payload,
        pendingReqs: pendingReqs,
        rejectedReqs: rejectedReqs,
        approvedReqs: approvedReqs,
      };

    case SET_EMP_CHANGE_REQUESTS:
      return {
        ...state,
        empChangeRequests: action.payload,
      };

    case SET_EMP_MANUAL_ATTEND_REQS:
      return {
        ...state,
        manualAttendRequests: action.payload,
      };
    case SET_MAN_WISE_MANUAL_ATTEND_REQS:
      return {
        ...state,
        mangrWiseManualAttendReqs: action.payload,
      };
    case SET_WORK_TYPES:
      return {
        ...state,
        workTypes: action.payload.workTypes,
        manualAttendCounts: action.payload?.counts,
        manualAttendCards: action.payload?.cards,

      };

    default:
      return state;
  }
};
