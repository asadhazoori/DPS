import { RESET_SERVER, SET_DATABASE, SET_IP_URL_DBS } from "./server.types";


const initialState = {
    IP: null,
    URL: null,
    DBS: [],
    selectedDB: null,
}

export const ServerReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_IP_URL_DBS:
            return {
                ...state,
                IP: action.payload.IP,
                URL: action.payload.URL,
                DBS: action.payload.DBS,
                selectedDB: null,
            }

        case SET_DATABASE:
            return {
                ...state,
                selectedDB: action.payload,
            }

        case RESET_SERVER:
            return initialState;

        default:
            return state;
    }
}