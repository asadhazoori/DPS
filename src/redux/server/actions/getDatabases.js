import { Alert } from 'react-native';
import { DatabaseApi } from '../../../utilities/api/apiController';
import Toast from 'react-native-simple-toast';
import { t } from 'i18next';
import { setIP_URL_DBs } from '../server.actions';

export const getDatabases = ({ navigation, IP }) => {
  return async dispatch => {
    try {
      const body = {
        jsonrpc: '2.0',
        method: 'call',
      };

      const { response, URL } = await DatabaseApi({ IP, body, navigation });

      if (response?.data?.result?.length > 0) {
        await dispatch(setIP_URL_DBs(IP, URL, response?.data?.result));
        return response?.data?.result;
      } else if (response == 'AxiosError: Network Error') {
        setTimeout(() => { Toast.show(t('verify-ip-internet-msg')); }, 300);
      } else if (response?.data?.error) {
        setTimeout(() => {
          Alert.alert(
            response?.data?.error?.message,
            `Method: database/list\n${response?.data?.error?.data?.message}`,
          );
        }, 300);
      }
    } catch (error) {
      console.log('GetDatabases', error);
    }
  };
};
