import { RESET_SERVER, SET_DATABASE, SET_IP_URL_DBS } from "./server.types";


export function setIP_URL_DBs(ip, url, dbs) {
    return {
        type: SET_IP_URL_DBS,
        payload: {
            IP: ip,
            URL: url,
            DBS: dbs
        }
    }
}

export function setDatabase(data) {
    return {
        type: SET_DATABASE,
        payload: data
    }
}

export function resetServer(data) {
    
    return {
        type: RESET_SERVER,
    }
}