import i18n from 'i18next';
import en from '../locales/en.json'
import ar from '../locales/ar.json'
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import { I18nManager } from 'react-native';

export const languageResources = {
    en: { translation: en },
    ar: { translation: ar },
};


i18n.use(initReactI18next)
    .init({
        compatibilityJSON: 'v3',
        lng: I18nManager.isRTL ? 'ar' : 'en',
        resources: languageResources,
        fallbackLng: 'en',
        keySeparator: false, //for allowing dots in keys, like 'nav.home'
        interpolation: {
            escapeValue: false,
        },
    });

export default i18next;
