export const COLORS = {

    white: '#FFFFFF',
    black: '#000000',
    darkBlack: "#0D1224",
    red: '#FF0000',
    red1: '#E06565',
    backgroundModal: '#F3F3F3',
    lightBlueBck: "#E1F3FF",
    SubHeadingColor: '#7D94A0',
    buttonText: '#FFEEE3',
    unSelected: '#464646',
    logout: '#CB3629',
    green: '#3BCA78',
    backgroundInput: '#e3e8e8',//'#dedede',
    orange: '#FF824C',
    yellow: '#F8B142',
    lightYellow: '#fde5c0',
    // background: '#e2e7e7',//'#F9FAFA',
    background: '#ebefef',//'#ebefef',//'#F9FAFA',
    background1: '#D4E3ED',
    primaryColor: "#8E3A64",
    // primaryColor: '#253463', //blue shade
    // icons light color B1C2CD
    // icons dark color 85BFE8
    secondaryColor: '#efd7e3',
    secondaryColor2: 'rgba(239, 215, 227, 0.6)',
    lightPrimaryColor: '#be5f8f',

    lightGrey: '#dbddda',
    grey: '#464646',
    grey1: "#8A8A8A",
    grey2: '#878686',
    grey3: "#8B8B8B",
    grey4: "#9E9EA0",
    grey5: "#5A5C63",
    buttonBack: '#587CF3',
    metallic: '#455A64',
    notify: '#F73C3C',
    orangeBack: '#F2994A',
    blue: '#587CF3',


}