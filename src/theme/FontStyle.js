import { StyleSheet, Platform } from "react-native";
import { COLORS } from "./colors";
import { fonts } from "./fonts";

const bigHeading = Platform.select({
    ios: 20,
    android: 18
});
const heading = Platform.select({
    ios: 18,
    android: 16
});

const body = Platform.select({
    ios: 16,
    android: 14
});

const caption = Platform.select({
    ios: 14,
    android: 12
});


export const FontStyle = StyleSheet.create({

    Regular10: {
        color: COLORS.darkBlack,
        fontFamily: fonts.regular,
        fontSize: 10,
        fontWeight: '700',
    },

    Regular12: {
        color: COLORS.grey,
        fontFamily: fonts.regular,
        fontSize: caption,
        fontWeight: '400',
    },

    Regular12_500: {
        color: COLORS.primaryColor,
        fontFamily: fonts.regular,
        fontSize: caption,
        fontWeight: '500',
    },

    Regular12_400: {
        color: COLORS.darkBlack,
        fontFamily: fonts.regular,
        fontSize: caption,
        fontWeight: '400',
    },

    Regular14_500: {
        color: COLORS.primaryColor,
        fontFamily: fonts.regular,
        fontSize: body,
        fontWeight: '500',
    },

    Regular14_500M: {
        color: COLORS.primaryColor,
        fontFamily: fonts.medium,
        fontSize: body,
        fontWeight: '500',
    },

    Regular14: {
        color: COLORS.buttonText,
        fontFamily: fonts.regular,
        fontSize: body,
        fontWeight: '700',
    },

    Regular16_500: {
        color: COLORS.primaryColor,
        fontFamily: fonts.regular,
        fontSize: heading,
        fontWeight: '500',
    },

    Regular16_500M: {
        color: COLORS.primaryColor,
        fontFamily: fonts.medium,
        fontSize: heading,
        fontWeight: '500',
    },

    Regular16: {
        color: COLORS.darkBlack,
        fontFamily: fonts.regular,
        fontSize: heading,
        fontWeight: '700',
    },

    Regular18: {
        color: COLORS.darkBlack,
        fontFamily: fonts.regular,
        fontSize: bigHeading,
        fontWeight: '700',
        textAlign: 'left',
    },

    Regular20: {
        color: COLORS.primaryColor,
        fontFamily: fonts.regular,
        fontSize: 20,
        fontWeight: '500',
    },

    Regular22: {
        color: "#171717",
        fontFamily: fonts.regular,
        fontSize: 22,
        fontWeight: '700',
    },

    Regular24: {
        color: COLORS.primaryColor,
        fontFamily: fonts.medium,
        fontSize: 24,
    },

})