import { COLORS } from "./colors";

const { StyleSheet, Platform } = require("react-native");

const Theme = StyleSheet.create({

    SafeArea: {
        flex: 1,
        backgroundColor: COLORS.background
    },

    Shadow: {

        backgroundColor: COLORS.white,
        borderRadius: 8,
        borderColor: '#BEBEBE',
        borderWidth: 0.6,

        ...Platform.select({
            ios: {
                shadowColor: '#D7D7D7',
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.6,
                shadowRadius: 1,
            },
            android: {
                elevation: 3,
            },
        }),
    },


    DashboardCardShadow: {

        backgroundColor: COLORS.white,
        borderRadius: 8,
        borderColor: '#BEBEBE',
        borderWidth: 0.6,

        ...Platform.select({
            ios: {
                shadowColor: '#D7D7D7',
                shadowOffset: {
                    width: 0,
                    height: 4,
                },
                shadowOpacity: 0.8,
                shadowRadius: 2,
            },
            android: {
                elevation: 4,
            },
        }),
    },

    card: {
        backgroundColor: '#fff',
        borderRadius: 8,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0, 0, 0, 0.25)',
                shadowOffset: { width: 1, height: 3 },
                shadowOpacity: 1,
                shadowRadius: 4,
            },
            android: {
                elevation: 4,
            },
        }),
    },

    ImageShadow: {
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0, 0, 0, 0.25)',
                shadowOffset: { width: 0, height: 4 },
                shadowOpacity: 1,
                shadowRadius: 4,
            },
            android: {
                elevation: 4,
            },
        }),
    },

    BottomTabButtonShadow: {
        ...Platform.select({
            ios: {
                shadowColor: COLORS.primaryColor,
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.6,
            },
            android: {
                elevation: 16,
                shadowColor: COLORS.primaryColor,
            },
        }),
    },
    flex1: {
        flex: 1
    }

})



export default Theme;