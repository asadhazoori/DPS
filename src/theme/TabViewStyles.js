import { StyleSheet } from "react-native";
import { COLORS } from "./colors";
import { FontStyle } from "./FontStyle";

export const TabViewStyles = StyleSheet.create({
    container: {
        flex: 1,
    },

    tabBar: {
        borderColor: 'red',
        flexDirection: 'row',
        marginHorizontal: 16,
    },

    iconView: {
        justifyContent: 'center'
    },

    TabViewCreateContainer: {
        backgroundColor: 'transparent',

        elevation: 0,
        flex: 1,

    },

    TabViewIndicator: {
        backgroundColor: COLORS.primaryColor,
        borderRadius: 16,

    },

    bodyView: {
        borderWidth: 1,
        flex: 1
    },

    titleText: {
        ...FontStyle.Regular16_500M,
        fontWeight: '500',
    }


});