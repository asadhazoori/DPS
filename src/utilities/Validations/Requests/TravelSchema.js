import * as yup from 'yup';

export const TravelSchema = (t) => {
    return yup.object().shape({

        travelPurpose: yup.string().trim().required(t('purpose-required')),
        fromCountry: yup.number().required(t('country-required')),
        fromCity: yup.number().required(t('city-required')),
        toCountry: yup.number().required(t('country-required')),
        toCity: yup.number().required(t('city-required')),
        departureDate: yup.string().required(t('date-required')),
        returnDate: yup.string().required(t('date-required')),
        modeOfTravel: yup.number().required(t('mode-required')),
        description: yup.string().trim().required(t('description-required')),
    })
}