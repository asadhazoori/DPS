import * as yup from 'yup';

export const IqamaSchema = t => {
  return yup.object().shape({
    reason: yup.string().required(t('reason-required')),
  });
};
