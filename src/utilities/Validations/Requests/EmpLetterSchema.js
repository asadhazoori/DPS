import * as yup from 'yup';

export const EmpLetterSchema = (t) => {
    return yup.object().shape({

        letterType: yup.object().required(t('required')),
        eStamp: yup.object().required(t('required')),
        reason: yup.string().trim().required(t('reason-required')),

    })
}