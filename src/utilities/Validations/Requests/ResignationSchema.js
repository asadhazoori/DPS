import * as yup from 'yup';

export const ResignationSchema = (t) => {
    return yup.object().shape({

        lastWorkDate: yup.string().required(t('date-required')),
        resignDate: yup.string().required(t('date-required')),
        reason: yup.string().trim().required(t('reason-required')),


    })
}