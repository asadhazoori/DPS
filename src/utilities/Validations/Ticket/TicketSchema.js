import * as yup from 'yup';

export const TicketSchema = (t) => {
    return yup.object().shape({

        subject: yup.string().required(t("subject-required")),
        ticketType: yup.number().required(t("ticket-type-required")),
        description: yup
            .string()
            .transform((value) => (typeof value === 'string' ? value.trim() : value))
            .required(t('description-required')),

    })
}