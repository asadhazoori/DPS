import * as yup from 'yup';

export const OvertimeRequestSchema = (t) => {
    return yup.object().shape({

        date: yup.string().required(t('date-required')),
        noOfDays: yup.number()
            .typeError(t('invalid-number'))
            .required(t('number-of-days-required')),
        duration: yup.number()
            .typeError(t('invalid-duration'))
            .required(t('no-of-hours-required')),
        reason: yup.string().trim().required(t('purpose-required')),

    })
}
