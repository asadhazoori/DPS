import * as yup from 'yup';


export const ChangePasswordSchema = (t) => {
    return yup.object().shape({

        currentPassword: yup.string().trim().required(t("password-required")),
        newPassword: yup.string().trim().required(t("password-required")),
        confirmPassword: yup.string().trim()
            .oneOf([yup.ref('newPassword'), null], t('password-not-matched'))
            .required(t("password-required")),

    })
}