import * as yup from 'yup';

export const LoginSchema = (t) => {
    return yup.object().shape({

        username: yup.string().trim().required(t("username-required")),
        password: yup.string().trim()
            .required(t("password-required")),


    })
}