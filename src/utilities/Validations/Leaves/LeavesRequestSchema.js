import * as yup from 'yup';

export const LeavesRequestSchema = t => {
  return yup.object().shape({
    // holidayType: yup.object().required(t('leave-type-required')),
    holidayType: yup
      .object()
      .shape({
        is_short_leave: yup.boolean(),
      })
      .required(t('leave-type-required')),

    startDate: yup.string().required(t('date-required')),
    // endDate: yup.string().required(t('date-required')),
    reason: yup.string().trim().required(t('purpose-required')),

    endDate: yup
      .string()
      .nullable()
      .when('holidayType.is_short_leave', {
        is: false,
        then: schema => schema.required(t('date-required')),
        otherwise: schema => schema.optional(),
      }),

    leaveTime: yup
      .string()
      .nullable()
      .when('holidayType.is_short_leave', {
        is: true,
        then: schema => schema.required(t('leave-time-required')),
        otherwise: schema => schema.optional(),
      }),

    returnTime: yup
      .string()
      .trim()
      .nullable()
      .test('is-after-punchIn', t('returnTime-after-leaveTime'), function (value) {
        const {leaveTime} = this.parent;
        return !leaveTime || !value || leaveTime < value;
      }),
  });
};
