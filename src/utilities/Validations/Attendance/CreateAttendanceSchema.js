import * as yup from 'yup';

export const CreateAttendanceSchema = yup.object().shape({

    startDate: yup.string().nullable(),
    endDate: yup.string().nullable(),
})
    .test('at-least-one-required', 'Either Start Time or End Time is required', function (values) {
        const { startDate, endDate } = values;
        if (!startDate && !endDate) {
            return this.createError({
                path: 'startDate',
                message: 'Either Start Date or End Date is required',
            });
        }
        return true;
    })




export const ChangeAttendanceSchema = (t) => {
    return yup.object().shape({
        reason: yup.string().trim().required(t("reason-required")),
        punchInUpd: yup.string().nullable(),
        punchOutUpd: yup.string()
            .nullable()
            .when('punchInUpd', (punchInUpd, schema) => {
                return schema.test({
                    test: (punchOutUpd) => {
                        if (punchOutUpd == "false") {
                            return true;
                        }
                        else {
                            return new Date(punchOutUpd) > new Date(punchInUpd);

                        }
                    },
                    message: t('punchout-less-punchin'),
                })
                    .test({
                        test: function (punchOutUpd, { parent }) {

                            const punchOutUpd1 = new Date(punchOutUpd);
                            const punchOutPre1 = new Date(parent.punchOutPre);

                            const punchOutUpdWithoutSeconds = new Date(
                                punchOutUpd1.getFullYear(),
                                punchOutUpd1.getMonth(),
                                punchOutUpd1.getDate(),
                                punchOutUpd1.getHours(),
                                punchOutUpd1.getMinutes()
                            );

                            const punchOutPreWithoutSeconds = new Date(
                                punchOutPre1.getFullYear(),
                                punchOutPre1.getMonth(),
                                punchOutPre1.getDate(),
                                punchOutPre1.getHours(),
                                punchOutPre1.getMinutes()
                            );


                            const punchInUpd1 = new Date(parent.punchInUpd);
                            const punchInPre1 = new Date(parent.punchInPre);

                            const punchInUpdWithoutSeconds = new Date(
                                punchInUpd1.getFullYear(),
                                punchInUpd1.getMonth(),
                                punchInUpd1.getDate(),
                                punchInUpd1.getHours(),
                                punchInUpd1.getMinutes()
                            );

                            const punchInPreWithoutSeconds = new Date(
                                punchInPre1.getFullYear(),
                                punchInPre1.getMonth(),
                                punchInPre1.getDate(),
                                punchInPre1.getHours(),
                                punchInPre1.getMinutes()
                            );


                            if (punchOutUpdWithoutSeconds.getTime() === punchOutPreWithoutSeconds.getTime()  && 
                            punchInUpdWithoutSeconds.getTime() === punchInPreWithoutSeconds.getTime()) {
                                return false;
                            } else {
                                return true;
                            }
                        },
                        message: t('request-time-same'),
                    })
                    .test({
                        test: (punchOutUpd) => {
                            if (punchOutUpd == 'false') {
                                return false;
                            }
                            return true;
                        },
                        message: t('punchout-missing'),
                    });
            }),
    })

}