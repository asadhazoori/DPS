import * as yup from 'yup';

export const ManualAttendRequestSchema = (t, editable, type) => {
  return yup.object().shape({
    workType: yup.object().required(t('type-required')),
    ...(editable && { date: yup.string().required(t('date-required')) }),
    ...(editable && { reason: yup.string().trim().required(t('reason-required')) }),
    ...(editable && type !== 'missed_checkout' && {
      punchInTime: yup.string().trim().required(t('punchIn-required')),
    }),
    ...(editable &&
      type != 'missed_checkin' && {
      punchOutTime: yup
        .string()
        .trim()
        .required(t('punchOut-required'))
        .test(
          'is-after-punchIn',
          t('punchOut-after-punchIn'),
          function (value) {
            if (type !== 'missed_checkout' || type != 'missed_checkin') {
              const { punchInTime } = this.parent;
              return !punchInTime || !value || punchInTime < value;
            }
          },
        ),
    }),
  });
};
