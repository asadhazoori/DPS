import * as yup from 'yup';

export const LoanRequestSchema = (t) => {
    return yup.object().shape({

        amount: yup.number()
            .typeError(t('invalid-amount'))
            .required(t('amount-required')),
        type: yup.number().required(t('loantype-required')),
        startDate: yup.string().required(t('date-required')),
        reason: yup.string().trim().required(t('reason-required')),

    })
}