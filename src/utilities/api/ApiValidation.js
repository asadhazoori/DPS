import Toast from 'react-native-simple-toast'
import { t } from 'i18next';

const ApiStatus = async (error, navigation) => {

    console.log("APIValidation: ", error);

    if (error == "AxiosError: Network Error") {

        setTimeout(() => { Toast.show(t('internet-connection-failed')); }, 500)
    } else {
        setTimeout(() => { Toast.show(`${error}`) }, 500)
    }
}

export { ApiStatus }