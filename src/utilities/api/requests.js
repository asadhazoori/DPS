import axios from "axios";
import { ApiStatus } from "./ApiValidation";
import { RequestHeaders } from "./RequestHeader";
import store from "../../redux/store";

const post_request = async ({ target, body, params, navigation, formData = false, checkUrl = null, timeout = null }) => {

    try {

        const headers = RequestHeaders(formData)

        const baseURL = store.getState().server.URL

        const instance = axios.create({
            baseURL: baseURL,
            params: params,
            headers: headers,
            ...(timeout && {
                timeout: timeout,
                timeoutErrorMessage: "Slow Network Detected or Server Problem, Request Timeout!"
            })
        });

        const response = await instance.post(target, body)
            .catch((error) => {
                ApiStatus(error, navigation)
                return error
            }
            )
        return response

    } catch (e) {
        return e
    }
}


const post_request1 = async ({ IP, target, body, params, navigation, formData = false, checkUrl = null, timeout = null }) => {
    try {
        const headers = RequestHeaders(formData);
        const URL = `http://${IP}`;

        const httpInstance = axios.create({
            baseURL: URL,
            params: params,
            headers: headers,
            ...(timeout && {
                timeout: timeout,
                timeoutErrorMessage: "Slow Network Detected, Request Timeout!"
            })
        });

        try {
            const response = await httpInstance.post(target, body);
            return { response, URL };
        } catch (httpError) {
            console.log('HTTP Request failed, calling HTTPS', httpError);
            const URL = `https://${IP}`;
            const httpsInstance = axios.create({
                baseURL: URL,
                params: params,
                headers: headers,
            });

            try {
                const response = await httpsInstance.post(target, body);
                return { response, URL };
            } catch (error) {
                console.log('HTTPS Request failed', error);
                return { response: error };
            }
        }
    } catch (e) {
        console.log('Unexpected error', e);
        return e;
    }
};



export { post_request, post_request1 }
