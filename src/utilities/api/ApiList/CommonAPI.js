import { post_request } from "../requests";

export const commonApi = async ({ body, navigation, params, timeout }) => {
    const data = await post_request({
        target: "/web/dataset/call_kw", body: body, navigation: navigation, params: params,
        ...(timeout && { timeout: timeout, })
    });
    return data;
}

export const sessionExpire = async ({ body, navigation, params }) => {
    const data = await post_request({ target: "/session/logout", body: body, navigation: navigation, params: params });
    return data;
}