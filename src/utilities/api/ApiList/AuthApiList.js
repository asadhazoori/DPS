import { post_request, post_request1 } from "../requests";

export const DatabaseApi = async ({ IP, body, navigation, params }) => {
    const data = await post_request1({ IP, target: "/web/database/list", body: body, navigation: navigation, params: params, timeout: 9000 });
    return data;
}

export const LoginApi = async ({ body, navigation, params }) => {
    const data = await post_request({ target: "/web/session/authenticate", body: body, navigation: navigation, params: params, timeout: 9000 });
    return data;
}

export const LogoutApi = async ({ body, navigation, params }) => {
    const data = await post_request({ target: "/web/session/logout/api", body: body, navigation: navigation, params: params });
    return data;
}

export const getCompanyDetailsApi = async ({ body, navigation, params }) => {
    const data = await post_request({ target: "/web/get_company_logo", body: body, navigation: navigation, params: params, timeout: 9000 });
    return data;
}
