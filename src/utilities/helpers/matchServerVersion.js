import { getAppVersion } from "../../redux/settings/actions/get_app_version";
import { AppVersion } from "../constant/version";

export const matchServerVerions = async ({ navigation, dispatch }) => {
    try {
        const ServerVersion = await getAppVersion({ navigation, dispatch });
        console.log("AppVersion:", AppVersion, 'Vs', "ServerVersion:", ServerVersion)
        if (ServerVersion) {
            if (AppVersion === ServerVersion) {
                return "matched"
            }
            else {
                return ServerVersion
            }
        } else if (ServerVersion == false) {
            return "Null"
        } else {
            return "session_expired";
        }
    } catch (error) {
        console.log("Error in matching server versions: " + error);
    }
}
