
import { Alert, PermissionsAndroid, Platform } from 'react-native';
import { isLocationEnabled, promptForEnableLocationIfNeeded } from 'react-native-android-location-enabler';

import Geolocation from 'react-native-geolocation-service';

export function getPermissionJust() {
  return new Promise(async (resolve, reject) => {
    if (Platform.OS === 'android') {
      try {

        const result = await checkAndRequestLocationPermission();
        resolve(result);

      } catch (error) {
        console.log("getPermissionJust", error.message);
        resolve('denied');
      }
    }
    else if (Platform.OS === "ios") {
      try {
        const iosPermissionResp = await Geolocation.requestAuthorization("whenInUse");
        // console.log(iosPermissionResp, "iosPermissionResp");
        if (iosPermissionResp == 'granted') {
          resolve("granted")
        } else {
          resolve('denied');
        }
      } catch (error) {
        console.log("getPermissionJust", error.message);
        resolve('denied');
      }
    }

  });
}




const checkLocationPermission = async () => {
  const result = await PermissionsAndroid.check(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
  // console.log("check Location permission", result);
  return result;
};

const checkGPSStatus = async () => {
  try {
    const checkEnabled = await isLocationEnabled();
    if (checkEnabled) {
      // console.log("Everything is fine");
      return 'fine';
    }
    else {
      const result = await promptForEnableLocationIfNeeded();
      if (result) {
        console.log("Everything is fine", result);
        return 'fine';
      }
    }
  }
  catch (error) {
    console.log("checkGPSStatus", error)
    return 'GPS_disabled';
  }

}


export const checkAndRequestLocationPermission = async () => {
  // console.log("PermissionsAndroid.RESULTS.GRANTED", PermissionsAndroid.RESULTS.GRANTED)
  const checkPermission = await checkLocationPermission();
  if (checkPermission) {
    const result = await checkGPSStatus();
    return result;
  }
  else {
    // else if (!checkPermission) {
    const request = await requestLocationPermission();
    if (request == 'granted') {
      const result = await checkGPSStatus();
      return result;
    }
    else {
      return 'denied';
    }

  }

};


const requestLocationPermission = async () => {

  const result = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    {
      title: 'Location Permission',
      message: 'This app needs to access your location',
      buttonNeutral: 'Ask Me Later',
      buttonNegative: 'Cancel',
      buttonPositive: 'OK',
    }
  );
  console.log("After request Location permission", result);
  return result;
}

export function getCoordinatesServices() {
  return new Promise((resolve, reject) => {
    Geolocation.getCurrentPosition(
      position => {
        resolve(position);

      },
      error => {
        console.log(error.code, error.message);
        reject(error)
      },
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0
      },
    );

  });
}
