import NetInfo from "@react-native-community/netinfo";

const fetchWithTimeout = (url, options, timeout = 5000) => {
    return Promise.race([
        fetch(url, options),
        new Promise((_, reject) =>
            setTimeout(() => reject(new Error('timeout')), timeout)
        )
    ]);
};

export const checkInternetAccess = async () => {
    const state = await NetInfo.fetch();

    if (state.isConnected) {
        try {
            const response = await fetchWithTimeout("https://www.google.com", { method: "HEAD" }, 5000);

            if (response.ok) {
                return true;
            } else {
                return false;
            }
        } catch (error) {
            return false;
        }
    } else {
        return false;
    }
};