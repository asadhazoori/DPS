import { I18nManager } from 'react-native';
import RNRestart from 'react-native-restart';
import i18next from '../../i18n/services/i18next'

const changeLng = async (rtl) => {

    const lng = rtl ? 'ar' : 'en';
    I18nManager.forceRTL(rtl);
    i18next.changeLanguage(lng);
    RNRestart.Restart(); 
    return;

}

export default changeLng;
