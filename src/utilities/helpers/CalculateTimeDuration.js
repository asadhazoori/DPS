import moment from 'moment';

export const CalculateTimeDuration = (punchInTime, punchOutTime) => {
  const punchInMoment = moment(punchInTime);
  const punchOutMoment = moment(punchOutTime);
  const duration = moment.duration(punchOutMoment.diff(punchInMoment));
  const hours = Math.floor(duration.asHours());
  const minutes = Math.floor(duration.asMinutes()) % 60;
  return {hours, minutes};
};

export const formattedDuration = decimalHours => {
  const duration = moment.duration(decimalHours, 'hours');
  const hours = Math.floor(duration.asHours());
  const minutes = Math.round(duration.minutes());

  return {hours, minutes};
};
