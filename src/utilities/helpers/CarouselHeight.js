import { Dimensions } from "react-native";

const { width, height } = Dimensions.get('window');

export function calculateParallaxHeight() {
    const a = 1 / 190;
    const b = 115.2632;
    return a * height + b;
}

export function calculateParallaxScrollingScale() {
    const width1 = 384;
    const width2 = 1280;
    const scale1 = 0.9;
    const scale2 = 0.964;

    return scale1 + ((width - width1) / (width2 - width1)) * (scale2 - scale1);
}

export function calculateParallaxScrollingOffset() {
    const width1 = 384;
    const width2 = 1280;
    const offset1 = 150;
    const offset2 = 420;

    return offset1 + ((width - width1) / (width2 - width1)) * (offset2 - offset1);
}