import { Alert } from 'react-native';
import { commonApi } from '../api/apiController';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../redux/users/user.actions';
import { t } from 'i18next';

export const getTravelAttachments = async (Id, setLoading, navigation, dispatch) => {
  try {
    const body = {
      params: {
        model: 'travel.request.wags',
        method: 'get_travel_ticket_attachments',
        args: [
          {
            record_id: Id,
          },
        ],
        kwargs: {},
      },
    };
    const response = await commonApi({ body, navigation });

    if (response?.data?.result?.attachments) {
      return response?.data?.result?.attachments;
    }
    //  else if (response?.data?.result?.status == 400) {
    //   setLoading(false);
    //   return response?.data?.result;
    // }
    else if (response?.data?.error) {
      setLoading(false);
      if (response?.data?.error?.message == 'Odoo Session Expired') {
        setTimeout(() => { Toast.show(t('session-expired')); }, 500);
        navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        });
        dispatch(logout_user(false));
      } else {
        setLoading(false);
        setTimeout(() => {
          Alert.alert(
            response?.data?.error?.message,
            `Method: get_travel_ticket_attachments\n${response?.data?.error?.data?.message}`,
          );
        }, 300);
      }
    } else if (response == 'AxiosError: Request failed with status code 404') {
      setLoading(false);
      setTimeout(() => { Toast.show(t('session-expired')); }, 500);
      navigation.reset({
        index: 0,
        routes: [{ name: 'Login' }],
      });
      dispatch(logout_user(false));
    } else {
      setLoading(false);
    }
  } catch (error) {
    setLoading(false);
    console.log('Download PDf', error);
  }
};
