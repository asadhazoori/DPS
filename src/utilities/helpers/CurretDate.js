import moment from 'moment';
import { I18nManager } from 'react-native';

export const getCurrentDate = () => {
  const now = new Date();
  const DateEng = now.toDateString();
  const options = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    timeZone: 'UTC',
  };
  const DateArb = now.toLocaleDateString('ar-EG', options);
  const formattedDate = I18nManager.isRTL ? DateArb : DateEng;

  return formattedDate;
};

export const getFormattedTime = time => {
  const obj1 = moment.utc(time, 'YYYY-MM-DD HH:mm:ss').local();
  const obj = new Date(obj1);
  const TimeArb = obj.toLocaleTimeString('ar-EG');
  const TimeEng = moment
    .utc(time, 'YYYY-MM-DD HH:mm:ss')
    .local()
    .format('h:mm A');
  const formattedDate = I18nManager.isRTL ? TimeArb : TimeEng;

  return formattedDate;
};


export const getFormattedTimeWithoutUTC = time => {
  const obj1 = moment.utc(time, 'YYYY-MM-DD HH:mm:ss').local();
  const obj = new Date(obj1);
  const TimeArb = obj.toLocaleTimeString('ar-EG');
  const TimeEng = moment
    .utc(time, 'YYYY-MM-DD HH:mm:ss')
    // .local()
    .format('h:mm A');
  const formattedDate = I18nManager.isRTL ? TimeArb : TimeEng;

  return formattedDate;
};

export const getFormattedMonth = date => {
  const obj = new Date(date);
  const options = { month: 'long', timeZone: 'UTC' };
  const MonthArb = obj.toLocaleDateString('ar-EG', options);
  const MonthEng = moment(date).format('MMMM');
  const formattedMonth = I18nManager.isRTL ? MonthArb : MonthEng;

  return formattedMonth;
};

export const getFormattedDate = (date, includeWeekday = true) => {
  var DateEng = '';
  if (includeWeekday) {
    DateEng = moment(date).format('dddd, DD MMM YYYY');
  } else {
    DateEng = moment(date).format('DD MMM YYYY');
  }

  const obj = new Date(date);
  const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    timeZone: 'UTC',
  };

  if (includeWeekday) {
    options.weekday = 'long';
  }
  const DateArb = obj.toLocaleDateString('ar-EG', options);
  const formattedDate = I18nManager.isRTL ? DateArb : DateEng;

  return formattedDate;
};

export function floatToTimeString(floatTime) {
  const hours = Math.floor(floatTime);
  const minutes = Math.round((floatTime - hours) * 60);

  const period = hours >= 12 ? 'PM' : 'AM';
  const adjustedHours = hours % 12 === 0 ? 12 : hours % 12;

  const timeString = `${adjustedHours}:${minutes
    .toString()
    .padStart(2, '0')} ${period}`;
  return timeString;
}
