const { PermissionsAndroid } = require("react-native");

const requestNotificationPermission = async () => {

    // console.log("request permision")
    const result = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.POST_NOTIFICATIONS,
        {
            title: 'Notifications Permission',
            message: 'This app needs to send you Notifications',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
        }
    );
    // console.log("request permission",result);
    return result;
}

const checkNotificationPermission = async () => {
    const result = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.POST_NOTIFICATIONS);
    // console.log("check permission", result);
    return result;
};


export const checkNotificationsPermission = async () => {
    const checkPermission = await checkNotificationPermission();
    if (checkPermission !== PermissionsAndroid.RESULTS.GRANTED) {
        const request = await requestNotificationPermission();
        if (request !== PermissionsAndroid.RESULTS.GRANTED) {
            // permission not granted
        }
    }
};
























const requestStoragePermission = async () => {

    // console.log("request permision")
    const result = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
            title: 'Storage Permission',
            message: 'This app needs to send you writeon ypur Storage',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
        }
    );
    // console.log("request permission",result);
    return result;
}

const checkStoragePermission = async () => {
    const result = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
    // console.log("check Write Storage Permission", result);
    return result;
};


export const checkWriteStoragePermission = async () => {
    const checkPermission = await checkStoragePermission();
    if (checkPermission !== PermissionsAndroid.RESULTS.GRANTED) {
        const request = await requestStoragePermission();
        if (request !== PermissionsAndroid.RESULTS.GRANTED) {
            // permission not granted
        }
    }
};