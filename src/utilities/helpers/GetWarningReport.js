import { Alert } from "react-native";
import { commonApi } from "../api/apiController";
import Toast from 'react-native-simple-toast'
import { logout_user } from "../../redux/users/user.actions";
import { t } from "i18next";

export const GetWarningReport = async (Id, setLoading, navigation, dispatch) => {

    try {
        const body = {
            "params": {
                "model": "hr.warning",
                "method": "generate_employee_warning_report",
                "args": [Id],
                "kwargs": {}
            }
        }
        const response = await commonApi({ body, navigation });

        if (response?.data?.result?.b64_pdf) {
            return response?.data?.result;
        }

        else if (response?.data?.result?.status == 400) {
            setLoading(false)
            return response?.data?.result
        }

        else if (response?.data?.error) {
            setLoading(false)
            if (response?.data?.error?.message == "Odoo Session Expired") {
                dispatch(logout_user(false))
                setTimeout(() => { Toast.show(t('session-expired')); }, 500)
                navigation.reset({
                    index: 0,
                    routes: [
                        { name: 'Login' }
                    ],
                })
            }
            else {
                setLoading(false);
                setTimeout(() => {
                    Alert.alert(response?.data?.error?.message, `Method: generate_employee_warning_report\n${response?.data?.error?.data?.message}`)
                }, 300);
            }
        }

        else if (response == 'AxiosError: Request failed with status code 404') {
            setLoading(false)
            dispatch(logout_user(false))
            setTimeout(() => { Toast.show(t('session-expired')); }, 500)
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' }
                ],
            })
        }


        else {
            setLoading(false)

        }

    }

    catch (error) {
        setLoading(false);
        console.log("Download PDf", error);

    }
};

