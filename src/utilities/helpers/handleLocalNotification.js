import PushNotification from "react-native-push-notification";

const handleLocalNotification = ({ remoteMessage }) => {
  if (!remoteMessage?.notification) return;

  PushNotification.localNotification({
    channelId: 'fcm_fallback_notification_channel',
    channelName: 'My channel',

    message: remoteMessage.notification?.body || 'Notification Body',
    title: remoteMessage.notification?.title || 'Notification Title',
    vibrate: true, 
    onlyAlertOnce: true,
    // bigPictureUrl: remoteMessage.notification.android.imageUrl,
    // smallIcon: remoteMessage.notification.android.imageUrl,
  });
}

export default handleLocalNotification

