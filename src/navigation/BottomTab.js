import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React, { useEffect, useRef, useState } from 'react';
import {
  Attendance,
  AttendanceDetails,
  AttendanceRequests,
  Dashboard,
  Leaves,
  ManagerAttendance,
  Loans,
  MedicalClaim,
  Notifications,
  OvertimeTracking,
  Reports,
  Requests,
  Shifts,
  Timesheet,
  ManualAttendance,
  ManualAttendanceReqs,
} from '../screens';
import { COLORS } from '../theme/colors';
import { SvgXml } from 'react-native-svg';
import { DrawerIcons } from '../assets/SvgIcons/DrawerIcons';
import { BottomTabIcons } from '../assets/SvgIcons/BottomTabIcons';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { FontStyle } from '../theme/FontStyle';
import { useSelector } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import PunchLocation from '../screens/Attendance/PunchLocation/PunchLocation';
import { Payslips } from '../screens';
import PunchFloatingButton from '../components/Buttons/PunchFloatingButton';

const Tab = createBottomTabNavigator();
const InnerStack = createNativeStackNavigator();

function AttendanceStack({ route }) {
  return (
    <InnerStack.Navigator
      initialRouteName={
        route?.params?.isManager ? 'ManagerAttendance' : 'Attendance'
      }
      screenOptions={{ headerShown: false }}>
      <InnerStack.Screen name="Attendance" component={Attendance} />
      <InnerStack.Screen
        name="AttendanceDetails"
        component={AttendanceDetails}
      />
      <InnerStack.Screen
        name="AttendanceRequests"
        component={AttendanceRequests}
      />
      <InnerStack.Screen
        name="ManagerAttendance"
        component={ManagerAttendance}
      />
      <InnerStack.Screen
        name="ManualAttendance"
        component={ManualAttendance}
      />
      <InnerStack.Screen
        name="ManualAttendanceReqs"
        component={ManualAttendanceReqs}
      />
    </InnerStack.Navigator>
  );
}

const BottomTab = ({ navigation, route }) => {
  const { activeAttendance } = useSelector(state => state?.attendance);
  const { activeScreen } = useSelector(state => state?.settings);
  const { isManager, data } = useSelector(state => state?.employeeProfile);
  const punchStatus = activeAttendance?.last_checkout
    ? 'Punch-In'
    : 'Punch-Out';

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  const Modalbackgroundscreen = () => {
    return null;
  };

  const DashboardBottomTabIcon = ({ focused, icon }) =>
    focused ? (
      <View style={styles.activeIcon}>
        <SvgXml xml={icon} />
      </View>
    ) : (
      <SvgXml xml={icon} />
    );

  return (
    <>
      <Tab.Navigator
        initialRouteName={"Dashboard"}
        screenOptions={{
          tabBarShowLabel: false,
          headerShown: false,
        }}
        sceneContainerStyle={{ borderBottomWidth: 1, borderColor: '#E2E2E2' }}>
        <Tab.Screen
          options={{
            tabBarIcon: ({ focused }) => (
              <DashboardBottomTabIcon
                focused={focused}
                icon={BottomTabIcons.dashboard}
              />
            ),
          }}
          name="Dashboard"
          component={Dashboard}
          initialParams={route?.params}
        />

        <Tab.Screen
          options={{
            tabBarIcon: ({ focused }) => (
              <DashboardBottomTabIcon
                focused={focused}
                icon={BottomTabIcons.attendance}
              />
            ),
          }}
          name="Attendance1"
          component={AttendanceStack}
          initialParams={{ isManager: isManager }}
        />

        <Tab.Screen
          options={{
            tabBarButton: () => (
              <PunchFloatingButton
                onPress={() => {
                  // if (activeAttendance?.overlapping) {
                  if (activeScreen == 'Attendance') {
                    navigation.navigate('LocationOld', {
                      activeAttendance: activeAttendance,
                      punchStatus: punchStatus,
                      log_attendance_id: activeAttendance?.log_attendance_id,
                      nav: true,
                    });
                  } else {
                    // navigation.navigate('Attendance')
                    // navigation.navigate('Attendance1', {
                    //   screen: 'Attendance',
                    // });
                    navigation.reset({
                      index: 0,
                      routes: [
                        {
                          name: 'Attendance1',
                          params: {
                            screen: 'Attendance',
                          },
                        },
                      ],
                    })
                  }
                  // } else {
                  //     navigation.navigate('LocationOld', { activeAttendance: activeAttendance, punchStatus: punchStatus, log_attendance_id: activeAttendance?.log_attendance_id, nav: true })
                  // }
                }}
              />
            ),
          }}
          name="Null"
          component={Modalbackgroundscreen}
        />

        <Tab.Screen
          options={{
            tabBarIcon: ({ focused }) => (
              <DashboardBottomTabIcon
                focused={focused}
                icon={DrawerIcons.leaves}
              />
            ),
          }}
          name="Leaves"
          component={Leaves}
        />

        <Tab.Screen
          options={{
            tabBarIcon: ({ focused }) => (
              <DashboardBottomTabIcon
                focused={focused}
                icon={DrawerIcons.payslip}
              />
            ),
          }}
          name="EmployeePayslip"
          component={Payslips}
        />
      </Tab.Navigator>
    </>
  );
};

export default BottomTab;

const styles = StyleSheet.create({
  label: {
    ...FontStyle.Regular10,
    fontWeight: '400',
    color: '#AFA9A9',
  },

  activeIcon: {
    backgroundColor: COLORS.secondaryColor,
    height: 34,
    width: 34,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
