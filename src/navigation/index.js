import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import BottomTab from './BottomTab';
import {
  Location,
  Login,
  Onbording1,
  Onbording2,
  ProfileDetails,
  AttendaceChangeRequest,
  Profile,
  PayslipDetails,
  Loans,
  MedicalClaim,
  Requests,
  Notifications,
  OvertimeTracking,
  Shifts,
  Timesheet,
  ApplyMedicalClaims,
  ApplyTimeSheet,
  Leaves,
  Splash,
  PayslipReport,
  ServerScreen,
  HomeChats,
  ConversationChat,
  CreateRequests,
  ChangePassword,
} from '../screens';
import TestScreen from '../screens/TestScreen/TestScreen';
import RequestDetails from '../screens/Requests/RequestDetails/RequestDetails';
import {AttendanceProvider} from '../context/AttendanceContext';
import InternalTicket from '../screens/InternalTicket/InternalTicket';
import InternalTicketDetail from '../screens/InternalTicket/InternalTicketDetail/InternalTicketDetail';
import ApplyInternalTicket from '../screens/InternalTicket/ApplyInternalTicket/ApplyInternalTicket';

const Stack = createNativeStackNavigator();

const Navigation = () => {
  const user = useSelector(state => state.signin);
  return (
    // <NavigationContainer>
      <AttendanceProvider>
        <Stack.Navigator
          initialRouteName={'Splash'}
          screenOptions={{headerShown: false}}>
          <Stack.Screen name="Splash" component={Splash} />
          <Stack.Screen name="Onbording1" component={Onbording1} />
          <Stack.Screen name="Onbording2" component={Onbording2} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="ChangePassword" component={ChangePassword} />
          <Stack.Screen name="ServerScreen" component={ServerScreen} />
          {/* <Stack.Screen name='DrawerNavigation' component={DrawerNavigation} /> */}
          <Stack.Screen name="BottomTab" component={BottomTab} />
          <Stack.Screen name="LocationOld" component={Location} />
          <Stack.Screen name="EmployeeProfile" component={ProfileDetails} />
          <Stack.Screen name="Profile" component={Profile} />
          <Stack.Screen name="Requests" component={Requests} />
          <Stack.Screen name="CreateRequests" component={CreateRequests} />
          <Stack.Screen
            name="AttendaceChangeRequest"
            component={AttendaceChangeRequest}
          />
          <Stack.Screen name="PayslipDetails" component={PayslipDetails} />
          <Stack.Screen name="Loans" component={Loans} />
          <Stack.Screen name="MedicalClaim" component={MedicalClaim} />
          <Stack.Screen
            name="ApplyMedicalClaims"
            component={ApplyMedicalClaims}
          />
          <Stack.Screen name="Notifications" component={Notifications} />
          <Stack.Screen name="OvertimeTracking" component={OvertimeTracking} />
          <Stack.Screen name="Shifts" component={Shifts} />
          <Stack.Screen name="Timesheet" component={Timesheet} />
          <Stack.Screen name="ApplyTimeSheet" component={ApplyTimeSheet} />
          <Stack.Screen name="PayslipReport" component={PayslipReport} />
          <Stack.Screen name="RequestDetails" component={RequestDetails} />

          <Stack.Screen name="HomeChats" component={HomeChats} />
          <Stack.Screen name="ConversationChat" component={ConversationChat} />

          <Stack.Screen name="TestScreen1" component={TestScreen} />
          <Stack.Screen name="InternalTicket" component={InternalTicket} />
          <Stack.Screen name="ApplyInternalTicket" component={ApplyInternalTicket} />
          <Stack.Screen name="InternalTicketDetail" component={InternalTicketDetail} />
        </Stack.Navigator>
      </AttendanceProvider>
    // </NavigationContainer>
  );
};

export default Navigation;
