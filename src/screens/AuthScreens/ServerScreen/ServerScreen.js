import { Dimensions, Image, Keyboard, KeyboardAvoidingView, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native'
import Theme from '../../../theme/theme'
import React, { useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { FontStyle } from '../../../theme/FontStyle';
import { getDatabases } from '../../../redux/server/actions/getDatabases';
import { setDatabase } from '../../../redux/server/server.actions';
import { ServerSchema, ServerSchema1 } from '../../../utilities/Validations/AuthValidations/ServerSchema';
import { DBPicker, NextButton, TextInputAuth } from '../../../components/Inputs';
import inputValidation from '../../../utilities/Validations/YupValidate';
import { getCompanyDetails } from '../../../redux/settings/actions/get_company_details';

const ServerScreen = ({ navigation }) => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { IP, selectedDB, DBS } = useSelector((state) => state.server);
    const data = useSelector((state) => state.server);
    const [isIPEditable, setIPEditable] = useState(IP ? false : true);
    const [loading, setLoading] = useState(false);
    const [databases, setDBS] = useState(DBS);
    const [openDropdown, setOpenDropdown] = useState(false);
    const [showDBPicker, setshowDBPicker] = useState(false);

    const [inputs, setInputs] = useState({
        ip: IP,
        database: selectedDB,
        errors: null,
    });


    const validate = async () => {

        const schema = ServerSchema1(t);
        const result = await inputValidation(schema, inputs)

        if (result.isValidate) {
            setLoading(true)
            await dispatch(setDatabase(inputs.database));
            await dispatch(getCompanyDetails({ navigation }));

            setLoading(false);
            navigation.navigate('Login')

        } else {
            setInputs(prev => ({
                ...prev,
                errors: result?.err
            }))
        }

    }
    const handleOnChange = (field, value) => {
        if (field == "ip") {
            setDBS([])
            setInputs({
                ...inputs,
                [field]: value,
                ['database']: null,
                errors: {
                    ...inputs.errors,
                    [field]: false
                }
            })
        }
        if (field == "database") {

            setInputs({
                ...inputs,
                [field]: value,
                errors: {
                    ...inputs.errors,
                    [field]: false
                }
            })
        }
    }


    const getDatabasesList = async () => {
        const schema = ServerSchema(t);
        const result = await inputValidation(schema, inputs)
        if (result.isValidate) {
            setLoading(true);
            Keyboard.dismiss();
            const databases = await dispatch(getDatabases({ IP: inputs.ip }))
            setLoading(false);
            if (databases) {
                setDBS(databases);
                setOpenDropdown(true);
                setshowDBPicker(true);
            }
            else {
                console.log("else", databases)
                setDBS([]);
                setshowDBPicker(true);

            }

        } else {
            setInputs(prev => ({
                ...prev,
                errors: result?.err
            }))
        }


    }

    useEffect(() => {
        setInputs({
            ip: IP,
            database: selectedDB
        })
        if (selectedDB) {
            setshowDBPicker(true)
        }
        setIPEditable(IP ? false : true);
        setDBS(DBS)


    }, [])


    return (
        <SafeAreaView style={Theme.SafeArea}>
            <KeyboardAvoidingView style={{ flex: 1 }}>

                <ScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>
                    <View style={styles.logoView1}>


                        <View style={styles.iconView}>
                            <Image source={require('../../../assets/images/WilluLogo.png')} style={{
                                height: 140,
                                width: 140
                            }} />
                        </View>
                        <Text style={styles.headerText}>{t('WagsHR')}</Text>
                    </View>

                    <View style={{ paddingHorizontal: 20, }}>
                        <TextInputAuth
                            label={t('server-ip')}
                            placeholder={t('ip-placeholder')}
                            value={isIPEditable ? inputs.ip : IP}
                            error={inputs?.errors?.ip}
                            onChangeText={(text) => handleOnChange('ip', text)}
                            editable={isIPEditable}
                            rightIcon={inputs.ip ? true : false}
                            onChangeEditable={(newEditable) => { setIPEditable(newEditable) }}
                            onSubmitEditing={getDatabasesList}
                        />

                        {showDBPicker &&
                            <View style={{ marginTop: 16, zIndex: 9999 }}>
                                <DBPicker
                                    label={t('database')}
                                    data={databases}
                                    value1={inputs.database}
                                    openDropdown={openDropdown}
                                    setOpenDropdown={setOpenDropdown}
                                    placeholder={t('select-db')}
                                    onChange={(db) => handleOnChange('database', db)}
                                    error={inputs?.errors?.database}
                                // loading={loading}
                                />
                            </View>
                        }


                    </View>
                    <View style={{
                        marginTop: 24,
                        //  borderWidth: 1,
                        flex: 1,
                        justifyContent: 'flex-end',
                        marginVertical: 24,
                        paddingHorizontal: 16,
                    }}>

                        <NextButton container={{ zIndex: -1, }} title={inputs.ip && inputs.database ? t('connect') : t('get-dbs')} loader={loading} onPress={() => inputs.ip && inputs.database ? validate() : getDatabasesList()} />
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

export default ServerScreen

const styles = StyleSheet.create({
    logoView1: {
        height: Dimensions.get('window').height * 0.4,
        alignItems: 'center',
        justifyContent: 'center',
    },

    iconView: {
        width: 140,
        height: 140,
        borderRadius: 70,
        overflow: 'hidden',

    },

    headerText: {
        ...FontStyle.Regular24,
        marginTop: 12,
        color: '#8E3A64'
    },
})