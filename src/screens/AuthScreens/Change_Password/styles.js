import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    mainView: {
        paddingHorizontal: 16,
        marginTop: 30,
        flex: 1,
    },

    bottomView: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 24,
        paddingHorizontal: 16,
    },


});