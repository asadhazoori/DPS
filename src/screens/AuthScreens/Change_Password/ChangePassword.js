import { SafeAreaView, View, ScrollView, StatusBar } from 'react-native'
import inputValidation from '../../../utilities/Validations/YupValidate'
import { ChangePasswordSchema } from '../../../utilities/Validations'
import { useDispatch, useSelector } from 'react-redux'
import React, { useEffect, useRef, useState } from 'react'
import { styles } from './styles'
import GeneralHeader from '../../../components/Headers/GeneralHeader'
import { NextButton, TextInputAuth } from '../../../components/Inputs'
import { Icons } from '../../../assets/SvgIcons/Icons'
import Toast from 'react-native-simple-toast';
import { commonApi } from '../../../utilities/api/apiController'
import { AlertModal } from '../../../components/Modals'
import { DrawerIcons } from '../../../assets/SvgIcons/DrawerIcons'
import { SvgXml } from 'react-native-svg'
import { expireWebSession } from '../../../redux/users/actions/expireWebSession'
import { logout_user, set_change_Password } from '../../../redux/users/user.actions'
import { useTranslation } from 'react-i18next'
import ReactNativeVI from '../../../components/Helpers/ReactNativeVI'
import SplashScreen from 'react-native-splash-screen'
import Theme from '../../../theme/theme'
import { COLORS } from '../../../theme/colors'

const ResetPassword = ({ navigation, route }) => {

    const data = route.params

    const { t } = useTranslation();
    const dispatch = useDispatch();
    const uid = useSelector((state) => state.signin.uid);
    const input1Ref = useRef(null);
    const input2Ref = useRef(null);
    const input3Ref = useRef(null);
    const [loading, setLoading] = useState(false);
    const [inputs, setInputs] = useState({
        currentPassword: null,
        newPassword: null,
        confirmPassword: null,
    });

    const [alertBox, setAlertBox] = useState({
        showBox: false,
        title: null,
        message: null,
        confirmbtn: null,
        okbtn: null,
    });

    const closeCustomAlert = () => {
        setTimeout(() => {
            setAlertBox({ showBox: false });
        }, 300);
    };

    const handleInputChange = (field, value) => {
        setInputs({
            ...inputs,
            [field]: value,
            errors: {
                ...inputs.errors,
                [field]: false
            }
        })
    }

    const handleInput1Forward = () => {
        input2Ref.current.focus();
    };

    const handleInput2Forward = () => {
        input3Ref.current.focus();
    };

    const validate = async () => {

        const schema = ChangePasswordSchema(t);
        const result = await inputValidation(schema, inputs);

        if (result.isValidate) {
            handleSubmit();

        } else {
            setInputs(prev => ({
                ...prev,
                errors: result?.err
            }))
        }

    }

    const handleSubmit = async () => {

        setLoading(true);

        try {
            const body = {
                params: {
                    model: "res.company",
                    method: "change_user_password",
                    args: [
                        {
                            user_id: uid,
                            old_password: inputs?.currentPassword,
                            new_password: inputs.newPassword,
                        }
                    ],
                    kwargs: {}
                }
            }


            const response = await commonApi({ body, navigation });

            if (response?.data?.result) {

                if (response?.data?.result?.status === "success") {
                    setLoading(false);
                    setInputs({
                        currentPassword: null,
                        newPassword: null,
                        confirmPassword: null,
                    });



                    // const status = await dispatch(expireWebSession({ navigation }));
                    // console.log("status", status)
                    // if (status === "Success") {
                    setTimeout(() => { Toast.show(`${t('password-changed-sucfly')} !`); }, 500)

                    dispatch(set_change_Password(false));
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Login' }],
                    });
                    dispatch(logout_user(false));
                    // }
                } else if (response?.data?.result?.status === "error") {
                    setLoading(false);
                    setTimeout(() => {
                        setAlertBox({
                            okButton: true,
                            title: t("incorrect-password"),
                            message: t("incorrect-password-decription"),
                            icon: (
                                <ReactNativeVI
                                    Lib={'MaterialCommunityIcons'}
                                    name={'alert'}
                                    color={'#F73C3C'}
                                    size={26}
                                />
                            ),
                        });
                    }, 300);

                }
            }

            else {

                setLoading(false);
                if (response?.data?.error) {
                    if (response?.data?.error?.message == 'Odoo Session Expired') {
                        setTimeout(() => { Toast.show(t('session-expired')); }, 500)
                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Login' }],
                        });
                        dispatch(logout_user(false));
                    } else {
                        setTimeout(() => {
                            setAlertBox({
                                showBox: true,
                                title: `${'Error'}`,
                                message: `${response?.data?.error?.data?.message}`,
                            });
                        }, 300);
                    }
                } else if (
                    response == 'AxiosError: Request failed with status code 404'
                ) {
                    setTimeout(() => { Toast.show(t('session-expired')); }, 500)
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Login' }],
                    });
                    dispatch(logout_user(false));
                }
            }

        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        SplashScreen.hide();
    }, []);

    return (
        <SafeAreaView style={Theme.SafeArea}>
            <StatusBar
                barStyle={'dark-content'}
                backgroundColor={COLORS.background}
            />
            <View style={styles.container}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>

                    <GeneralHeader title={t('change-password')} backIcon={data?.isPasswordChanged ? false : true} profileIcon={true} navigation={navigation} notificationIcon={false} />

                    <View style={styles.mainView}>

                        <TextInputAuth
                            ref={input1Ref}
                            label={t('current-password')}
                            placeholder={t("enter-current-passw")}
                            value={inputs.currentPassword}
                            marginTop={12}
                            error={inputs?.errors?.currentPassword}
                            onChangeText={(text) => handleInputChange('currentPassword', text)}
                            password={true}
                            icon={Icons.password}
                            returnKeyType={'next'}
                            onSubmitEditing={handleInput1Forward}
                        />

                        <TextInputAuth
                            ref={input2Ref}
                            label={t('new-password')}
                            placeholder={t("enter-new-passw")}
                            value={inputs.newPassword}
                            marginTop={12}
                            error={inputs?.errors?.newPassword}
                            onChangeText={(text) => handleInputChange('newPassword', text)}
                            password={true}
                            icon={Icons.password}
                            returnKeyType={'next'}
                            onSubmitEditing={handleInput2Forward}
                        />

                        <TextInputAuth
                            ref={input3Ref}
                            label={t('confirm-password')}
                            placeholder={t("conf-new-passw")}
                            value={inputs.confirmPassword}
                            marginTop={12}
                            error={inputs?.errors?.confirmPassword}
                            onChangeText={(text) => handleInputChange('confirmPassword', text)}
                            password={true}
                            icon={Icons.password}
                            returnKeyType={'done'}
                            onSubmitEditing={validate}
                        />

                    </View>

                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        {
                            data?.isPasswordChanged &&
                            <View style={styles.bottomView}>
                                <NextButton
                                    title={t("cancel")}
                                    onPress={() => {
                                        dispatch(set_change_Password(false));
                                        navigation.reset({
                                            index: 0,
                                            routes: [
                                                {
                                                    name: 'BottomTab',
                                                },
                                            ],
                                        });
                                    }}
                                />
                            </View>}

                        <View style={styles.bottomView}>
                            <NextButton
                                title={t("submit")}
                                loader={loading}
                                onPress={validate}
                            />
                        </View>
                    </View>

                </ScrollView>

                <AlertModal
                    visible={alertBox.showBox}
                    icon={
                        <View style={styles.alertIconView}>
                            <SvgXml xml={DrawerIcons.shifts} />
                        </View>
                    }
                    title={alertBox.title}
                    message={alertBox.message}
                    onClose={closeCustomAlert}
                    confirmbtn={alertBox.confirmbtn}
                />
            </View>
        </SafeAreaView>
    )
}

export default ResetPassword