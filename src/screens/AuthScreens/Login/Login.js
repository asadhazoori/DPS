import { SafeAreaView, StatusBar } from 'react-native'
import React, { useEffect } from 'react'
import Theme from '../../../theme/theme';
import { AuthProvider } from '../../../context/AuthContext';
import Wrapper from './Wrapper';
import SplashScreen from 'react-native-splash-screen';
import { COLORS } from '../../../theme/colors';

const Login = ({ navigation, route }) => {

    const data = route?.params;

    useEffect(() => {
        SplashScreen.hide();
    }, [])

    return (
        <SafeAreaView style={Theme.SafeArea}>
            <StatusBar barStyle={'dark-content'} backgroundColor={COLORS.background} />
            <AuthProvider navigation={navigation} status={data?.status}>
                <Wrapper navigation={navigation} />

            </AuthProvider>
        </SafeAreaView>
    )
}

export default Login