import { Dimensions, StyleSheet } from 'react-native'
import { COLORS } from '../../../theme/colors';
import { FontStyle } from '../../../theme/FontStyle';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    logoView: {
        height: Dimensions.get('window').height * 0.46,
        backgroundColor: 'rgba(233, 228, 255, 0.55)',
        borderBottomLeftRadius: 33,
        borderBottomRightRadius: 33
    },

    langView: {
        flexDirection: 'row',
        marginTop: 22,
        marginHorizontal: 16,
        justifyContent: 'space-between'

    },

    logoView1: {
        alignItems: 'center',
        flex: 1,
        marginBottom: 50,
        justifyContent: 'center',
    },

    iconView: {
        width: 140,
        height: 140,
        borderRadius: 70,
        overflow: 'hidden',

    },
    alertIconView: {
        backgroundColor: "#F73C3C3D",
        height: 48,
        width: 48,
        borderRadius: 48,
        justifyContent: 'center',
        alignItems: 'center'
    },

    headerText: {
        ...FontStyle.Regular24,
        marginTop: 12,
        color: '#8E3A64'
    },


    headerView: {
    },

    headerCaption: {
        marginTop: 4
    },

    checkBoxView: {
        marginTop: 8
    },

    mainView: {
        paddingHorizontal: 16,
        marginTop: -50,
        flex: 1
    },

    forgetView: {
        marginTop: 4,
        paddingVertical: 3,
        alignSelf: 'flex-end'
    },

    forgetText: {
        ...FontStyle.Regular16_500,
        letterSpacing: 0.16,
    },

    header: {
        fontSize: 30,
        textAlign: 'center',
        color: COLORS.black,
        fontWeight: 'bold'
    },

    inputField: {
        borderWidth: 1,
        marginVertical: 15

    },

    bottomView: {
        justifyContent: 'flex-end',
        marginVertical: 24,
        paddingHorizontal: 16,
    },



});