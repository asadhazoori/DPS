import { View, Text, ScrollView, TouchableOpacity, Image, KeyboardAvoidingView, Platform, Linking, } from 'react-native'
import React, { useContext } from 'react'
import { styles } from './styles';
import { CheckBox, NextButton, TextInputAuth } from '../../../components/Inputs';
import { COLORS } from '../../../theme/colors';
import { AuthContext } from '../../../context/AuthContext';
import { Icons } from '../../../assets/SvgIcons/Icons';
import { Frames } from '../../../assets/SvgIcons/Frames';
import { AlertModal, LangModal, Loader, ServerModal } from '../../../components/Modals';
import changeLng from '../../../utilities/helpers/ChangeLanguage';
import ReactNativeVI from '../../../components/Helpers/ReactNativeVI';
import { useSelector } from 'react-redux';
// import { enableBiometricAuth, handleBiometricAuth } from '../../../utilities/helpers/Biometrics';


const Wrapper = ({ navigation }) => {

    const {
        loading,
        inputs,
        validate,
        handleOnChange,
        setInputs,
        t,
        // changeLng,
        lngModalVisible,
        setLngModalVisible,
        serverModal,
        setServerModal,
        input1Ref,
        input2Ref,
        handleInput1Forward,
        alertBox,
        closeCustomAlert,
    } = useContext(AuthContext);

    const companyLogo = useSelector(state => state?.settings?.companyLogo);

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            style={styles.container}
        >
            <ScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>

                <View style={styles.logoView}>
                    <View style={styles.langView}>
                        <TouchableOpacity activeOpacity={0.5} onPress={() => setLngModalVisible(true)} style={{ paddingHorizontal: 8, paddingVertical: 4 }}>
                            <ReactNativeVI Lib={'FontAwesome'} name={'language'} color={COLORS.primaryColor} size={26} />
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={0.5} onPress={() => setServerModal(true)} style={{ paddingHorizontal: 8, paddingVertical: 4 }}>
                            <ReactNativeVI Lib={'Ionicons'} name={'server-sharp'} color={COLORS.primaryColor} size={26} />
                        </TouchableOpacity>
                    </View>


                    <View style={styles.logoView1}>

                        {companyLogo ?
                            <View style={{
                                height: 160,
                                width: 160
                            }}>

                                <Image
                                    source={{ uri: `data:image/jpeg;base64,${companyLogo}` }}
                                    style={{
                                        flex: 1,

                                    }}
                                    resizeMode='contain'
                                />
                            </View>
                            :
                            <View style={styles.iconView}>

                                <Image source={require('../../../assets/images/WilluLogo.png')} style={{
                                    height: 140,
                                    width: 140
                                }} />
                                <Text style={styles.headerText}>{t('WagsHR')}</Text>
                            </View>
                        }

                    </View>
                </View>

                <View style={styles.mainView}>

                    <TextInputAuth
                        ref={input1Ref}
                        label={t('username')}
                        placeholder={t('username-placeholder')}
                        value={inputs.username}
                        error={inputs?.errors?.username}
                        onChangeText={(text) => handleOnChange('username', text)}
                        icon={Icons.email}
                        keyboardType={'email-address'}
                        returnKeyType={'next'}
                        onSubmitEditing={handleInput1Forward}
                    />

                    <TextInputAuth
                        ref={input2Ref}
                        // ref={input2Ref}
                        label={t('password')}
                        placeholder={t('password-placeholder')}
                        value={inputs.password}
                        marginTop={12}
                        error={inputs?.errors?.password}
                        onChangeText={(text) => handleOnChange('password', text)}
                        password={true}
                        icon={Icons.password}
                        returnKeyType={'done'}
                        onSubmitEditing={validate}
                    />

                    <View style={styles.checkBoxView}>
                        <CheckBox
                            status={inputs.loggedIn}
                            onPress={() => {
                                setInputs({
                                    ...inputs,
                                    ['loggedIn']: !inputs.loggedIn,
                                })
                            }} />
                    </View>

                    {/* <TouchableOpacity style={styles.forgetView}>
                    <Text style={[styles.forgetText]}>Forget Password ?</Text>
                </TouchableOpacity> */}


                </View>
                <View style={styles.bottomView}>
                    {/* <View style={{ marginVertical: 12, gap: 12 }}>
                        <NextButton
                            title={t('Check Biometric Supported')}
                            onPress={enableBiometricAuth}
                        />
                        <NextButton
                            title={t('Biometric & Face ID')}
                            onPress={handleBiometricAuth}
                        />
                    </View> */}
                    <NextButton
                        title={t('login')}
                        onPress={validate}
                    />
                </View>

                <Loader
                    loading={loading}
                    title={t('signing-in')}
                />

                <LangModal
                    modalVisible={lngModalVisible}
                    setModalVisible={setLngModalVisible}
                    // onChangeSelection={changeLng}
                    onChangeSelection={changeLng}
                />

                <ServerModal
                    modalVisible={serverModal}
                    setModalVisible={setServerModal}
                    navigation={navigation}
                />

                <AlertModal
                    visible={alertBox.showBox}
                    icon={alertBox.icon}
                    title={alertBox.title}
                    message={alertBox.message}
                    confirmbtn={alertBox.confirmbtn}

                    onClose={closeCustomAlert}
                    okButton={alertBox.okButton}
                    retry={alertBox?.retry}
                    updateBtn={alertBox?.updateBtn}
                    updateAction={() => {
                        if (Platform.OS == 'android') {
                            Linking.openURL('https://play.google.com/store/apps/details?id=com.wags.hr')
                        }
                    }}
                />



            </ScrollView >
        </KeyboardAvoidingView>

    )
}

export default Wrapper