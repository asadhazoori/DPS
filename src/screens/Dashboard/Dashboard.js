import { View, SafeAreaView, ScrollView, Platform, Linking, StatusBar } from 'react-native';
import React, { useEffect, useState } from 'react';
import { styles } from './styles';
import Theme from '../../theme/theme';
import { COLORS } from '../../theme/colors';
import { DashboardIcons } from '../../assets/SvgIcons/DashboardIcons';
import DashboardCard from '../../components/Cards/DashboardCard';
import { useDispatch, useSelector } from 'react-redux';
import { requestAndGetLocation } from '../../redux/location/location.action';
import AlertModal from '../../components/Modals/AlertModal';
import { useTranslation } from 'react-i18next';
import ReactNativeVI from '../../components/Helpers/ReactNativeVI';
import { getPermissionJust } from '../../utilities/helpers/AccessLocation';
import { checkNotificationsPermission, checkWriteStoragePermission } from '../../utilities/helpers/RequestPermission';
import WelcomeDashoard from '../../components/Helpers/WelcomeDashoard';
import DashboardCardAttendance from '../../components/Cards/DashboardCardAttendance';
import { AppVersion } from '../../utilities/constant/version';
import { getEmployeeProfile } from '../../redux/profile/actions/getEmployeeProfile';
import { matchServerVerions } from '../../utilities/helpers/matchServerVersion';
import GeneralHeader from '../../components/Headers/GeneralHeader';
import { compareVersions } from '../../utilities/helpers/checkLowerVersion';
import EmployeeWarnings from '../../components/Helpers/EmployeeWarnings';
import { registerFCMToken } from '../../redux/users/actions/registerFCMToken';

const Dashboard = ({ navigation, route }) => {
  const params = route?.params;

  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);

  const user = useSelector(state => state.signin);
  const { allRequests, isEmpLetterReqEnabled, isTravelReqEnabled, isIqamaReqEnabled, isResignationReqEnabled, ticketCounts } = useSelector(state => state?.request);
  const { overtimeBalance, employeeWiseLoanCounts, isManager, name } = useSelector(state => state?.employeeProfile);
  const { activeAttendance, yesterdayAttendance, manualAttendCounts } = useSelector(state => state?.attendance);
  const { data } = useSelector(state => state?.employeeProfile);

  const selectedAttendance = activeAttendance?.checkin ? activeAttendance : yesterdayAttendance;
  const punchStatus1 = selectedAttendance?.checkin
    ? selectedAttendance?.last_checkout
      ? 'punched-out'
      : 'punched-in'
    : 'no-record-found';

  const iqamaRenewalCount = allRequests?.filter(request => request.request_type === 'Iqama Renewal')?.length;
  const ticketCount = allRequests?.filter(request => request.request_type === 'Travel & Ticket',)?.length;
  const resignationCount = allRequests?.filter(request => request.request_type === 'Employee Resignation',)?.length;
  const empLetterCount = allRequests?.filter(request => request.request_type === 'Employment Letter',)?.length;

  const getCaptionAndValue = () => {
    let c1 = null;
    let c2 = null;

    if (isTravelReqEnabled) {
      c1 = {
        caption: t('Travel & Ticket'),
        value: `${ticketCount ? ticketCount : 0}`,
      };
    }

    if (isIqamaReqEnabled) {
      if (!c1) {
        c1 = {
          caption: t('Iqama Renewal'),
          value: `${iqamaRenewalCount ? iqamaRenewalCount : 0}`,
        };
      } else {
        c2 = {
          caption: t('Iqama Renewal'),
          value: `${iqamaRenewalCount ? iqamaRenewalCount : 0}`,
        };
      }
    }

    if (isEmpLetterReqEnabled) {
      if (!c1) {
        c1 = {
          caption: t('Employment Letter'),
          value: `${empLetterCount ? empLetterCount : 0}`,
        };
      } else if (!c2) {
        c2 = {
          caption: t('Employment Letter'),
          value: `${empLetterCount ? empLetterCount : 0}`,
        };
      }
    }

    if (isResignationReqEnabled) {
      if (!c1) {
        c1 = {
          caption: t('Employee Resignation'),
          value: `${resignationCount ? resignationCount : 0}`,
        };
      } else if (!c2) {
        c2 = {
          caption: t('Employee Resignation'),
          value: `${resignationCount ? resignationCount : 0}`,
        };
      }
    }

    return { c1, c2 };
  };

  const { c1, c2 } = getCaptionAndValue();

  const [alertBox, setAlertBox] = useState({
    showBox: false,
    title: null,
    message: null,
    confirmbtn: null,
    retry: null,
    icon: null,
    updateBtn: null,
    okButton: false,
  });

  useEffect(() => {
    if (params?.versionMatched && params?.versionMatched == 'Null') {
      setTimeout(() => {
        setAlertBox({
          showBox: true,
          confirmbtn: false,
          okButton: true,
          retry: 'retry',
          title: 'Server Version Not Configured',
          message: `${t(
            'server-update-msg',
          )}\nApp Version: ${AppVersion}\nServer Version: ${params?.versionMatched
            }`,
          icon: (
            <View style={styles.iconView}>
              <ReactNativeVI
                Lib={'FontAwesome5'}
                name={'server'}
                color={'#F73C3C'}
                size={26}
              />
            </View>
          ),
        });
      }, 300);
    } else if (
      params?.versionMatched &&
      params?.versionMatched != 'matched' &&
      params?.versionMatched != 'session_expired'
    ) {
      const lowerVersion = compareVersions(AppVersion, params?.versionMatched);

      setTimeout(() => {
        setAlertBox({
          showBox: true,
          confirmbtn: false,
          okButton: true,
          retry: 'retry',
          updateBtn:
            Platform.OS == 'android' && lowerVersion == 'updateApp'
              ? true
              : false,
          title:
            lowerVersion == 'updateServer'
              ? t('server-update-req')
              : t('app-update-req'),
          message: `${lowerVersion == 'updateServer'
            ? t('server-update-msg')
            : t('app-update-msg')
            }\nApp Version: ${AppVersion}\nServer Version: ${params?.versionMatched
            }`,
          icon: (
            <View style={styles.iconView}>
              <ReactNativeVI
                Lib={
                  lowerVersion == 'updateServer'
                    ? 'FontAwesome5'
                    : 'MaterialIcons'
                }
                name={
                  lowerVersion == 'updateServer' ? 'server' : 'system-update'
                }
                color={'#F73C3C'}
                size={26}
              />
            </View>
          ),
        });
      }, 100);
    }
  }, [params?.versionMatched]);

  useEffect(() => {
    if (params?.versionMatched != false) {
      setTimeout(() => {
        checkPermissions();
      }, 1500);
    }
  }, []);

  const checkPermissions = async () => {
    try {
      if (Platform.OS == 'android') {
        await checkNotificationsPermission();
        await checkWriteStoragePermission();
      }
      registerFCMToken({ dispatch, navigation })

      const result = await getPermissionJust();
      if (result == 'fine') {
        await dispatch(requestAndGetLocation());
      } else if (result == 'GPS_disabled') {
        setTimeout(() => {
          setAlertBox({
            showBox: true,
            confirmbtn: false,
            title: t('gps-loc-disabled'),
            message: t('gps-loc-disabled-msg'),
            icon: (
              <View style={styles.iconView}>
                <ReactNativeVI
                  Lib={'MaterialIcons'}
                  name={'location-off'}
                  color={'#F73C3C'}
                  size={26}
                />
              </View>
            ),
          });
        }, 300);
      } else if (result == 'denied') {
        setTimeout(() => {
          setAlertBox({
            showBox: true,
            title: t('permission-denied'),
            message: t('permission-denied-msg'),
            confirmbtn: true,
            icon: (
              <View style={styles.iconView}>
                <ReactNativeVI
                  Lib={'MaterialIcons'}
                  name={'location-off'}
                  color={'#F73C3C'}
                  size={26}
                />
              </View>
            ),
          });
        }, 300);
      }
    } catch (error) {
      console.log('Error in Getinng Permission');
    }
  };

  const closeCustomAlert = () => {
    setTimeout(() => {
      setAlertBox({ showBox: false });
    }, 300);
  };

  const Retry = async () => {
    try {
      setLoading(true);
      const versionMatched = await matchServerVerions({ navigation, dispatch });

      if (versionMatched == 'matched') {
        setTimeout(() => {
          setAlertBox({ showBox: false });
        }, 300);
        await dispatch(
          getEmployeeProfile({
            uid: user?.uid,
            navigation,
            callingFromProfileScreen: true
          }),
        );
        setLoading(false);
        checkPermissions();
      } else if (versionMatched == 'Null') {
        setLoading(false);
        setTimeout(() => {
          setAlertBox({
            showBox: true,
            confirmbtn: false,
            okButton: true,
            retry: 'retry',
            title: 'Server Version Not Configured',
            message: `${t(
              'server-update-msg',
            )}\nApp Version: ${AppVersion}\nServer Version: ${versionMatched}`,
            icon: (
              <View style={styles.iconView}>
                <ReactNativeVI
                  Lib={'FontAwesome5'}
                  name={'server'}
                  color={'#F73C3C'}
                  size={26}
                />
              </View>
            ),
          });
        }, 300);
      } else if (
        versionMatched !== 'matched' &&
        versionMatched !== 'session_expired'
      ) {
        const lowerVersion = compareVersions(AppVersion, versionMatched);
        setLoading(false);

        setTimeout(() => {
          setAlertBox({
            showBox: true,
            confirmbtn: false,
            okButton: true,
            retry: 'retry',
            updateBtn:
              Platform.OS == 'android' && lowerVersion == 'updateApp'
                ? true
                : false,
            title:
              lowerVersion == 'updateServer'
                ? t('server-update-req')
                : t('app-update-req'),
            message: `${lowerVersion == 'updateServer'
              ? t('server-update-msg')
              : t('app-update-msg')
              }\nApp Version: ${AppVersion}\nServer Version: ${versionMatched}`,
            icon: (
              <View style={styles.iconView}>
                <ReactNativeVI
                  Lib={
                    lowerVersion == 'updateServer'
                      ? 'FontAwesome5'
                      : 'MaterialIcons'
                  }
                  name={
                    lowerVersion == 'updateServer' ? 'server' : 'system-update'
                  }
                  color={'#F73C3C'}
                  size={26}
                />
              </View>
            ),
          });
        }, 100);
      }
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={COLORS.background}
      />
      <View style={Theme.flex1}>
        <GeneralHeader
          title={t('dashboard')}
          backIcon={false}
          navigation={navigation}
        />

        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.scrollView}>
          <WelcomeDashoard name={name} />

          <View
            style={[
              styles.featureView,
              { paddingBottom: allRequests?.length > 0 ? 0 : 16 },
            ]}>
            <DashboardCardAttendance
              progressBar={true}
              icon={DashboardIcons.attendance}
              title={t('attendance')}
              onPress={() => {
                if (isManager) {
                  navigation.reset({
                    index: 0,
                    routes: [
                      {
                        name: 'Attendance1',
                        params: {
                          screen: 'ManagerAttendance',
                        },
                      },
                    ],
                  })
                } else {
                  navigation.reset({
                    index: 0,
                    routes: [
                      {
                        name: 'Attendance1',
                        params: {
                          screen: 'Attendance',
                        },
                      },
                    ],
                  })
                }
              }}
              punchStatus={punchStatus1}
              onPressMiddle={() =>
                navigation.reset({
                  index: 0,
                  routes: [
                    {
                      name: 'Attendance1',
                      params: {
                        screen: 'AttendanceRequests',
                      },
                    },
                  ],
                })

              }
            />

            <View style={[styles.row, { marginTop: 16 }]}>
              <DashboardCard
                icon={DashboardIcons.loans}
                title={t('loans-advances')}
                onPress={() => navigation.navigate('Loans')}
                c1={{
                  caption: t('approved'),
                  value:
                    employeeWiseLoanCounts?.approved +
                    employeeWiseLoanCounts?.first_approval,
                }}
                c2={{
                  caption: t('pending'),
                  value: employeeWiseLoanCounts?.pending,
                }}
              />

              <DashboardCard
                icon={DashboardIcons.overTime}
                title={t('overtime-tracking')}
                onPress={() => navigation.navigate('OvertimeTracking')}
                index={2}
                c1={{
                  caption: t('worked-hours'),
                  value: `${overtimeBalance?.approved_overtime
                    ? overtimeBalance?.approved_overtime?.toFixed(1)
                    : '0.0'
                    }`,
                }}
                c2={{
                  caption: t('requested-hours'),
                  value: `${overtimeBalance?.pending_overtime
                    ? overtimeBalance?.pending_overtime?.toFixed(1)
                    : '0.0'
                    }`,
                }}
              />
            </View>

            <View style={styles.row}>
              <DashboardCard
                icon={DashboardIcons.shift}
                title={t('requests')}
                c1={c1 || { caption: '', value: '' }}
                c2={c2 || { caption: '', value: '' }}
                onPress={() => navigation.navigate('Requests')}
              />
              {data?.enable_manual_attendance && (
                <DashboardCard
                  manualAttendIcon
                  title={t('manual-attendance')}
                  onPress={() =>
                    navigation.reset({
                      index: 0,
                      routes: [
                        {
                          name: 'Attendance1',
                          params: {
                            screen: 'ManualAttendanceReqs',
                          },
                        },
                      ],
                    })
                  }
                  c2={{
                    caption: t('pending'),
                    value: manualAttendCounts?.submit,
                  }}
                  c1={{
                    caption: t('approved'),
                    value:
                      manualAttendCounts?.manager_approved +
                      manualAttendCounts?.validate,
                  }}
                />
              )}
            </View>

            {
              data?.enable_internal_tickets &&
              <View style={styles.row}>
                <DashboardCard
                  icon={DashboardIcons.timesheet}
                  title={t("internal-ticket")}
                  onPress={() => navigation.navigate('InternalTicket')}
                  c1={{ caption: t('approved'), value: ticketCounts?.approved }}
                  c2={{ caption: t('pending'), value: ticketCounts?.pending }}
                />
              </View>}

            {/* <View style={styles.row}>
                        <DashboardCard
                            icon={DashboardIcons.medical}
                            title={t('medical-claims')}
                            onPress={() => navigation.navigate('MedicalClaim')}
                            c1={{ caption: t('balance'), value: "24,000" }}
                            c2={{ caption: t('approved'), value: "6,000" }}
                            c3={{ caption: t('pending'), value: "18,000" }}
                        />

                        <DashboardCard
                            icon={DashboardIcons.timesheet}
                            title={t('timesheet')}
                            c1={{ caption: "For sick Leaves", value: "10" }}
                            c2={{ caption: "For Advance salary", value: "14" }}
                            c3={{ caption: "For Change shift", value: "12" }}
                            onPress={() => navigation.navigate('Timesheet')}
                        />

                    </View> */}
          </View>

          <EmployeeWarnings navigation={navigation} />

          <AlertModal
            visible={alertBox.showBox}
            icon={alertBox?.icon}
            title={alertBox.title}
            message={alertBox.message}
            loading={loading}
            okButton={alertBox.okButton}
            onClose={alertBox.retry == 'retry' ? Retry : closeCustomAlert}
            confirmbtn={alertBox.confirmbtn}
            retry={alertBox?.retry}
            updateBtn={alertBox?.updateBtn}
            updateAction={() => {
              if (Platform.OS == 'android') {
                Linking.openURL(
                  'https://play.google.com/store/apps/details?id=com.wags.hr',
                );
              }
            }}
          />
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default Dashboard;
