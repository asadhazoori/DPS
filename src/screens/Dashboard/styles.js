import { StyleSheet } from "react-native";
import { COLORS } from "../../theme/colors";

export const styles = StyleSheet.create({

    scrollView: {
    },

    textView: {
        marginTop: 16,
    },

    featureView: {

        borderColor: 'green',

        paddingVertical: 16,
        paddingHorizontal: 20,
    },

    row: {
        flexDirection: 'row',
        marginBottom: 16,
        gap: 16
    },


    container: {
        flex: 1,
        backgroundColor: COLORS.white,
    },
    imageView: {
        width: '100%',
        backgroundColor: COLORS.grey
    },

    mainView: {
        paddingLeft: 10,
        flex: 1,
    },

    functionView: {
        height: 170,
        marginVertical: 20,
    },

    innerView: {

    },

    taskView: {
        backgroundColor: COLORS.grey, //'#c6c9c6',
        borderRadius: 12,
        width: 210,
        marginTop: 10,
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },

    header: {
        color: COLORS.black,
        fontSize: 20,
        fontWeight: 'bold'
    },

    text: {
        color: COLORS.black,
        fontWeight: '500',
        fontSize: 14,

    },


    iconView: {
        backgroundColor: "#F73C3C3D",
        height: 48,
        width: 48,
        borderRadius: 48,
        justifyContent: 'center',
        alignItems: 'center'
    },




});