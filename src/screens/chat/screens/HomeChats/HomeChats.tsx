import {
  ActivityIndicator,
  AppState,
  FlatList,
  I18nManager,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import { SvgXml } from 'react-native-svg';
import { SearchIcon } from '../../assets/svgs/SearchIcon';
import { COLORS } from '../../../../theme/colors';
import { fonts } from '../../../../theme/fonts';
import ChatItem from '../../components/ChatItem';
import { getChatGroups } from '../../redux/apis/getChatGroups';
import ReactNativeVI from '../../../../components/Helpers/ReactNativeVI';
import { FontStyle } from '../../../../theme/FontStyle';
import LinearGradient from 'react-native-linear-gradient';
import { useTranslation } from 'react-i18next';
import { BackArrow } from '../../assets/svgs';
import { useDispatch, useSelector } from 'react-redux';
import { styles } from './styles';

const HomeChats = ({ navigation }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [refresh, setRefresh] = useState(false);
  const { chatGroups } = useSelector(state => state?.chat);
  const { partnerId } = useSelector(state => state?.signin);

  const getData = async (isRefresh: any) => {
    if (isRefresh) {
      setRefresh(true);
    }
    await getChatGroups({ navigation, dispatch, partnerId });
    if (isRefresh) {
      setRefresh(false);
    }
  };

  useEffect(() => {
    getData(true);

    const intervalId = setInterval(() => {
      if (AppState.currentState == 'active') {
        getData(false);
      }
    }, 60000);

    return () => clearInterval(intervalId);
  }, []);

  return (
    <SafeAreaView style={styles.SafeAreaViewView}>
      <StatusBar backgroundColor={COLORS.white} barStyle={'dark-content'} />
      {/* <LinearGradient
                colors={['rgba(255, 255, 255, 1)', 'rgba(244, 244, 244, 1)']}
                style={{ flex: 1 }}
            > */}

      <View style={styles.header}>
        <TouchableOpacity
          style={[
            styles.backIcon,
            { transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] },
          ]}
          onPress={() => navigation.goBack()}>
          <SvgXml xml={BackArrow} />
        </TouchableOpacity>

        <Text style={styles.headerText}>{t('messages')}</Text>
        {/* <SvgXml xml={SearchIcon} /> */}
        {/* <SvgXml xml={MenuDots} /> */}
        <TouchableOpacity
          style={styles.menuIcon}
          onPress={() => getData(true)}
          disabled={refresh}>
          {refresh ? (
            <ActivityIndicator color={COLORS.primaryColor} size={22} />
          ) : (
            <ReactNativeVI
              color={COLORS.primaryColor}
              name={'refresh'}
              size={22}
              Lib={'FontAwesome'}
            />
          )}
        </TouchableOpacity>
      </View>

      <FlatList
        style={styles.chatsView}
        contentContainerStyle={styles.contentContainerStyle}
        data={chatGroups}
        renderItem={({ item }) => (
          <ChatItem item={item} navigation={navigation} />
        )}
        keyExtractor={(item, index) => index.toString()}
        ListEmptyComponent={() => (
          <>
            {chatGroups?.length < 1 && (
              <Text style={[styles.dateText, { alignSelf: 'center' }]}>
                {t('chat-not-exist')}
              </Text>
            )}
          </>
        )}
      />
      {/* </LinearGradient> */}
    </SafeAreaView>
  );
};

export default HomeChats;
