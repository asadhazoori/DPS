import { StyleSheet } from "react-native";
import { COLORS } from "../../../../theme/colors";
import { FontStyle } from "../../../../theme/FontStyle";
import { fonts } from "../../../../theme/fonts";

export const styles = StyleSheet.create({
    SafeAreaViewView: {
        flex: 1,
        backgroundColor: COLORS.white,
    },

    header: {
        paddingRight: 16,
        paddingLeft: 8,
        flexDirection: 'row',
        paddingVertical: 13,
        alignItems: 'center',
    },
    backIcon: {
        paddingVertical: 4,
        marginRight: 4,
        paddingRight: 8,
    },

    headerText: {
        ...FontStyle.Regular20,
        fontWeight: '500',
        color: COLORS.primaryColor,
        fontFamily: fonts.medium,
        flex: 1,
        textAlign: 'left'
    },
    chatsView: {
        flex: 1,
    },
    contentContainerStyle: {
        gap: 12, paddingVertical: 12,

    },
    menuIcon: {
        paddingHorizontal: 10,
        paddingVertical: 4,
        marginLeft: 14
    },
    dateText: {
        ...FontStyle.Regular12,
        fontWeight: '500',
        color: COLORS.grey5,
      },
})