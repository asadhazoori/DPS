import {
  ActivityIndicator,
  AppState,
  Button,
  FlatList,
  I18nManager,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  Touchable,
  TouchableOpacity,
  View,
} from 'react-native';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { COLORS } from '../../../../theme/colors';
import { SvgXml } from 'react-native-svg';
import {
  Attachments,
  BackArrow,
  MenuDots,
  Microphone,
  Smile,
} from '../../assets/svgs';
import { getChatById } from '../../redux/apis/getChatById';
import { sendMessage } from '../../redux/apis/sendMessage';
import { useRoute } from '@react-navigation/native';
import ReactNativeVI from '../../../../components/Helpers/ReactNativeVI';
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch, useSelector } from 'react-redux';
import { styles } from './styles';
import { FontStyle } from '../../../../theme/FontStyle';

const ConversationChat = ({ navigation }) => {
  const route = useRoute();
  const routeData = route?.params;

  const dispatch = useDispatch();
  const flatListRef = useRef<FlatList>(null);
  const [messages, setMessages] = useState([]);
  const [refresh, setRefresh] = useState(false);
  const [inputMessage, setInputMessage] = useState('');
  const { partnerId, name } = useSelector(state => state?.signin);

  //   useEffect(() => {
  //     flatListRef.current?.scrollToEnd({animated: true});
  //   });

  const generateColor = (senderId: any) => {
    const colors = [
      '#FF5733',
      '#3357FF',
      '#FF33A8',
      'rgba(242, 153, 74, 1)',
      '#A833FF',
      '#33FFD1',
      '#FFC133',
    ];
    return colors[senderId % colors.length];
  };

  const getChat = async (isRefresh: any) => {
    if (isRefresh) {
      setRefresh(true);
    }

    const groupID = routeData?.chatInfo?.group_id;
    const message = await getChatById({
      navigation,
      groupId: groupID,
      dispatch,
      partnerId,
    });
    if (message) {
      setMessages(message?.data?.messages);
    }
    if (isRefresh) {
      setRefresh(false);
    }
  };

  const sendMessages = async () => {
    if (inputMessage.trim()) {
      setMessages([
        ...messages,
        {
          sender_id: partnerId,
          sender_name: name,
          is_sent: true,
          subject: '',
          body: inputMessage,
          create_date: '',
        },
      ]);
      setInputMessage('');

      await sendMessage({
        navigation,
        groupId: routeData?.chatInfo?.group_id,
        message: inputMessage,
        partnerId: partnerId,
        dispatch,
      });
      getChat(false);
    }
  };

  // useEffect(() => {
  //   getChat(true);
  // }, []);

  useEffect(() => {
    getChat(true);

    const intervalId = setInterval(() => {
      if (AppState.currentState == 'active') {
        getChat(false);
      }
    }, 60000);

    return () => clearInterval(intervalId);
  }, []);

  const renderItem = ({ item }) => {
    const timeOnly = item?.create_date
      ? new Date(item.create_date + 'Z').toLocaleString('en-US', {
        timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        hour: 'numeric',
        minute: 'numeric',
        year: '2-digit',
        hour12: true,
        month: 'short',
        day: 'numeric',
      }).replace(' at ', ', ')
      : '';

    const message = item?.body.replace(/\n\n/g, '');

    const senderColor = generateColor(item.sender_id);

    return (
      <View style={{ marginBottom: 10 }}>
        <View style={item.is_sent ? styles.myMessage1 : styles.theirMessage1}>
          <View style={item.is_sent ? styles.myMessage : styles.theirMessage}>
            {!item.is_sent && (
              <Text style={[styles.senderText, { color: senderColor }]}>
                {item.sender_name}
              </Text>
            )}

            <Text
              style={[
                styles.messageText,
                { color: item.is_sent ? 'white' : 'rgba(27, 26, 87, 1)' },
              ]}>
              {message}
            </Text>
          </View>

          <Text
            style={[
              styles.timeText,
              item.is_sent
                ? { alignSelf: 'flex-end', marginRight: 8 }
                : { marginLeft: 16, alignSelf: 'flex-start' },
            ]}>
            {timeOnly}
          </Text>
        </View>
      </View>
    );
  };
  

  return (
    <SafeAreaView style={styles.SafeAreaViewView}>
      <View style={styles.header}>
        <View style={styles.innerHeader}>
          <TouchableOpacity
            style={[
              styles.backIcon,
              { transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] },
            ]}
            onPress={() => navigation.goBack()}>
            <SvgXml xml={BackArrow} />
          </TouchableOpacity>

          {/* <Image style={styles.imgView}
                            resizeMode='repeat'
                            source={require('../assets/images/Group.png')} /> */}

          {/* <Image
              source={{
                uri: `data:image/jpeg;base64,${routeData?.chatInfo?.group_image}`,
              }}
              style={styles.imgView}
            /> */}

          {
            routeData?.chatInfo?.group_image &&
              routeData?.chatInfo?.group_image != '' ? (
              <Image
                source={{
                  uri: `data:image/jpeg;base64,${routeData?.chatInfo?.group_image}`,
                }}
                style={styles.imgView}
              />
            ) : (
              <View
                style={[
                  styles.imgView,
                  {
                    backgroundColor: COLORS.background,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 100,
                  },
                ]}>
                <Text style={FontStyle.Regular16_500M}>
                  {routeData?.chatInfo?.group_name?.[0]}
                </Text>
              </View>
            )
            // <Image source={require('../assets/images/Group.png')} />
          }

          <View style={styles.nameView}>
            <Text style={styles.headerText}>
              {routeData?.chatInfo?.group_name}
            </Text>
          </View>

          <TouchableOpacity
            style={styles.menuIcon}
            onPress={() => getChat(true)}
            disabled={refresh}>
            {/* <SvgXml xml={MenuDots} /> */}
            {refresh ? (
              <ActivityIndicator color={COLORS.primaryColor} size={22} />
            ) : (
              <ReactNativeVI
                color={COLORS.primaryColor}
                name={'refresh'}
                size={22}
                Lib={'FontAwesome'}
              />
            )}
          </TouchableOpacity>
        </View>
      </View>

      <FlatList
        data={messages?.slice()?.reverse()}
        contentContainerStyle={{ paddingVertical: 16 }}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        ref={flatListRef}
        inverted 

      />

      {/* <View style={styles.inputContainer}>
          <View style={styles.innerInputView}>
             ////////  <TouchableOpacity style={{ paddingRight: 16 }}
                    // onPress={() => navigation.goBack()}
                    >
                        <SvgXml xml={Smile} />
                    </TouchableOpacity> ////////////

            <View style={{flex: 1}}>
              <TextInput
                style={styles.inputText}
                placeholder={'Write a message...'}
                placeholderTextColor={'rgba(27, 26, 87, 1)'}
                cursorColor={COLORS.primaryColor}
                multiline={true}
                value={inputMessage}
                onChangeText={setInputMessage}
              />
            </View>

            //////// <TouchableOpacity style={{ paddingRight: 16 }} //27 in design
                    // onPress={() => navigation.goBack()}
                    >
                        <SvgXml xml={Attachments} />
                    </TouchableOpacity>

                    <TouchableOpacity
                    // onPress={() => sendMessages()}
                    >
                        <SvgXml xml={Microphone} />
                    </TouchableOpacity> ////////

            <TouchableOpacity
              style={{marginLeft: 16}}
              onPress={() => sendMessages()}>
              <ReactNativeVI
                color={COLORS.primaryColor}
                name={'send-sharp'}
                size={22}
                Lib={'Ionicons'}
              />
            </TouchableOpacity>
          </View>
        </View> */}
    </SafeAreaView>
  );
};

export default ConversationChat;
