import {StyleSheet} from 'react-native';
import {COLORS} from '../../../../theme/colors';
import {FontStyle} from '../../../../theme/FontStyle';
import {fonts} from '../../../../theme/fonts';

export const styles = StyleSheet.create({
  SafeAreaViewView: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  header: {
    borderBottomWidth: 1,
    borderColor: 'rgba(237, 237, 237, 1)',
    paddingHorizontal: 8,
    paddingVertical: 12,
    flexDirection: 'row',
    // paddingBottom: 22,
    // borderWidth:2,
  },

  innerHeader: {
    // borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },

  backIcon: {
    paddingVertical: 4,
    marginRight: 10,
    paddingRight: 8,
  },

  nameView: {
    borderWidth: 0,
    flex: 1,
    marginLeft: 14,
  },
  headerText: {
    ...FontStyle.Regular18,
    // fontSize: 18,
    fontWeight: '500',
    color: COLORS.primaryColor,
    fontFamily: fonts.medium,
    // lineHeight: 27,
  },

  imgView: {
    // height: 34,
    // width: 34,
    height: 48,
    width: 48,
    maxHeight: 48,
    maxWidth: 48,
    // borderRadius: 100,
    // borderWidth: 1,
  },

  menuIcon: {
    borderWidth: 0,
    paddingHorizontal: 10,
    paddingVertical: 4,
    marginLeft: 14,
  },

  /////////////////////////////////

  inputContainer: {
    borderTopWidth: 1,
    borderColor: 'rgba(237, 237, 237, 1)',

    backgroundColor: 'white',

    paddingHorizontal: 16,
    paddingTop: 16,
    height: 86,
  },
  innerInputView: {
    flex: 1,
    flexDirection: 'row',
  },

  inputText: {
    ...FontStyle.Regular16,
    flex: 1,
    padding: 0,
    margin: 0,
    textAlignVertical: 'top',
    color: COLORS.black,
    fontFamily: fonts.regular,
    fontWeight: '400',
  },

  /////////////////////////////////////////////////
  myMessage: {
    backgroundColor: 'rgba(84, 127, 231, 1)',
    paddingHorizontal: 10,
    paddingVertical: 8,
    marginVertical: 5,
    marginRight: 8,
    borderRadius: 8,
    borderBottomEndRadius: 0,
  },
  theirMessage: {
    backgroundColor: COLORS.background,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderRadius: 8,

    marginVertical: 5,
    marginLeft: 12,
  },

  myMessage1: {
    alignSelf: 'flex-end',
    maxWidth: '70%',
  },
  theirMessage1: {
    alignSelf: 'flex-start',
    maxWidth: '70%',
  },
  senderText: {
    ...FontStyle.Regular14_500,
    fontWeight: '500',
    marginBottom: 6,
  },
  messageText: {
    ...FontStyle.Regular16_500,
    fontWeight: '400',
  },
  timeText: {
    ...FontStyle.Regular12,
    color: 'rgba(161, 161, 188, 1)',
    // borderWidth:1,
  },
});
