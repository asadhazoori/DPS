import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { commonApi } from '../../../../utilities/api/apiController';
import { logout_user } from '../../../../redux/users/user.actions';
import { t } from 'i18next';
import { getChatGroups } from './getChatGroups';
import store from '../../../../redux/store';

export const MarkMessageAsRead = async ({
  navigation,
  groupId,
  dispatch,
  partnerId,
}) => {
  try {
    const { uid } = store.getState()?.signin;
    const body = {
      params: {
        model: 'chat.group.wags',
        method: 'mark_messages_as_read',
        args: [
          {
            user_id: uid,
            group_id: groupId,
          },
        ],
        kwargs: {},
      },
    };

    const response = await commonApi({ body, navigation });

    if (response?.data?.result) {
      await getChatGroups({ navigation, dispatch, partnerId });
    } else {
      if (response?.data?.error) {
        if (response?.data?.error?.message == 'Odoo Session Expired') {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        } else {
          setTimeout(() => {
            Alert.alert(
              response?.data?.error?.message,
              `Method: mark_messages_as_read\n${response?.data?.error?.data?.message}`,
            );
          }, 300);
        }
      } else if (
        response == 'AxiosError: Request failed with status code 404'
      ) {
        setTimeout(() => { Toast.show(t('session-expired')); }, 500);
        navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        });
        dispatch(logout_user(false));
      }
    }
  } catch (error) {
    console.log('MarkMessageAsRead', error);
  }
};
