import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { commonApi } from '../../../../utilities/api/apiController';
import { logout_user } from '../../../../redux/users/user.actions';
import { t } from 'i18next';
import { MarkMessageAsRead } from './MarkMessageAsRead';
import store from '../../../../redux/store';

export const getChatById = async ({
  navigation,
  groupId,
  dispatch,
  partnerId,
}) => {
  const { uid } = store.getState()?.signin;
  try {
    const body = {
      params: {
        model: 'chat.group.wags',
        method: 'get_chat_by_id',
        args: [
          {
            user_id: uid,
            chat_group_id: groupId,
          },
        ],
        kwargs: {},
      },
    };

    const response = await commonApi({ body, navigation });

    if (response?.data?.result) {
      MarkMessageAsRead({ navigation, groupId, dispatch, partnerId });
      const res = response?.data?.result;
      return res;
    } else {
      if (response?.data?.error) {
        if (response?.data?.error?.message == 'Odoo Session Expired') {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        } else {
          setTimeout(() => {
            Alert.alert(
              response?.data?.error?.message,
              `Method: get_chat_by_id\n${response?.data?.error?.data?.message}`,
            );
          }, 300);
        }
      } else if (
        response == 'AxiosError: Request failed with status code 404'
      ) {
        setTimeout(() => { Toast.show(t('session-expired')); }, 500);
        navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        });
        dispatch(logout_user(false));
      }
    }
  } catch (error) {
    console.log('getChatById', error);
  }
};
