import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { commonApi } from '../../../../utilities/api/apiController';
import { logout_user } from '../../../../redux/users/user.actions';
import { t } from 'i18next';

export const sendMessage = async ({
  navigation,
  partnerId,
  groupId,
  message,
  dispatch,
}) => {
  try {
    const body = {
      params: {
        model: 'chat.group.wags',
        method: 'create_message',
        args: [
          {
            sender_id: partnerId,
            chat_group_id: groupId,
            // "subject": "107777777",
            body: message,
          },
        ],
        kwargs: {},
      },
    };

    const response = await commonApi({ body, navigation });
    // console.log(response?.data);

    if (response?.data?.result) {
      // const res = response?.data?.result;
    } else {
      if (response?.data?.error) {
        if (response?.data?.error?.message == 'Odoo Session Expired') {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        } else {
          setTimeout(() => {
            Alert.alert(
              response?.data?.error?.message,
              `Method: create_message\n${response?.data?.error?.data?.message}`,
            );
          }, 300);
        }
      } else if (
        response == 'AxiosError: Request failed with status code 404'
      ) {
        setTimeout(() => { Toast.show(t('session-expired')); }, 500);
        navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        });
        dispatch(logout_user(false));
      }
    }
  } catch (error) {
    console.log('getChatGroups', error);
  }
};
