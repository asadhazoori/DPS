import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';
import { commonApi } from '../../../../utilities/api/apiController';
import { t } from 'i18next';
import { logout_user } from '../../../../redux/users/user.actions';
import { setChatGroups } from '../chat.action';
import store from '../../../../redux/store';

export const getChatGroups = async ({ navigation, dispatch }) => {
  try {
    const { uid } = store.getState()?.signin;
    const body = {
      params: {
        model: 'chat.group.wags',
        method: 'get_chat_groups',
        args: [uid],
        kwargs: {},
      },
    };

    const response = await commonApi({ body, navigation });

    if (response?.data?.result?.chat_groups) {
      const res = response?.data?.result?.chat_groups;
      let totalUnreadCount = 0;

      res?.forEach(item => {
        totalUnreadCount += item.unread_count;
      });

      res?.sort(
        (a, b) =>
          new Date(b?.message_create_date) - new Date(a?.message_create_date),
      );

      dispatch(setChatGroups(res, totalUnreadCount));
    } else {
      if (response?.data?.error) {
        if (response?.data?.error?.message == 'Odoo Session Expired') {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        } else {
          setTimeout(() => {
            Alert.alert(
              response?.data?.error?.message,
              `Method: get_chat_groups\n${response?.data?.error?.data?.message}`,
            );
          }, 300);
        }
      } else if (
        response == 'AxiosError: Request failed with status code 404'
      ) {
        setTimeout(() => { Toast.show(t('session-expired')); }, 500);
        navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        });
        dispatch(logout_user(false));
      }
    }
  } catch (error) {
    console.log('getChatGroups', error);
  }
};
