import {
  SET_CHAT_GROUPS,
  SET_EMPLOYEE_WISE_LOAN_REQ,
  SET_MANAGER_WISE_LOAN_REQ,
} from './chat.types';

const initialState = {
  chatGroups: [],
  totalUnreadCount: 0,
};

export const ChatReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CHAT_GROUPS: {
      return {
        ...state,
        chatGroups: action.payload.data,
        totalUnreadCount: action.payload.totalUnreadCount,
      };
    }

    default:
      return state;
  }
};
