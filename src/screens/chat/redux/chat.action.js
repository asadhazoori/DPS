import {SET_CHAT_GROUPS} from './chat.types';

export const setChatGroups = (data, totalUnreadCount) => ({
  type: SET_CHAT_GROUPS,
  payload: {data: data, totalUnreadCount: totalUnreadCount},
});
