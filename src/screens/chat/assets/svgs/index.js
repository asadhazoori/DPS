import { Attachments } from "./Attachments"
import { BackArrow } from "./BackArrow"
import { MenuDots } from "./MenuDots"
import { Microphone } from "./Microphone"
import { SearchIcon } from "./SearchIcon"
import { Smile } from "./Smile"

export {
    Smile,
    BackArrow,
    MenuDots,
    SearchIcon,
    Attachments,
    Microphone,
}