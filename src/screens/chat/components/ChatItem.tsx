import {
  Image,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {COLORS} from '../../../theme/colors';
import {fonts} from '../../../theme/fonts';
import {FontStyle} from '../../../theme/FontStyle';

const ChatItem = ({item, navigation}) => {
  const formatMessageDate = (message_create_date: any) => {
    if (!message_create_date) return '';

    const now = new Date();
    const messageDate = new Date(message_create_date.replace(' ', 'T') + 'Z');

    const isToday = now.toDateString() === messageDate.toDateString();

    const yesterday = new Date(now);
    yesterday.setDate(now.getDate() - 1);
    const isYesterday = yesterday.toDateString() === messageDate.toDateString();

    if (isToday) {
      return messageDate.toLocaleTimeString('en-US', {
        hour: 'numeric',
        minute: 'numeric',
        hour12: true,
      });
    } else if (isYesterday) {
      return 'Yesterday';
    } else {
      return messageDate.toLocaleDateString('en-GB');
    }
  };

  const timeOnly = formatMessageDate(item?.message_create_date);
  const message = item?.message_body.replace(/\n\n/g, '');

  return (
    <View style={styles.container}>
      <Pressable
        style={({pressed}) => [
          styles.innerContainer,
          {
            backgroundColor: item?.noOfMessages
              ? 'rgba(47, 128, 237, 0.1)'
              : 'transparent',
          },
          pressed && {
            opacity: 0.8,
            backgroundColor: 'rgba(47, 128, 237, 0.1)',
          },
        ]}
        onPress={() =>
          navigation.navigate('ConversationChat', {
            chatInfo: item,
          })
        }>
        <View style={styles.imgView}>
          {
            item?.group_image && item?.group_image != '' ? (
              <Image
                source={{uri: `data:image/jpeg;base64,${item?.group_image}`}}
                resizeMode="repeat"
                style={[{flex: 1}]}
              />
            ) : (
              <View
                style={[
                  styles.imgView,
                  {
                    backgroundColor: COLORS.background,
                    justifyContent: 'center',
                    alignItems: 'center',
                  },
                ]}>
                <Text style={FontStyle.Regular16_500M}>
                  {item?.group_name?.[0]}
                </Text>
              </View>
            )
            // <Image source={require('../assets/images/Group.png')} />
          }
        </View>
        <View style={styles.nameView}>
          <Text numberOfLines={1} style={styles.nameText}>
            {item.group_name}
          </Text>
          <Text numberOfLines={1} style={styles.messagetext}>
            {message}
          </Text>
        </View>
        <View style={styles.statusView}>
          <Text style={[styles.messagetext, {marginTop: 0, color: '#333333'}]}>
            {timeOnly}
          </Text>
          {item?.unread_count > 0 && (
            <View style={styles.notificationView}>
              <Text
                style={[
                  styles.notificationNo,
                  {paddingHorizontal: item?.unread_count > 999 ? 6 : 0},
                ]}>
                {item?.unread_count}
              </Text>
            </View>
          )}
        </View>
      </Pressable>
    </View>
  );
};

export default ChatItem;

const styles = StyleSheet.create({
  container: {
    // borderWidth: 1,
  },
  innerContainer: {
    borderColor: 'pink',
    marginHorizontal: 8,
    padding: 8,
    flexDirection: 'row',
    borderRadius: 8,
    // backgroundColor: 'rgba(47, 128, 237, 0.1)',
    // borderWidth: 1,
  },
  imgView: {
    height: 48,
    width: 48,
    maxHeight: 48,
    maxWidth: 48,
    borderRadius: 100,
  },
  nameView: {
    marginLeft: 16,
    marginRight: 8,
    flex: 1,
    marginTop: -3,
    alignItems: 'flex-start',
  },
  nameText: {
    ...FontStyle.Regular16_500,
    color: COLORS.primaryColor,
    fontFamily: fonts.medium,
  },
  messagetext: {
    marginTop: 6,
    ...FontStyle.Regular14_500,
    fontWeight: '400',
    color: '#4F5E7B',
    fontFamily: fonts.regular,
  },
  notificationNo: {
    fontWeight: '700',
    fontSize: 12,
    color: '#FFFFFF',
    fontFamily: fonts.regular,
  },
  statusView: {
    alignItems: 'flex-end',
  },
  notificationView: {
    marginTop: 6,
    backgroundColor: '#547FE7',
    height: 26,
    minWidth: 26,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
