import {StyleSheet} from 'react-native';
import Theme from '../../../theme/theme';
import {COLORS} from '../../../theme/colors';

export const styles = StyleSheet.create({
  container: {
    paddingBottom: 16,
    paddingHorizontal: 16,
    gap: 12,
  },

  bottomView: {
    marginTop: 12,
    marginHorizontal: 4,
  },

  alertIconView: {
    backgroundColor: '#F73C3C3D',
    height: 48,
    width: 48,
    borderRadius: 48,
    justifyContent: 'center',
    alignItems: 'center',
  },

  inputStyle: {
    borderWidth: 1,
    height: 50,
    marginTop: 4,
    justifyContent: 'center',
    paddingHorizontal: 10,
    alignItems: 'flex-start',
  },

  addImage: {
    ...Theme.Shadow,
    marginTop: 4,
    padding: 8,
    backgroundColor: 'white',
    borderRadius: 8,
    marginHorizontal: 4,
    borderColor: COLORS.primaryColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconView: {
    position: 'absolute',
    right: -10,
    top: -10,
    padding: 4,
    backgroundColor: COLORS.backgroundInput,
    borderRadius: 100,
  },
  imgView: {
    marginTop: 4,
    marginHorizontal: 4,
    borderColor: COLORS.primaryColor,
    width: 75,
    height: 75,
    borderRadius: 8,
    // overflow: 'hidden',
  },

  checkBox: {
    gap: 16,
    flexDirection: 'row-reverse',
    alignItems: 'center',
    alignSelf: 'flex-start',
    borderColor: COLORS.black,
    // marginTop: 10
  },

  checkBoxContainer: {
    marginLeft: 6,
    gap: 6,
  }

});
