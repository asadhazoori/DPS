import React, { useState } from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import inputValidation from '../../../utilities/Validations/YupValidate';
import { commonApi } from '../../../utilities/api/apiController';
import { LeavesRequestSchema } from '../../../utilities/Validations';
import { useDispatch, useSelector } from 'react-redux';
import { styles } from './styles';
import { CheckBox, NextButton, TextInputField } from '../../../components/Inputs';
import YearPicker from '../../../components/Inputs/YearPicker';
import {
  getFormattedDate,
  getFormattedTime,
} from '../../../utilities/helpers/CurretDate';
import { useTranslation } from 'react-i18next';
import { COLORS } from '../../../theme/colors';
import { FontStyle } from '../../../theme/FontStyle';
import { logout_user } from '../../../redux/users/user.actions';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import { AlertModal } from '../../../components/Modals';
import { SvgXml } from 'react-native-svg';
import { DrawerIcons } from '../../../assets/SvgIcons/DrawerIcons';
import DatePickerNew from '../../../components/DateTimePicker/DatePickerNew';
import GenericModal from '../../../components/Modals/GenericModal';
import DropdownComponent from '../../../components/Inputs/ElementDropdown/ElementDropdown';
import TimePickerLatest from '../../../components/DateTimePicker/TimePickerLatest';
import ImagePicker from '../../../components/Helpers/ImagePicker';
import { Text } from 'react-native-paper';
import Theme from '../../../theme/theme';
import ReactNativeVI from '../../../components/Helpers/ReactNativeVI';

const ApplyLeave = ({
  navigation,
  modalVisible1,
  setModalVisible1,
  getLeaves,
}) => {
  const { t } = useTranslation();
  const leaveTypes = useSelector(state => state?.employeeProfile?.leave_types);
  const { employeeID, name, data } = useSelector(state => state?.employeeProfile);
  const [loading, setLoading] = useState(false);
  const [showStartDatePicker, setShowStartDatePicker] = useState(false);
  const [showEndDatePicker, setShowEndDatePicker] = useState(false);
  const [numberOfDays, setNumberOfDays] = useState(0);
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [showTimePicker1, setShowTimePicker1] = useState(false);
  const [modalVisible3, setModalVisible3] = useState(false);

  const dispatch = useDispatch();

  const [alertBox, setAlertBox] = useState({
    showBox: false,
    title: null,
    message: null,
    confirmbtn: null,
    okbtn: null,
  });

  const [inputs, setInputs] = useState({
    name: name,
    holidayType: null,
    type: null,
    startDate: null,
    endDate: null,
    leaveTime: null,
    returnTime: null,
    isAdvaneSalary: false,
    isFlightTicket: false,
    isExitReEntry: false,
    image: null,
    reason: null,
  });

  const closeCustomAlert = () => {
    setTimeout(() => {
      setAlertBox({ showBox: false });
    }, 300);
  };

  const handleInputChange = (field, value) => {
    setInputs({
      ...inputs,
      [field]: value,
      errors: {
        ...inputs.errors,
        [field]: false,
      },
    });
  };

  const handleDateChange = (selectedDate, field) => {
    const dateObject = new Date(selectedDate);
    const formattedDate = dateObject.toISOString().split('T')[0];

    if (field === 'startDate') {
      setShowStartDatePicker(false);

      if (inputs.endDate) {
        const startDate = moment(formattedDate, 'YYYY-MM-DD');
        const endDate = moment(inputs.endDate, 'YYYY-MM-DD');
        const diffDays = endDate.diff(startDate, 'days') + 1;
        setNumberOfDays(diffDays);
      }
    } else if (field === 'endDate') {
      setShowEndDatePicker(false);

      if (inputs.startDate) {
        const startDate = moment(inputs.startDate, 'YYYY-MM-DD');
        const endDate = moment(formattedDate, 'YYYY-MM-DD');
        const diffDays = endDate.diff(startDate, 'days') + 1;
        setNumberOfDays(diffDays);
      }
    }
    handleInputChange(field, formattedDate);
  };

  const validate = async () => {
    const schema = LeavesRequestSchema(t);
    const result = await inputValidation(schema, inputs);

    if (result.isValidate) {
      handleSubmit();
    } else {
      setInputs(prev => ({
        ...prev,
        errors: result?.err,
      }));
    }
  };

  function calculateNoOfDays(leaveTime, returnTime = null) {
    const leaveDate = new Date(leaveTime);
    const endDate = returnTime ? new Date(returnTime) : new Date();
    const diffInMs = endDate - leaveDate;
    if (diffInMs < 0) return 0;
    const diffInHours = diffInMs / (1000 * 60 * 60);
    const noOfDays = Math.min(diffInHours / 24, 1);
    console.log(
      '<<<<<<<<<first>>>>>>>>>',
      leaveTime,
      returnTime,
      parseFloat(noOfDays.toFixed(2)),
    );
    return parseFloat(noOfDays.toFixed(2));
  }

  const handleSubmit = async () => {
    setLoading(true);

    try {
      const body = {
        params: {
          method: 'create_leave_request',
          model: 'hr.holidays.wags',
          args: [
            {
              employee_id: employeeID,
              leaves_type: inputs.holidayType?.id,
              date_from: inputs?.startDate,
              ...(inputs.holidayType?.is_short_leave && {
                is_short_leave: true,
                outtime: inputs?.floatLeaveTime,
                intime: inputs?.floatReturnTime,
                date_to: inputs?.startDate,
                is_advance_salary: inputs.isAdvaneSalary,
                is_flight_ticket: inputs.isFlightTicket,
                is_exit_re_entry: inputs.isExitReEntry,
                number_of_days: calculateNoOfDays(
                  inputs?.leaveTime,
                  inputs?.returnTime,
                ),
              }),
              ...(inputs.holidayType?.annual_leave && {
                date_to: inputs?.startDate,
                is_advance_salary: inputs.isAdvaneSalary,
                is_flight_ticket: inputs.isFlightTicket,
                is_exit_re_entry: inputs.isExitReEntry,
                number_of_days: calculateNoOfDays(
                  inputs?.leaveTime,
                  inputs?.returnTime,
                ),
              }),

              ...(!inputs.holidayType?.is_short_leave && {
                date_to: inputs?.endDate,
                number_of_days: numberOfDays,
              }),
              name: inputs?.reason,
              ...(inputs.image && { image: inputs.image }),
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });
      // const response = {};

      if (response?.data?.result) {
        if (response?.data?.result?.id) {
          setLoading(false);
          setModalVisible1(false);
          setInputs({
            holidayType: null,
            type: null,
            startDate: null,
            endDate: null,
            image: null,
            reason: null,
          });
          setNumberOfDays(0);
          getLeaves();
          setTimeout(() => { Toast.show(`${t('leave-submitted-successfully')} !`); }, 300);
        } else if (response?.data?.result?.error) {
          setLoading(false);

          if (response?.data?.result?.error == 'Leave allocation not found') {
            setTimeout(() => {
              setAlertBox({
                showBox: true,
                title: `${response?.data?.result?.error}`,
                message:
                  'Sorry, you are unable to request leave as there are no allocated leaves for this type.',
              });
            }, 300);
          } else {
            setTimeout(() => {
              setAlertBox({
                showBox: true,
                title: 'Validation Error',
                message: `${response?.data?.result?.error}`,
              });
            }, 300);
          }
        }
      } else {
        setLoading(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              setAlertBox({
                showBox: true,
                title: `${'Validation Error'}`,
                message: `${response?.data?.error?.data?.message}`,
              });
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleTimeChange = (field, field1, time) => {
    if (field == 'leaveTime') {
      setShowTimePicker(false);
    } else {
      setShowTimePicker1(false);
    }

    const forTime = moment(time).utc().format('YYYY-MM-DD HH:mm:ss');
    // const now = moment(time);
    // const utcDateTime = now.utc().format('YYYY-MM-DD HH:mm:ss');

    const timeee = getFormattedTime(forTime);
    const [time1, period] = timeee.split(' ');
    let [hours, minutes] = time1.split(':').map(Number);

    if (period === 'PM' && hours !== 12) {
      hours += 12;
    } else if (period === 'AM' && hours === 12) {
      hours = 0;
    }

    const floatTime = hours + minutes / 60;
    const final = parseFloat(floatTime.toFixed(2));

    setInputs({
      ...inputs,
      [field]: forTime,
      [field1]: final,
      errors: {
        ...inputs.errors,
        [field]: false,
      },
    });
  };

  const handleCheckBoxPress = (key) => {

    setInputs((prevInputs) => ({
      ...prevInputs,
      [key]: !prevInputs[key],
    }));

  };

  return (
    <GenericModal
      modalVisible={modalVisible1}
      setModalVisible={setModalVisible1}
      heading={t('apply-leave')}>
      <View style={styles.container}>
        <View style={{ zIndex: 9999 }}>
          <DropdownComponent
            label={t('leave-type')}
            data={leaveTypes}
            value={inputs?.holidayType}
            placeholder={t('select-leave-type')}
            error={inputs?.errors?.holidayType}
            onChange={selectedType =>
              handleInputChange('holidayType', selectedType)
            }
            fullItemReq={true}
          />
        </View>

        <DatePickerNew
          mode="date"
          date={inputs.startDate ? new Date(inputs.startDate) : new Date()}
          value={inputs.startDate && getFormattedDate(inputs.startDate)}
          label={
            inputs.holidayType?.is_short_leave ? t('date') : t('start-date')
          }
          onChange={selectedDate => handleDateChange(selectedDate, 'startDate')}
          showDatePicker={showStartDatePicker}
          setShowDatePicker={setShowStartDatePicker}
          placeholder={t('start-date')}
          error={inputs?.errors?.startDate}
          minimumDate={
            data?.leave_past_days_limit
              ? new Date(new Date().setDate(new Date().getDate() - data?.leave_past_days_limit))
              : new Date()
          }
        />

        {!inputs.holidayType?.is_short_leave && (
          <DatePickerNew
            mode="date"
            date={inputs.endDate ? new Date(inputs.endDate) : new Date()}
            label={t('end-date')}
            value={inputs?.endDate && getFormattedDate(inputs.endDate)}
            onChange={selectedDate => handleDateChange(selectedDate, 'endDate')}
            showDatePicker={showEndDatePicker}
            setShowDatePicker={setShowEndDatePicker}
            placeholder={t('end-date')}
            error={inputs?.errors?.endDate}
            minimumDate={
              data?.leave_past_days_limit
                ? new Date(new Date().setDate(new Date().getDate() - data?.leave_past_days_limit))
                : new Date()
            }
          />
        )}
        {!inputs.holidayType?.is_short_leave && (
          <TextInputField
            label={t('no-of-days')}
            editable={false}
            value={`${numberOfDays}`}
            labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
          />
        )}

        {inputs.holidayType?.is_short_leave && (
          <TimePickerLatest
            icon
            label={t('leave-time')}
            showTimePicker={showTimePicker}
            setShowTimePicker={setShowTimePicker}
            labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
            initialValue={
              inputs?.leaveTime
                ? // ? moment.utc(new Date(inputs?.leaveTime))
                new Date(moment.utc(inputs?.leaveTime))
                : new Date()
            }
            color={inputs?.leaveTime ? COLORS.primaryColor : COLORS.grey3}
            value={
              inputs?.leaveTime
                ? getFormattedTime(inputs?.leaveTime)
                : t('time')
            }
            onChange={time =>
              handleTimeChange('leaveTime', 'floatLeaveTime', time)
            }
            error={inputs.errors?.leaveTime}
          />
        )}

        {inputs.holidayType?.is_short_leave && (
          <TimePickerLatest
            icon
            label={t('return-time')}
            showTimePicker={showTimePicker1}
            setShowTimePicker={setShowTimePicker1}
            labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
            // initialValue={new Date()}
            initialValue={
              inputs?.returnTime
                ? new Date(moment.utc(inputs?.returnTime))
                : inputs?.leaveTime
                  ? new Date(moment.utc(inputs?.leaveTime))
                  : new Date()
            }
            color={inputs?.returnTime ? COLORS.primaryColor : COLORS.grey3}
            value={
              inputs?.returnTime
                ? getFormattedTime(inputs?.returnTime)
                : t('time')
            }
            onChange={time =>
              handleTimeChange('returnTime', 'floatReturnTime', time)
            }
            error={inputs.errors?.returnTime}
          />
        )}

        <TextInputField
          label={t('purpose')}
          placeholder={t('purpose-plcholder')}
          value={inputs.reason}
          error={inputs?.errors?.reason}
          onChangeText={text => handleInputChange('reason', text)}
          labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
        />

        {
          inputs.holidayType?.annual_leave && (
            <View style={styles.checkBoxContainer}>

              <CheckBox
                title={t("is-advance-salary")}
                container={styles.checkBox}
                status={inputs.isAdvaneSalary}
                onPress={() => {
                  handleCheckBoxPress("isAdvaneSalary")

                }} />

              <CheckBox
                title={t("is-flight-ticket")}
                container={styles.checkBox}
                status={inputs.isFlightTicket}
                onPress={() => {
                  handleCheckBoxPress("isFlightTicket")

                }} />

              <CheckBox
                title={t("is-exit-re-entry")}
                container={styles.checkBox}
                status={inputs.isExitReEntry}
                onPress={() => {
                  handleCheckBoxPress("isExitReEntry")
                }} />

            </View>
          )}


        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.addImage}
          onPress={() => setModalVisible3(true)}>
          <Text style={[FontStyle.Regular14_500M]}>{t('add-image')}</Text>
        </TouchableOpacity>

        {inputs?.image && (
          <View style={styles.imgView}>
            <Image
              source={{
                uri: `data:image/jpeg;base64,${inputs?.image}`,
              }}
              style={Theme.flex1}
            />
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => handleInputChange('image', null)}
              style={styles.iconView}>
              <ReactNativeVI
                Lib={'Ionicons'}
                name={'close'}
                color={COLORS.primaryColor}
                size={20}
              />
            </TouchableOpacity>
          </View>
        )}

        <View style={styles.bottomView}>
          <NextButton title={t('submit')} onPress={validate} loader={loading} />
        </View>
      </View>

      <ImagePicker
        modalVisible={modalVisible3}
        setModalVisible={setModalVisible3}
        onImageSelected={imagebase64 => handleInputChange('image', imagebase64)}
      />
      <AlertModal
        visible={alertBox.showBox}
        icon={
          <View style={styles.alertIconView}>
            <SvgXml xml={DrawerIcons.leaves1} />
          </View>
        }
        title={alertBox.title}
        message={alertBox.message}
        onClose={closeCustomAlert}
        confirmbtn={alertBox.confirmbtn}
      />
    </GenericModal>
  );
};

export default ApplyLeave;
