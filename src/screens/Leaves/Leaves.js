import {
  View,
  SafeAreaView,
  StyleSheet,
  FlatList,
  Text,
  Dimensions,
  RefreshControl,
  Platform,
  I18nManager,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import GeneralHeader from '../../components/Headers/GeneralHeader';
import Theme from '../../theme/theme';
import {useTranslation} from 'react-i18next';
import LeaveBalanceCard from '../../components/Cards/LeaveBalanceCard';
import LeaveRequestCard from '../../components/Cards/LeaveRequestCard';
import SmartButtonTrans from '../../components/Buttons/SmartButtonTrans';
import {Icons} from '../../assets/SvgIcons/Icons';
import {FontStyle} from '../../theme/FontStyle';
import {COLORS} from '../../theme/colors';
import Carousel from 'react-native-reanimated-carousel';
import {getEmpWiseLeaves} from '../../redux/leaves/actions/getEmpWiseLeaves';
import {useDispatch, useSelector} from 'react-redux';
import {getMangrWiseLeaves} from '../../redux/leaves/actions/getMangrWiseLeaves';
import {getAllocatedLeavesCount} from '../../redux/leaves/actions/getAllocatedLeavesCount';
import ApplyLeave from './ApplyLeave/ApplyLeave';

export const window = Dimensions.get('window');

const PAGE_WIDTH = window.width;

const baseOptions = {
  vertical: false,
  width: PAGE_WIDTH,
  height: null,
};

const Leaves = ({navigation}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const {employeeID, leaveCards, isManager} = useSelector(
    state => state?.employeeProfile,
  );
  const [selectedCategoryId, setSelectedCategoryId] = useState(11111);
  const [leaves_requested, setLeaves_requested] = useState([]);
  const [modalVisible1, setModalVisible1] = useState(false);
  const [employeeLeaves, setEmployeeLeaves] = useState([]);
  const [managersData, setManagersData] = useState(false);
  const [filteredData2, setFilteredData2] = useState([]);
  const [refresh, setRefresh] = useState(false);

  const getData = async category => {
    try {
      setRefresh(true);

      await dispatch(getAllocatedLeavesCount({navigation}));

      if (category === 'emp') {
        await getEmpWiseLeaves(navigation, employeeID, dispatch)
          .then(result => {
            setLeaves_requested(result);
            if (selectedCategoryId) {
              if (selectedCategoryId == 11111) {
                setFilteredData2(result);
              } else if (selectedCategoryId == 99999) {
                setFilteredData2(employeeLeaves);
              } else {
                const matchedData = result.filter(
                  item => item.leave_type_id == selectedCategoryId,
                );
                setFilteredData2(matchedData);
              }
            } else {
              setSelectedCategoryId(11111);
              setFilteredData2(result);
            }
          })
          .catch(() => {});
      } else if (category === 'man') {
        setManagersData(true);
        await getMangrWiseLeaves(navigation, employeeID, dispatch)
          .then(result => {
            setEmployeeLeaves(result);
            setFilteredData2(result);
          })
          .catch(() => {});
      } else {
        await getEmpWiseLeaves(navigation, employeeID, dispatch)
          .then(async result => {
            setLeaves_requested(result);
            if (isManager) {
              await getMangrWiseLeaves(navigation, employeeID, dispatch)
                .then(result1 => {
                  setEmployeeLeaves(result1);
                  if (selectedCategoryId == 11111) {
                    setFilteredData2(result);
                  } else if (selectedCategoryId == 99999) {
                    setFilteredData2(result1);
                  } else {
                    const matchedData = result.filter(
                      item => item.leave_type_id == selectedCategoryId,
                    );
                    setFilteredData2(matchedData);
                  }
                })
                .catch(() => {});
            }
          })
          .catch(() => {});
      }

      setRefresh(false);
    } catch (error) {
      setRefresh(false);
      console.log('Error while calling getAllEmployeeLoans', error);
    }
  };

  const handleButtonPress = selected => {
    if (selected?.isManager) {
      setSelectedCategoryId(selected.leave_type_id);
      setFilteredData2(employeeLeaves);
    } else if (selected?.leave_type_id == 11111) {
      setSelectedCategoryId(selected.leave_type_id);
      setFilteredData2(leaves_requested);
    } else {
      setSelectedCategoryId(selected.leave_type_id);
      const matchedData = leaves_requested.filter(
        item => item.leave_type_id == selected.leave_type_id,
      );
      setFilteredData2(matchedData);
    }
  };

  useEffect(() => {
    getData('emp');
  }, []);

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader title={t('leaves')} navigation={navigation} />
      <View style={styles.container}>
        <View style={styles.addNewButtonView}>
          <SmartButtonTrans
            title={t('add-new')}
            rightIcon={Icons.plus}
            onPress={() => setModalVisible1(true)}
          />
        </View>

        {leaveCards?.length > 0 && (
          <View
            style={{
              height: Platform.OS == 'ios' && I18nManager.isRTL ? 150 : 120,
            }}>
            <Carousel
              loop={false}
              {...baseOptions}
              style={{
                width: PAGE_WIDTH,
              }}
              mode="parallax"
              modeConfig={{
                parallaxScrollingScale: 0.9,
                parallaxScrollingOffset: 110,
              }}
              data={leaveCards}
              onSnapToItem={async index => {
                const selected = leaveCards[index];
                if (selected?.isManager) {
                  setSelectedCategoryId(selected.leave_type_id);
                  setFilteredData2(employeeLeaves);
                  if (!managersData) {
                    await getData('man');
                  }
                } else if (selected.leave_type_id == 11111) {
                  setSelectedCategoryId(selected.leave_type_id);
                  setFilteredData2(leaves_requested);
                } else {
                  setSelectedCategoryId(selected.leave_type_id);
                  const matchedData = leaves_requested.filter(
                    item => item.leave_type_id == selected.leave_type_id,
                  );
                  setFilteredData2(matchedData);
                }
              }}
              renderItem={({item, index}) => {
                return (
                  <LeaveBalanceCard
                    item={item}
                    index={index}
                    selectedCategoryId={selectedCategoryId}
                    setSelectedCategoryId={handleButtonPress}
                  />
                );
              }}
            />
          </View>
        )}

        <View style={styles.body}>
          <FlatList
            data={filteredData2}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={() => {
                  if (selectedCategoryId == 99999) {
                    getData('man');
                  } else {
                    getData('emp');
                  }
                }}
                tintColor={COLORS.primaryColor}
                colors={[COLORS.primaryColor]}
              />
            }
            removeClippedSubviews={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.flatListView}
            renderItem={({item, index}) => (
              <LeaveRequestCard
                key={index}
                data={item}
                navigation={navigation}
                employeeLeaves={selectedCategoryId == 99999 ? true : false}
                getEmployeeLeaves={getData}
              />
            )}
            keyExtractor={(item, index) => index}
            ItemSeparatorComponent={() => <View style={{height: 12}} />}
         
            ListEmptyComponent={() => (
                <>
                  {filteredData2?.length < 1 && !refresh && (
                    <Text style={[styles.dateText, {alignSelf: 'center'}]}>
                      {t('no-record-found')}
                    </Text>
                  )}
                </>
              )}
          />
        </View>

        <ApplyLeave
          navigation={navigation}
          modalVisible1={modalVisible1}
          setModalVisible1={setModalVisible1}
          getLeaves={getData}
        />
      </View>
    </SafeAreaView>
  );
};

export default Leaves;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: 'red',
  },

  body: {
    flex: 1,
    marginHorizontal: 16,
  },

  flatListView: {
    paddingBottom: 16,
    flexGrow: 1,
  },

  dateText: {
    ...FontStyle.Regular14_500,
    marginTop: 12,
    fontWeight: '500',
    color: COLORS.grey5,
  },

  addNewButtonView: {
    alignItems: 'flex-end',
    marginVertical: 16,
    marginBottom: 16,
    marginRight: 16,
  },
});
