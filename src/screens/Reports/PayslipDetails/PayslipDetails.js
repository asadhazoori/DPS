import { SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import Theme from '../../../theme/theme';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import { useTranslation } from 'react-i18next';
import { AlertModal, Loader } from '../../../components/Modals';
import { COLORS } from '../../../theme/colors';
import { Icons } from '../../../assets/SvgIcons/Icons';
import { GetPaySlipAPI } from '../../../utilities/helpers/GetPaySlipAPI';
import { useDispatch } from 'react-redux';
import { FontStyle } from '../../../theme/FontStyle';
import { SmartButton } from '../../../components/Inputs';
import { logout_user } from '../../../redux/users/user.actions';
import { commonApi } from '../../../utilities/api/apiController';
import Toast from 'react-native-simple-toast';
import { SvgXml } from 'react-native-svg';
import { downloadPdf } from '../../../utilities/helpers/DownloadPdf';
import PayslipReportModal from '../PayslipReport/PayslipReportModal';

const PayslipDetails = ({ navigation, route }) => {
  const { payslipID, date, month } = route?.params;
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);
  const [payslipdata, setpayslipData] = useState(null);
  const [base64, setBase64] = useState(null);
  const dispatch = useDispatch();
  const [showReportModal, setShowReportModal] = useState(false);

  const [alertBox, setAlertBox] = useState({
    showBox: false,
    title: null,
    message: null,
  });

  const closeCustomAlert = () => {
    setTimeout(() => {
      setAlertBox({ showBox: false });
    }, 300);
  };

  const downloadReport = async bs64 => {
    if (bs64) {
      await downloadPdf(bs64, `WagsHR Payslip (${month})`, setLoading);
    } else {
      await downloadPdf(base64, `WagsHR Payslip (${month})`, setLoading);
    }
  };

  const getPdfReport = async download => {
    if (base64) {
      download ? downloadReport() : setShowReportModal(true);
    } else {
      setLoading(true);
      const res = await GetPaySlipAPI(
        payslipID,
        setLoading,
        navigation,
        dispatch,
        month,
      );
      if (res?.b64_pdf) {
        setBase64(res?.b64_pdf);
        download ? downloadReport(res?.b64_pdf) : setShowReportModal(true);
        setLoading(false);
      } else {
        setLoading(false);
        setTimeout(() => {
          setAlertBox({
            showBox: true,
            title: `${t('pdf-not-found')}`,
            message: res,
          });
        }, 500);
      }
    }
  };

  const getpaySlip = async () => {
    try {
      const body = {
        params: {
          model: 'hr.payslip',
          method: 'get_payslip_details',
          args: [payslipID],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });
      setLoading(false);

      if (response?.data?.result) {
        if (response?.data?.result?.length > 0) {
          setpayslipData(response?.data?.result);
        } else {
          setTimeout(() => {
            setAlertBox({
              showBox: true,
              title: `${t('slip-not-found')}`,
              message: `${t('slip-not-found-msg')}`,
            });
          }, 500);
        }
      } else if (response?.data?.error) {
        if (response?.data?.error?.message == 'Odoo Session Expired') {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        } else {
          setTimeout(() => {
            setAlertBox({
              showBox: true,
              title: `${response?.data?.error?.message}`,
              message: `${response?.data?.error?.data?.message}`,
              confirmbtn: false,
            });
          }, 500);
        }
      } else if (
        response == 'AxiosError: Request failed with status code 404'
      ) {
        setTimeout(() => { Toast.show(t('session-expired')); }, 500);
        navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        });
        dispatch(logout_user(false));
      }
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    getpaySlip();
  }, []);

  const Item = ({ title, value }) => (
    <View style={{ flexDirection: 'row', marginVertical: 6 }}>
      <View style={{ flex: 1 }}>
        <Text style={[FontStyle.Regular14_500, { color: COLORS.grey5 }]}>
          {title}
        </Text>
      </View>
      <View style={{ flex: 1, alignItems: 'flex-end' }}>
        <Text
          style={[
            FontStyle.Regular14_500,
            { fontWeight: '500', color: COLORS.darkBlack },
          ]}>
          {value}
        </Text>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader
        title={`${month} ${t('payslip')}`}
        navigation={navigation}
      />
      <View style={styles.container}>
        <View style={{ alignItems: 'flex-start' }}>
          <Text style={styles.dateText}>{date}</Text>
        </View>

        <ScrollView showsVerticalScrollIndicator={false}>
          {payslipdata && (
            <View style={{ flex: 1 }}>
              <View style={styles.detailsView}>
                {payslipdata?.map((item, index) => (
                  <Item key={index} title={item.name} value={item.amount} />
                ))}
              </View>
              <View
                style={{
                  marginVertical: 16,
                  justifyContent: 'flex-end',
                  flexDirection: 'row',
                  gap: 12,
                }}>
                <SmartButton
                  title={t('open-pdf-report')}
                  handlePress={() => {
                    getPdfReport(false);
                  }}
                />

                <SmartButton
                  title={t('download-pdf')}
                  handlePress={() => {
                    getPdfReport(true);
                  }}
                />
              </View>
            </View>
          )}

          <Loader loading={loading} title={t('loading')} />
          <AlertModal
            visible={alertBox.showBox}
            icon={
              <View style={styles.iconView}>
                <SvgXml xml={Icons.payslip} />
              </View>
            }
            title={alertBox.title}
            message={alertBox.message}
            onClose={closeCustomAlert}
          />

          <PayslipReportModal
            modalVisible={showReportModal}
            setModalVisible={setShowReportModal}
            base64={base64}
          />
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default PayslipDetails;

const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    marginHorizontal: 16,
    flex: 1,
  },

  detailsView: {
    backgroundColor: COLORS.white,
    borderRadius: 8,
    marginTop: 12,
    paddingHorizontal: 30,
    paddingVertical: 8,
  },
  dateText: {
    ...FontStyle.Regular16_500M,
    color: COLORS.grey5,
  },

  iconView: {
    backgroundColor: '#d0f7e5',
    height: 48,
    width: 48,
    borderRadius: 48,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
