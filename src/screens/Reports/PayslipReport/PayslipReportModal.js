import {
  Alert,
  Linking,
  Modal,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import React, {useEffect, useState} from 'react';
import {COLORS} from '../../../theme/colors';
import {FontStyle} from '../../../theme/FontStyle';
import Pdf from 'react-native-pdf';
import ReactNativeVI from '../../../components/Helpers/ReactNativeVI';
import RNFS from 'react-native-fs';

const PayslipReportModal = ({modalVisible, setModalVisible, base64}) => {
  const [pdfFilePath, setPdfFilePath] = useState('');
  const {t} = useTranslation();

  useEffect(() => {
    if (base64) {
      const decodedFile = RNFS.CachesDirectoryPath + '/temp.pdf';
      RNFS.writeFile(decodedFile, base64, 'base64')
        .then(() => {
          setPdfFilePath(decodedFile);
        })
        .catch(error => {
          console.error('Error writing PDF file:', error);
        });
    }
  }, [base64]);

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(false);
      }}>
      <View style={styles.container1}>
        <View style={styles.innerContainer}>
          <View style={styles.container}>
            {pdfFilePath && (
              <Pdf
                source={{uri: pdfFilePath, cache: true}}
                // onLoadComplete={(numberOfPages, filePath) => {
                //     console.log(`Number of pages: ${numberOfPages}`);
                // }}
                // onPageChanged={(page, numberOfPages) => {
                //     console.log(`Current page: ${page}`);
                // }}
                onError={error => {
                  console.log(error);
                }}
                onPressLink={uri => {
                  console.log(`Link pressed: ${uri}`);
                }}
                style={{
                  flex: 1,
                  width: '100%',
                  height: '100%',
                }}
              />
            )}
          </View>
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => setModalVisible(false)}
            style={{
              padding: 8,
              paddingLeft: 8,
              alignSelf: 'flex-end',
              position: 'absolute',
            }}>
            <ReactNativeVI
              Lib={'Ionicons'}
              name={'close'}
              color={COLORS.black}
              size={28}
            />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default PayslipReportModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  container1: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'center',
  },

  innerContainer: {
    backgroundColor: COLORS.white,
    borderRadius: 8,
    width: '90%',
    height: '70%',
    overflow: 'hidden',
  },

  detailsView: {
    backgroundColor: COLORS.white,
    borderRadius: 8,
    marginTop: 12,
    paddingHorizontal: 30,
    paddingVertical: 8,
  },
  dateText: {
    ...FontStyle.Regular14_500,
    fontWeight: '500',
    color: COLORS.grey5,
  },

  iconView: {
    backgroundColor: '#d0f7e5',
    height: 48,
    width: 48,
    borderRadius: 48,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
