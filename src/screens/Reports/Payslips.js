import {
  ActivityIndicator,
  Alert,
  FlatList,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import GeneralHeader from '../../components/Headers/GeneralHeader';
import Theme from '../../theme/theme';
import { useTranslation } from 'react-i18next';
import PayslipCard from '../../components/Cards/PayslipCard';
import { commonApi } from '../../utilities/api/apiController';
import { logout_user } from '../../redux/users/user.actions';
import {
  getFormattedDate,
  getFormattedMonth,
} from '../../utilities/helpers/CurretDate';
import { COLORS } from '../../theme/colors';
import { FontStyle } from '../../theme/FontStyle';
import Toast from 'react-native-simple-toast';
import { useDispatch, useSelector } from 'react-redux';

const Payslips = ({ navigation }) => {
  const { t } = useTranslation();
  const [payslips, setPayslips] = useState([]);
  const [loading, setLoading] = useState(true);
  const [refresh, setRefresh] = useState(false);
  const dispatch = useDispatch();
  const { employeeID } = useSelector(state => state?.employeeProfile);

  const getpaySlip = async () => {
    try {
      const body = {
        params: {
          model: 'hr.payslip',
          method: 'get_employee_payslips',
          args: [employeeID],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });
      setLoading(false);

      if (response?.data?.result) {
        if (response?.data?.result?.length > 0) {
          setPayslips(response?.data?.result);
        } else {
          // setAlertBox({
          //     showBox: true,
          //     title: `${t('slip-not-found')}`,
          //     message: `${t('slip-not-found-msg')}`,
          // });
        }
      } else {
        setLoading(false);

        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: get_employee_payslips\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getpaySlip();
  }, []);

  const onRefresh = async () => {
    setRefresh(true);
    await getpaySlip();
    setRefresh(false);
  };

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader title={t('payslip')} navigation={navigation} />

      <View style={styles.container}>
        {loading && <ActivityIndicator color={COLORS.primaryColor} />}

        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={refresh}
              onRefresh={onRefresh}
              tintColor={COLORS.primaryColor}
              colors={[COLORS.primaryColor]}
            />
          }
          style={{ flex: 1 }}
          data={payslips}
          renderItem={({ item, index }) => {
            const month = getFormattedMonth(`${item?.date_from}`);
            const date = `${getFormattedDate(
              item?.date_from,
              false,
            )} - ${getFormattedDate(item?.date_to, false)}`;
            return (
              <PayslipCard
                key={index}
                title={month}
                caption={date}
                navigate={() => {
                  navigation.navigate('PayslipDetails', {
                    payslipID: item?.id,
                    month: month,
                    date: date,
                  });
                }}
              />
            );
          }}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={() => (
            <>
              {payslips?.length < 1 && !refresh && !loading && (
                <Text style={[styles.dateText, { alignSelf: 'center' }]}>
                  {t('no-payslip-found')}
                </Text>
              )}
            </>
          )}
        />
      </View>
    </SafeAreaView>
  );
};

export default Payslips;

const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    marginHorizontal: 16,
    flex: 1,
  },

  dateText: {
    ...FontStyle.Regular14_500,
    fontWeight: '500',
    color: COLORS.grey5,
  },
});
