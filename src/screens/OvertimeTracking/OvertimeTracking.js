import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  StyleSheet,
  RefreshControl,
  Dimensions,
  Platform,
  I18nManager,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import GeneralHeader from '../../components/Headers/GeneralHeader';
import Theme from '../../theme/theme';
import { COLORS } from '../../theme/colors';
import AddOvertimeRequest from './AddOvertimeRequest/AddOvertimeRequest';
import { useTranslation } from 'react-i18next';
import SmartButtonTrans from '../../components/Buttons/SmartButtonTrans';
import { Icons } from '../../assets/SvgIcons/Icons';
import { FontStyle } from '../../theme/FontStyle';
import OvertimeTrackingBalanceCard from '../../components/Cards/OvertimeTrackingBalanceCard';
import { useDispatch, useSelector } from 'react-redux';
import { getEmpWiseOvertimeReqs } from '../../redux/overtime/actions/getEmpWiseOvertimeReqs';
import { getMangrWiseOvertimeReqs } from '../../redux/overtime/actions/getMangrWiseOvertimeReqs';
import Carousel from 'react-native-reanimated-carousel';
import { getOvertimeSummary } from '../../redux/overtime/actions/getOvertimeSummary';
import OvertimeRequestCard from '../../components/Cards/OverTimeRequestCard';

export const window = Dimensions.get('window');

const PAGE_WIDTH = window.width;

const baseOptions = {
  vertical: false,
  width: PAGE_WIDTH,
  height: null,
};

const OvertimeTracking = ({ navigation }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { isManager, overtimeCards, overtimeBalance } = useSelector(
    state => state?.employeeProfile,
  );
  const { empWiseOvertimeReqs, mangrWiseOvertimeReqs } = useSelector(
    state => state?.overtime,
  );
  const [filteredData, setFilteredData] = useState(empWiseOvertimeReqs);
  const [selectedCategoryId, setSelectedCategoryId] = useState(1);
  const [modalVisible1, setModalVisible1] = useState(false);
  const [managersData, setManagersData] = useState(false);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    if (selectedCategoryId == 1) {
      setFilteredData(empWiseOvertimeReqs);
    } else {
      setFilteredData(mangrWiseOvertimeReqs);
    }
  }, [empWiseOvertimeReqs, mangrWiseOvertimeReqs]);

  useEffect(() => {
    getData('emp');
  }, []);

  const getData = async category => {
    try {
      setRefresh(true);

      await dispatch(getOvertimeSummary({ navigation }));

      if (category === 'emp') {
        await dispatch(getEmpWiseOvertimeReqs({ navigation }));
      } else if (category === 'man') {
        setManagersData(true);
        await dispatch(getMangrWiseOvertimeReqs({ navigation }));
      } else {
        if (isManager) {
          await dispatch(getMangrWiseOvertimeReqs({ navigation }));
        }
        await dispatch(getEmpWiseOvertimeReqs({ navigation }));
      }

      setRefresh(false);
    } catch (error) {
      setRefresh(false);
      console.log('Error while calling getAllEmployeeLeaves', error);
    }
  };

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader title={t('overtime-tracking')} navigation={navigation} />
      <View style={styles.container}>
        <View
          style={{
            alignItems: 'flex-end',
            marginVertical: 16,
            marginBottom: 16,
            marginRight: 16,
          }}>
          <SmartButtonTrans
            title={t('add-new')}
            rightIcon={Icons.plus}
            onPress={() => setModalVisible1(true)}
          />
        </View>

        {isManager ? (
          <View
            style={{
              height: Platform.OS == 'ios' && I18nManager.isRTL ? 150 : 120,
            }}>
            <Carousel
              loop={false}
              {...baseOptions}
              style={{
                width: PAGE_WIDTH,
              }}
              mode="parallax"
              modeConfig={{
                parallaxScrollingScale: 0.9,
                parallaxScrollingOffset: 110,
              }}
              data={overtimeCards}
              onSnapToItem={async index => {
                const selected = overtimeCards[index];
                if (selected?.id == 1) {
                  setSelectedCategoryId(selected.id);
                  setFilteredData(empWiseOvertimeReqs);
                } else if (selected.id == 2) {
                  setSelectedCategoryId(selected.id);
                  setFilteredData(mangrWiseOvertimeReqs);
                  if (!managersData) {
                    await getData('man');
                  }
                }
              }}
              renderItem={({ item, index }) => {
                return (
                  <OvertimeTrackingBalanceCard
                    item={item}
                    selectedCategoryId={selectedCategoryId}
                  />
                );
              }}
            />
          </View>
        ) : (
          <View style={{ paddingHorizontal: 16 }}>
            <OvertimeTrackingBalanceCard item={overtimeBalance} />
          </View>
        )}
        <View style={styles.body}>
          <FlatList
            data={filteredData}
            removeClippedSubviews={false}
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={() => {
                  if (selectedCategoryId == 1) {
                    getData('emp');
                  } else {
                    getData('man');
                  }
                }}
                tintColor={COLORS.primaryColor}
                colors={[COLORS.primaryColor]}
              />
            }
            contentContainerStyle={{
              paddingBottom: 16,
              paddingTop: isManager ? 0 : 16,
              flexGrow: 1,
            }}
            renderItem={({ item, index }) => (
              <OvertimeRequestCard
                key={index}
                data={item}
                employeeOvertime={selectedCategoryId == 1 ? false : true}
                navigation={navigation}
                getEmployeeOvertimes={getData}
              />
            )}
            keyExtractor={(item, index) => index}
            ItemSeparatorComponent={() => <View style={{ height: 12 }} />}
            ListEmptyComponent={() => (
              <>
                {filteredData?.length < 1 && !refresh && (
                  <Text style={[styles.dateText, { alignSelf: 'center' }]}>
                    {t('no-record-found')}
                  </Text>
                )}
              </>
            )}
          />
        </View>

        <AddOvertimeRequest
          modalVisible1={modalVisible1}
          setModalVisible1={setModalVisible1}
          navigation={navigation}
          getOvertimes={getData}
        />
      </View>
    </SafeAreaView>
  );
};

export default OvertimeTracking;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    flex: 1,
    marginHorizontal: 16,
  },
  dateText: {
    ...FontStyle.Regular12,
    fontWeight: '500',
    color: COLORS.grey5,
  },
});
