import { View } from 'react-native';
import React, { useState } from 'react';
import { styles } from './styles';
import TextInput from '../../../components/Inputs/TextInputField';
import { useTranslation } from 'react-i18next';
import { COLORS } from '../../../theme/colors';
import { NextButton } from '../../../components/Inputs';
import { getFormattedDate } from '../../../utilities/helpers/CurretDate';
import DatePickerNew from '../../../components/DateTimePicker/DatePickerNew';
import { FontStyle } from '../../../theme/FontStyle';
import { OvertimeRequestSchema } from '../../../utilities/Validations';
import inputValidation from '../../../utilities/Validations/YupValidate';
import GenericModal from '../../../components/Modals/GenericModal';
import { commonApi } from '../../../utilities/api/apiController';
import { logout_user } from '../../../redux/users/user.actions';
import { useDispatch, useSelector } from 'react-redux';
import Toast from 'react-native-simple-toast';
import { AlertModal } from '../../../components/Modals';
import { DrawerIcons } from '../../../assets/SvgIcons/DrawerIcons';
import { SvgXml } from 'react-native-svg';

const AddOvertimeRequest = ({
  navigation,
  modalVisible1,
  setModalVisible1,
  getOvertimes,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const { employeeID } = useSelector(state => state?.employeeProfile);
  const [inputs, setInputs] = useState({
    date: null,
    startTime: null,
    duration: null,
    noOfDays: null,
    reason: null,
  });

  const [alertBox, setAlertBox] = useState({
    showBox: false,
    title: null,
    message: null,
    confirmbtn: null,
    okbtn: null,
  });

  const handleInputChange = (field, value) => {
    setInputs({
      ...inputs,
      [field]: value,
      errors: {
        ...inputs.errors,
        [field]: false,
      },
    });
  };

  const handleDateChange = (selectedDate, field) => {
    const dateObject = new Date(selectedDate);
    const formattedDate = dateObject.toISOString().split('T')[0];

    handleInputChange(field, formattedDate);
  };

  const validate = async () => {
    const schema = OvertimeRequestSchema(t);
    const result = await inputValidation(schema, inputs);

    if (result.isValidate) {
      handleSubmit();
    } else {
      setInputs(prev => ({
        ...prev,
        errors: result?.err,
      }));
    }
  };

  const handleSubmit = async () => {
    setLoading(true);

    const dateObject = new Date(inputs.date);
    const formattedDate = dateObject.toISOString().split('T')[0];

    try {
      const body = {
        params: {
          method: 'create_overtime_request',
          model: 'hr.overtime.wags',
          args: [
            {
              employee_id: employeeID,
              overtime_date: formattedDate,
              overtime: inputs?.duration,
              overtime_days: inputs?.noOfDays,
              reason: inputs?.reason,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });
      setLoading(false);

      if (response?.data?.result) {
        if (response?.data?.result) {
          setLoading(false);
          setModalVisible1(false);
          setInputs({
            amount: null,
            type: null,
            date: null,
            reason: null,
            errors: null,
          });
          getOvertimes();
          setTimeout(() => { Toast.show(`${t('overtime-applied-successfully')} !`); }, 300);
        } else if (response?.data?.result?.error) {
          setLoading(false);
        }
      } else {
        setLoading(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              setAlertBox({
                showBox: true,
                title: `${'Validation Error'}`,
                message: `${response?.data?.error?.data?.message}`,
              });
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const closeCustomAlert = () => {
    setTimeout(() => {
      setAlertBox({ showBox: false });
    }, 300);
  };

  return (
    <GenericModal
      modalVisible={modalVisible1}
      setModalVisible={setModalVisible1}
      heading={t('apply-overtime')}>
      <View style={styles.container}>
        <DatePickerNew
          mode="date"
          date={inputs.date ? new Date(inputs.date) : new Date()}
          value={inputs.date && getFormattedDate(inputs.date)}
          label={t('date')}
          onChange={selectedDate => handleDateChange(selectedDate, 'date')}
          showDatePicker={showDatePicker}
          setShowDatePicker={setShowDatePicker}
          placeholder={t('date')}
          error={inputs?.errors?.date}
          // minimumDate={new Date()}
          minimumDate={new Date(new Date().setDate(new Date().getDate() - 30))}
        />

        <TextInput
          label={t('no-of-days')}
          placeholder={`0 ${t('no-of-days')}`}
          keyboardType={'number-pad'}
          value={inputs.noOfDays}
          error={inputs?.errors?.noOfDays}
          labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
          onChangeText={text => handleInputChange('noOfDays', text)}
        />

        <TextInput
          label={t('hours1')}
          placeholder={`0 ${t('hours')}`}
          keyboardType={'number-pad'}
          value={inputs.duration}
          error={inputs?.errors?.duration}
          labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
          onChangeText={text => handleInputChange('duration', text)}
        />

        <TextInput
          label={t('reason')}
          placeholder={t('purpose-plcholder')}
          value={inputs.reason}
          error={inputs?.errors?.reason}
          labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
          onChangeText={text => handleInputChange('reason', text)}
        />

        <View style={styles.bottomView}>
          <NextButton title={t('submit')} onPress={validate} loader={loading} />
        </View>
      </View>
      <AlertModal
        visible={alertBox.showBox}
        icon={
          <View style={styles.alertIconView}>
            <SvgXml xml={DrawerIcons.overTime} />
          </View>
        }
        title={alertBox.title}
        message={alertBox.message}
        onClose={closeCustomAlert}
        confirmbtn={alertBox.confirmbtn}
      />
    </GenericModal>
  );
};

export default AddOvertimeRequest;
