import { View, Text, SafeAreaView, TouchableOpacity, StatusBar } from 'react-native'
import React, { useEffect, useState } from 'react'
import { SvgXml } from 'react-native-svg'
import SplashScreen from 'react-native-splash-screen'
import Theme from '../../theme/theme'
import TouchableButton from '../../components/Buttons/TouchableButton'
import { Frames } from '../../assets/SvgIcons/Frames'
import { FontStyle } from '../../theme/FontStyle'
import { NextButton } from '../../components/Inputs'
import { styles } from './styles'
import { useTranslation } from 'react-i18next'
import { LangModal } from '../../components/Modals'
import { COLORS } from '../../theme/colors'
import changeLng from '../../utilities/helpers/ChangeLanguage'
import ReactNativeVI from '../../components/Helpers/ReactNativeVI'

const Onbording1 = ({ navigation }) => {

  const { t } = useTranslation();
  const [lngModalVisible, setLngModalVisible] = useState(false);

  useEffect(() => {
    SplashScreen.hide();
  }, [])

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <StatusBar barStyle={'dark-content'} backgroundColor={COLORS.background} />

      <View style={styles.container}>

        <View style={styles.skipView}>

          <TouchableOpacity activeOpacity={0.5} onPress={() => setLngModalVisible(true)} style={{ paddingHorizontal: 8 }}>
            <ReactNativeVI Lib={'FontAwesome'} name={'language'} color={COLORS.primaryColor} size={26} />
          </TouchableOpacity>

          <TouchableButton
            title={t('skip')}
            onPress={() => { navigation.navigate('ServerScreen') }}
            container={styles.skipButton} />
        </View>

        <View style={styles.mainView}>

          <View style={styles.frameView}>
            <SvgXml xml={Frames.Onbording1} />
          </View>

          <View style={styles.textView}>
            <Text style={FontStyle.Regular20}>{t('onbording1-title')}</Text>
            <Text style={[FontStyle.Regular16_500, { textAlign: 'center', marginTop: 12, color: COLORS.grey, }]}>{t('onbording-screen1-msg')}</Text>
          </View>

        </View>

        <View style={styles.bottomView}>
          <NextButton title={t('next')} onPress={() => navigation.navigate('Onbording2')} />
        </View>

      </View>

      <LangModal
        modalVisible={lngModalVisible}
        setModalVisible={setLngModalVisible}
        onChangeSelection={changeLng}
      />

    </SafeAreaView >
  )
}

export default Onbording1