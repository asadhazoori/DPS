import {View, Text, SafeAreaView} from 'react-native';
import React, {useEffect} from 'react';
import {SvgXml} from 'react-native-svg';
import SplashScreen from 'react-native-splash-screen';
import Theme from '../../theme/theme';
import TouchableButton from '../../components/Buttons/TouchableButton';
import {Frames} from '../../assets/SvgIcons/Frames';
import {FontStyle} from '../../theme/FontStyle';
import {NextButton} from '../../components/Inputs';
import {styles} from './styles';
import {useTranslation} from 'react-i18next';
import {COLORS} from '../../theme/colors';

const Onbording2 = ({navigation}) => {
  const {t} = useTranslation();

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <View style={styles.container}>
        <View style={styles.mainView}>
          <View style={[styles.frameView, {marginTop: 115}]}>
            <SvgXml xml={Frames.Onbording2} />
          </View>

          <View style={styles.textView}>
            <Text style={FontStyle.Regular20}>{t('onbording2-title')}</Text>
            <Text
              style={[
                FontStyle.Regular16_500,
                {textAlign: 'center', marginTop: 12, color: COLORS.grey},
              ]}>
              {t('onbording-screen1-msg')}
            </Text>
          </View>
        </View>

        <View style={styles.bottomView}>
          <NextButton
            title={t('lets-work')}
            onPress={() => navigation.navigate('ServerScreen')}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Onbording2;
