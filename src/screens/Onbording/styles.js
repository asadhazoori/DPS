import { StyleSheet } from 'react-native'
import { COLORS } from '../../../theme/colors'

export const styles = StyleSheet.create({

    container: {
        marginHorizontal: 16,
        flex: 1
    },

    skipView: {
        flexDirection: 'row',
        marginTop: 48,
        justifyContent: 'space-between',
    },

    mainView: {
        alignItems: 'center',
    },

    frameView: {
        marginTop: 44,
        borderRadius: 145.5,
        overflow: 'hidden',
    },

    textView: {
        marginTop: 48,
        marginHorizontal: 24,
        alignItems: 'center',
        width: 291,
    },

    bottomView: {
        justifyContent: 'flex-end',
        flex: 1,
        marginBottom: 24,

    },


})