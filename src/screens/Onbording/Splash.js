import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getEmployeeProfile } from '../../redux/profile/actions/getEmployeeProfile';
import { matchServerVerions } from '../../utilities/helpers/matchServerVersion';
import { checkInternetAccess } from '../../utilities/helpers/checkNetwork';

const Splash = ({ navigation }) => {
  const { firstLogin, loggedIn, uid, isPasswordChanged } = useSelector(state => state.signin);
  const { data } = useSelector(state => state.employeeProfile);
  const dispatch = useDispatch();

  useEffect(() => {
    (async () => {
      await handleNavigation();
    })();
  }, []);

  const handleNavigation = async () => {
    if (!firstLogin) {
      navigateTo('Onbording1');
      return;
    }

    if (!loggedIn) {
      navigateTo('Login');
      return;
    }

    try {
      if (await checkInternetAccess()) {
        const versionMatched = await matchServerVerions({ navigation, dispatch });
        if (versionMatched == 'matched') {
          const empNotFound = await dispatch(getEmployeeProfile({ uid, navigation }),);
          // if (!empNotFound) {
          //   if (data?.allow_change_password && isPasswordChanged) {
          //     navigateTo('ChangePassword', { isPasswordChanged: true });
          //   } else {
          //     navigateTo('BottomTab');
          //   }
          // }
        } else if (
          versionMatched != 'matched' &&
          versionMatched != 'session_expired'
        ) {
          navigateTo('BottomTab', { versionMatched: versionMatched });
        } else {
          navigateTo('Login');
        }
      } else {
        navigateTo('BottomTab');
      }
    } catch (error) {
      console.log(error);
    }
  };

  const navigateTo = (routeName, params = {}) => {
    navigation.reset({
      index: 0,
      routes: [{ name: routeName, params }],
    });
  };

  return null;
};

export default Splash;
