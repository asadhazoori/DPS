import Splash from "./Onbording/Splash"
import Onbording1 from "./Onbording/Onbording1"
import Onbording2 from "./Onbording/Onbording2"
import Login from "./AuthScreens/Login/Login"
import Dashboard from "./Dashboard/Dashboard"
import Profile from "./Profile/Profile/Profile"
import ProfileDetails from "./Profile/ProfileDetails/ProfileDetails"

import Attendance from "./Attendance/Attendance"
import AttendanceDetails from "./Attendance/AttendanceDetail/AttendanceDetails"
import AttendaceChangeRequest from "./Attendance/AttendaceChangeRequest/AttendaceChangeRequest"
import Location from "./Attendance/PunchLocation/PunchLocation"
import Leaves from "./Leaves/Leaves"
import Loans from "./Loans/Loans"
import Shifts from "./Shifts/Shifts"
import OvertimeTracking from "./OvertimeTracking/OvertimeTracking"

import MedicalClaim from "./MedicalClaims/MedicalClaim"
import ApplyMedicalClaims from "./MedicalClaims/ApplyMedicalClaims/ApplyMedicalClaims"

import Timesheet from "./Timesheet/Timesheet"
import ApplyTimeSheet from "./Timesheet/ApplyTimeSheet/ApplyTimeSheet"

import Payslips from "./Reports/Payslips"
import PayslipDetails from "./Reports/PayslipDetails/PayslipDetails"

import Notifications from "./Notifications/Notifications"
import Requests from "./Requests/Requests"
import ChangePassword from "./AuthScreens/Change_Password/ChangePassword"
import AttendanceRequests from "./Attendance/AttendanceRequests/AttendanceRequests"
import PayslipReportModal from "./Reports/PayslipReport/PayslipReportModal"
import ServerScreen from "./AuthScreens/ServerScreen/ServerScreen"
import ManagerAttendance from "./Attendance/ManagerAttendance/ManagerAttendance"
import HomeChats from "./chat/screens/HomeChats/HomeChats"
import ConversationChat from "./chat/screens/ConversationChat/ConversationChat"
import CreateRequests from "./Requests/CreateRequests/CreateRequests"
import ManualAttendance from "./Attendance/ManualAttendance/ManualAttendance"
import ManualAttendanceReqs from "./Attendance/ManualAttendance/ManualAttendanceReqs"

export {
  Splash,
  Onbording1,
  Onbording2,
  ServerScreen,
  Login,
  Dashboard,
  Profile,
  ProfileDetails,
  Attendance,
  AttendanceDetails,
  AttendaceChangeRequest,
  ManualAttendance,
  ManualAttendanceReqs,
  Location,
  Leaves,
  Loans,
  MedicalClaim,
  ApplyMedicalClaims,
  Timesheet,
  ApplyTimeSheet,
  Shifts,
  OvertimeTracking,
  Payslips,
  PayslipDetails,
  Requests,
  Notifications,
  ChangePassword,
  AttendanceRequests,
  PayslipReportModal as PayslipReport,
  ManagerAttendance,

  HomeChats,
  ConversationChat,
  CreateRequests

}