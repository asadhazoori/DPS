import { View, SafeAreaView, FlatList, StyleSheet, Dimensions, Text } from 'react-native'
import React, { useEffect, useState } from 'react'
import GeneralHeader from '../../components/Headers/GeneralHeader'
import Theme from '../../theme/theme'
import { useTranslation } from 'react-i18next'
import SmartButtonTrans from '../../components/Buttons/SmartButtonTrans'
import { Icons } from '../../assets/SvgIcons/Icons'
import { COLORS } from '../../theme/colors'
import { FontStyle } from '../../theme/FontStyle'
import Carousel from 'react-native-reanimated-carousel';
import TimeSheetBalanceCard from '../../components/Cards/TimeSheetBalanceCard'
import TimeSheetRequestCard from '../../components/Cards/TimeSheetRequestCard'

const Timesheet = ({ navigation, route }) => {
    const { t } = useTranslation();

    const [selectedCategoryId, setSelectedCategoryId] = useState(null);
    const [modalVisible1, setModalVisible1] = useState(false);
    const [filteredData2, setFilteredData2] = useState([]);
    const [isVertical, setIsVertical] = React.useState(false);

    const leaves = [
        {
            "leaves_type": "WillU HR",
            "leave_remaining": 14.0,
            "total_leaves": 14.0,
            "leave_availed": 0.0,
            "id": 1,
        },
        {
            "leaves_type": "Tracking Technician App",
            "leave_remaining": 12.0,
            "total_leaves": 12.0,
            "leave_availed": 0.0,
            "id": 2,
        },
        {
            "leaves_type": "ERP Handy",
            "leave_remaining": 10.0,
            "total_leaves": 10.0,
            "leave_availed": 0.0,
            "id": 3,
        },

    ];

    const leaves_requested = [
        {
            "date_to_ecube": "2023-10-13",
            "description": "Asad",
            "days": 1.0,
            "state": "confirm",
            "leaves_type": "WillU HR",
            "task": "Attendance Module",
            "date_from_ecube": "2023-10-13",
            "date": "2022-03-13",
            "startTime": "8:30 PM",
            "endTime": "11:00 PM",
            "hours": "2.5",
        },
        {
            "date_to_ecube": "2023-10-13",
            "description": "Asad",
            "days": 1.0,
            "state": "confirm",
            "leaves_type": "WillU HR",
            "task": "Leaves Module",
            "date_from_ecube": "2023-10-13",
            "date": "2022-03-13",
            "startTime": "8:30 PM",
            "endTime": "11:00 PM",
            "hours": "2.5",
        },
        {
            "date_to_ecube": "2023-05-29",
            "description": "Muree",
            "days": 1.0,
            "state": "validate",
            "task": "Loans Module",
            "leaves_type": "Tracking Technician App",
            "date_from_ecube": "2023-05-29",
            "date": "2022-03-13",
            "startTime": "8:30 PM",
            "endTime": "11:00 PM",
            "hours": "2.5",
        },
        {
            "date_to_ecube": "2023-05-29",
            "description": "Muree",
            "days": 1.0,
            "state": "validate",
            "task": "Payslips Module",
            "leaves_type": "Tracking Technician App",
            "date_from_ecube": "2023-05-29",
            "date": "2022-03-13",
            "startTime": "8:30 PM",
            "endTime": "11:00 PM",
            "hours": "2.5",
        },
        {
            "date_to_ecube": "2023-05-29",
            "description": "Muree",
            "days": 1.0,
            "state": "validate",
            "leaves_type": "WillU HR",
            "task": "TimeSheet Module",
            "date_from_ecube": "2023-05-29",
            "date": "2022-03-13",
            "startTime": "8:30 PM",
            "endTime": "11:00 PM",
            "hours": "2.5",
        },
        {
            "date_to_ecube": "2023-05-29",
            "description": "Muree",
            "days": 1.0,
            "state": "validate",
            "task": "Profile Module",
            "leaves_type": "Tracking Technician App",
            "date_from_ecube": "2023-05-29",
            "date": "2022-03-13",
            "startTime": "8:30 PM",
            "endTime": "11:00 PM",
            "hours": "2.5",
        },
        {
            "date_to_ecube": "2023-05-29",
            "description": "Muree",
            "days": 1.0,
            "state": "validate",
            "task": "Overtime Tracking Module",
            "leaves_type": "Tracking Technician App",
            "date_from_ecube": "2023-05-29",
            "date": "2022-03-13",
            "startTime": "8:30 PM",
            "endTime": "11:00 PM",
            "hours": "2.5",
        },
        {
            "date_to_ecube": "2023-05-29",
            "description": "Muree",
            "days": 1.0,
            "state": "refuse",
            "task": "Medical Claims Module",
            "leaves_type": "WillU HR",
            "date_from_ecube": "2023-05-29",
            "date": "2022-03-13",
            "startTime": "8:30 PM",
            "endTime": "11:00 PM",
            "hours": "2.5",
        }
    ]

    const handleButtonPress = (selected) => {
        setSelectedCategoryId(selected.id);
        const matchedData = leaves_requested.filter((item) => item.leaves_type == selected.leaves_type);
        setFilteredData2(matchedData);
    };
    const width = Dimensions.get('window').width;
    const baseOptions = isVertical
        ? {
            vertical: true,
            width: width * 0.86,
            height: width * 0.6,
        }
        : {
            vertical: false,
            width: width,
            height: width * 0.6,
        };

    return (
        <SafeAreaView style={Theme.SafeArea}>
            <GeneralHeader title={t('timesheet-management')} navigation={navigation} />

            <View style={styles.container}>


                <View style={{ alignItems: 'flex-end', marginVertical: 24, marginBottom: 16, marginRight: 16 }}>
                    <SmartButtonTrans title={t('add-new')} rightIcon={Icons.plus} onPress={() => {
                        navigation.navigate('ApplyTimeSheet');

                        setModalVisible1(true)
                    }} />

                </View>

                <View style={{ borderWidth: 0 }}>
                    <Carousel
                        // loop
                        {...baseOptions}
                        style={{
                            width: width,
                        }}
                        // pagingEnabled={true}
                        // snapEnabled={true}
                        // autoPlay={true}
                        // autoPlayInterval={1500}
                        // width={width}
                        mode='parallax'
                        height={width / 3.8}
                        modeConfig={{
                            parallaxScrollingScale: 0.9,
                            parallaxScrollingOffset: 90,
                            // parallaxAdjacentItemScale: 0.6,
                        }}
                        // autoPlay={true}
                        data={leaves}
                        // scrollAnimationDuration={500}
                        onSnapToItem={(index) => {
                            const selected = leaves[index];
                            setSelectedCategoryId(selected.id);
                            const matchedData = leaves_requested.filter((item) => item.leaves_type == selected.leaves_type);
                            setFilteredData2(matchedData);
                        }}
                        renderItem={({ item, index }) => (<TimeSheetBalanceCard item={item} index={index} leavesLength={leaves.length} selectedCategoryId={selectedCategoryId} setSelectedCategoryId={handleButtonPress} />)}
                    />
                </View>

                <View style={styles.body}>

                    <FlatList
                        data={selectedCategoryId ? filteredData2 : leaves_requested}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ paddingVertical: 16, flexGrow: 1 }}
                        renderItem={({ item, index }) => (<TimeSheetRequestCard key={index} data={item} />)}
                        keyExtractor={(item, index) => index}
                        ItemSeparatorComponent={() => <View style={{ height: 12 }} />}
                        ListEmptyComponent={() => (<Text style={[styles.dateText, { alignSelf: 'center' }]}>{t('no-record-found')}</Text>)}


                    />
                </View>



            </View>

        </SafeAreaView>
    )
}

export default Timesheet


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // borderWidth: 1,

    },
    body: {
        // borderWidth: 1,
        flex: 1,
        marginHorizontal: 16,

    },
    dateText: {
        ...FontStyle.Regular12,
        fontWeight: '500',
        color: COLORS.grey5

    },
})