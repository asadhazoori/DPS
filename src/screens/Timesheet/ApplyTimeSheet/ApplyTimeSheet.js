import React, { useState } from 'react'
import { ActivityIndicator, Alert, View, ScrollView, Modal, Text, TouchableOpacity } from 'react-native'
import inputValidation from '../../../utilities/Validations/YupValidate';
import TouchableView from '../../../components/Buttons/TouchableView';
import { commonApi } from '../../../utilities/api/apiController';
import { LeavesRequestSchema } from '../../../utilities/Validations';
import { useSelector } from 'react-redux';
import { styles } from './styles';
import { NextButton, TextInputField } from '../../../components/Inputs';
import YearPicker from '../../../components/Inputs/YearPicker';
import TouchableViewModal from '../../../components/Buttons/TouchableViewModal';
import RadioSelectionModal from '../../../components/Helpers/RadioSelectionModal';
import { getFormattedDate } from '../../../utilities/helpers/CurretDate';
import { useTranslation } from 'react-i18next';
import { COLORS } from '../../../theme/colors';
import Theme from '../../../theme/theme';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import { RadioButton } from 'react-native-paper';
import { FontStyle } from '../../../theme/FontStyle';
import ReactNativeVI from '../../../components/Helpers/ReactNativeVI';


const ApplyTimeSheet = ({ navigation, modalVisible1, setModalVisible1 }) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [showStartTimePicker, setShowStartTimePicker] = useState(false);
    const [showEndTimePicker, setShowEndTimePicker] = useState(false);
    const { t } = useTranslation();
    const [selectedType, setSelectedType] = useState(null);
    const handleRadioButtonChange = (itemId) => {
        setSelectedType(itemId);
    };

    const data = [
        { id: 1, name: 'Completed', key: 'compl' },
        { id: 2, name: "In Progress", key: 'inPro' }
    ]

    const holidayStatus = [
        {
            "id": 2,
            "name": "Sick Leaves",
        },
        {
            "id": 4,
            "name": "Short leave"
        },
        {
            "id": 6,
            "name": "Annual Leaves"
        },
        {
            "id": 7,
            "name": "Casual Leaves"
        },
        {
            "id": 11,
            "name": "CPL"
        },
        {
            "id": 16,
            "name": "CPL (Non-Encashable)"
        }
    ];
    const holidayStatus1 = [
        {
            "id": 2,
            "name": "Sick Leaves",
        },
        {
            "id": 4,
            "name": "Short leave"
        },
        {
            "id": 6,
            "name": "Annual Leaves"
        },
        {
            "id": 7,
            "name": "Casual Leaves"
        },
        {
            "id": 11,
            "name": "CPL"
        },
        {
            "id": 16,
            "name": "CPL (Non-Encashable)"
        }
    ];
    const { employeeID, name } = useSelector((state) => state.employeeProfile); // also getting holidayStatus
    const [loading, setLoading] = useState(false);
    const [showStartDatePicker, setShowStartDatePicker] = useState(false);
    const [showEndDatePicker, setShowEndDatePicker] = useState(false);

    const [inputs, setInputs] = useState({
        name: name,
        holidayType: null,
        type: null,
        startDate: null,
        endDate: null,
        reason: null,

        startTime: null,
        endTime: null,
    });

    const handleInputChange = (field, value) => {
        setInputs({
            ...inputs,
            [field]: value,
            errors: {
                ...inputs.errors,
                [field]: false
            }
        })
    }

    console.log(inputs)


    const handleTimeChange = (selectedDate, field) => {

        // const dateObject = new Date(selectedDate);
        // const formattedDate = dateObject.toISOString().split('T')[0];
        // console.log("overtime", selectedDate);

        const localDateTime = moment(selectedDate);
        const formattedTime = localDateTime.format('hh:mm A');
        const formattedDate = localDateTime.format('DD-MM-YYYY');

        // console.log("first", "handleTimeChange")

        switch (field) {
            // case "date":
            //     setShowDatePicker(false);
            //     handleInputChange(field, formattedDate);
            //     break;

            case "startTime":
                setShowStartTimePicker(false);
                handleInputChange(field, formattedTime);
                break;

            case "endTime":
                setShowEndTimePicker(false)
                handleInputChange(field, formattedTime);
                break;

            default:
                break;
        }


    };


    const handleDateChange = (selectedDate, field) => {

        const dateObject = new Date(selectedDate);
        const formattedDate = dateObject.toISOString().split('T')[0];

        if (field === 'startDate') {
            setShowStartDatePicker(false);
        } else if (field === 'endDate') {
            setShowEndDatePicker(false);
        }

        handleInputChange(field, formattedDate);

    };

    const validate = async () => {

        const schema = LeavesRequestSchema(t);
        const result = await inputValidation(schema, inputs)

        // const result = await inputValidation(LeavesRequestSchema, inputs)

        if (result.isValidate) {
            // handleSubmit();

        } else {
            setInputs(prev => ({
                ...prev,
                errors: result?.err
            }))
        }

    }

    const handleSubmit = async () => {
        setLoading(true);

        try {
            const body = {
                "jsonrps": 2.0,
                "params": {
                    "employee_id": employeeID,
                    "name": name,
                    "holiday_status_id": inputs?.holidayType?.id,
                    "date_from_ecube": inputs.startDate,
                    "date_to_ecube": inputs.endDate,
                    "type": 1,
                    "reason": inputs.reason
                }
            }

            const response = await commonApi({ body, navigation });
            // console.log(response?.data)

            setLoading(false);

            if (response?.data?.result?.response) {
                Alert.alert("Confirmation", "Leave Submitted Successfully")
            }

            else if (response?.data?.error) {
                Alert.alert(response?.data?.error?.message, `${response?.data?.error?.data?.message}`);
            }

            else if (response == 'AxiosError: Request failed with status code 404') {
                Alert.alert("Session Expired", `Please Login Again`);
            }

            else {
                Alert.alert("Internet Connection Failed", `${response}`);

            }

        } catch (error) {
            console.error(error);
        }
    };


    return (


        <View style={Theme.SafeArea}>

            <GeneralHeader title={t('Create Tasks')} navigation={navigation} />
            {/* <Modal
            animationType='fade'
            transparent={true}
            visible={modalVisible1}
            onRequestClose={() => {
                setModalVisible1(false);
            }}


        >

            <View style={styles.container1}>
                <View style={styles.innerContainer}>

                    <View style={styles.container}> */}
            {/* <ScrollView style={styles.container} showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ justifyContent: 'space-between', flexGrow: 1 }}> */}

            <ScrollView>
                <View style={{ marginHorizontal: 16, marginTop: 24 }}>

                    {/* <View style={styles.textView}>
                    <Text style={styles.header}>{t('create-tasks')}</Text>
                    <TouchableOpacity onPress={() => setModalVisible1(false)}>
                        <ReactNativeVI Lib={'Ionicons'} name={'close'} color={COLORS.black} size={22} />
                    </TouchableOpacity>
                </View> */}

                    {/* <TouchableView
                                label={'Leave Type'}
                                header={'Select Leave Type'}
                                text={inputs?.holidayType}
                                error={inputs?.errors?.holidayType}
                                data={holidayStatus}
                                onChange={(selectedType) => handleInputChange('holidayType', selectedType)}
                            /> */}
                    <View>


                        <YearPicker
                            label={t('Project')}
                            data={holidayStatus}
                            placeholder={t('Select Project')}
                            onChange={(selectedType) => handleInputChange('holidayType', selectedType)} />
                    </View>
                    <View style={{ zIndex: 1 }}>

                        <YearPicker
                            label={t('Task')}
                            data={holidayStatus1}
                            placeholder={t('Select Task')}
                            onChange={(selectedType) => handleInputChange('holidayType', selectedType)} />

                    </View>
                    {/* <TouchableViewModal
                                header={'Select Leave Type'}
                                text={inputs?.holidayType?.name}
                                handleModal={() => setModalVisible(true)}
                                error={inputs?.errors?.holidayType}
                            /> */}

                    {/* <DatePicker
                        date={new Date()}
                        value={inputs.startDate && getFormattedDate(inputs.startDate)}
                        label={t('date')}
                        onChange={(selectedDate) => handleDateChange(selectedDate, 'startDate')}
                        showDatePicker={showStartDatePicker}
                        setShowDatePicker={setShowStartDatePicker}
                        placeholder={t('date')}
                        error={inputs?.errors?.startDate}
                    /> */}

                    <View style={{ flexDirection: 'row' }}>

                        <View style={{ flex: 1 }}>

                            {/* <DatePicker
                                date={new Date()}
                                value={inputs.startTime}
                                label={'Start Time'}
                                onChange={(selectedDate) => handleTimeChange(selectedDate, 'startTime')}
                                showDatePicker={showStartTimePicker}
                                setShowDatePicker={setShowStartTimePicker}
                                placeholder={'9:00 pm'}
                                error={inputs?.errors?.startTime}
                                mode='time'
                            /> */}

                        </View>
                        <View style={{ flex: 1 }}>
                            {/* <DatePicker
                                date={new Date()}
                                value={inputs.endTime}
                                label={'End Time'}
                                onChange={(selectedDate) => handleTimeChange(selectedDate, 'endTime')}
                                showDatePicker={showEndTimePicker}
                                setShowDatePicker={setShowEndTimePicker}
                                placeholder={'11:00 pm'}
                                error={inputs?.errors?.endTime}
                                mode='time'
                            /> */}
                        </View>
                    </View>

                    {/* <DatePicker
                                date={new Date()}
                                label={t('end-date')}
                                value={inputs?.endDate && getFormattedDate(inputs.endDate)}
                                onChange={(selectedDate) => handleDateChange(selectedDate, 'endDate')}
                                showDatePicker={showEndDatePicker}
                                setShowDatePicker={setShowEndDatePicker}
                                placeholder={t('end-date')}
                                error={inputs?.errors?.endDate}
                            /> */}

                    <TextInputField
                        label={t('working-hours')}
                        editable={false}
                        value={'2'}
                    />

                    {/* <TextInputField
                    label={'Returning to work'}
                    value={'Date'}
                    editable={false}
                /> */}

                    <View style={{ marginVertical: 8, }}>

                        <Text style={[FontStyle.Regular14, { color: COLORS.darkBlack }]}>Task Status</Text>
                        <View style={{ marginTop: 4, justifyContent: 'space-around' }}>
                            {data?.map((item) => (
                                <View key={item.id} style={{ flexDirection: 'row' }}>
                                    <View>
                                        <RadioButton.Android color={COLORS.primaryColor}
                                            value={item.id}
                                            status={selectedType?.id === item.id ? 'checked' : 'unchecked'}
                                            onPress={() => handleRadioButtonChange(item)}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center' }}>
                                        <Text style={styles.text}>{item.name}</Text>
                                    </View>
                                </View>
                            ))}
                        </View>
                    </View>

                    <TextInputField
                        label={t('Comments')}
                        placeholder={t('enter-comments')}
                        value={inputs.reason}
                        error={inputs?.errors?.reason}
                        multiline={true}
                        height={75}
                        onChangeText={(text) => handleInputChange('reason', text)}
                    />

                    <View style={styles.bottomView}>
                        <NextButton title={t('submit')} onPress={validate} />
                    </View>

                </View>
            </ScrollView>
            {loading &&
                <ActivityIndicator size={'large'} />}

            {/* <RadioSelectionModal
                            modalVisible={modalVisible}
                            setModalVisible={setModalVisible}
                            header={'Holiday Type'}
                            data={holidayStatus}
                            onChangeSelection={(leaveType) => handleInputChange('holidayType', leaveType)}
                        /> */}
            {/* </ScrollView> */}
            {/* </View>
                </View>
            </View>
        </Modal> */}

        </View>
    )
}

export default ApplyTimeSheet