import { StyleSheet } from 'react-native'
import { COLORS } from '../../../theme/colors';
import { FontStyle } from '../../../theme/FontStyle';

export const styles = StyleSheet.create({

    container: {
        margin: 16,
    },

    bottomView: {
        marginVertical: 24,
        marginHorizontal: 4,

    },

    textView: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 12,
    },

    header: {
        ...FontStyle.Regular16_500,
        fontWeight: '700',
        color: COLORS.black,
        flex: 1,

    },


    container1: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        justifyContent: 'center',
        alignItems: 'center',

    },

    innerContainer: {
        backgroundColor: COLORS.white,
        borderRadius: 8,
        width: '85%',

    },

    text: {
        ...FontStyle.Regular14_500,
        marginLeft: 4,
        color: COLORS.black,
        alignSelf: 'flex-start',
        justifyContent: 'flex-start',

    },

});