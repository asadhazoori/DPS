import { View, Text, SafeAreaView, FlatList } from 'react-native'
import React from 'react'
import Theme from '../../theme/theme'
import GeneralHeader from '../../components/Headers/GeneralHeader'
import { styles } from './styles'
import { useTranslation } from 'react-i18next'
import NotifiationCard from '../../components/Cards/NotificationsCard'

const Notifications = ({ navigation }) => {
    const { t } = useTranslation();

    const data = [
        {
            "date_to_ecube": "2023-10-13",
            "description": "Asad",
            "days": 1.0,
            "state": "confirm",
            "leaves_type": "Annual Leaves",
            "date_from_ecube": "2023-10-13",
            "remarks": "Approved for 8 days till 20 Dec",
            "caption": "12 Dec 2023 - 22 Dec 2023"
        },

        {
            "date_to_ecube": "2023-05-29",
            "description": "Muree",
            "days": 1.0,
            "state": "validate",
            "leaves_type": "Attendance Change Request",
            "date_from_ecube": "2023-05-29",
            "remarks": "Rejected beause there's no solid reason",
            "caption": "12:30 AM -> 12: 40 AM"
        },
        {
            "date_to_ecube": "2023-05-29",
            "description": "Muree",
            "days": 1.0,
            "state": "validate",
            "leaves_type": "Casual Leaves",
            "date_from_ecube": "2023-05-29",
            "remarks": "Rejected beause there's no solid reason",
            "caption": "12:30 AM -> 12: 40 AM"
        },
        {
            "date_to_ecube": "2023-05-29",
            "description": "Muree",
            "days": 1.0,
            "state": "confirm",
            "leaves_type": "Medical Claims",
            "date_from_ecube": "2023-05-29",
            remarks: 'Rs/ 3500.0 Approved for Son ',
            "caption": "Rs/ 6000"
        },
        {
            "date_to_ecube": "2023-05-29",
            "description": "Muree",
            "days": 1.0,
            "state": "confirm",
            "leaves_type": "Attendance Change Request",
            "date_from_ecube": "2023-05-29",
            "remarks": "Approved, Don't be late Again",
            "caption": "12:30 AM -> 12: 40 AM"
        },
        {
            "date_to_ecube": "2023-05-29",
            "description": "Muree",
            "days": 1.0,
            "state": "validate",
            "leaves_type": "Personal Laon",
            "date_from_ecube": "2023-05-29",
            "remarks": "Approved",
            "caption": "Rs/ 50,000 for Home Maintainance"
        },
        {
            "date_to_ecube": "2023-10-13",
            "description": "Asad",
            "days": 1.0,
            "state": "validate",
            "leaves_type": "Annual Leaves",
            "date_from_ecube": "2023-10-13",
            "remarks": "Rejected because there are too many employees are on leave",
            "caption": "10 Nov 2023 - 21 Jan 2023"
        },
        {
            "date_to_ecube": "2023-10-13",
            "description": "Asad",
            "days": 1.0,
            "state": "confirm",
            "leaves_type": "Annual Leaves",
            "date_from_ecube": "2023-10-13",
            "remarks": "Approved for 8 days till 20 Dec",
            "caption": "12 Dec 2023 - 22 Dec 2023"
        },
        {
            "date_to_ecube": "2023-10-13",
            "description": "Asad",
            "days": 1.0,
            "state": "validate",
            "leaves_type": "Annual Leaves",
            "date_from_ecube": "2023-10-13",
            "remarks": "Rejected because there are too many employees are on leave",
            "caption": "10 Nov 2023 - 21 Jan 2023"
        },
        {
            "date_to_ecube": "2023-10-13",
            "description": "Asad",
            "days": 1.0,
            "state": "validate",
            "leaves_type": "Annual Leaves",
            "date_from_ecube": "2023-10-13",
            "remarks": "Rejected because there are too many employees are on leave",
            "caption": "10 Nov 2023 - 21 Jan 2023"
        },

    ]

    return (
        <SafeAreaView style={Theme.SafeArea} >
            <GeneralHeader title={t('notifications')} navigation={navigation} />

            <View style={styles.container}>
                <View style={{ flex: 1 }}>

                    <FlatList
                        data={data}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ paddingTop: 24, paddingBottom: 16, flexGrow: 1 }}
                        renderItem={({ item, index }) => (<NotifiationCard key={index} data={item} />)}
                        keyExtractor={(item, index) => index}
                        ItemSeparatorComponent={() => <View style={{ height: 12 }} />}
                        ListEmptyComponent={() => (<Text style={[styles.dateText, { alignSelf: 'center' }]}>{t('no-record-found')}</Text>)}


                    />

                </View>

            </View>

        </SafeAreaView>
    )
}

export default Notifications