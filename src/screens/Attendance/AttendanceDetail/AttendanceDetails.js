import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Theme from '../../../theme/theme';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import {useTranslation} from 'react-i18next';
import {FontStyle} from '../../../theme/FontStyle';
import {COLORS} from '../../../theme/colors';
import {
  getFormattedDate,
  getFormattedTime,
} from '../../../utilities/helpers/CurretDate';
import AttendanceCardNew from '../../../components/Cards/AttendanceCardNew';
import {getPunchDetails} from '../../../redux/attendance/actions/getPunchDetails';
import {useDispatch} from 'react-redux';
import {getTodayPunches} from '../../../redux/attendance/actions/getTodayDetails';
import {getYesterdayPunchDetails} from '../../../redux/attendance/actions/getYesterdayPunchDetails';

const AttendanceDetails = ({navigation, route}) => {
  const {t} = useTranslation();
  const [punches, setPunches] = useState(null);
  const [loading, setLoading] = useState(true);
  const [refresh, setRefresh] = useState(false);
  const {employeeID, attendanceID, disabled, date, today, employeeAttendance} =
    route?.params;
  const dispatch = useDispatch();

  useEffect(() => {
    getDetails();
  }, []);

  const getDetails = async () => {
    try {
      if (disabled) {
        var result = null;
        if (today) {
          result = await dispatch(
            getTodayPunches(navigation, employeeID, attendanceID),
          );
        } else {
          result = await dispatch(
            getYesterdayPunchDetails(navigation, employeeID, attendanceID),
          );
        }
        setLoading(false);
        setPunches(result);
      } else {
        await getPunchDetails(navigation, employeeID, attendanceID, dispatch)
          .then(result => {
            setLoading(false);
            setPunches(result);
          })
          .catch(() => {});
      }
    } catch (error) {
      setLoading(false);
      console.log('Error while calling getPunchDetails', error);
    }
  };

  const onRefresh = async () => {
    setRefresh(true);
    await getDetails();
    setRefresh(false);
  };

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader title={t('attendance-details')} navigation={navigation} />
      <View style={styles.container}>
        <>
          <View style={styles.dateView}>
            <Text style={styles.dateText}>{getFormattedDate(date)}</Text>
          </View>

          {loading && (
            <ActivityIndicator
              color={COLORS.primaryColor}
              style={{marginTop: 12}}
            />
          )}

          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={onRefresh}
                tintColor={COLORS.primaryColor}
                colors={[COLORS.primaryColor]}
              />
            }
            contentContainerStyle={{paddingVertical: 16, flexGrow: 1}}
            style={{flex: 1}}
            data={punches}
            ListEmptyComponent={() => (
              <>
                {punches?.length < 1 && (
                  <Text style={[styles.dateText, {alignSelf: 'center'}]}>
                    {t('no-record-found')}
                  </Text>
                )}
              </>
            )}
            renderItem={({item, index}) => (
              <AttendanceCardNew
                disabled={disabled}
                key={index}
                punchIn={
                  item?.final_checkin && getFormattedTime(item?.final_checkin)
                }
                punchOut={
                  item?.final_checkout && getFormattedTime(item?.final_checkout)
                }
                onPress={() =>
                  navigation.navigate('AttendaceChangeRequest', {
                    date: date,
                    punch: item,
                    canEdit: employeeAttendance ? false : true,
                    employeeAttendance,
                  })
                }
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            ItemSeparatorComponent={() => <View style={{height: 10}} />}
          />
        </>
      </View>
    </SafeAreaView>
  );
};

export default AttendanceDetails;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 16,
    flex: 1,
    marginTop: 16,
  },
  dateView: {
    alignItems: 'flex-start',
  },

  dateText: {
    ...FontStyle.Regular16_500M,
    color: COLORS.grey5,
  },
});
