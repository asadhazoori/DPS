import { View, Text, SafeAreaView, FlatList, RefreshControl, Platform, I18nManager, Dimensions } from 'react-native';
import React, { useCallback, useEffect, useState } from 'react';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import Theme from '../../../theme/theme';
import { styles } from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import SmartButtonTrans from '../../../components/Buttons/SmartButtonTrans';
import { Icons } from '../../../assets/SvgIcons/Icons';
import { COLORS } from '../../../theme/colors';
import ManualAttendBalanceCard from '../../../components/Cards/ManualAttendBalanceCard';
import { getManualAttendReqs } from '../../../redux/attendance/actions/ManualAttendance/getManualAttendReqs';
import RequestsCard from '../../../components/Cards/RequestsCard';
import { getManualAttendCount_workType } from '../../../redux/attendance/actions/ManualAttendance/getManualAttendCount_workType';
import Carousel from 'react-native-reanimated-carousel';
import { getMangWiseManualAttendReqs } from '../../../redux/attendance/actions/ManualAttendance/getMangWiseManualAttendReqs';
import ManualAttenanceReqCard from '../../../components/Cards/ManualAttenanceReqCard';

export const window = Dimensions.get('window');

const PAGE_WIDTH = window.width;

const baseOptions = {
  vertical: false,
  width: PAGE_WIDTH,
  height: null,
};
const ManualAttendanceReqs = ({ navigation }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [refresh, setRefresh] = useState(false);
  const { manualAttendRequests, manualAttendCounts, manualAttendCards, mangrWiseManualAttendReqs } = useSelector(
    state => state?.attendance,
  );
  const [filteredData, setFilteredData] = useState(manualAttendRequests);
  const [selectedCategoryId, setSelectedCategoryId] = useState(1);
  const [managersData, setManagersData] = useState(false);
  const { isManager, } = useSelector(state => state?.employeeProfile,);

  useEffect(() => {
    if (selectedCategoryId == 1) {
      setFilteredData(manualAttendRequests);
    } else {
      setFilteredData(mangrWiseManualAttendReqs);
    }
  }, [manualAttendRequests, mangrWiseManualAttendReqs]);

  useEffect(() => {
    getData('emp');
  }, []);

  const getData = async category => {
    try {
      setRefresh(true);

      await dispatch(getManualAttendCount_workType({ navigation }));

      if (category === 'emp') {
        await dispatch(getManualAttendReqs({ navigation }));
      } else if (category === 'man') {
        setManagersData(true);
        await dispatch(getMangWiseManualAttendReqs({ navigation }));
      } else {
        if (isManager) {
          await dispatch(getMangWiseManualAttendReqs({ navigation }));
        }
        await dispatch(getManualAttendReqs({ navigation }));
      }

      setRefresh(false);
    } catch (error) {
      setRefresh(false);
      console.log('Error while calling getManualAttendance', error);
    }
  };

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader title={t('manual-attendance')} navigation={navigation} />
      <View style={styles.container}>
        <View style={styles.btnView}>
          <SmartButtonTrans
            title={t('add-new')}
            rightIcon={Icons.plus}
            onPress={() => navigation.navigate('ManualAttendance')}
          />
        </View>
        {isManager ? (
          <View
            style={{
              height: Platform.OS == 'ios' && I18nManager.isRTL ? 150 : 120,
            }}>
            <Carousel
              loop={false}
              {...baseOptions}
              style={{
                width: PAGE_WIDTH,
              }}
              mode="parallax"
              modeConfig={{
                parallaxScrollingScale: 0.9,
                parallaxScrollingOffset: 110,
              }}
              data={manualAttendCards}
              onSnapToItem={async index => {
                const selected = manualAttendCards[index];
                if (selected?.id == 1) {
                  setSelectedCategoryId(selected.id);
                  setFilteredData(manualAttendRequests);
                } else if (selected.id == 2) {
                  setSelectedCategoryId(selected.id);
                  setFilteredData(mangrWiseManualAttendReqs);
                  if (!managersData) {
                    await getData('man');
                  }
                }
              }}
              renderItem={({ item, index }) => {
                return (
                  <ManualAttendBalanceCard item={item} selectedCategoryId={selectedCategoryId} />
                );
              }}
            />
          </View>
        ) : (
          <View style={{ paddingHorizontal: 16 }}>
            <ManualAttendBalanceCard item={manualAttendCounts} />
          </View>
        )}

        <View style={styles.body}>
          <FlatList
            data={filteredData}
            removeClippedSubviews={false}
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={() => {
                  if (selectedCategoryId == 1) {
                    getData('emp');
                  } else {
                    getData('man');
                  }
                }}
                tintColor={COLORS.primaryColor}
                colors={[COLORS.primaryColor]}
              />
            }
            contentContainerStyle={{
              paddingVertical: 16,
              flexGrow: 1,
            }}
            renderItem={({ item, index }) => (
              <ManualAttenanceReqCard
                data={item}
                employeeLoan={selectedCategoryId == 1 ? false : true}
                navigation={navigation}
                getEmployeeLoans={getData}
              />

            )}
            keyExtractor={(item, index) => index}
            ItemSeparatorComponent={() => <View style={{ height: 12 }} />}
            ListEmptyComponent={() => (
              <>
                {filteredData?.length < 1 && !refresh && (
                  <Text style={[styles.dateText, { alignSelf: 'center' }]}>
                    {t('no-record-found')}
                  </Text>
                )}
              </>
            )}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ManualAttendanceReqs;
