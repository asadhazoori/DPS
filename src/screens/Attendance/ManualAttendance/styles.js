import { StyleSheet } from 'react-native';
import { FontStyle } from '../../../theme/FontStyle';
import { COLORS } from '../../../theme/colors';

export const styles = StyleSheet.create({
  bottomView: {
    marginTop: 12,
    marginHorizontal: 4,
  },
  alertIconView: {
    backgroundColor: '#F73C3C3D',
    height: 48,
    width: 48,
    borderRadius: 48,
    justifyContent: 'center',
    alignItems: 'center',
  },

  container: {
    flex: 1,
  },
  body: {
    flex: 1,
    marginHorizontal: 16,
  },
  dateText: {
    ...FontStyle.Regular12,
    fontWeight: '500',
    color: COLORS.grey5,
  },

  formContainer: {
    marginHorizontal: 16,
    padding: 16,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    gap: 12,
  },

  inputStyle: {
    height: 50,
    marginTop: 4,
    justifyContent: 'center',
    paddingHorizontal: 10,
    alignItems: 'flex-start',
  },
  btnView: {
    alignItems: 'flex-end',
    marginVertical: 16,
    marginBottom: 16,
    marginRight: 16,
  },
  dateText: {
    ...FontStyle.Regular12,
    fontWeight: '500',
    color: COLORS.grey5,
  },
});
