import { SafeAreaView, ScrollView, Text, View } from 'react-native';
import React, { useState } from 'react';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import Theme from '../../../theme/theme';
import { useTranslation } from 'react-i18next';
import DatePickerNew from '../../../components/DateTimePicker/DatePickerNew';
import {
  getFormattedDate,
  getFormattedTime,
} from '../../../utilities/helpers/CurretDate';
import { NextButton, TextInputField } from '../../../components/Inputs';
import { FontStyle } from '../../../theme/FontStyle';
import { COLORS } from '../../../theme/colors';
import DropdownComponent from '../../../components/Inputs/ElementDropdown/ElementDropdown';
import { styles } from './styles';
import { commonApi } from '../../../utilities/api/apiController';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../../redux/users/user.actions';
import { useDispatch, useSelector } from 'react-redux';
import inputValidation from '../../../utilities/Validations/YupValidate';
import { AlertModal } from '../../../components/Modals';
import { OvertimeRequestSchema } from '../../../utilities/Validations';
import { SvgXml } from 'react-native-svg';
import { DrawerIcons } from '../../../assets/SvgIcons/DrawerIcons';
import TimePicker from '../../../components/DateTimePicker/TimePicker';
import moment from 'moment';
import { ManualAttendRequestSchema } from '../../../utilities/Validations/Attendance/ManualAttendRequestSchema';
import { getManualAttendCount_workType } from '../../../redux/attendance/actions/ManualAttendance/getManualAttendCount_workType';
import { getManualAttendReqs } from '../../../redux/attendance/actions/ManualAttendance/getManualAttendReqs';

const ManualAttendance = ({ navigation }) => {
  const { t } = useTranslation();
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [showTimePicker1, setShowTimePicker1] = useState(false);
  const { workTypes } = useSelector(state => state?.attendance);
  const [editable, setEditable] = useState(false);
  const { employeeID } = useSelector(state => state?.employeeProfile);

  const [inputs, setInputs] = useState({
    workType: null,
    date: null,
    punchInTime: null,
    punchOutTime: null,
    reason: null,
    errors: null,
  });

  const [alertBox, setAlertBox] = useState({
    showBox: false,
    title: null,
    message: null,
    confirmbtn: null,
    okbtn: null,
  });

  const handleDateChange = (selectedDate, field) => {
    const dateObject = new Date(selectedDate);
    const formattedDate = dateObject.toISOString().split('T')[0];

    handleInputChange(field, formattedDate);
  };

  const handleInputChange = (field, value) => {
    if (field == 'workType') {
      setEditable(true);
      setInputs({
        ...inputs,
        [field]: value,
        errors: false,
      });
    } else {
      setInputs({
        ...inputs,
        [field]: value,
        errors: {
          ...inputs.errors,
          [field]: false,
        },
      });
    }
  };

  const validate = async () => {
    const schema = ManualAttendRequestSchema(
      t,
      editable,
      inputs?.workType?.work_type,
    );
    const result = await inputValidation(schema, inputs);
    if (result.isValidate) {
      handleSubmit();
    } else {
      setInputs(prev => ({
        ...prev,
        errors: result?.err,
      }));
    }
  };

  const handleSubmit = async () => {
    setLoading(true);

    try {
      const body = {
        params: {
          model: 'manual.attendance.wags',
          method: 'create_manual_attendance',
          args: [
            {
              employee_id: employeeID,
              date: inputs.date,
              checkin: inputs?.punchInTime,
              checkout: inputs?.punchOutTime,
              reason: inputs?.reason,
              work_type_id: inputs?.workType?.id,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });
      if (response?.data?.result) {
        if (response?.data?.result?.record_id) {
          await dispatch(getManualAttendCount_workType({ navigation }));
          setLoading(false);
          navigation.goBack();
          await dispatch(getManualAttendReqs({ navigation }));
          setTimeout(() => { Toast.show(`${t('manual-attendance-submitted-successfully')} !`); }, 300);
        }
        if (response?.data?.result?.error) {
          setLoading(false);
          setTimeout(() => {
            setAlertBox({
              showBox: true,
              title: `${'Validation Error'}`,
              message: `${response?.data?.result?.error}`,
            });
          }, 300);
        }
      } else {
        setLoading(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              setAlertBox({
                showBox: true,
                title: `${'Error'}`,
                message: `${response?.data?.error?.data?.message}`,
              });
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      setLoading(false);
      console.error(error);
    }
  };

  const closeCustomAlert = () => {
    setTimeout(() => {
      setAlertBox({ showBox: false });
    }, 300);
  };

  const handleTimeChange = (field, field1, time) => {
    if (field == 'punchInTime') {
      setShowTimePicker(false);
    } else {
      setShowTimePicker1(false);
    }

    const forTime = moment(time).utc().format('YYYY-MM-DD HH:mm:ss');
    const now = moment(time);
    const utcDateTime = now.utc().format('YYYY-MM-DD HH:mm:ss');

    setInputs({
      ...inputs,
      [field]: forTime,
      [field1]: utcDateTime,
      errors: {
        ...inputs.errors,
        [field]: false,
      },
    });
  };

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader
        title={t('create-manual-attendance')}
        navigation={navigation}
      />
      <ScrollView style={{ marginTop: 16 }} showsVerticalScrollIndicator={false}>
        <View style={styles.formContainer}>
          <View style={{ zIndex: 9999 }}>
            <DropdownComponent
              labelField={'work_type_label'}
              label={t('type')}
              data={workTypes}
              value={inputs?.workType}
              placeholder={t('select-type')}
              error={inputs?.errors?.workType}
              onChange={selectedType =>
                handleInputChange('workType', selectedType)
              }
              fullItemReq
            />
          </View>
          <DatePickerNew
            mode="date"
            date={inputs.date ? new Date(inputs.date) : new Date()}
            value={inputs.date && getFormattedDate(inputs.date)}
            label={t('date')}
            onChange={selectedDate => handleDateChange(selectedDate, 'date')}
            showDatePicker={showDatePicker}
            setShowDatePicker={setShowDatePicker}
            placeholder={t('date')}
            error={inputs?.errors?.date}
            editable={editable}
          />
          {inputs?.workType?.work_type !== 'missed_checkout' && (
            <TimePicker
              disabled={!editable ? !editable : !inputs?.date}
              showTimePicker={showTimePicker}
              setShowTimePicker={setShowTimePicker}
              labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
              initialValue={
                inputs?.punchInTime
                  ? new Date(moment.utc(inputs?.punchInTime))
                  : new Date(moment.utc(inputs?.date))
              }
              value={
                inputs?.punchInTime
                  ? getFormattedTime(inputs?.punchInTime)
                  : t('time')
              }
              onChange={time =>
                handleTimeChange('punchInTime', 'utcCheckIn', time)
              }
              label={t('punch-in')}
              color={inputs?.punchInTime ? COLORS.primaryColor : COLORS.grey3}
              inputStyle={styles.inputStyle}
              containerStyle={{ marginHorizontal: 4 }}
              error={inputs.errors?.punchInTime}
            />
          )}
          {inputs?.workType?.work_type !== 'missed_checkin' && (
            <TimePicker
              disabled={!editable ? !editable : !inputs?.date}
              containerStyle={{ marginHorizontal: 4 }}
              labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
              showTimePicker={showTimePicker1}
              setShowTimePicker={setShowTimePicker1}
              initialValue={
                inputs?.punchOutTime
                  ? new Date(moment.utc(inputs?.punchOutTime))
                  : inputs?.punchInTime
                    ? new Date(moment.utc(inputs?.punchInTime))
                    : new Date(moment.utc(inputs?.date))
              }
              color={inputs?.punchOutTime ? COLORS.primaryColor : COLORS.grey3}
              value={
                inputs?.punchOutTime
                  ? getFormattedTime(inputs?.punchOutTime)
                  : t('time')
              }
              onChange={time =>
                handleTimeChange('punchOutTime', 'utcCheckOut', time)
              }
              error={inputs.errors?.punchOutTime}
              label={t('punch-out')}
              inputStyle={styles.inputStyle}
            />
          )}
          <TextInputField
            editable={editable}
            label={t('reason')}
            placeholder={t('reason-plcholder')}
            value={inputs.reason}
            error={inputs?.errors?.reason}
            labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
            onChangeText={text => handleInputChange('reason', text)}
          />
          <View style={styles.bottomView}>
            <NextButton
              title={t('submit')}
              onPress={validate}
              loader={loading}
            />
          </View>
        </View>
      </ScrollView>
      <AlertModal
        visible={alertBox.showBox}
        icon={
          <View style={styles.alertIconView}>
            <SvgXml xml={DrawerIcons.attendance} />
          </View>
        }
        title={alertBox.title}
        message={alertBox.message}
        onClose={closeCustomAlert}
        confirmbtn={alertBox.confirmbtn}
      />
    </SafeAreaView>
  );
};

export default ManualAttendance;
