import {StyleSheet} from 'react-native';
import {COLORS} from '../../../theme/colors';
import {FontStyle} from '../../../theme/FontStyle';
import Theme from '../../../theme/theme';

export const styles = StyleSheet.create({
  container: {
    marginHorizontal: 16,
    flex: 1,
  },

  mapView: {
    marginTop: 16,
    width: '100%',
    borderRadius: 8,
    backgroundColor: COLORS.white,
    padding: 8,
  },

  innerMapView: {
    flex: 1,
    overflow: 'hidden',
    borderRadius: 8,
  },
  tabBar: {
    borderColor: 'red',
    flexDirection: 'row',
    marginTop: 12,
    marginBottom: 24,
    marginHorizontal: 16,
  },

  iconView: {
    justifyContent: 'center',
  },

  TabViewCreateContainer: {
    backgroundColor: 'transparent',
    elevation: 0,
    flex: 1,
  },
  dateView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  dateText: {
    ...FontStyle.Regular16_500M,
    color: COLORS.grey5,
  },

  TabViewIndicator: {
    backgroundColor: COLORS.blue,
    borderRadius: 16,
  },

  bodyView: {
    borderWidth: 1,
    flex: 1,
  },

  iconView: {
    backgroundColor: '#d0f7e5',
    height: 48,
    width: 48,
    borderRadius: 48,
    justifyContent: 'center',
    alignItems: 'center',
  },

  container2: {
    marginTop: 16,
    borderColor: 'green',
  },
  container1: {
    marginHorizontal: 4,
    flex: 1,
  },

  inputView: {
    ...Theme.Shadow,
    marginTop: 4,
    paddingHorizontal: 8,
    paddingVertical: 14,
    flexDirection: 'row',
    alignItems: 'center',
  },

  label: {
    ...FontStyle.Regular14_500,
    fontWeight: '400',
    paddingHorizontal: 1,
    marginLeft: 2,
  },

  header: {
    ...FontStyle.Regular16_500M,
    color: COLORS.darkBlack,
    fontWeight: '700',
    textAlign: 'left',
  },
  daysText: {
    ...FontStyle.Regular12,
    color: COLORS.darkBlack,
    fontWeight: '400',
    textAlign: 'left',
  },

  editButon: {
    ...Theme.Shadow,
    padding: 6,
    paddingHorizontal: 10,
    width: 45,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 4,
  },

  punchText: {
    ...FontStyle.Regular14_500,
    color: COLORS.darkBlack,
    flex: 1,
    textAlign: 'left',
  },
});
