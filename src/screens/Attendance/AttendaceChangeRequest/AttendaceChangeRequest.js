
import { Alert, SafeAreaView, View, Text, ScrollView, TouchableOpacity, KeyboardAvoidingView, Platform } from 'react-native'
import React, { useEffect, useState } from 'react'
import { styles } from './styles';
import { COLORS } from '../../../theme/colors';
import { FontStyle } from '../../../theme/FontStyle';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import Theme from '../../../theme/theme';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { useTranslation } from 'react-i18next';
import { getFormattedDate, getFormattedTime } from '../../../utilities/helpers/CurretDate';
import { SvgXml } from 'react-native-svg';
import { Icons } from '../../../assets/SvgIcons/Icons';
import { SmartButton, TextInputField } from '../../../components/Inputs';
import Geocoder from 'react-native-geocoding';
import TimePicker from '../../../components/DateTimePicker/TimePicker';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../../redux/users/user.actions';
import moment from 'moment';
import { commonApi } from '../../../utilities/api/apiController';
import inputValidation from '../../../utilities/Validations/YupValidate';
import { ChangeAttendanceSchema } from '../../../utilities/Validations';
import { AlertModal, Loader } from '../../../components/Modals';
import ReactNativeVI from '../../../components/Helpers/ReactNativeVI';
import { getAllChangeRequests } from '../../../redux/attendance/actions/getAllChangeRequests';
import { useDispatch, useSelector } from 'react-redux';
import ChangedRequestDetailCard from '../../../components/Cards/ChangedRequestDetailCard';

const AttendaceChangeRequest = ({ navigation, route }) => {

    const { t } = useTranslation();
    const { date, punch, canEdit, employeeAttendance } = route?.params;
    const dispatch = useDispatch();


    let state = ''
    let status = ''
    let color = ''


    switch (punch?.state) {
        case 'change_request':
            status = t('pending')
            state = 'Cancel'
            color = COLORS.yellow
            break;

        case 'rejected':
            status = t('rejected')
            state = 'Rejected'
            color = COLORS.red1
            break;

        case 'approved':
            status = t('approved')
            state = 'Approved'
            color = COLORS.green

            break;

        default:
            break;
    }


    const [editable, setEditable] = useState(false);
    const [showTimePicker, setShowTimePicker] = useState(false);
    const [showTimePicker1, setShowTimePicker1] = useState(false);
    const [loading, setLoading] = useState(false);
    const { employeeID } = useSelector((state) => state?.employeeProfile);

    const [inputs, setInputs] = useState({

        punchInLat: punch?.checkin_latitude,
        punchInLong: punch?.checkin_longitude,

        punchOutLat: punch?.checkout_latitude,
        punchOutLong: punch?.checkout_longitude,

        punchInLoc: null,
        punchOutLoc: null,

        punchInPre: punch?.final_checkin,
        punchOutPre: punch?.final_checkout,

        punchInUpd: punch?.final_checkin,
        punchOutUpd: punch?.final_checkout,

        utcCheckIn: null,
        utcCheckOut: null,
        reason: null,
    });

    const [alertBox, setAlertBox] = useState({
        showBox: false,
        title: null,
        message: null,
        confirmbtn: null,
    });

    const handleTimeChange = (field, field1, time) => {

        if (field == 'punchInUpd') {
            setShowTimePicker(false);
        } else {
            setShowTimePicker1(false);
        }

        const forTime = moment(time).utc().format("YYYY-MM-DD HH:mm:ss")
        const now = moment(time)
        const utcDateTime = now.utc().format('YYYY-MM-DD HH:mm:ss');

        setInputs({
            ...inputs,
            [field]: forTime,
            [field1]: utcDateTime,
            errors: {
                ...inputs.errors,
                reqTime: false,
                punchOutUpd: false
            }
        })

    };

    const handleInputChange = (field, value) => {
        setInputs({
            ...inputs,
            [field]: value,
            errors: {
                ...inputs.errors,
                [field]: false
            }
        })
    }


    const validate = async () => {

        const schema = ChangeAttendanceSchema(t);
        const result = await inputValidation(schema, inputs)

        if (result.isValidate) {
            changeAttendance();

        } else {
            setInputs(prev => ({
                ...prev,
                errors: result?.err
            }))
        }

    }

    const closeCustomAlert = () => {
        setTimeout(() => {
            setAlertBox({ showBox: false });
        }, 300);
        navigation.goBack();
    }


    const changeAttendance = async () => {
        try {
            setLoading(true)

            const body = {
                "params": {
                    "model": "raw.attendance.wags",
                    "method": "update_requested_time",
                    "args": [
                        {
                            "punch_attendance_id": punch?.punch_attendance_id,
                            ...(inputs?.utcCheckIn && { "requested_checkin": inputs.utcCheckIn }),
                            ...(inputs?.utcCheckOut && { "requested_checkout": inputs.utcCheckOut }),
                            "remarks": inputs?.reason
                        }
                    ],
                    "kwargs": {}
                }
            }


            const response = await commonApi({ body, navigation });

            if (response?.data?.result == true) {
                await dispatch(getAllChangeRequests({
                    navigation,
                    employeeID
                }))
                setLoading(false);
                setTimeout(() => {
                    setAlertBox({
                        showBox: true,
                        title: t('confirmation'),
                        message: t('change-request-sent'),
                        confirmbtn: false
                    });
                }, 300)

            }
            else {
                setLoading(false);


                if (response?.data?.error) {
                    if (response?.data?.error?.message == "Odoo Session Expired") {
                        dispatch(logout_user(false))
                        setTimeout(() => { Toast.show(t('session-expired')); }, 500)

                        navigation.reset({
                            index: 0,
                            routes: [
                                { name: 'Login' }
                            ],
                        })

                    }
                    else {
                        setTimeout(() => {
                            Alert.alert(response?.data?.error?.message, `Method: update_requested_time\n${response?.data?.error?.data?.message}`);
                        }, 300);
                    }
                }

                else if (response == 'AxiosError: Request failed with status code 404') {
                    dispatch(logout_user(false))
                    setTimeout(() => { Toast.show(t('session-expired')); }, 500)
                    navigation.reset({
                        index: 0,
                        routes: [
                            { name: 'Login' }
                        ],
                    })
                }
            }
        }

        catch (error) {
            setLoading(false)
            console.log(error, "error");
        }
    };


    const Input = ({ label }) => (

        <View
            style={[styles.container1, { borderColor: 'blue' }]}>
            <View>
                <Text style={[styles.header, { color: COLORS.darkBlack, textAlign: 'left' }]}>{label}</Text>
            </View>

            <View
                style={[{
                    flex: 1,
                    marginTop: 4,
                    flexDirection: 'row',
                    gap: 12
                }]}>

                <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 8 }}>

                    <Text style={styles.punchText}>{`${t('punched-in')}:`}</Text>
                    <Text style={[styles.punchText, { marginLeft: 2, fontWeight: '700' }]}>{getFormattedTime(punch?.final_checkin)}</Text>

                </View>
                <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 8, }}>
                    <Text style={styles.punchText}>{`${t('punched-out')}:`}</Text>
                    <Text style={[styles.punchText, { marginLeft: 2, fontWeight: '700' }]}>{punch?.final_checkout ? getFormattedTime(punch?.final_checkout) : t('missing')}</Text>
                </View>
            </View>


        </View >

    )
    const Input1 = ({ icon, label, val1, val2, placeholder, error }) => {

        const date1 = new Date(moment.utc(inputs?.punchInUpd, "YYYY-MM-DD HH:mm:ss"))
        const date2 = inputs?.punchOutUpd ?
            new Date(moment.utc(inputs?.punchOutUpd, "YYYY-MM-DD HH:mm:ss"))
            :
            new Date(moment.utc(inputs?.punchInUpd, "YYYY-MM-DD HH:mm:ss"));

        return (

            <View
                style={[styles.container1, { borderColor: 'blue' }]}>
                <View>
                    <Text style={[styles.header, { color: COLORS.darkBlack, textAlign: 'left' }]}>{label}</Text>

                </View>

                <View style={{ flexDirection: 'row', flex: 1, gap: 12 }}>
                    <TimePicker
                        disabled={!editable}
                        showTimePicker={showTimePicker}
                        setShowTimePicker={setShowTimePicker}
                        initialValue={date1}
                        value={getFormattedTime(inputs?.punchInUpd)}
                        color={inputs?.utcCheckIn ? COLORS.primaryColor : COLORS.grey3}
                        onChange={(time) => handleTimeChange('punchInUpd', "utcCheckIn", time)}
                        label={`${t('punched-in')}:`}
                        containerStyle={{ flexDirection: 'row', flex: 1, alignItems: 'center', }}
                    />
                    <TimePicker
                        disabled={!editable}
                        showTimePicker={showTimePicker1}
                        setShowTimePicker={setShowTimePicker1}
                        initialValue={date2}
                        color={inputs?.utcCheckOut ? COLORS.primaryColor : COLORS.grey3}
                        value={inputs?.punchOutUpd ? getFormattedTime(inputs?.punchOutUpd) : t('missing')}
                        onChange={(time) => handleTimeChange('punchOutUpd', "utcCheckOut", time)}
                        label={`${t('punched-out')}:`}
                        containerStyle={{ flexDirection: 'row', flex: 1, alignItems: 'center', }}
                    />
                </View>
                {error &&
                    < Text style={[styles.label, { color: COLORS.red, marginLeft: 2, fontSize: 12, marginTop: 4 }]}>{error}</Text>
                }


            </View>

        )
    }


    const getLocation = async (lat, long, field) => {

        try {
            const response = await Geocoder.from(lat, long);
            const addressComponent = response?.results[0]?.formatted_address;

            setInputs((prevInputs) => ({
                ...prevInputs,
                [field]: addressComponent,
            }));
        } catch (error) {
            console.log(error);
            setTimeout(() => { Toast.show(t('internet-connection-failed')); }, 300);
        }
    }

    const getLocations = async () => {
        await getLocation(inputs?.punchInLat, inputs?.punchInLong, 'punchInLoc')

        if (inputs?.punchOutLat && inputs?.punchOutLong) {
            await getLocation(inputs?.punchOutLat, inputs?.punchOutLong, 'punchOutLoc')
        }
        else {
            setInputs((prevInputs) => ({
                ...prevInputs,
                ['punchOutLoc']: t('missing'),
            }));
        }

    }


    useEffect(() => {
        Geocoder.init("AIzaSyDWx388FSWws16YhFRgUu4AW6pUy0miVUk");
        getLocations();

    }, [])


    return (
        <SafeAreaView style={Theme.SafeArea}>
            <GeneralHeader title={t('punch-details')} navigation={navigation} />
            <KeyboardAvoidingView
                style={styles.container}
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            >
                <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingVertical: 16 }}>

                    <View style={styles.dateView}>
                        <Text style={styles.dateText}>{getFormattedDate(date)}</Text>

                        {canEdit &&
                            <TouchableOpacity
                                onPress={() => { setEditable(true) }}
                                activeOpacity={0.7}
                                style={styles.editButon} >
                                <SvgXml xml={Icons.editIcon} />
                            </TouchableOpacity>
                        }
                    </View>


                    <View style={styles.mapView}>
                        {((inputs?.punchInLat && inputs?.punchInLong) || (inputs?.punchOutLat && inputs?.punchOutLong)) &&
                            <View style={styles.innerMapView}>

                                <MapView
                                    style={{ flex: 1, height: 200 }}

                                    initialRegion={{
                                        latitude: inputs?.punchInLat ? parseFloat(inputs?.punchInLat) : parseFloat(inputs?.punchOutLat),
                                        longitude: inputs?.punchInLong ? parseFloat(inputs?.punchInLong) : parseFloat(inputs?.punchOutLong),
                                        latitudeDelta: 0.01,
                                        longitudeDelta: 0.01

                                    }}

                                    region={{
                                        latitude: inputs?.punchInLat ? parseFloat(inputs?.punchInLat) : parseFloat(inputs?.punchOutLat),
                                        longitude: inputs?.punchInLong ? parseFloat(inputs?.punchInLong) : parseFloat(inputs?.punchOutLong),
                                        latitudeDelta: 0.01,
                                        longitudeDelta: 0.01

                                    }}

                                    // showsUserLocation={true}
                                    zoomControlEnabled={true}
                                    provider={PROVIDER_GOOGLE}
                                >

                                    {inputs?.punchInLat && inputs?.punchInLong &&
                                        <Marker

                                            coordinate={{
                                                latitude: inputs?.punchInLat ? parseFloat(inputs?.punchInLat) : 0,
                                                longitude: inputs?.punchInLong ? parseFloat(inputs?.punchInLong) : 0,
                                            }}
                                            title={t('Punch-Out')}
                                        />
                                    }


                                    {
                                        inputs?.punchOutLat && inputs?.punchOutLong &&
                                        <Marker

                                            coordinate={{
                                                latitude: inputs?.punchOutLat ? parseFloat(inputs?.punchOutLat) : 0,
                                                longitude: inputs?.punchOutLong ? parseFloat(inputs?.punchOutLong) : 0,
                                            }}
                                            title={t('Punch-Out')}
                                        />}

                                </MapView>
                            </View>
                        }
                        <View style={{ marginTop: 12, }}>

                            <View style={{ flexDirection: 'row', }}>
                                <View style={{ flex: 1.4, marginRight: 2 }}>
                                    <Text style={[FontStyle.Regular14, { color: COLORS.primaryColor, }]}>{`${t('punch-in')} : `}</Text>
                                </View>
                                <View style={{ flex: 4, alignItems: 'flex-start' }}>
                                    <Text style={[FontStyle.Regular14_500, { color: COLORS.black }]}>{inputs?.punchInLoc}</Text>
                                </View>

                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 8, }}>
                                <View style={{ flex: 1.4, marginRight: 2 }}>

                                    <Text style={[FontStyle.Regular14, { color: COLORS.primaryColor, }]}>{`${t('punch-out')} : `}</Text>
                                </View>

                                <View style={{ flex: 4, alignItems: 'flex-start' }}>
                                    <Text style={[FontStyle.Regular14_500, { color: COLORS.black }]}>{inputs?.punchOutLoc}</Text>
                                </View>

                            </View>
                        </View>
                    </View>

                    {canEdit ?

                        <View style={styles.container2}>

                            <View style={{ gap: 8 }}>

                                <Input
                                    label={t('current-record')}
                                />

                                <Input1 label={t('updated-record')}
                                    val1={punch?.final_checkin}
                                    val2={punch?.final_checkout}
                                    error={inputs?.errors?.reqTime || inputs?.errors?.punchOutUpd}
                                />

                                <TextInputField
                                    label={t('reason')}
                                    editable={editable}
                                    placeholder={t('reason-placeholder')}
                                    labelStyle={{ fontWeight: '700' }}
                                    height={50}
                                    value={inputs.reason}
                                    error={inputs?.errors?.reason}
                                    onChangeText={(text) => handleInputChange('reason', text)}
                                />
                            </View>

                            <View style={{ marginVertical: 16, justifyContent: 'flex-end', flexDirection: 'row', marginRight: 4 }}>
                                <SmartButton title={t('update')} handlePress={validate} disabled={!editable} />
                            </View>

                        </View>
                        :

                        <ChangedRequestDetailCard navigation={navigation} punch={punch} status={status} state={state} color={color} employeeAttendance={employeeAttendance} />
                    }

                    <Loader title={t('loading')} loading={loading} setLoading={setLoading} />

                    <AlertModal
                        visible={alertBox.showBox}
                        icon={<View style={styles.iconView}>
                            <ReactNativeVI Lib={'MaterialIcons'} name={'check'} color={'#3BCA78'} size={26} />
                        </View>}
                        title={alertBox.title}
                        message={alertBox.message}
                        onClose={closeCustomAlert}
                        confirmbtn={alertBox.confirmbtn}
                    />

                </ScrollView>
            </KeyboardAvoidingView>
        </SafeAreaView >
    )
}

export default AttendaceChangeRequest
