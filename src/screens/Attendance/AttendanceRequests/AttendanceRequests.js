import {
  View,
  SafeAreaView,
  StyleSheet,
  FlatList,
  Text,
  ActivityIndicator,
  RefreshControl,
  Dimensions,
  Platform,
  I18nManager,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useTranslation} from 'react-i18next';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import Theme from '../../../theme/theme';
import {COLORS} from '../../../theme/colors';
import {FontStyle} from '../../../theme/FontStyle';
import RequestsBalanceCard from '../../../components/Cards/RequestsBalanceCard';
import AttendanceRequestsCard from '../../../components/Cards/AttendanceRequestsCard';
import {useDispatch, useSelector} from 'react-redux';
import {getAllChangeRequests} from '../../../redux/attendance/actions/getAllChangeRequests';
import {getAllEmployeeRequests} from '../../../redux/attendance/actions/getAllEmployeeRequests';
import Carousel from 'react-native-reanimated-carousel';
import AttendanceRequestsBalanceCard from '../../../components/Cards/AttendanceRequestsBalanceCard';

export const window = Dimensions.get('window');

const PAGE_WIDTH = window.width;

const baseOptions = {
  vertical: false,
  width: PAGE_WIDTH,
  height: null,
};

const AttendanceRequests = ({navigation}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const [refresh, setRefresh] = useState(false);
  const isManager = useSelector(state => state?.employeeProfile?.isManager);
  const {employeeID} = useSelector(state => state?.employeeProfile);
  const requests = useSelector(state => state?.attendance?.changeRequests);
  const employeeRequests = useSelector(
    state => state?.attendance?.empChangeRequests,
  );
  const cards = [{id: 1}, {id: 2}];
  const [filteredData, setFilteredData] = useState(requests);
  const [selectedCategoryId, setSelectedCategoryId] = useState(1);

  const pendingReqs = requests?.filter(
    item => item?.state === 'change_request',
  ).length;
  const rejectedReqs = requests?.filter(
    item => item?.state === 'rejected',
  ).length;
  const approvedReqs = requests?.filter(
    item => item?.state === 'approved',
  ).length;

  const pendingEmpReqs = employeeRequests?.filter(
    item => item?.state === 'change_request',
  ).length;
  const rejectedEmpReqs = employeeRequests?.filter(
    item => item?.state === 'rejected',
  ).length;
  const approvedEmpReqs = employeeRequests?.filter(
    item => item?.state === 'approved',
  ).length;

  const getRequests = async () => {
    await dispatch(
      getAllChangeRequests({
        navigation,
        employeeID,
      }),
    );
  };

  useEffect(() => {
    getRequests();
    if (isManager) {
      getEmployeeRequests();
    }
  }, []);

  useEffect(() => {
    if (selectedCategoryId == 1) {
      setFilteredData(requests);
    } else {
      setFilteredData(employeeRequests);
    }
  }, [requests, employeeRequests]);

  const onRefresh = async () => {
    setRefresh(true);
    if (selectedCategoryId == 1) {
      await getRequests();
    } else {
      await getEmployeeRequests();
    }

    setRefresh(false);
  };

  const getEmployeeRequests = async () => {
    try {
      await dispatch(
        getAllEmployeeRequests({
          navigation,
          employeeID,
        }),
      );
    } catch (error) {
      setRefresh(false);
      console.log('Error while calling getAllEmployeeRequests', error);
    }
  };

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader
        title={t('attendance-change-requests')}
        navigation={navigation}
      />
      <View style={styles.container}>
        {isManager ? (
          <View
            style={{
              marginTop: 16,
              height: Platform.OS == 'ios' && I18nManager.isRTL ? 150 : 120,
            }}>
            <Carousel
              loop={false}
              {...baseOptions}
              style={{
                width: PAGE_WIDTH,
              }}
              mode="parallax"
              modeConfig={{
                parallaxScrollingScale: 0.9,
                parallaxScrollingOffset: 110,
              }}
              data={cards}
              onSnapToItem={async index => {
                const selected = cards[index];
                if (selected?.id == 1) {
                  setFilteredData(requests);
                } else if (selected.id == 2) {
                  setFilteredData(employeeRequests);
                }
                setSelectedCategoryId(selected.id);
              }}
              renderItem={({item, index}) => {
                return (
                  <AttendanceRequestsBalanceCard
                    selectedCategoryId={selectedCategoryId}
                    pending={item.id == 1 ? pendingReqs : pendingEmpReqs}
                    approved={item.id == 1 ? approvedReqs : approvedEmpReqs}
                    rejected={item.id == 1 ? rejectedReqs : rejectedEmpReqs}
                    item={item}
                  />
                );
              }}
            />
          </View>
        ) : (
          <View style={{marginHorizontal: 16, marginTop: 16}}>
            <AttendanceRequestsBalanceCard
              pending={pendingReqs}
              approved={approvedReqs}
              rejected={rejectedReqs}
            />
          </View>
        )}

        <View style={styles.body}>
          <FlatList
            data={filteredData}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={onRefresh}
                tintColor={COLORS.primaryColor}
                colors={[COLORS.primaryColor]}
              />
            }
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              paddingBottom: 16,
              paddingTop: isManager ? 0 : 16,
              flexGrow: 1,
            }}
            renderItem={({item, index}) => (
              <AttendanceRequestsCard
                getAllEmployeeRequests={getAllEmployeeRequests}
                key={index}
                data={item}
                navigation={navigation}
              />
            )}
            keyExtractor={(item, index) => index}
            ItemSeparatorComponent={() => <View style={{height: 12}} />}
            ListEmptyComponent={() => (
              <>
                {filteredData?.length < 1 && !refresh && (
                  <Text style={[styles.dateText, {alignSelf: 'center'}]}>
                    {t('no-record-found')}
                  </Text>
                )}
              </>
            )}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default AttendanceRequests;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    flex: 1,
    marginHorizontal: 16,
  },
  dateText: {
    ...FontStyle.Regular14_500,
    fontWeight: '500',
    color: COLORS.grey5,
  },
});
