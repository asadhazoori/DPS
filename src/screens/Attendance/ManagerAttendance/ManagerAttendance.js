import {
  View,
  Text,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import Theme from '../../../theme/theme';
import {useTranslation} from 'react-i18next';
import {getManagerWiseEmployees} from '../../../redux/attendance/actions/getManagerWiseEmployees';
import {useDispatch, useSelector} from 'react-redux';
import {COLORS} from '../../../theme/colors';
import {FontStyle} from '../../../theme/FontStyle';

const ManagerAttendance = ({navigation}) => {
  const {t} = useTranslation();
  const {dispatch} = useDispatch();
  const {employeeID} = useSelector(state => state.employeeProfile);
  const [loading, setLoading] = useState(true);
  const [employees, setEmployees] = useState(false);
  const [refresh, setRefresh] = useState(false);

  const getEmployees = async () => {
    await getManagerWiseEmployees({navigation, employeeID, dispatch})
      .then(result => {
        setLoading(false);
        setEmployees(result);
      })
      .catch(error => {
        setLoading(false);
        console.log(error);
      });
  };

  useEffect(() => {
    getEmployees();
  }, []);

  const onRefresh = async () => {
    setRefresh(true);
    await getEmployees();
    // setLoading(false);
    setRefresh(false);
  };

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader title={t('attendance')} navigation={navigation} />
      <View style={{flex: 1}}>
        <TouchableOpacity
          activeOpacity={0.8}
          style={[
            Theme.Shadow,
            {
              marginTop: 16,
              padding: 20,
              marginHorizontal: 16,
              backgroundColor: COLORS.white,
            },
          ]}
          onPress={() => {
            navigation.navigate('Attendance');
          }}>
          <Text
            style={[
              FontStyle.Regular16_500M,
              {color: COLORS.blue, textAlign: 'left'},
            ]}>
            {t('personal')}
          </Text>
        </TouchableOpacity>

        <View style={{flex: 1}}>
          <Text
            style={[
              FontStyle.Regular16,
              {
                marginLeft: 16,
                marginTop: 12,
                color: COLORS.black,
                textAlign: 'left',
              },
            ]}>
            {t('employees-attendance')}
          </Text>
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={onRefresh}
                tintColor={COLORS.primaryColor}
                colors={[COLORS.primaryColor]}
              />
            }
            contentContainerStyle={{
              paddingVertical: 16,
              flexGrow: 1,
              paddingHorizontal: 16,
            }}
            data={employees}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity
                activeOpacity={0.8}
                style={[
                  Theme.Shadow,
                  {
                    padding: 16,
                    flex: 1,
                    backgroundColor: COLORS.white,
                    alignItems: 'flex-start',
                  },
                ]}
                onPress={() => {
                  navigation.navigate('Attendance', {
                    employeeAttendance: true,
                    employeeID: item?.employee_id,
                    workingHours: item?.working_hours,
                  });
                }}>
                <Text style={FontStyle.Regular16_500M}>
                  {item.employee_name}
                </Text>
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
            ListFooterComponent={() =>
              loading && (
                <ActivityIndicator
                  color={COLORS.primaryColor}
                  size={24}
                  style={{marginTop: 12}}
                />
              )
            }
            ItemSeparatorComponent={() => <View style={{height: 14}} />}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ManagerAttendance;
