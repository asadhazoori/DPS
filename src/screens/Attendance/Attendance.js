import {
  View,
  SafeAreaView,
  StyleSheet,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import GeneralHeader from '../../components/Headers/GeneralHeader';
import Theme from '../../theme/theme';
import { useTranslation } from 'react-i18next';
import AttendanceCardNew from '../../components/Cards/AttendanceCardNew';
import { COLORS } from '../../theme/colors';
import { useDispatch, useSelector } from 'react-redux';
import { requestAndGetLocation } from '../../redux/location/location.action';
import { AlertModal, Loader } from '../../components/Modals';
import { getDailyAttendance } from '../../redux/attendance/actions/getDailyAttendance';
import {
  getFormattedDate,
  getFormattedTime,
} from '../../utilities/helpers/CurretDate';
import { formattedDuration } from '../../utilities/helpers/CalculateTimeDuration';
import ReactNativeVI from '../../components/Helpers/ReactNativeVI';
import { getPermissionJust } from '../../utilities/helpers/AccessLocation';
import TodayAttendanceReport from '../../components/Cards/TodayAttendanceReport';
import { useIsFocused } from '@react-navigation/native';
import { setActiveScreen } from '../../redux/settings/settings.actions';
import { getTodayPunches } from '../../redux/attendance/actions/getTodayDetails';
import { getYesterdayPunchDetails } from '../../redux/attendance/actions/getYesterdayPunchDetails';

const Attendance = ({ navigation, route }) => {
  const params = route?.params;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const { lat, lng } = useSelector(state => state?.location);
  const { employeeID } = useSelector(state => state?.employeeProfile);
  const profileData = useSelector(state => state?.employeeProfile);
  const {
    activeAttendance,
    monthlyDatewiseAttendance,
    attendanceRecords,
    yesterdayAttendance,
    yesterdayAttendanceRecords,
  } = useSelector(state => state?.attendance);
  const punchStatus = activeAttendance?.last_checkout
    ? 'Punch-In'
    : !activeAttendance?.checkin
      ? 'Punch-In'
      : 'Punch-Out';
  const [loading1, setLoading1] = useState(
    params?.employeeAttendance ? true : false,
  );
  const [visibleData, setVisibleData] = useState(10);
  const [data, setData] = useState({
    activeAttendance: null,
    monthlyDatewiseAttendance: [],
    attendanceRecords: [],
    yesterdayAttendance: null,
    yesterdayAttendanceRecords: [],
  });

  const [dataToShow, setDataToShow] = useState([]);

  const [alertBox, setAlertBox] = useState({
    showBox: false,
    title: null,
    message: null,
    confirmbtn: null,
  });

  useEffect(() => {
    if (params?.employeeAttendance) {
      getData();
    } else {
      setData({
        activeAttendance: activeAttendance,
        monthlyDatewiseAttendance: monthlyDatewiseAttendance,
        attendanceRecords: attendanceRecords,
        yesterdayAttendance: yesterdayAttendance,
        yesterdayAttendanceRecords: yesterdayAttendanceRecords,
      });
    }
  }, [activeAttendance, monthlyDatewiseAttendance, attendanceRecords]);

  useEffect(() => {
    if (data && data.monthlyDatewiseAttendance) {
      setDataToShow(
        Object.keys(data.monthlyDatewiseAttendance)
          .slice(0, 10)
          .map(date => ({ ...data.monthlyDatewiseAttendance[date], date })),
      );
      setPage(1);

    }
  }, [data]);

  useEffect(() => {
    if (isFocused) {
      if (
        !params?.employeeAttendance &&
        (params?.refreshDisabled == undefined ||
          params?.refreshDisabled == true)
      ) {
        dispatch(
          getDailyAttendance({
            navigation,
            employeeID: employeeID,
            setLoading1: setLoading1,
          }),
        ).then(() => setLoading1(false));
      }
      dispatch(setActiveScreen('Attendance'));
    }

    return () => {
      dispatch(setActiveScreen('other'));
    };
  }, [isFocused, dispatch]);

  const totalPage = 3;
  const [page, setPage] = useState(1);

  const fetchLocation = async () => {
    try {
      const result = await getPermissionJust();
      if (result == 'fine') {
        await dispatch(requestAndGetLocation());
      } else if (result == 'GPS_disabled') {
        setTimeout(() => {
          setAlertBox({
            showBox: true,
            confirmbtn: false,
            title: t('gps-loc-disabled'),
            message: t('gps-loc-disabled-msg'),
          });
        }, 300);
      } else if (result == 'denied') {
        setTimeout(() => {
          setAlertBox({
            showBox: true,
            title: t('permission-denied'),
            message: t('permission-denied-msg'),
            confirmbtn: true,
          });
        }, 300);
      }
    } catch (error) {
      console.log('Error in Getting Permission');
    }
  };

  const closeCustomAlert = () => {
    setTimeout(() => {
      setAlertBox({ showBox: false });
    }, 300);
  };

  const renderItem = ({ item, index }) => {
    if (
      index == 0 &&
      (item.overlapping || data?.activeAttendance?.overlapping)
    ) {
      const overlapPunchStatus = data.yesterdayAttendance?.last_checkout
        ? 'Punch-In'
        : !data.yesterdayAttendance?.checkin
          ? 'Punch-In'
          : 'Punch-Out';
      return (
        <TodayAttendanceReport
          date={item.date && getFormattedDate(item.date)}
          firstCheckIn={
            data.yesterdayAttendance?.checkin &&
            getFormattedTime(data.yesterdayAttendance?.checkin)
          }
          lastCheckIn={
            data.yesterdayAttendance?.last_checkin
              ? getFormattedTime(data.yesterdayAttendance?.last_checkin)
              : '----'
          }
          lastCheckOut={
            data.yesterdayAttendance?.last_checkout
              ? getFormattedTime(data.yesterdayAttendance?.last_checkout)
              : '----'
          }
          punchStatus={
            overlapPunchStatus == 'Punch-In' ? t('punch-in') : t('punch-out')
          }
          punchPress={() =>
            navigation.navigate('LocationOld', {
              activeAttendance: data.yesterdayAttendance,
              punchStatus: overlapPunchStatus,
              lat: lat,
              long: lng,
              log_attendance_id: data.yesterdayAttendance?.log_attendance_id,
            })
          }
          morePress={() =>
            navigation.navigate('AttendanceDetails', {
              employeeID: params?.employeeAttendance
                ? params.employeeID
                : employeeID,
              attendanceID: data.yesterdayAttendance?.log_attendance_id,
              disabled: true,
              date: item?.date,
              today: false,
            })
          }
          activeAttendance={data.yesterdayAttendance}
          attendanceRecords={data.yesterdayAttendanceRecords}
          employeeAttendance={params?.employeeAttendance}
          workingHours={
            params?.workingHours
              ? params.workingHours
              : profileData?.data?.working_hours
          }
        />
      );
    } else {
      const { hours, minutes } = formattedDuration(item.total_time);
      const formHours = `${hours > 0 ? `${hours} ${t('hours')}\n` : ''}`;
      const formMins = `${hours > 0 && minutes == 0 ? '' : `${minutes} ${t('minutes')}`
        }`;
      const time2 = `${formHours && formHours}${formMins && formMins}`;
      const disabled =
        item?.attendance_status == 'absent' ||
        item?.attendance_status == 'holiday' ||
        (!item.checkin && !item.checkout);

      return (
        <AttendanceCardNew
          key={index}
          item={item}
          date={item.date && getFormattedDate(item.date)}
          punchIn={item.checkin && getFormattedTime(item.checkin)}
          punchOut={item.checkout && getFormattedTime(item.checkout)}
          time={item.total_time === 0 ? '----' : time2}
          disabled={disabled}
          attendance_status={item?.attendance_status}
          onPress={() =>
            navigation.navigate('AttendanceDetails', {
              employeeID: params?.employeeAttendance
                ? params.employeeID
                : employeeID,
              attendanceID: item.log_attendance_id,
              date: item.date,
              employeeAttendance: params?.employeeAttendance,
            })
          }
        />
      );
    }
  };

  const loadMoreData = () => {
    const nextPage = page + 1;

    if (nextPage <= totalPage) {
      setPage(nextPage);
      if (data.monthlyDatewiseAttendance) {
        setDataToShow(
          Object?.keys(data.monthlyDatewiseAttendance)
            .slice(0, visibleData + 10)
            .map(date => ({ ...data.monthlyDatewiseAttendance[date], date })),
        );

        setVisibleData(prevVisibleData => prevVisibleData + 10);
      }
    }
  };

  const getData = async () => {
    try {
      dispatch(
        getDailyAttendance({
          navigation,
          employeeID: params?.employeeAttendance
            ? params.employeeID
            : employeeID,
          employeeAttendance: params?.employeeAttendance ? true : false,
        }),
      )
        .then(data => {
          if (params?.employeeAttendance) {
            const attendanceID = data?.active_attendance?.log_attendance_id;
            dispatch(
              getTodayPunches(
                navigation,
                params?.employeeAttendance ? params.employeeID : employeeID,
                attendanceID,
                undefined,
                true,
              ),
            ).then(todayPunches => {
              if (data?.active_attendance?.overlapping) {
                const monthly_datewise_attendance =
                  data?.monthly_datewise_attendance;
                const yesterdayLogID = Object.values(
                  monthly_datewise_attendance,
                )?.[0]?.log_attendance_id;
                dispatch(
                  getYesterdayPunchDetails(
                    navigation,
                    params?.employeeAttendance ? params.employeeID : employeeID,
                    yesterdayLogID,
                    undefined,
                    true,
                  ),
                ).then(yesterdayData => {
                  setData({
                    activeAttendance: data?.active_attendance,
                    monthlyDatewiseAttendance:
                      data?.monthly_datewise_attendance,
                    attendanceRecords: todayPunches,
                    yesterdayAttendance: Object.values(
                      data?.monthly_datewise_attendance,
                    )?.[0],
                    yesterdayAttendanceRecords: yesterdayData,
                  });
                });
                setLoading1(false);
              } else {
                setLoading1(false);
                setData({
                  activeAttendance: data?.active_attendance,
                  monthlyDatewiseAttendance: data?.monthly_datewise_attendance,
                  attendanceRecords: todayPunches,
                });
              }
            });
          }
        })
        .catch(error => {
          setLoading1(false);
          console.error('Error fetching data:', error);
        });
    } catch (error) {
      setLoading1(false);
      console.log(error);
    }
  };

  useEffect(() => {
    if (!lat || !lng) {
      fetchLocation();
    }
  }, []);

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader
        title={t('attendance')}
        navigation={navigation}
        rightIcon={params?.employeeAttendance ? false : true}
        manualAttendance
      />
      <View style={{ marginHorizontal: 14, marginTop: 12 }}>
        <TodayAttendanceReport
          date={
            data?.activeAttendance?.date &&
            getFormattedDate(data?.activeAttendance?.date)
          }
          firstCheckIn={
            data?.activeAttendance?.checkin &&
            getFormattedTime(data?.activeAttendance?.checkin)
          }
          lastCheckIn={
            data?.activeAttendance?.last_checkin
              ? getFormattedTime(data?.activeAttendance?.last_checkin)
              : '----'
          }
          lastCheckOut={
            data?.activeAttendance?.last_checkout
              ? getFormattedTime(data?.activeAttendance?.last_checkout)
              : '----'
          }
          punchStatus={
            punchStatus == 'Punch-In' ? t('punch-in') : t('punch-out')
          }
          punchPress={() =>
            navigation.navigate('LocationOld', {
              activeAttendance: data?.activeAttendance,
              punchStatus: punchStatus,
              lat: lat,
              long: lng,
              log_attendance_id: data?.activeAttendance?.log_attendance_id,
            })
          }
          morePress={() =>
            navigation.navigate('AttendanceDetails', {
              employeeID: params?.employeeAttendance
                ? params?.employeeID
                : employeeID,
              attendanceID: data?.activeAttendance?.log_attendance_id,
              disabled: true,
              date: data?.activeAttendance?.date,
              today: true,
            })
          }
          attendanceRecords={data.attendanceRecords}
          activeAttendance={data.activeAttendance}
          employeeAttendance={params?.employeeAttendance}
          workingHours={
            params?.workingHours
              ? params.workingHours
              : profileData?.data?.working_hours
          }
        />
      </View>

      <View style={styles.container}>
        <FlatList
          contentContainerStyle={{
            paddingVertical: 16,
            flexGrow: 1,
            paddingHorizontal: 16,
          }}
          data={dataToShow}
          showsVerticalScrollIndicator={false}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={loadMoreData}
          onEndReachedThreshold={0.3}
          ListFooterComponent={
            (data?.monthlyDatewiseAttendance &&
              Object?.keys(data?.monthlyDatewiseAttendance)?.length) > 11 &&
            page < totalPage && (
              <View style={{ alignItems: 'center', marginTop: 4 }}>
                <ActivityIndicator color={COLORS.primaryColor} size={'small'} />
              </View>
            )
          }
          ItemSeparatorComponent={() => <View style={{ height: 16 }} />}
        />
      </View>

      <AlertModal
        visible={alertBox.showBox}
        icon={
          <View style={styles.iconView}>
            <ReactNativeVI
              Lib={'MaterialIcons'}
              name={'location-off'}
              color={'#F73C3C'}
              size={26}
            />
          </View>
        }
        title={alertBox.title}
        message={alertBox.message}
        onClose={closeCustomAlert}
        confirmbtn={alertBox.confirmbtn}
      />

      <Loader loading={loading1} title={t('loading')} />
    </SafeAreaView>
  );
};

export default Attendance;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 4,
  },

  iconView: {
    backgroundColor: '#F73C3C3D',
    height: 48,
    width: 48,
    borderRadius: 48,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
