import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        marginHorizontal: 24,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },

    mapView: {
        marginTop: 16,
        width: '100%',
        height: '75%',
        overflow: 'hidden',
        borderRadius: 24,
    },


    iconView: {
        backgroundColor: "#F73C3C3D",
        height: 48,
        width: 48,
        borderRadius: 48,
        justifyContent: 'center',
        alignItems: 'center'
    },

})