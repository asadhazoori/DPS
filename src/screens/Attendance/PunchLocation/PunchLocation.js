import { View, SafeAreaView, I18nManager } from 'react-native';
import React, { useEffect, useState } from 'react';
import Theme from '../../../theme/theme';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { styles } from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { createPunchIn } from '../../../redux/attendance/actions/createPunchIn';
import { createPunchOut } from '../../../redux/attendance/actions/createPunchOut';
import { getCurrentDateTime } from '../../../utilities/helpers/CurrentTime';
import { commonApi } from '../../../utilities/api/apiController';
import Loader from '../../../components/Modals/Loader';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import { requestAndGetLocation } from '../../../redux/location/location.action';
import AlertModal from '../../../components/Modals/AlertModal';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../../redux/users/user.actions';
import { useTranslation } from 'react-i18next';
import {
  Swipe,
  SwipeRTL,
  SwipeReverse,
  SwipeReverseRTL,
} from '../../../components/Swipe';
import ReactNativeVI from '../../../components/Helpers/ReactNativeVI';
import { getPermissionJust } from '../../../utilities/helpers/AccessLocation';
import Geocoder from 'react-native-geocoding';
import DeviceInfo from 'react-native-device-info';

const PunchLocation = ({ navigation, route }) => {
  const { activeAttendance } = route?.params;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const uid = useSelector(state => state.signin.uid);
  const { employeeID, employeeCode, department } = useSelector(
    state => state.employeeProfile,
  );
  const { lat, lng } = useSelector(state => state.location);
  const punchStatus = activeAttendance?.last_checkout
    ? 'Punch-In'
    : !activeAttendance?.checkin
      ? 'Punch-In'
      : 'Punch-Out';
  const [loading, setLoading] = useState(false);
  const [freeze, setFreeze] = useState(false);
  const [address, setAddress] = useState(null);
  const [uniqueId, setUniqueId] = useState(null);
  const [title, setTitle] = useState(t('verfiying-location'));

  const [updatedLoc, setUpdatedLoc] = useState({
    lat: lat ?? 0,
    lng: lng ?? 0,
  });

  const [alertBox, setAlertBox] = useState({
    showBox: false,
    title: null,
    message: null,
    confirmbtn: null,
  });

  useEffect(() => {
    DeviceInfo.getUniqueId().then(uniqueId => {
      setUniqueId(uniqueId);
    });
  }, []);

  const punch = async type => {
    const title = type === 'In' ? t('punching-in') : t('punching-out');
    const result = getCurrentDateTime();
    const resp = await checkAttendance(updatedLoc?.lat, updatedLoc?.lng);
    if (resp) {
      setLoading(true);
      setFreeze(false);
      setTitle(title);

      const punchAction = type === 'In' ? createPunchIn : createPunchOut;
      dispatch(
        punchAction({
          time: result,
          latitude: updatedLoc?.lat,
          longitude: updatedLoc?.lng,
          uid,
          navigation,
          employeeID,
          employeeCode,
          department,
          log_attendance_id: activeAttendance?.log_attendance_id,
          setTitle,
          setFreeze,
          address,
          uniqueId,
        }),
      );
      setLoading(false);
    }
  };

  const getLocation = async (lat, long, field) => {
    try {
      const response = await Geocoder.from(lat, long);
      const addressComponent = response?.results[0]?.formatted_address;

      setAddress(addressComponent);
      console.log(addressComponent);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    Geocoder.init('AIzaSyDWx388FSWws16YhFRgUu4AW6pUy0miVUk');
  }, []);

  const checkAttendance = async (lat, lng) => {
    setLoading(true);
    setFreeze(false);
    setTitle(t('verfiying-location'));
    try {
      const body = {
        params: {
          model: 'attendance.location.wags',
          method: 'check_locations',
          args: [
            [
              {
                user_id: uid,
                latitude: lat,
                longitude: lng,
              },
            ],
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });
      // console.log("Check Location API", response?.data);
      getLocation(lat, lng);
      setLoading(false);

      if (response?.data?.result?.message == 'True') {
        setFreeze(true);
        return true;
      } else if (response?.data?.result?.message == 'False') {
        setTimeout(() => {
          setAlertBox({
            showBox: true,
            title: t('location-verification'),
            message: t('location-not-allowed-msg'),
            confirmbtn: false,
          });
        }, 300);
        setTitle('Punching Not Allowed Here');
        setFreeze(false);
        return false;
      } else {
        if (response == 'AxiosError: Network Error') {
          navigation.goBack();
        } else if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              setAlertBox({
                showBox: true,
                title: response?.data?.error?.message,
                message: `${response?.data?.error?.data?.message}`,
                confirmbtn: false,
              });
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }

        return false;
      }
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const fetchNewLocation = async () => {
    setLoading(true);
    try {
      const result = await getPermissionJust();

      if (result == 'fine' || result == 'granted') {
        const updatedLocation = await dispatch(requestAndGetLocation());
        if (updatedLocation) {
          setUpdatedLoc({
            lat: updatedLocation?.lat,
            lng: updatedLocation?.lng,
          });
          checkAttendance(updatedLocation?.lat, updatedLocation?.lng);
        }
      } else if (result == 'GPS_disabled') {
        setLoading(false);
        setTimeout(() => {
          setAlertBox({
            showBox: true,
            confirmbtn: false,
            title: t('gps-loc-disabled'),
            message: t('gps-loc-disabled-msg'),
          });
        }, 300);
      } else if (result == 'denied') {
        setLoading(false);
        setTimeout(() => {
          setAlertBox({
            confirmbtn: true,
            showBox: true,
            title: t('permission-denied'),
            message: t('permission-denied-msg'),
          });
        }, 300);
      }
    } catch (error) {
      console.log('Error in Getinng Permission');
    }
  };

  useEffect(() => {
    fetchNewLocation();
  }, []);

  const closeCustomAlert = () => {
    setTimeout(() => {
      setAlertBox({ showBox: false });
    }, 300);
    navigation.goBack();
  };

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader
        title={punchStatus == 'Punch-In' ? t('punch-in') : t('punch-out')}
        navigation={navigation}
      />

      <View style={styles.mapView}>
        <MapView
          style={{ flex: 1 }}
          initialRegion={{
            latitude: updatedLoc?.lat ? updatedLoc?.lat : 24.7136,
            longitude: updatedLoc?.lng ? updatedLoc?.lng : 46.6753,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01,
          }}
          region={{
            latitude: updatedLoc?.lat ? updatedLoc?.lat : 24.7136,
            longitude: updatedLoc?.lng ? updatedLoc?.lng : 46.6753,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01,
          }}
          showsUserLocation={true}
          zoomControlEnabled={true}
          provider={PROVIDER_GOOGLE}>
          <Marker
            coordinate={{
              latitude: updatedLoc?.lat,
              longitude: updatedLoc?.lng,
            }}
            title={t('current-location')}
          />
        </MapView>
      </View>

      <View style={styles.container}>
        {punchStatus == 'Punch-In' ? (
          I18nManager.isRTL ? (
            <SwipeRTL
              onSlide={() => punch('In')}
              freeze={freeze}
              title={title}
              loading={loading}
            />
          ) : (
            <Swipe
              onSlide={() => punch('In')}
              freeze={freeze}
              title={title}
              loading={loading}
            />
          )
        ) : I18nManager.isRTL ? (
          <SwipeReverseRTL
            onSlide={() => punch('Out')}
            freeze={freeze}
            title={title}
            loading={loading}
          />
        ) : (
          <SwipeReverse
            onSlide={() => punch('Out')}
            freeze={freeze}
            title={title}
            loading={loading}
          />
        )}
      </View>
      <AlertModal
        visible={alertBox.showBox}
        icon={
          <View style={styles.iconView}>
            <ReactNativeVI
              Lib={'MaterialIcons'}
              name={'location-off'}
              color={'#F73C3C'}
              size={26}
            />
          </View>
        }
        title={alertBox.title}
        message={alertBox.message}
        onClose={closeCustomAlert}
        confirmbtn={alertBox.confirmbtn}
      />
    </SafeAreaView>
  );
};

export default PunchLocation;
