// MyForm.js
import React, { useState, useRef } from 'react';
import { View, Button, StyleSheet } from 'react-native';
// import TextInputField from './TextInputField';
import SplashScreen from 'react-native-splash-screen';
import TestTextInputField from './TestCom';

const TestScreen = () => {
    SplashScreen.hide();
    const [input1, setInput1] = useState('');
    const [input2, setInput2] = useState('');
    const [input3, setInput3] = useState('');

    // Create an array of refs for the text inputs
    const inputRefs = [useRef(), useRef(), useRef()];

    // Function to focus on the next input
    const focusNextInput = (index) => {
        if (index < inputRefs.length - 1) {
            inputRefs[index + 1].current.focus();
        }
    };

    return (
        <View style={styles.container}>
            <TestTextInputField
                placeholder="Input 1"
                value={input1}
                onChangeText={(text) => setInput1(text)}
                onSubmitEditing={() => focusNextInput(0)}
                ref={inputRefs[0]}
            />
            <TestTextInputField
                placeholder="Input 2"
                value={input2}
                onChangeText={(text) => setInput2(text)}
                onSubmitEditing={() => focusNextInput(1)}
                ref={inputRefs[1]}
            />
            <TestTextInputField
                placeholder="Input 3"
                value={input3}
                onChangeText={(text) => setInput3(text)}
                onSubmitEditing={() => focusNextInput(2)}
                ref={inputRefs[2]}
            />
            {/* <Button
                title="Next"
                onPress={() => focusNextInput(0)}
            /> */}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 16,
    },
});

export default TestScreen;






// // MyForm.js
// import React, { useState, useRef } from 'react';
// import { View, Button, StyleSheet } from 'react-native';
// import TestTextInputField from './TestCom';
// import SplashScreen from 'react-native-splash-screen';

// const TestScreen = () => {
//     SplashScreen.hide();
//     const [input1, setInput1] = useState('');
//     const [input2, setInput2] = useState('');
//     const [input3, setInput3] = useState('');

//     // Create an array of refs for the text inputs
//     const inputRefs = [useRef(), useRef(), useRef()];

//     // Function to focus on the next input
//     const focusNextInput = (index) => {
//         if (index < inputRefs.length - 1) {
//             inputRefs[index + 1].current.focus();
//         }
//     };

//     return (
//         <View style={styles.container}>
//             <TestTextInputField
//                 placeholder="Input 1"
//                 value={input1}
//                 onChangeText={(text) => setInput1(text)}
//                 onSubmitEditing={() => focusNextInput(0)}
//                 ref={inputRefs[0]}
//             />
//             <TestTextInputField
//                 placeholder="Input 2"
//                 value={input2}
//                 onChangeText={(text) => setInput2(text)}
//                 onSubmitEditing={() => focusNextInput(1)}
//                 ref={inputRefs[1]}
//             />
//             <TestTextInputField
//                 placeholder="Input 3"
//                 value={input3}
//                 onChangeText={(text) => setInput3(text)}
//                 onSubmitEditing={() => focusNextInput(2)}
//                 ref={inputRefs[2]}
//             />
//             <Button
//                 title="Next"
//                 onPress={() => focusNextInput(0)}
//             />
//         </View>
//     );
// };

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         justifyContent: 'center',
//         padding: 16,
//     },
// });

// export default TestScreen;
