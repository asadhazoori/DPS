import React, { forwardRef } from 'react';
import { TextInput, StyleSheet } from 'react-native';

const TestTextInputField = forwardRef(({ placeholder, value, onChangeText, onSubmitEditing }, ref) => {
  return (
    <TextInput
      style={styles.input}
      placeholder={placeholder}
      value={value}
      onChangeText={onChangeText}
      onSubmitEditing={onSubmitEditing}
      ref={ref}
    />
  );
});

const styles = StyleSheet.create({
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
    paddingLeft: 8,
  },
});

export default TestTextInputField;
