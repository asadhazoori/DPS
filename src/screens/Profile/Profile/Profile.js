import { View, Text, SafeAreaView, ScrollView, TouchableOpacity, I18nManager } from 'react-native'
import React, { useEffect, useState } from 'react'
import Theme from '../../../theme/theme'
import GeneralHeader from '../../../components/Headers/GeneralHeader'
import { styles } from './styles'
import { useDispatch, useSelector } from 'react-redux'
import { Avatar } from 'react-native-paper'
import { SvgXml } from 'react-native-svg'
import { DrawerIcons } from '../../../assets/SvgIcons/DrawerIcons'
import { FontStyle } from '../../../theme/FontStyle'
import { ProfileIcons } from '../../../assets/SvgIcons/ProfileIcons'
import { COLORS } from '../../../theme/colors'
import ProfileItem from '../../../components/Helpers/ProfileItem'
import { Icons } from '../../../assets/SvgIcons/Icons'
import { useTranslation } from 'react-i18next'
import { logout_user } from '../../../redux/users/user.actions'
import changeLng from '../../../utilities/helpers/ChangeLanguage'
import { LangModal } from '../../../components/Modals'
import AsyncStorage from '@react-native-async-storage/async-storage'
import ReactNativeVI from '../../../components/Helpers/ReactNativeVI'


const Profile = ({ navigation }) => {
    const { t } = useTranslation();
    const profileData = useSelector((state) => state.employeeProfile.data);
    const [requests, setOpenRequests] = useState(false);
    const [privacy, setOpenPrivacy] = useState(false);
    const dispatch = useDispatch();
    const [lngModalVisible, setLngModalVisible] = useState(false);
    // const [selectedLng, setSelectedLng] = useState('');

    const selectedLng = I18nManager.isRTL ? 'العربية' : 'English'

    // useEffect(() => {
    //     const checkLang = async () => {
    //         // const language = await AsyncStorage.getItem("language");
    //         // if (language == 'en') {
    //         if (I18nManager.isRTL == 'en') {
    //             setSelectedLng("English")

    //         } else {
    //             setSelectedLng("العربية")

    //         }
    //     }
    //     checkLang();
    // }, [])

    return (
        <SafeAreaView style={Theme.SafeArea}>
            <GeneralHeader title={t('profile')} navigation={navigation} profileIcon={false} />

            <ScrollView showsVerticalScrollIndicator={false}>

                <View style={styles.mainView}>

                    <View style={styles.imageView}>
                        <View style={{}}>
                            {profileData?.image_1920 ?

                                <Avatar.Image
                                    source={{ uri: `data:image/jpeg;base64,${profileData?.image_1920}` }}
                                    size={60}
                                    style={[Theme.ImageShadow]}
                                />
                                :
                                <View style={[Theme.ImageShadow, { backgroundColor: '#fafafb', height: 60, width: 60, borderRadius: 30, alignItems: 'center', justifyContent: 'center' }]}>
                                    <ReactNativeVI Lib={'MaterialCommunityIcons'} name={'account'} color={'#a9adc1'} size={40} />
                                </View>
                            }
                            {/* <View style={styles.cameraIconView}>
                                <SvgXml xml={DrawerIcons.camera} />
                            </View> */}
                        </View>

                        <View style={{ marginTop: 12 }}>
                            <Text style={[FontStyle.Regular16_500, { color: COLORS.darkBlack, fontWeight: '700' }]}>{profileData?.name}</Text>
                        </View>
                        {profileData?.job_name &&
                            <View style={{ marginTop: 4 }}>
                                <Text style={[FontStyle.Regular14_500, { color: COLORS.darkBlack }]}>{profileData?.job_name}</Text>
                            </View>
                        }

                    </View>

                    <View style={styles.featuresView}>

                        <ProfileItem
                            icon={ProfileIcons.person}
                            title={t('personal-information')}
                            caption={t('personal-information-caption')}
                            onPress={() => navigation.navigate('EmployeeProfile')}
                        // onPress={() => navigation.navigate('EmployeeProfile', { route: 0 })}
                        />

                        <ProfileItem
                            icon={'language'}
                            title={t('change-language')}
                            vectorIcon={true}
                            caption={selectedLng}

                            onPress={() => setLngModalVisible(true)}
                        />

                        <ProfileItem
                            icon={Icons.password}
                            title={t('change-password')}
                            // caption={selectedLng}

                            onPress={() => navigation.navigate('ChangePassword')}
                        />

                        <ProfileItem
                            icon={DrawerIcons.logout}
                            title={t('logout')}
                            logout={true}
                            // caption={'Qualification, Certifications, etc.'}
                            onPress={() => {
                                dispatch(logout_user(false))
                                navigation.reset({
                                    index: 0,
                                    routes: [
                                        { name: 'Login' }
                                    ],
                                })
                            }}
                        />

                        {/* <ProfileItem
                            icon={ProfileIcons.education}
                            title={'Educational Details'}
                            caption={'Qualification, Certifications, etc.'}
                            onPress={() => navigation.navigate('EmployeeProfile', { route: 2 })}
                        />

                        <ProfileItem
                            icon={ProfileIcons.experience}
                            title={'Past Experience Details'}
                            caption={'Experience,'}
                            onPress={() => navigation.navigate('EmployeeProfile', { route: 3 })}
                        />

                        <ProfileItem
                            icon={ProfileIcons.notification}
                            title={'Notifications'}
                            caption={'See your Notifications & updates here'}
                            onPress={() => { }}
                        />

                        <ProfileItem
                            icon={ProfileIcons.requests}
                            title={'Requests'}
                            caption={'User’s requests and status'}
                            onPress={() => setOpenRequests(!requests)}
                            rightIcon={requests ? ProfileIcons.downArrow : ProfileIcons.right_Arrow}
                            container={styles.itemView}
                        />

                        {requests &&


                            <View style={styles.dropDowView}>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    // onPress={()=> navigation.navigate('')}
                                    style={styles.dropDown}>
                                    <Text style={styles.dropDownText}>Apply Leaves</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    // onPress={()=> navigation.navigate('')}
                                    style={styles.dropDown}>
                                    <Text style={styles.dropDownText}>Medical claims</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    // onPress={()=> navigation.navigate('')}
                                    style={styles.dropDown}>
                                    <Text style={styles.dropDownText}>Change shift</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    // onPress={()=> navigation.navigate('')}
                                    style={styles.dropDown}>
                                    <Text style={styles.dropDownText}>Payslip</Text>
                                </TouchableOpacity>

                            </View>
                        }
                        <ProfileItem
                            icon={ProfileIcons.privacy}
                            title={'Privacy and Policy'}
                            caption={'Company policies, Users privacy, FAQs'}
                            onPress={() => setOpenPrivacy(!privacy)}
                            rightIcon={privacy ? ProfileIcons.downArrow : ProfileIcons.right_Arrow}
                            container={styles.itemView}
                        />

                        {privacy &&

                            <View style={styles.dropDowView}>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    // onPress={()=> navigation.navigate('')}
                                    style={styles.dropDown}>
                                    <Text style={styles.dropDownText}>Change Password</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    // onPress={()=> navigation.navigate('')}
                                    style={styles.dropDown}>
                                    <Text style={styles.dropDownText}>Use Two-Factor Authentication</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    // onPress={()=> navigation.navigate('')}
                                    style={styles.dropDown}>
                                    <Text style={styles.dropDownText}>Save your login info.</Text>
                                </TouchableOpacity>

                            </View>
                        } */}


                    </View>

                </View>
                <LangModal
                    modalVisible={lngModalVisible}
                    setModalVisible={setLngModalVisible}
                    onChangeSelection={changeLng}
                />

            </ScrollView>
        </SafeAreaView>
    )
}

export default Profile