import { SafeAreaView, View, } from 'react-native'
import React, { useEffect, useState } from 'react'
import { TabViewStyles } from '../../../theme/TabViewStyles';
import { useDispatch, useSelector } from 'react-redux';
import { getEmployeeProfile } from '../../../redux/profile/actions/getEmployeeProfile';
import { TabView } from 'react-native-tab-view';
import GeneralHeader from '../../../components/Headers/GeneralHeader'
import Theme from '../../../theme/theme'
import Experiences from './Experiences';
import Qualifications from './Qualifications';
import PersonalInfo from './PersonalInfo';
import FamilyInfo from './FamilyInfo';
import { useTranslation } from 'react-i18next';
import { renderTabBar } from '../../../components/Helpers/renderTabBar';

const ProfileDetails = ({ navigation, route }) => {

    const { t } = useTranslation();
    // const initalRoute = route.params;
    const profileData = useSelector((state) => state.employeeProfile.data);
    const uid = useSelector((state) => state.signin.uid);
    // const [loading, setLoading] = useState(true);

    const dispatch = useDispatch();

    useEffect(() => {
        // setLoading(true) 
        dispatch(getEmployeeProfile({ uid, navigation, callingFromProfileScreen: true }))

    }, [])



    const [index, setIndex] = useState(0);

    const [routes] = useState([
        { key: 'first', title: "personal-info" },
        // { key: 'second', title: "Family Info" },
        // { key: 'third', title: "Qualification" },
        // { key: 'fourth', title: "Experience" }
    ]);


    // useEffect(() => {
    //     if (initalRoute !== undefined) {
    //         setIndex(initalRoute.route);
    //     }
    // }, [initalRoute]);

    const RenderScene = (e, navigation) => {

        switch (e.route.key) {
            case 'first':
                return <PersonalInfo />;
            case 'second':
            //     return <FamilyInfo />;
            // case 'third':
            //     return <Qualifications />;
            // case 'fourth':
            //     return <Experiences />;
        }
    };


    return (

        <SafeAreaView style={Theme.SafeArea}>
            <GeneralHeader title={t('profile')} navigation={navigation} profileIcon={false} />

            <View style={TabViewStyles.container}>

                <TabView
                    navigationState={{ index, routes }}
                    renderScene={(e) => RenderScene(e, navigation)}
                    onIndexChange={setIndex}
                    renderTabBar={renderTabBar}
                />
            </View>

        </SafeAreaView>

    )
}

export default ProfileDetails