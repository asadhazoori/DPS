import React, { useState } from 'react'
import { SafeAreaView, ScrollView, Text, View, StyleSheet } from 'react-native'
import { useSelector } from 'react-redux'
import { SvgXml } from 'react-native-svg';

// import { Icons } from '../../../assets/SvgIcons/Icons';
// import Theme from '../../../theme/theme';
import { COLORS } from '../../../../theme/colors';
import TextInput from '../../../../components/Inputs/TextInputField';
import { useTranslation } from 'react-i18next';
import { getFormattedDate, getFormattedTime } from '../../../../utilities/helpers/CurretDate';

const PersonalInfo = () => {

    const { t } = useTranslation();
    const profileData = useSelector((state) => state.employeeProfile.data);
    const [editable, setEditable] = useState(false);

    // const [startDateString, endDateString] = profileData?.shift_name ? profileData?.shift_name.split(' - ') : ''

    // console.log(startDateString)


    return (
        <View style={styles.container} >
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingVertical: 16, }}>


                <View style={{ gap: 14 }}>

                    <TextInput

                        label={t('name')}
                        value={profileData?.name}
                        editable={false}
                    />

                    {profileData?.identification_id &&
                        <TextInput
                            label={t('identification-id')}
                            value={profileData?.identification_id}
                            editable={false}
                        />
                    }

                    {profileData?.company_name &&
                        <TextInput
                            label={t('company')}
                            value={profileData?.company_name}
                            editable={false}
                        />
                    }

                    {profileData?.department_name &&
                        <TextInput
                            label={t('department')}
                            value={`${profileData?.department_name}`}
                            editable={false}
                        />
                    }
                    {profileData?.job_name &&
                        <TextInput
                            label={t('job-title')}
                            value={`${profileData?.job_name}`}
                            editable={false}
                        />
                    }

                    {profileData?.manager_name &&
                        <TextInput
                            label={t('manager')}
                            value={`${profileData?.manager_name}`}
                            editable={false}
                        />
                    }
                    {/* {profileData?.company_location?.[1] &&

                        < TextInput
                            label={t('company-location')}
                            value={`${profileData?.company_location?.[1]}`}
                            editable={false}
                        />
                    } */}
                    {profileData?.work_email &&
                        <TextInput
                            label={t('work-email')}
                            value={`${profileData?.work_email}`}
                            editable={false}
                        />
                    }

                    {profileData?.mobile_phone &&
                        <TextInput
                            label={t('work-mobile')}
                            value={`${profileData?.mobile_phone}`}
                            editable={false}
                        />
                    }

                    {profileData?.joining_date &&

                        <TextInput
                            label={t('joining-date')}
                            value={`${getFormattedDate(profileData?.joining_date)}`}
                            editable={false}
                        />
                    }

                    {profileData?.no_of_years &&
                        <TextInput
                            label={t('service-year')}
                            value={`${profileData?.no_of_years}`}
                            editable={false}
                        />
                    }

                    {profileData?.nationality_type &&
                        <TextInput
                            label={t('nationality-type')}
                            value={profileData?.nationality_type == "non_local" ? 'Non Local' : 'Local'}
                            editable={false}
                        />
                    }

                    {profileData?.country_id[1] &&
                        <TextInput
                            label={t('nationality')}
                            value={`${profileData?.country_id[1]}`}
                            editable={false}
                        />
                    }

                    {profileData?.birthday &&
                        <TextInput
                            label={t('date-of-birth')}
                            value={`${getFormattedDate(profileData?.birthday)}`}
                            // value={`${profileData?.birthday}`}
                            editable={false}
                        />
                    }


                    {profileData?.shift_name &&
                        <TextInput
                            label={t('shift')}
                            // value={`${getFormattedTime(startDateString)} - ${getFormattedTime(endDateString)}`}
                            value={`${profileData?.shift_name}`}
                            editable={false}
                        />
                    }
                    <TextInput
                        label={t('working-hours')}
                        value={`${profileData?.working_hours}`}
                        editable={false}
                    />



                    {/* 
                    {profileData?.working_hours &&
                        <TextInput
                            label={t('working-hours')}
                            value={`${profileData?.working_hours}`}
                            editable={false}
                            />
                        } 
                        */}

                </View>


            </ScrollView>
        </View>
    )
}

export default PersonalInfo

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // borderWidth: 1,
        marginHorizontal: 16,
        // backgroundColor: COLORS.white,
        // padding: 20,

    },

    headerView: {
        backgroundColor: COLORS.grey,
        padding: 10,
        borderRadius: 10,
        paddingHorizontal: 16,

    },

    headerText: {
        color: COLORS.white,
        fontWeight: '700',
        fontSize: 16
    },

    infoView: {
        // borderWidth: 1,
        // padding: 10
    },

    section: {
        marginBottom: 10
    },

    innerView: {
        padding: 10
    },

    rowView: {
        flexDirection: 'row',
        marginBottom: 10,
        // borderWidth: 1,
        // flex:1
    },

    heading: {
        color: COLORS.black,
        fontSize: 16,
        fontWeight: '600',
    },

    textView: {
        flex: 1
    },

    title: {
        color: COLORS.black,
        fontSize: 14,
        fontWeight: '400',

    }


});