import React from 'react'
import { SafeAreaView, ScrollView, Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import { SvgXml } from 'react-native-svg';
import { useSelector } from 'react-redux'
import { COLORS } from '../../../../theme/colors';
import TextInput from '../../../../components/Inputs/TextInputField';
import { Icons } from '../../../../assets/SvgIcons/Icons';
import Theme from '../../../../theme/theme';

// import { FontStyle } from '../../../theme/FontStyle';
// import { COLORS } from '../../../theme/colors';
// import ProfileTextInput from '../../Inputs/ProfileTextInput';
// import Theme from '../../../theme/theme';
// import { Icons } from '../../../assets/SvgIcons/Icons';


const FamilyInfo = () => {

    const profileData = useSelector((state) => state.employeeProfile.data);

    return (
        <View style={styles.container} >
            <ScrollView showsVerticalScrollIndicator={false}>

                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginTop: 12, marginBottom: 24, }}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={[Theme.Shadow, { width: 40, alignItems: 'center', paddingVertical: 5, marginHorizontal: 4 }]}>
                        <SvgXml xml={Icons.editIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={[Theme.Shadow, { width: 40, alignItems: 'center', paddingVertical: 5, marginHorizontal: 4 }]}>
                        <SvgXml xml={Icons.download} />
                    </TouchableOpacity>

                </View>


                <TextInput
                    label={'Father Name'}
                    value={profileData?.father_name}
                    editable={false}
                />

                <TextInput
                    label={'Status'}
                    value={(profileData?.father_live == 'dead') ? 'Deceased' : 'Alive'}
                    editable={false}
                />
                <TextInput
                    label={'Mother Name'}
                    value={profileData?.mother_name}
                    editable={false}
                />
                <TextInput
                    label={'Status'}
                    value={(profileData?.mother_live == 'dead') ? 'Deceased' : 'Alive'}
                    editable={false}
                />

                {profileData?.spouse_info?.map((spouse, index) => (

                    <TextInput
                        label={'Spouse Name'}
                        value={spouse.spouse_name}
                        editable={false}
                        key={spouse.id}
                    />
                ))}

                {profileData?.child_info?.map((child) => (

                    <TextInput
                        label={'Child Name'}
                        value={child.child_name}
                        editable={false}
                        key={child.id}
                    />
                ))}




            </ScrollView>
        </View>
    )
}

export default FamilyInfo

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 16,
        // borderWidth: 1,
        // backgroundColor: COLORS.white,
        // padding: 20,

    },

    headerView: {
        backgroundColor: COLORS.grey,
        padding: 10,
        borderRadius: 10,
        paddingHorizontal: 16,

    },

    headerText: {
        color: COLORS.white,
        fontWeight: '700',
        fontSize: 16
    },

    infoView: {
        // borderWidth: 1,
        // padding: 10
    },

    section: {
        marginBottom: 10
    },

    innerView: {
        padding: 10
    },

    rowView: {
        flexDirection: 'row'
    },

    heading: {
        color: COLORS.black,
        fontWeight: '600',
    },

    title: {
        color: COLORS.black,
        fontWeight: '400',

    }


});