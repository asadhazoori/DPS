import { View, SafeAreaView, TouchableOpacity, FlatList, StyleSheet } from 'react-native'
import React, { useEffect, useState } from 'react'
import GeneralHeader from '../../components/Headers/GeneralHeader'
import Theme from '../../theme/theme'
import { useTranslation } from 'react-i18next'
import SmartButtonTrans from '../../components/Buttons/SmartButtonTrans'
import MedicalClaimCard from '../../components/Cards/MedicalClaimCard'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'
import { Icons } from '../../assets/SvgIcons/Icons'
import MedicalClaimBalanceCard from '../../components/Cards/MedicalClaimBalanceCard'

const MedicalClaim = ({ navigation }) => {

    const { t } = useTranslation();
    const dummyData = [
        { "claim_type": "self", "state": "validate", "name": "Touqeer Zahid", "apporve_amount": 4995.0, "date": "2021-08-16", "tree": [{ "claim_amount": 4995.0, "date_claim": "2021-08-14", "description": "Chest Infection - fever ", "disease_type": false }], "id": 2389, "record_claim_amount": 4995.0 },
        { "claim_type": "children", "state": "cancel", "name": " Aaraiz Touqeer", "apporve_amount": 5021.0, "date": "2021-09-29", "tree": [{ "claim_amount": 6420.0, "date_claim": "2021-09-29", "description": "Fever ", "disease_type": false }], "id": 3012, "record_claim_amount": 6420.0 },
        { "claim_type": "self", "state": "validate", "name": "Touqeer Zahid", "apporve_amount": 2000.0, "date": "2021-10-22", "tree": [{ "claim_amount": 2000.0, "date_claim": "2021-10-21", "description": "flu", "disease_type": false }], "id": 3251, "record_claim_amount": 2000.0 },
        { "claim_type": "spouse", "state": "draft", "name": "Yamna Touqeer", "apporve_amount": 2066.0, "date": "2022-03-25", "tree": [{ "claim_amount": 2755.0, "date_claim": "2022-03-24", "description": "Food poisoning ", "disease_type": "Stomach" }], "id": 4820, "record_claim_amount": 2755.0 },
    ]

    return (
        <SafeAreaView style={Theme.SafeArea}>
            <GeneralHeader title={t('medical-claims')} navigation={navigation} />

            <View style={styles.container}>


                <View style={{ alignItems: 'flex-end', marginVertical: 24, marginRight: 16 }}>
                    <SmartButtonTrans title={'Add New'} rightIcon={Icons.plus} onPress={() => { navigation.navigate('ApplyMedicalClaims') }} />

                </View>
                <View style={styles.body}>

                    <MedicalClaimBalanceCard />

                    <View style={{ flex: 1 }} >


                        <FlatList
                            data={dummyData}
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{ paddingVertical: 16, }}
                            renderItem={({ item, index }) => (<MedicalClaimCard key={index} data={item} />)}
                            keyExtractor={(item, index) => index}
                            ItemSeparatorComponent={() => <View style={{ height: 12 }} />}
                            ListEmptyComponent={() => (<Text style={[styles.dateText, { alignSelf: 'center' }]}>{t('no-record-found')}</Text>)}


                        />
                    </View>
                </View>

            </View>


        </SafeAreaView>
    )
}

export default MedicalClaim


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    body: {
        marginHorizontal: 16,
        flex: 1
    },
    dateText: {
        ...FontStyle.Regular12,
        fontWeight: '500',
        color: COLORS.grey5

    },
})
