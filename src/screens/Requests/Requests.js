import {
  View,
  SafeAreaView,
  StyleSheet,
  FlatList,
  Text,
  RefreshControl,
  Dimensions,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import GeneralHeader from '../../components/Headers/GeneralHeader';
import Theme from '../../theme/theme';
import {useTranslation} from 'react-i18next';
import SmartButtonTrans from '../../components/Buttons/SmartButtonTrans';
import {Icons} from '../../assets/SvgIcons/Icons';
import {COLORS} from '../../theme/colors';
import {FontStyle} from '../../theme/FontStyle';
import RequestsCard from '../../components/Cards/RequestsCard';
import RequestsBalanceCard from '../../components/Cards/RequestsBalanceCard';
import {useDispatch, useSelector} from 'react-redux';
import {getAllRequests} from '../../redux/travel/actions/getAllRequests';
import Carousel from 'react-native-reanimated-carousel';
export const window = Dimensions.get('window');

const PAGE_WIDTH = window.width;

const baseOptions = {
  vertical: false,
  width: PAGE_WIDTH,
  height: null,
};
const Requests = ({navigation, route}) => {
  const dispatch = useDispatch();
  const {t} = useTranslation();
  const [refresh, setRefresh] = useState(false);
  const {allRequests, requestTypeCards} = useSelector(state => state?.request);
  const [filteredData, setFilteredData] = useState(allRequests);
  const [selectedCategoryId, setSelectedCategoryId] = useState(1);

  const iqamaRenewal = allRequests?.filter(
    item => item?.request_type === 'Iqama Renewal',
  );
  const employeeResignation = allRequests?.filter(
    item => item?.request_type === 'Employee Resignation',
  );
  const travelTicket = allRequests?.filter(
    item => item?.request_type === 'Travel & Ticket',
  );
  const employmentLetter = allRequests?.filter(
    item => item?.request_type === 'Employment Letter',
  );

  useEffect(() => {
    if (selectedCategoryId == 1) {
      setFilteredData(allRequests);
    } else if (selectedCategoryId == 2) {
      setFilteredData(employmentLetter);
    } else if (selectedCategoryId == 3) {
      setFilteredData(travelTicket);
    } else if (selectedCategoryId == 4) {
      setFilteredData(employeeResignation);
    } else if (selectedCategoryId == 5) {
      setFilteredData(iqamaRenewal);
    }
  }, [allRequests]);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      setRefresh(true);
      await dispatch(getAllRequests({navigation}));
      setRefresh(false);
    } catch (error) {
      setRefresh(false);
      console.log('Error while calling getAllEmployeeRequest', error);
    }
  };

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader title={t('requests')} navigation={navigation} />
      <View style={styles.container}>
        <View
          style={{
            alignItems: 'flex-end',
            marginVertical: 16,
            marginBottom: 16,
            marginRight: 16,
          }}>
          <SmartButtonTrans
            title={t('add-new')}
            rightIcon={Icons.plus}
            onPress={() => navigation.navigate('CreateRequests')}
          />
        </View>

        <View style={{height: 85}}>
          <Carousel
            loop={false}
            {...baseOptions}
            style={{
              width: PAGE_WIDTH,
            }}
            mode="parallax"
            modeConfig={{
              parallaxScrollingScale: 0.9,
              parallaxScrollingOffset: 220,
            }}
            data={requestTypeCards}
            onSnapToItem={async index => {
              const selected = requestTypeCards[index];

              if (selected?.id == 1) {
                setFilteredData(allRequests);
              } else if (selected?.id == 2) {
                setFilteredData(employmentLetter);
              } else if (selected?.id == 3) {
                setFilteredData(travelTicket);
              } else if (selected?.id == 4) {
                setFilteredData(employeeResignation);
              } else {
                setFilteredData(iqamaRenewal);
              }
              setSelectedCategoryId(selected.id);
            }}
            renderItem={({item, index}) => {
              return (
                <RequestsBalanceCard
                  selectedCategoryId={selectedCategoryId}
                  item={item}
                />
              );
            }}
          />
        </View>

        <View style={styles.body}>
          <FlatList
            data={filteredData}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={() => getData()}
                tintColor={COLORS.primaryColor}
                colors={[COLORS.primaryColor]}
              />
            }
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{paddingBottom: 16, flexGrow: 1}}
            renderItem={({item, index}) => (
              <RequestsCard key={index} data={item} navigation={navigation} />
            )}
            keyExtractor={(item, index) => index}
            ItemSeparatorComponent={() => <View style={{height: 12}} />}
            ListEmptyComponent={() => (
              <>
                {filteredData?.length < 1 && !refresh && (
                  <Text style={[styles.dateText, {alignSelf: 'center'}]}>
                    {t('no-record-found')}
                  </Text>
                )}
              </>
            )}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Requests;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    flex: 1,
    marginHorizontal: 16,
  },
  dateText: {
    ...FontStyle.Regular12,
    fontWeight: '500',
    color: COLORS.grey5,
  },
});
