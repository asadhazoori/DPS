import { Alert, StyleSheet, Text, View } from 'react-native';
import React, { useState } from 'react';
import { COLORS } from '../../../theme/colors';
import { FontStyle } from '../../../theme/FontStyle';
import { useTranslation } from 'react-i18next';
import { NextButton, TextInputField } from '../../../components/Inputs';
import { useDispatch, useSelector } from 'react-redux';
import inputValidation from '../../../utilities/Validations/YupValidate';
import { commonApi } from '../../../utilities/api/apiController';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../../redux/users/user.actions';
import { IqamaSchema } from '../../../utilities/Validations/Requests/IqamaSchema';
import { getAllRequests } from '../../../redux/travel/actions/getAllRequests';

const IqamaRenewal = ({ navigation }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const { employeeID } = useSelector(state => state?.employeeProfile);
  const [inputs, setInputs] = useState({
    reason: null,
    errors: null,
  });

  const validate = async () => {
    const schema = IqamaSchema(t);
    const result = await inputValidation(schema, inputs);

    if (result.isValidate) {
      handleSubmit();
    } else {
      setInputs(prev => ({
        ...prev,
        errors: result?.err,
      }));
    }
  };

  const handleSubmit = async () => {
    setLoading(true);

    try {
      const body = {
        params: {
          model: 'employee.renewal',
          method: 'create_iqama_renewal',
          args: [
            {
              employee_id: employeeID,
              reason: inputs?.reason,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        await dispatch(getAllRequests({ navigation }));
        setLoading(false);
        setTimeout(() => { Toast.show(`${t('request-submitted-successfully')} !`); }, 300);
        navigation.goBack();
      } else {
        setLoading(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: create_iqama_renewal\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      setLoading(false);
      console.error(error);
    }
  };

  const handleInputChange = (field, value) => {
    setInputs({
      ...inputs,
      [field]: value,
      errors: {
        ...inputs.errors,
        [field]: false,
      },
    });
  };

  return (
    <View style={styles.container}>
      <Text
        style={[
          FontStyle.Regular16,
          { marginBottom: 16, color: COLORS.primaryColor, textAlign: 'left' },
        ]}>
        {t('Iqama Renewal')}
      </Text>

      <View style={{ gap: 12 }}>
        <TextInputField
          label={t('reason')}
          editable={true}
          placeholder={t('reason-plcholder')}
          value={inputs.reason}
          error={inputs?.errors?.reason}
          multiline={true}
          height={100}
          onChangeText={text => handleInputChange('reason', text)}
          labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
        />

        <View style={styles.bottomView}>
          <NextButton title={t('submit')} onPress={validate} loader={loading} />
        </View>
      </View>
    </View>
  );
};

export default IqamaRenewal;

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    padding: 8,
    backgroundColor: COLORS.white,
  },

  radioView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
    justifyContent: 'center',
  },

  text: {
    ...FontStyle.Regular14_500,
    marginLeft: 4,
    color: COLORS.black,
    alignSelf: 'flex-start',
    justifyContent: 'flex-start',
  },

  bottomView: {
    marginTop: 12,
    marginHorizontal: 4,
  },
});
