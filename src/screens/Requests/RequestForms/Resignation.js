import { Alert, StyleSheet, Text, View } from 'react-native';
import React, { useState } from 'react';
import { COLORS } from '../../../theme/colors';
import { FontStyle } from '../../../theme/FontStyle';
import { useTranslation } from 'react-i18next';
import { NextButton, TextInputField } from '../../../components/Inputs';
import DatePickerNew from '../../../components/DateTimePicker/DatePickerNew';
import { getFormattedDate } from '../../../utilities/helpers/CurretDate';
import { useDispatch, useSelector } from 'react-redux';
import { commonApi } from '../../../utilities/api/apiController';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../../redux/users/user.actions';
import inputValidation from '../../../utilities/Validations/YupValidate';
import { ResignationSchema } from '../../../utilities/Validations/Requests/ResignationSchema';
import { getAllRequests } from '../../../redux/travel/actions/getAllRequests';

const Resignation = ({ navigation }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const profileData = useSelector(state => state.employeeProfile.data);
  const [loading, setLoading] = useState(false);
  const [showLastWorkDatePicker, setShowLastWorkDatePicker] = useState(false);
  const [showResignDatePicker, setShowResignDatePicker] = useState(false);
  const [inputs, setInputs] = useState({
    lastWorkDate: null,
    resignDate: null,
    reason: null,
    errors: null,
  });

  const handleInputChange = (field, value) => {
    setInputs({
      ...inputs,
      [field]: value,
      errors: {
        ...inputs.errors,
        [field]: false,
      },
    });
  };

  const handleDateChange = (selectedDate, field) => {
    const dateObject = new Date(selectedDate);
    const formattedDate = dateObject.toISOString().split('T')[0];

    if (field === 'lastWorkDate') {
      setShowLastWorkDatePicker(false);
    } else if (field === 'resignDate') {
      setShowResignDatePicker(false);
    }
    handleInputChange(field, formattedDate);
  };

  const validate = async () => {
    const schema = ResignationSchema(t);
    const result = await inputValidation(schema, inputs);

    if (result.isValidate) {
      handleSubmit();
    } else {
      setInputs(prev => ({
        ...prev,
        errors: result?.err,
      }));
    }
  };

  const handleSubmit = async () => {
    setLoading(true);

    try {
      const body = {
        params: {
          model: 'employee.resignation',
          method: 'create_resignation_request',
          args: [
            {
              employee_id: profileData?.id,
              last_working_date: inputs?.lastWorkDate,
              resignation_date: inputs?.resignDate,
              reason: inputs?.reason,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        await dispatch(getAllRequests({ navigation }));
        setLoading(false);
        setTimeout(() => { Toast.show(`${t('request-submitted-successfully')} !`); }, 300);
        navigation.goBack();
      } else {
        setLoading(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: create_resignation_request\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      setLoading(false);
      console.error(error);
    }
  };

  return (
    <View style={styles.container}>
      <Text
        style={[
          FontStyle.Regular16,
          { marginBottom: 16, color: COLORS.primaryColor, textAlign: 'left' },
        ]}>
        {t('Resignation Letter')}
      </Text>

      <View style={{ gap: 12 }}>
        {profileData?.joining_date && (
          <TextInputField
            label={t('joining-date')}
            value={`${getFormattedDate(profileData?.joining_date)}`}
            editable={false}
            labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
          />
        )}

        <DatePickerNew
          mode="date"
          date={
            inputs.lastWorkDate ? new Date(inputs.lastWorkDate) : new Date()
          }
          value={inputs.lastWorkDate && getFormattedDate(inputs.lastWorkDate)}
          label={t('last_working_date')}
          onChange={selectedDate =>
            handleDateChange(selectedDate, 'lastWorkDate')
          }
          showDatePicker={showLastWorkDatePicker}
          setShowDatePicker={setShowLastWorkDatePicker}
          placeholder={t('date')}
          error={inputs?.errors?.lastWorkDate}
        // minimumDate={new Date()}
        />

        <DatePickerNew
          mode="date"
          date={inputs.resignDate ? new Date(inputs.resignDate) : new Date()}
          value={inputs.resignDate && getFormattedDate(inputs.resignDate)}
          label={t('resignation_date')}
          onChange={selectedDate =>
            handleDateChange(selectedDate, 'resignDate')
          }
          showDatePicker={showResignDatePicker}
          setShowDatePicker={setShowResignDatePicker}
          placeholder={t('date')}
          error={inputs?.errors?.resignDate}
        // minimumDate={new Date()}
        />

        <TextInputField
          label={t('reason')}
          editable={true}
          placeholder={t('reason-plcholder')}
          value={inputs.reason}
          error={inputs?.errors?.reason}
          // multiline={true}
          // height={75}
          onChangeText={text => handleInputChange('reason', text)}
          labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
        />

        <View style={styles.bottomView}>
          <NextButton title={t('submit')} onPress={validate} loader={loading} />
        </View>
      </View>
    </View>
  );
};

export default Resignation;

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    padding: 8,
    backgroundColor: COLORS.white,
  },

  radioView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
    justifyContent: 'center',
  },

  text: {
    ...FontStyle.Regular14_500,
    marginLeft: 4,
    color: COLORS.black,
    alignSelf: 'flex-start',
    justifyContent: 'flex-start',
  },

  bottomView: {
    marginTop: 12,
    marginHorizontal: 4,
  },
});
