import { Alert, StyleSheet, Text, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import { COLORS } from '../../../theme/colors';
import { FontStyle } from '../../../theme/FontStyle';
import { useTranslation } from 'react-i18next';
import { CheckBox, NextButton, TextInputField } from '../../../components/Inputs';
import DatePickerNew from '../../../components/DateTimePicker/DatePickerNew';
import { getFormattedDate } from '../../../utilities/helpers/CurretDate';
import DropdownComponent from '../../../components/Inputs/ElementDropdown/ElementDropdown';
import { useDispatch, useSelector } from 'react-redux';
import { getCountries } from '../../../redux/travel/actions/getCountries';
import { getCities } from '../../../redux/travel/actions/getCities';
import { getStates } from '../../../redux/travel/actions/getStates';
import { getTravelModes } from '../../../redux/travel/actions/geTravelModes';
import inputValidation from '../../../utilities/Validations/YupValidate';
import { TravelSchema } from '../../../utilities/Validations/Requests/TravelSchema';
import { commonApi } from '../../../utilities/api/apiController';
import { logout_user } from '../../../redux/users/user.actions';
import Toast from 'react-native-simple-toast';
import { getAllRequests } from '../../../redux/travel/actions/getAllRequests';

const Travels = ({ navigation }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [showdepartureDatePicker, setShowDepartureDatePicker] = useState(false);
  const [showreturnDatePicker, setShowReturnDatePicker] = useState(false);
  const { employeeID } = useSelector(state => state?.employeeProfile);
  const travelData = useSelector(state => state?.request);
  const [loading, setLoading] = useState(false);
  const [fromCities, setFromCities] = useState([]);
  const [toCities, setToCities] = useState([]);

  const [inputs, setInputs] = useState({
    travelPurpose: null,

    fromCountry: null,
    fromCity: null,
    fromState: null,

    toCountry: null,
    toCity: null,
    toState: null,

    departureDate: null,
    returnDate: null,
    modeOfTravel: null,

    description: null,
    familyTicket: false,
    errors: null,
  });

  useEffect(() => {
    dispatch(getCountries({ navigation }));
    dispatch(getTravelModes({ navigation }));
  }, []);

  const handleInputChange = (field, value) => {
    if (field == 'fromCountry') {
      const selectedCountry = travelData?.countries?.find(
        country => country.id == value,
      );
      setFromCities(selectedCountry?.cities);
      setInputs({
        ...inputs,
        [field]: value,
        fromCity: null,
        errors: {
          ...inputs.errors,
          [field]: false,
        },
      });
    } else if (field == 'toCountry') {
      const selectedCountry = travelData?.countries?.find(
        country => country.id == value,
      );
      setToCities(selectedCountry?.cities);
      setInputs({
        ...inputs,
        [field]: value,
        toCity: null,
        errors: {
          ...inputs.errors,
          [field]: false,
        },
      });
    } else {
      setInputs({
        ...inputs,
        [field]: value,
        errors: {
          ...inputs.errors,
          [field]: false,
        },
      });
    }
  };
  const handleDateChange = (selectedDate, field) => {
    const dateObject = new Date(selectedDate);
    const formattedDate = dateObject.toISOString().split('T')[0];

    if (field === 'departureDate') {
      setShowDepartureDatePicker(false);
    } else if (field === 'returnDate') {
      setShowReturnDatePicker(false);
    }
    handleInputChange(field, formattedDate);
  };

  const handleSubmit = async () => {
    setLoading(true);

    try {
      const body = {
        params: {
          model: 'travel.request.wags',
          method: 'create_travel_request',
          args: [
            {
              employee_id: employeeID,
              travel_purpose: inputs?.travelPurpose,
              from_country_id: inputs?.fromCountry,
              from_city_id: inputs?.fromCity,
              to_country_id: inputs?.toCountry,
              to_city_id: inputs?.toCity,
              request_departure_date: inputs?.departureDate,
              request_return_date: inputs?.returnDate,
              request_travel_mode_id: inputs?.modeOfTravel,
              description: inputs?.description,
              family_tickets: inputs?.familyTicket,
            },
          ],
          kwargs: {},
        },
      };
      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        await dispatch(getAllRequests({ navigation }));
        setLoading(false);
        setTimeout(() => { Toast.show(`${t('request-submitted-successfully')} !`); }, 300);
        navigation.goBack();
      } else {
        setLoading(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: create_travel_request\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      setLoading(false);
      console.error(error);
    }
  };

  const validate = async () => {
    const schema = TravelSchema(t);
    const result = await inputValidation(schema, inputs);

    if (result.isValidate) {
      handleSubmit();
    } else {
      setInputs(prev => ({
        ...prev,
        errors: result?.err,
      }));
    }
  };

  return (
    <View style={styles.container}>
      <Text
        style={[
          FontStyle.Regular16,
          { marginBottom: 12, color: COLORS.primaryColor, textAlign: 'left' },
        ]}>
        {t('Travel & Ticket')}
      </Text>

      <TextInputField
        label={t('travel_purpose')}
        editable={true}
        placeholder={t('purpose-plcholder')}
        value={inputs.travelPurpose}
        error={inputs?.errors?.travelPurpose}
        onChangeText={text => handleInputChange('travelPurpose', text)}
        labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
      />

      <View style={{ gap: 12, marginTop: 12 }}>
        <Text
          style={[
            FontStyle.Regular14_500M,
            { color: COLORS.primaryColor, textAlign: 'left' },
          ]}>
          {t('from')}
        </Text>

        <DropdownComponent
          label={t('country')}
          data={travelData?.countries}
          value={inputs?.fromCountry}
          placeholder={t('select-country')}
          error={inputs?.errors?.fromCountry}
          search={true}
          onChange={selectedType =>
            handleInputChange('fromCountry', selectedType)
          }
        />
        <DropdownComponent
          label={t('City')}
          data={fromCities}
          value={inputs?.fromCity}
          placeholder={t('select-city')}
          error={inputs?.errors?.fromCity}
          onChange={selectedType => handleInputChange('fromCity', selectedType)}
        />
      </View>

      <View style={{ gap: 12, marginTop: 12 }}>
        <Text
          style={[
            FontStyle.Regular14_500M,
            { color: COLORS.primaryColor, textAlign: 'left' },
          ]}>
          {t('to')}
        </Text>

        <DropdownComponent
          label={t('country')}
          data={travelData?.countries}
          value={inputs?.toCountry}
          placeholder={t('select-country')}
          search={true}
          error={inputs?.errors?.toCountry}
          onChange={selectedType =>
            handleInputChange('toCountry', selectedType)
          }
        />

        <DropdownComponent
          label={t('City')}
          data={toCities}
          value={inputs?.toCity}
          placeholder={t('select-city')}
          error={inputs?.errors?.toCity}
          onChange={selectedType => handleInputChange('toCity', selectedType)}
        />
      </View>

      <View style={{ gap: 12, marginTop: 12 }}>
        <Text
          style={[
            FontStyle.Regular14_500M,
            { color: COLORS.primaryColor, textAlign: 'left' },
          ]}>
          {t('other-info')}
        </Text>

        <DatePickerNew
          mode="date"
          date={
            inputs.departureDate ? new Date(inputs.departureDate) : new Date()
          }
          value={inputs.departureDate && getFormattedDate(inputs.departureDate)}
          label={t('departure_date')}
          onChange={selectedDate =>
            handleDateChange(selectedDate, 'departureDate')
          }
          showDatePicker={showdepartureDatePicker}
          setShowDatePicker={setShowDepartureDatePicker}
          placeholder={t('date')}
          error={inputs?.errors?.departureDate}
          minimumDate={new Date()}
        />

        <DatePickerNew
          mode="date"
          date={inputs.returnDate ? new Date(inputs.returnDate) : new Date()}
          value={inputs.returnDate && getFormattedDate(inputs.returnDate)}
          label={t('return_date')}
          onChange={selectedDate =>
            handleDateChange(selectedDate, 'returnDate')
          }
          showDatePicker={showreturnDatePicker}
          setShowDatePicker={setShowReturnDatePicker}
          placeholder={t('date')}
          error={inputs?.errors?.returnDate}
        // minimumDate={new Date()}
        />

        <DropdownComponent
          label={t('request_travel_mode')}
          data={travelData?.travelModes}
          value={inputs?.modeOfTravel}
          placeholder={t('select-mode')}
          error={inputs?.errors?.modeOfTravel}
          onChange={selectedType =>
            handleInputChange('modeOfTravel', selectedType)
          }
        />

        <TextInputField
          label={t('description')}
          editable={true}
          placeholder={t('description-plchld')}
          value={inputs.description}
          error={inputs?.errors?.description}
          // multiline={true}
          // height={75}
          onChangeText={text => handleInputChange('description', text)}
          labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
        />

        <View style={styles.checkBoxView}>
          <CheckBox
            status={inputs?.familyTicket}
            title={`${t('family-tickets')}`}
            onPress={() => {
              setInputs({
                ...inputs,
                ['familyTicket']: !inputs.familyTicket,
              });
            }}
          />
        </View>

        <View style={styles.bottomView}>
          <NextButton title={t('submit')} onPress={validate} loader={loading} />
        </View>
      </View>
    </View>
  );
};

export default Travels;

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    padding: 8,
    marginBottom: 12,
    backgroundColor: COLORS.white,
  },

  radioView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
    justifyContent: 'center',
  },

  text: {
    ...FontStyle.Regular14_500,
    marginLeft: 4,
    color: COLORS.black,
    alignSelf: 'flex-start',
    justifyContent: 'flex-start',
  },

  bottomView: {
    marginTop: 12,
    marginHorizontal: 4,
  },
});
