import { Alert, StyleSheet, Text, View } from 'react-native';
import React, { useState } from 'react';
import { COLORS } from '../../../theme/colors';
import { FontStyle } from '../../../theme/FontStyle';
import { useTranslation } from 'react-i18next';
import { NextButton, TextInputField } from '../../../components/Inputs';
import { RadioButton } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { commonApi } from '../../../utilities/api/apiController';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../../redux/users/user.actions';
import inputValidation from '../../../utilities/Validations/YupValidate';
import { EmpLetterSchema } from '../../../utilities/Validations/Requests/EmpLetterSchema';
import { getAllRequests } from '../../../redux/travel/actions/getAllRequests';

const EmploymentLetter = ({ navigation }) => {
  const { t } = useTranslation();

  const CommerceEStamps = [
    { id: 1, name: t('yes'), key: 'yes' },
    { id: 2, name: t('no'), key: 'no' },
  ];
  const LetterTypes = [
    {
      id: 1,
      name: t('identification_letter_with_salary'),
      key: 'identification_letter_with_salary',
    },
    {
      id: 2,
      name: t('identification_letter_without_salary'),
      key: 'identification_letter_without_salary',
    },
  ];

  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const { employeeID } = useSelector(state => state?.employeeProfile);
  const [inputs, setInputs] = useState({
    letterType: null,
    eStamp: null,
    reason: null,
    errors: null,
  });

  const handleInputChange = (field, value) => {
    setInputs({
      ...inputs,
      [field]: value,
      errors: {
        ...inputs.errors,
        [field]: false,
      },
    });
  };

  const validate = async () => {
    const schema = EmpLetterSchema(t);
    const result = await inputValidation(schema, inputs);

    if (result.isValidate) {
      handleSubmit();
    } else {
      setInputs(prev => ({
        ...prev,
        errors: result?.err,
      }));
    }
  };

  const handleSubmit = async () => {
    setLoading(true);

    try {
      const body = {
        params: {
          model: 'identification.letter',
          method: 'create_identification_letter',
          args: [
            {
              employee_id: employeeID,
              identification_letter_type: inputs?.letterType?.key, // identification_letter_without_salary
              chamber_commerce_stamp: inputs?.eStamp?.key, // yes or no
              reason: inputs?.reason,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        await dispatch(getAllRequests({ navigation }));
        setLoading(false);
        setTimeout(() => { Toast.show(`${t('request-submitted-successfully')} !`); }, 300);
        navigation.goBack();
      } else {
        setLoading(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: create_identification_letter\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      setLoading(false);
      console.error(error);
    }
  };

  return (
    <View style={styles.container}>
      <Text
        style={[
          FontStyle.Regular16,
          { marginBottom: 16, color: COLORS.primaryColor, textAlign: 'left' },
        ]}>
        {t('Employment Letter')}
      </Text>

      <View style={{ gap: 12 }}>
        <View style={{}}>
          <Text
            style={[
              FontStyle.Regular16_500M,
              [FontStyle.Regular14, { color: COLORS.darkBlack }],
              {
                color: COLORS.darkBlack,
                textAlign: 'left',
                marginLeft: 6,
                marginBottom: 4,
              },
            ]}>
            {t('chamber_commerce_stamp')}
          </Text>

          {CommerceEStamps?.map(item => (
            <View key={item.id} style={styles.radioView}>
              <View>
                <RadioButton.Android
                  color={COLORS.primaryColor}
                  value={item.id}
                  status={
                    inputs?.eStamp?.id === item.id ? 'checked' : 'unchecked'
                  }
                  onPress={() => handleInputChange('eStamp', item)}
                />
              </View>
              <View style={{ flex: 1 }}>
                <Text style={styles.text}>{item.name}</Text>
              </View>
            </View>
          ))}
          {inputs?.errors?.eStamp && (
            <Text
              style={[
                styles.label,
                { color: COLORS.red, marginTop: 2, fontSize: 12 },
              ]}>
              {inputs?.errors?.eStamp}
            </Text>
          )}
        </View>

        <View style={{}}>
          <Text
            style={[
              FontStyle.Regular16_500M,
              [FontStyle.Regular14, { color: COLORS.darkBlack }],
              {
                color: COLORS.darkBlack,
                textAlign: 'left',
                marginLeft: 6,
                marginBottom: 4,
              },
            ]}>
            {t('letter_type')}
          </Text>
          {LetterTypes?.map(item => (
            <View key={item.id} style={styles.radioView}>
              <View>
                <RadioButton.Android
                  color={COLORS.primaryColor}
                  value={item.id}
                  status={
                    inputs?.letterType?.id === item.id ? 'checked' : 'unchecked'
                  }
                  onPress={() => handleInputChange('letterType', item)}
                />
              </View>
              <View style={{ flex: 1 }}>
                <Text style={styles.text}>{item.name}</Text>
              </View>
            </View>
          ))}
          {inputs?.errors?.letterType && (
            <Text
              style={[
                styles.label,
                { color: COLORS.red, marginTop: 2, fontSize: 12 },
              ]}>
              {inputs?.errors?.letterType}
            </Text>
          )}
        </View>
        <View style={{}}>
          <TextInputField
            label={t('reason')}
            editable={true}
            placeholder={t('reason-plcholder')}
            value={inputs.reason}
            error={inputs?.errors?.reason}
            multiline={true}
            height={75}
            onChangeText={text => handleInputChange('reason', text)}
            labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
          />
        </View>

        <View style={styles.bottomView}>
          <NextButton title={t('submit')} onPress={validate} loader={loading} />
        </View>
      </View>
    </View>
  );
};

export default EmploymentLetter;

const styles = StyleSheet.create({
  container: {
    padding: 12,
    borderRadius: 8,
    backgroundColor: COLORS.white,
  },

  radioView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 4,
    justifyContent: 'center',
  },

  text: {
    ...FontStyle.Regular14_500,
    marginLeft: 4,
    color: COLORS.black,
    alignSelf: 'flex-start',
    justifyContent: 'flex-start',
  },

  bottomView: {
    marginTop: 12,
    marginHorizontal: 4,
  },
});
