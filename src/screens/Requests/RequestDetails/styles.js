import {StyleSheet} from 'react-native';
import {COLORS} from '../../../theme/colors';
import {FontStyle} from '../../../theme/FontStyle';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 16,
    marginHorizontal: 16,
  },
  body: {
    flex: 1,
    marginHorizontal: 16,
  },
  detailsView: {
    backgroundColor: COLORS.white,
    borderRadius: 8,
    marginTop: 12,
    paddingHorizontal: 24,
    paddingVertical: 8,
  },
  rowView: {
    marginVertical: 16,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    gap: 12,
  },

  columnGap: {
    justifyContent: 'flex-start',
    gap: 16,
  },
  flatlist: {
    marginVertical: 16,
    gap: 16,
    paddingHorizontal: 2,
  },
  imgView: {
    height: 120,
    flex: 1,
    borderRadius: 8,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: COLORS.primaryColor,
  },
  heading: {
    ...FontStyle.Regular16_500M,
    textAlign: 'left',
    marginTop: 12,
  },
});
