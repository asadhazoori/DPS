import {
  View,
  Text,
  SafeAreaView,
  ActivityIndicator,
  Image,
  FlatList,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import { useTranslation } from 'react-i18next';
import Theme from '../../../theme/theme';
import { styles } from './styles';
import { FontStyle } from '../../../theme/FontStyle';
import { COLORS } from '../../../theme/colors';
import { SmartButton } from '../../../components/Inputs';
import { downloadPdf } from '../../../utilities/helpers/DownloadPdf';
import PayslipReportModal from '../../Reports/PayslipReport/PayslipReportModal';
import { AlertModal, Loader } from '../../../components/Modals';
import { SvgXml } from 'react-native-svg';
import { Icons } from '../../../assets/SvgIcons/Icons';
import { GetPaySlipAPI } from '../../../utilities/helpers/GetPaySlipAPI';
import { useDispatch } from 'react-redux';
import { GetWarningReport } from '../../../utilities/helpers/GetWarningReport';
import { GetEmploymentLetter } from '../../../utilities/helpers/GetEmploymentLetter';
import { getFormattedDate } from '../../../utilities/helpers/CurretDate';
import { GetResignationReport } from '../../../utilities/helpers/GetResignationReport';
import { getTravelAttachments } from '../../../utilities/helpers/getTravelAttachments';
import ImageModal from '../../../components/Modals/ImageModal';

const RequestDetails = ({ navigation, route }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { data, color } = route?.params;
  const [showReportModal, setShowReportModal] = useState(false);
  const [showImageModal, setShowImageModal] = useState(false);
  const [base64, setBase64] = useState(null);
  const [loading, setLoading] = useState(false);
  const [attachLoading, setAttachLoading] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);
  const [attachments, setAttachments] = useState([]);

  const [alertBox, setAlertBox] = useState({
    showBox: false,
    title: null,
    message: null,
  });

  useEffect(() => {
    if (data?.request_type == 'Travel & Ticket' && data?.has_attachments) {
      getAttachments();
    }
  }, []);

  const getAttachments = async () => {
    setAttachLoading(true);
    const attachments = await getTravelAttachments(
      data?.record_id,
      setAttachLoading,
      navigation,
      dispatch
    );
    setAttachments(attachments);
    setAttachLoading(false);
  };

  const downloadReport = async bs64 => {
    if (bs64) {
      await downloadPdf(bs64, `${data?.request_type}`, setLoading);
    } else {
      await downloadPdf(base64, `${data?.request_type}`, setLoading);
    }
  };

  const getPdfReport = async (download, requestType, isWarningRep) => {
    if (base64) {
      download ? downloadReport() : setShowReportModal(true);
    } else {
      setLoading(true);
      const res = await (requestType == 'Employee Warning'
        ? GetWarningReport(data?.record_id, setLoading, navigation, dispatch)
        : requestType == 'Employment Letter'
          ? GetEmploymentLetter(data?.record_id, setLoading, navigation, dispatch)
          : GetResignationReport(
            data?.record_id,
            setLoading,
            navigation,
            dispatch,
          ));

      if (res?.b64_pdf) {
        setBase64(res?.b64_pdf);
        download ? downloadReport(res?.b64_pdf) : setShowReportModal(true);
        setLoading(false);
      } else {
        setLoading(false);
        setTimeout(() => {
          setAlertBox({
            showBox: true,
            title: `${t('pdf-not-found')}`,
            message: res,
          });
        }, 500);
      }
    }
  };

  const closeCustomAlert = () => {
    setTimeout(() => {
      setAlertBox({ showBox: false });
    }, 300);
  };

  const Item = ({ title, value, color }) => (
    <View
      style={{ flexDirection: 'row', marginVertical: 6, alignItems: 'center' }}>
      <View style={{ flex: 1, alignItems: 'flex-start' }}>
        <Text style={[FontStyle.Regular14_500, { color: COLORS.grey5 }]}>
          {title}
        </Text>
      </View>
      <View style={{ flex: 1, alignItems: 'flex-end' }}>
        <Text
          style={[
            FontStyle.Regular14_500,
            {
              fontWeight: color ? '700' : '500',
              color: color ? color : COLORS.darkBlack,
            },
          ]}>
          {value}
        </Text>
      </View>
    </View>
  );

  const Iqama = () => (
    <>
      <Text
        style={[
          FontStyle.Regular14_500M,
          { color: COLORS.primaryColor, textAlign: 'left' },
        ]}>
        {t('previous-iqama-detail')}
      </Text>
      <Item
        title={t('iqama-issue_date')}
        value={
          data?.iqama_issue_date
            ? getFormattedDate(data?.iqama_issue_date, false)
            : '-'
        }
      />
      <Item
        title={t('iqama-expiry_date')}
        value={
          data?.iqama_expiry_date
            ? getFormattedDate(data?.iqama_expiry_date, false)
            : '-'
        }
      />
      <Text
        style={[
          FontStyle.Regular14_500M,
          { color: COLORS.primaryColor, textAlign: 'left', marginTop: 4 },
        ]}>
        {t('renewal-info')}
      </Text>
      <Item
        title={t('issue_date')}
        value={
          data?.issue_date ? getFormattedDate(data?.issue_date, false) : '-'
        }
      />
      <Item
        title={t('expiry_date')}
        value={
          data?.expiry_date ? getFormattedDate(data?.expiry_date, false) : '-'
        }
      />
      <Item title={t('amount')} value={data?.amount} />
      <Item title={t('reason')} value={data?.reason} />
      <Item title={t('status')} color={color} value={data?.state} />
    </>
  );

  const EmploymentLetter = () => (
    <>
      <Item
        title={t('chamber_commerce_stamp')}
        value={data?.chamber_commerce_stamp}
      />
      <Item title={t('letter_type')} value={data?.identification_letter_type} />
      <Item title={t('reason')} value={data?.reason} />
      <Item title={t('status')} color={color} value={data?.state} />
    </>
  );
  const Resignation = () => (
    <>
      <Item
        title={t('joining-date')}
        value={
          data?.joining_date ? getFormattedDate(data?.joining_date, false) : '-'
        }
      />
      <Item
        title={t('last_working_date')}
        value={
          data?.last_working_date
            ? getFormattedDate(data?.last_working_date, false)
            : '-'
        }
      />
      <Item
        title={t('resignation_date')}
        value={
          data?.resignation_date
            ? getFormattedDate(data?.resignation_date, false)
            : '-'
        }
      />
      {data?.type_of_request && (
        <Item title={t('type_of_request')} value={data?.type_of_request} />
      )}
      {/* {data?.type_of_contract && (
        <Item title={t('type_of_contract')} value={data?.type_of_contract} />
      )} */}
      {data?.notice_period && (
        <Item title={t('notice_period')} value={data?.notice_period} />
      )}
      <Item title={t('resignation_resone')} value={data?.resignation_reason} />
      {data?.reject_reason && (
        <Item title={t('reject_reason')} value={data?.reject_reason} />
      )}
      <Item title={t('status')} color={color} value={data?.state} />
    </>
  );

  const Travel = () => (
    <>
      <Item title={t('travel_purpose')} value={data?.travel_purpose} />
      <Item
        title={t('from')}
        value={`${data?.from_country},\n${data?.from_city}`}
      />
      <Item title={t('to')} value={`${data?.to_country},\n${data?.to_city}`} />
      <Item
        title={t('departure_date')}
        value={
          data?.request_departure_date
            ? getFormattedDate(data?.request_departure_date, false)
            : '-'
        }
      />
      <Item
        title={t('return_date')}
        value={
          data?.request_return_date
            ? getFormattedDate(data?.request_return_date, false)
            : '-'
        }
      />
      <Item title={t('days1')} value={data?.days} />
      <Item
        title={t('request_travel_mode')}
        value={data?.request_travel_mode}
      />
      <Item title={t('total_members')} value={data?.total_members} />
      <Item title={t('description')} value={data?.description} />
      <Item
        title={t('request_date')}
        value={
          data?.request_date ? getFormattedDate(data?.request_date, false) : '-'
        }
      />
      <Item title={t('status')} color={color} value={data?.state} />
    </>
  );

  const Warning = () => (
    <>
      <Item title={t('warning_type')} value={data?.warning_type} />
      <Item title={t('deduction_type')} value={data?.deduction_type} />
      <Item title={t('warning_reasons')} value={data?.warning_reasons} />
      <Item title={t('warning_message')} value={data?.warning_message} />
      <Item
        title={t('date')}
        value={data?.date ? getFormattedDate(data?.date, false) : '-'}
      />
    </>
  );

  const Images = ({ item }) => (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={() => {
        setSelectedImage(item?.base64);
        setShowImageModal(true);
      }}
      style={styles.imgView}>
      <Image
        source={{
          uri: `data:image/jpeg;base64,${item?.base64}`,
        }}
        style={{ flex: 1 }}
      />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader title={t(data?.request_type)} navigation={navigation} />
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={styles.detailsView}>
          {data?.request_type == 'Iqama Renewal' ? (
            <Iqama />
          ) : data?.request_type == 'Employment Letter' ? (
            <EmploymentLetter />
          ) : data?.request_type == 'Employee Resignation' ? (
            <Resignation />
          ) : data?.request_type == 'Travel & Ticket' ? (
            <Travel />
          ) : data?.request_type == 'Employee Warning' ? (
            <Warning />
          ) : null}
        </View>

        {(data?.request_type == 'Employment Letter' ||
          (data?.request_type == 'Employee Resignation' &&
            data?.state == 'Exit') ||
          data?.request_type == 'Employee Warning') && (
            <View style={styles.rowView}>
              <SmartButton
                title={t('open-pdf-report')}
                handlePress={() => {
                  getPdfReport(false, data?.request_type);
                }}
              />

              <SmartButton
                title={t('download-pdf')}
                handlePress={() => {
                  getPdfReport(true, data?.request_type);
                }}
              />
            </View>
          )}

        {data?.has_attachments && (
          <View style={{ flex: 1 }}>
            <Text style={styles.heading}>{t('attachments')}</Text>

            {attachLoading && (
              <View style={{ marginBottom: 12 }}>
                <ActivityIndicator color={COLORS.primaryColor} size={24} />
              </View>
            )}
            <FlatList
              contentContainerStyle={styles.flatlist}
              numColumns={3}
              data={attachments}
              renderItem={Images}
              columnWrapperStyle={styles.columnGap}
              scrollEnabled={false}
            />
          </View>
        )}

        <AlertModal
          visible={alertBox.showBox}
          icon={
            <View style={styles.iconView}>
              <SvgXml xml={Icons.payslip} />
            </View>
          }
          title={alertBox.title}
          message={alertBox.message}
          onClose={closeCustomAlert}
        />

        <Loader loading={loading} title={t('loading')} />
        <PayslipReportModal
          modalVisible={showReportModal}
          setModalVisible={setShowReportModal}
          base64={base64}
        />

        <ImageModal
          modalVisible={showImageModal}
          setModalVisible={setShowImageModal}
          selectedImage={selectedImage}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default RequestDetails;
