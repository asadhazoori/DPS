import {StyleSheet} from 'react-native';
import {COLORS} from '../../../theme/colors';
import {FontStyle} from '../../../theme/FontStyle';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: 'blue',
    marginHorizontal: 16,
  },
  body: {
    flex: 1,
  },
  dateText: {
    ...FontStyle.Regular12,
    fontWeight: '500',
    color: COLORS.grey5,
  },
});
