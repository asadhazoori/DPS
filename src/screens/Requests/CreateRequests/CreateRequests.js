import {View, Text, SafeAreaView, ScrollView} from 'react-native';
import React, {useState} from 'react';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import Theme from '../../../theme/theme';
import {styles} from './styles';
import {useTranslation} from 'react-i18next';
import DropdownComponent from '../../../components/Inputs/ElementDropdown/ElementDropdown';
import EmploymentLetter from '../RequestForms/EmploymentLetter';
import Resignation from '../RequestForms/Resignation';
import IqamaRenewal from '../RequestForms/IqamaRenewal';
import Travels from '../RequestForms/Travels';
import {useSelector} from 'react-redux';

const CreateRequests = ({navigation}) => {
  const {t} = useTranslation();

  const [selectedRequestType, setRequestType] = useState();
  const {requestTypes} = useSelector(state => state?.request);

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader title={t('create-requests')} navigation={navigation} />
      <ScrollView style={{marginTop: 16}}>
        <View style={styles.container}>
          <View style={{zIndex: 9999, marginBottom: 16}}>
            <DropdownComponent
              label={t('request-type')}
              data={requestTypes}
              value={selectedRequestType}
              placeholder={t('select-request-type')}
              onChange={selectedType => setRequestType(selectedType)}
            />
          </View>

          {selectedRequestType == '2' ? (
            <EmploymentLetter navigation={navigation} />
          ) : selectedRequestType == '3' ? (
            <Travels navigation={navigation} />
          ) : selectedRequestType == '4' ? (
            <Resignation navigation={navigation} />
          ) : selectedRequestType == '5' ? (
            <IqamaRenewal navigation={navigation} />
          ) : null}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default CreateRequests;
