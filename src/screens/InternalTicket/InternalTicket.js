import {
    View,
    SafeAreaView,
    StyleSheet,
    FlatList,
    Text,
    RefreshControl,
    Dimensions,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import GeneralHeader from '../../components/Headers/GeneralHeader';
import Theme from '../../theme/theme';
import { useTranslation } from 'react-i18next';
import SmartButtonTrans from '../../components/Buttons/SmartButtonTrans';
import { Icons } from '../../assets/SvgIcons/Icons';
import { COLORS } from '../../theme/colors';
import { FontStyle } from '../../theme/FontStyle';
import { useDispatch, useSelector } from 'react-redux';
import { getTicketTypes } from '../../redux/travel/actions/getTicketTypes';
import { getTickets } from '../../redux/travel/actions/getTickets';
import TicketRequestCard from '../../components/Cards/TicketRequestCard';
import TicketBalanceCard from '../../components/Cards/TicketBalanceCard';

export const window = Dimensions.get('window');

const InternalTicket = ({ navigation }) => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { tickets, ticketCounts } = useSelector(state => state?.request);
    const [refresh, setRefresh] = useState(false);

    const getData = async () => {
        try {
            setRefresh(true);
            await dispatch(getTickets({ navigation }));
            setRefresh(false);
            await dispatch(getTicketTypes({ navigation }));
        } catch (error) {
            setRefresh(false);
            console.log('Error while calling getTicketTypes', error);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <SafeAreaView style={Theme.SafeArea}>
            <GeneralHeader title={t("internal-ticket")} navigation={navigation} />
            <View style={styles.container}>
                <View
                    style={{
                        alignItems: 'flex-end',
                        marginVertical: 16,
                        marginBottom: 16,
                        marginRight: 16,
                    }}>
                    <SmartButtonTrans
                        title={t('add-new')}
                        rightIcon={Icons.plus}
                        onPress={() => navigation.navigate("ApplyInternalTicket")}
                    />
                </View>

                <View style={{ paddingHorizontal: 16 }}>
                    <TicketBalanceCard item={ticketCounts} />
                </View>

                <View style={styles.body}>
                    <FlatList
                        data={tickets}
                        removeClippedSubviews={false}
                        showsVerticalScrollIndicator={false}
                        refreshControl={
                            <RefreshControl
                                refreshing={refresh}
                                onRefresh={() => {
                                    getData();
                                }}
                                tintColor={COLORS.primaryColor}
                                colors={[COLORS.primaryColor]}
                            />
                        }
                        contentContainerStyle={{
                            paddingBottom: 16,
                            paddingTop: 16,
                            flexGrow: 1,
                        }}
                        renderItem={({ item, index }) => {
                            return (
                                <TicketRequestCard
                                    data={item}
                                    navigation={navigation}
                                    getTickets={getData}
                                />
                            );
                        }}
                        keyExtractor={(item, index) => index}
                        ItemSeparatorComponent={() => <View style={{ height: 12 }} />}
                        ListEmptyComponent={() => (
                            <>
                                {/* && !refresh */}
                                {tickets?.length < 1 && (
                                    <Text style={[styles.dateText, { alignSelf: 'center' }]}>
                                        {t('no-record-found')}
                                    </Text>
                                )}
                            </>
                        )}
                    />
                </View>

            </View>
        </SafeAreaView>
    );
};

export default InternalTicket;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    body: {
        flex: 1,
        marginHorizontal: 16,
    },
    dateText: {
        ...FontStyle.Regular12,
        fontWeight: '500',
        color: COLORS.grey5,
    },
});
