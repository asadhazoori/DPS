import { SafeAreaView, ScrollView, Text, View } from 'react-native';
import React, { useState } from 'react';
import Theme from '../../../theme/theme';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import { useTranslation } from 'react-i18next';
import { AlertModal, Loader } from '../../../components/Modals';
import { COLORS } from '../../../theme/colors';
import { Icons } from '../../../assets/SvgIcons/Icons';
import { useDispatch } from 'react-redux';
import { FontStyle } from '../../../theme/FontStyle';
import { SmartButton } from '../../../components/Inputs';
import { logout_user } from '../../../redux/users/user.actions';
import { commonApi } from '../../../utilities/api/apiController';
import Toast from 'react-native-simple-toast';
import { SvgXml } from 'react-native-svg';
import { styles } from './styles';
import { LOGOUT_USER } from '../../../redux/users/user.types';
import { getFormattedDate } from '../../../utilities/helpers/CurretDate';
import ReactNativeVI from '../../../components/Helpers/ReactNativeVI';

const InternalTicketDetail = ({ navigation, route }) => {
    const { data } = route?.params;
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const [loading, setLoading] = useState(false)
    const [alertBox, setAlertBox] = useState({
        showBox: false,
        title: null,
        message: null,
    });

    const closeCustomAlert = () => {
        setTimeout(() => {
            setAlertBox({ showBox: false });
        }, 300);
    };

    let status = ''
    let color = ''

    switch (data?.state) {
        case 'Draft':
            color = COLORS.yellow
            status = 'Pending'
            break;

        case 'Approved':
            color = COLORS.primaryColor
            status = 'Approved'
            break;

        case 'Assigned':
            color = COLORS.primaryColor
            status = 'Assigned'
            break;

        case 'Complete':
            color = COLORS.green
            status = 'Complete'
            break;

        case 'Closed':
            color = COLORS.green
            status = 'Closed'
            break;

        case 'Re-opened':
            color = COLORS.red
            status = 'Re-Opened'
            break;

        default:
            color = 'grey'
            break;
    }

    const handleSubmit = async () => {
        setLoading(true);

        try {
            const body = {
                params: {
                    model: 'internal.ticket.wags',
                    method: 'close_internal_ticket',
                    args: [
                        {
                            ticket_id: data.id
                        },
                    ],
                    kwargs: {},
                },
            };

            const response = await commonApi({ body, navigation });
            // console.log("internal.ticket.wags")
            setLoading(false);

            if (response?.data?.result) {
                if (response?.data?.result) {
                    setLoading(false);
                    setTimeout(() => { Toast.show(`${"Ticket state update successfully"} !`); }, 300);
                } else if (response?.data?.result?.error) {
                    setLoading(false);
                }
            } else {
                setLoading(false);
                if (response?.data?.error) {
                    if (response?.data?.error?.message == 'Odoo Session Expired') {
                        setTimeout(() => { Toast.show(t('session-expired')); }, 500);
                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Login' }],
                        });
                        dispatch(logout_user(false));
                    } else {
                        setTimeout(() => {
                            setAlertBox({
                                showBox: true,
                                title: `${'Validation Error'}`,
                                message: `${response?.data?.error?.data?.message}`,
                            });
                        }, 300);
                    }
                } else if (
                    response == 'AxiosError: Request failed with status code 404'
                ) {
                    setTimeout(() => { Toast.show(t('session-expired')); }, 500);
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Login' }],
                    });
                    dispatch(logout_user(false));
                }
            }
        } catch (error) {
            console.error(error);
        }
    };

    const TextView = ({ title, value }) => (
        <View style={{ flexDirection: 'row', marginVertical: 6 }}>
            <View style={{ flex: 1, alignItems: 'flex-start' }}>
                <Text style={styles.label}>
                    {title}
                </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-start' }}>
                <Text
                    style={styles.title}>
                    {value}
                </Text>
            </View>
        </View>
    );

    return (
        <SafeAreaView style={Theme.SafeArea}>
            <GeneralHeader
                title={t("ticket-detail")}
                navigation={navigation}
            />
            <View style={styles.container}>

                <ScrollView contentContainerStyle={{ paddingVertical: 16 }} showsVerticalScrollIndicator={false}>

                    <View style={{ flex: 1 }}>

                        <View style={styles.detailsView}>

                            <View style={{ marginVertical: 6, alignItems: 'flex-start' }}>
                                <Text style={[FontStyle.Regular18, { fontWeight: "700", color: COLORS.black }]}>{data.subject_name?.trim()}</Text>
                            </View>
                            <View style={{ marginVertical: 6, alignItems: 'flex-start' }}>
                                <Text style={[FontStyle.Regular14_500, { fontWeight: "700" }]}>{t("description")}</Text>
                                <Text style={[FontStyle.Regular16_500, { color: COLORS.black }]}>{data?.description?.trim()}</Text>
                            </View>

                            <View style={{ marginVertical: 6, alignItems: 'flex-start' }}>
                                <Text style={[FontStyle.Regular14_500, { fontWeight: "700" }]}>{t("support-team")}</Text>
                            </View>

                            {
                                data.ticket_type_id[1] &&
                                <TextView title={t("ticket-type")} value={data.ticket_type_id[1]} />
                            }
                            {
                                data.ticket_category_id[1] &&
                                <TextView title={t("ticket-category")} value={data.ticket_category_id[1]} />
                            }
                            {
                                data.department_id[1] &&
                                <TextView title={t("department")} value={data.department_id[1]} />
                            }
                            {
                                data.team_id[1] &&
                                <TextView title={t("support-team")} value={data.team_id[1]} />
                            }
                            {
                                data.senior_manager_id[1] &&
                                <TextView title={t("senior-manager")} value={data.senior_manager_id[1]} />
                            }
                            {
                                data.team_manager_id[1] &&
                                <TextView title={t("team-manager")} value={data.team_manager_id[1]} />
                            }
                            <View style={{ marginVertical: 6, alignItems: 'flex-start' }}>
                                <Text style={[FontStyle.Regular14_500, { fontWeight: "700" }]}>{t("support-ticket")}</Text>
                            </View>

                            {
                                data?.rating &&
                                <View style={{ flexDirection: 'row', marginVertical: 6 }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.label}>{t("rating")}</Text>
                                    </View>

                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start', gap: 4, }}>
                                        {Array.from({ length: data?.rating - 1 || 0 }).map((_, index) => (
                                            <ReactNativeVI
                                                key={index}
                                                Lib={'AntDesign'}
                                                name={'star'}
                                                color={COLORS.yellow}
                                                size={20}
                                            />
                                        ))}
                                    </View>
                                </View>
                            }

                            {
                                data.create_date &&
                                <TextView title={t("created-date")} value={getFormattedDate(data.create_date)} />
                            }

                            {
                                data.assign_to_id &&
                                <TextView title={t("assign-to")} value={data.assign_to_id[1]} />
                            }
                            {
                                data.approve_date &&
                                <TextView title={t("approve-date")} value={getFormattedDate(data.approve_date)} />
                            }
                            {
                                data.assign_date &&
                                <TextView title={t("assign-date")} value={getFormattedDate(data.assign_date)} />
                            }
                            {
                                data.completion_date &&
                                <TextView title={t("completion-date")} value={getFormattedDate(data.completion_date)} />
                            }
                            {
                                data.closure_date &&
                                <TextView title={t("closure-date")} value={getFormattedDate(data.closure_date)} />
                            }

                            <View style={styles.statusView}>
                                <Text style={[FontStyle.Regular16_500M, { color: color ? color : COLORS.primaryColor, fontWeight: '700' }]}>{status}</Text>
                            </View>
                        </View>

                        {
                            status === "Complete" &&
                            <View
                                style={{
                                    margin: 16,
                                    justifyContent: 'flex-end',
                                    flexDirection: 'row',
                                }}>

                                <SmartButton
                                    title={t("close")}
                                    loader={loading}
                                    handlePress={() => handleSubmit()}
                                />
                            </View>}
                    </View>

                    <AlertModal
                        visible={alertBox.showBox}
                        icon={
                            <View style={styles.iconView}>
                                <SvgXml xml={Icons.payslip} />
                            </View>
                        }
                        title={alertBox.title}
                        message={alertBox.message}
                        onClose={closeCustomAlert}
                    />

                </ScrollView>
            </View>
        </SafeAreaView>
    );
};

export default InternalTicketDetail;
