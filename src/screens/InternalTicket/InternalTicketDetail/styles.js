import { StyleSheet } from "react-native";
import { COLORS } from "../../../theme/colors";
import { FontStyle } from "../../../theme/FontStyle";


export const styles = StyleSheet.create({
    container: {
        marginTop: 6,
        marginHorizontal: 16,
        flex: 1,
    },

    detailsView: {
        backgroundColor: COLORS.white,
        borderRadius: 8,
        paddingHorizontal: 18,
        paddingVertical: 8,
    },

    dateText: {
        ...FontStyle.Regular16_500M,
        color: COLORS.grey5,
    },

    statusView: {
        flex: 1,
        marginTop: 8,
        alignItems: 'flex-end',
        justifyContent: 'center',

    },

    iconView: {
        backgroundColor: '#d0f7e5',
        height: 48,
        width: 48,
        borderRadius: 48,
        justifyContent: 'center',
        alignItems: 'center',
    },

    label: {
        ...FontStyle.Regular14_500,
        color: COLORS.grey5
    },

    title: {
        ...FontStyle.Regular14_500,
        fontWeight: '500',
        color: COLORS.darkBlack,
    },
});