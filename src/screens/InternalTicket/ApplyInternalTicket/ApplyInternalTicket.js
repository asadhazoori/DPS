import { SafeAreaView, ScrollView, View } from 'react-native';
import React, { useState } from 'react';
import { styles } from './styles';
import TextInput from '../../../components/Inputs/TextInputField';
import { useTranslation } from 'react-i18next';
import { COLORS } from '../../../theme/colors';
import { NextButton } from '../../../components/Inputs';
import { getFormattedDate } from '../../../utilities/helpers/CurretDate';
import DatePickerNew from '../../../components/DateTimePicker/DatePickerNew';
import { FontStyle } from '../../../theme/FontStyle';
import { OvertimeRequestSchema } from '../../../utilities/Validations';
import inputValidation from '../../../utilities/Validations/YupValidate';
import GenericModal from '../../../components/Modals/GenericModal';
import { commonApi } from '../../../utilities/api/apiController';
import { logout_user } from '../../../redux/users/user.actions';
import { useDispatch, useSelector } from 'react-redux';
import Toast from 'react-native-simple-toast';
import { AlertModal } from '../../../components/Modals';
import { DrawerIcons } from '../../../assets/SvgIcons/DrawerIcons';
import { SvgXml } from 'react-native-svg';
import DropdownComponent from '../../../components/Inputs/ElementDropdown/ElementDropdown';
import { TicketSchema } from '../../../utilities/Validations/Ticket/TicketSchema';
import Theme from '../../../theme/theme';
import GeneralHeader from '../../../components/Headers/GeneralHeader';
import { getTickets } from '../../../redux/travel/actions/getTickets';
import { DashboardIcons } from '../../../assets/SvgIcons/DashboardIcons';

const ApplyInternalTicket = ({
    navigation,
}) => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    const { ticketTypes, ticketCategories } = useSelector(
        state => state?.request,
    );
    const [inputs, setInputs] = useState({
        subject: null,
        ticketType: null,
        // ticketCategories: null,
        description: null,
    });

    const [alertBox, setAlertBox] = useState({
        showBox: false,
        title: null,
        message: null,
        confirmbtn: null,
        okbtn: null,
    });

    const handleInputChange = (field, value) => {
        setInputs({
            ...inputs,
            [field]: value,
            errors: {
                ...inputs.errors,
                [field]: false,
            },
        });
    };

    const validate = async () => {
        const schema = TicketSchema(t);
        const result = await inputValidation(schema, inputs);

        if (result.isValidate) {
            handleSubmit();
        } else {
            setInputs(prev => ({
                ...prev,
                errors: result?.err,
            }));
        }
    };

    const handleSubmit = async () => {
        setLoading(true);

        try {
            const body = {
                params: {
                    method: 'create_internal_ticket',
                    model: 'internal.ticket.wags',
                    args: [
                        {
                            subject_name: inputs?.subject,
                            description: inputs.description,
                            ticket_type_id: inputs.ticketType,
                        },
                    ],
                    kwargs: {},
                },
            };

            const response = await commonApi({ body, navigation });
            setLoading(false);

            // console.log(response.data)
            if (response?.data?.result) {
                if (response?.data?.result?.success) {
                    setLoading(false);
                    dispatch(getTickets({ navigation }));
                    setTimeout(() => { Toast.show(`${"Ticket Added Successfully"} !`); }, 300);
                    navigation.goBack();
                } else {
                    setTimeout(() => {
                        setAlertBox({
                            showBox: true,
                            title: `${'Server Error'}`,
                            message: `${response?.data?.result?.response}`,
                        });
                    }, 300);
                }

            } else {
                setLoading(false);
                if (response?.data?.error) {
                    if (response?.data?.error?.message == 'Odoo Session Expired') {
                        setTimeout(() => { Toast.show(t('session-expired')); }, 500);
                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Login' }],
                        });
                        dispatch(logout_user(false));
                    } else {
                        setTimeout(() => {
                            setAlertBox({
                                showBox: true,
                                title: `${'Server Error'}`,
                                message: `${response?.data?.error?.data?.message}`,
                            });
                        }, 300);
                    }
                } else if (response == 'AxiosError: Request failed with status code 404') {
                    setTimeout(() => { Toast.show(t('session-expired')); }, 500);
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Login' }],
                    });
                    dispatch(logout_user(false));
                }
            }
        } catch (error) {
            setLoading(false);
            console.error(error);
        }
    };

    const closeCustomAlert = () => {
        setTimeout(() => {
            setAlertBox({ showBox: false });
        }, 300);
    };

    return (
        <SafeAreaView style={Theme.SafeArea}>

            <GeneralHeader title={t("add-ticket")} profileIcon={true} navigation={navigation} />
            <ScrollView style={{ marginTop: 16 }} showsVerticalScrollIndicator={false}>
                <View style={styles.container}>

                    <TextInput
                        label={t("subject")}
                        placeholder={t("subject-placeholder")}
                        value={inputs.subject}
                        error={inputs?.errors?.subject}
                        labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
                        onChangeText={text => handleInputChange('subject', text)}
                    />

                    <TextInput
                        label={t("description")}
                        placeholder={t("description-plchld")}
                        value={inputs.description}
                        error={inputs?.errors?.description}
                        multiline={true}
                        height={160}

                        labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
                        onChangeText={(text) => handleInputChange('description', text)}
                    />

                    <View style={{ zIndex: 9999 }}>
                        <DropdownComponent
                            label={t("ticket-type")}
                            data={ticketTypes}
                            value={inputs?.ticketType}
                            placeholder={t("select-ticket-type")}
                            error={inputs?.errors?.ticketType}
                            onChange={selectedType => handleInputChange('ticketType', selectedType)}
                        />
                    </View>

                    {/* <View style={{ zIndex: 9999 }}>
                        <DropdownComponent
                            label={t("ticket-category")}
                            data={ticketCategories}
                            value={inputs?.ticketCategories}
                            placeholder={t("ticket-category-placeholder")}
                            error={inputs?.errors?.ticketCategories}
                            onChange={selectedType => handleInputChange('ticketCategories', selectedType)}
                        />
                    </View> */}

                    <View style={styles.bottomView}>
                        <NextButton title={t('submit')} onPress={validate} loader={loading} />
                    </View>
                </View>
            </ScrollView>
            <AlertModal
                visible={alertBox.showBox}
                icon={
                    <View style={styles.alertIconView}>
                        <SvgXml xml={DashboardIcons.timesheet} />
                    </View>
                }
                title={alertBox.title}
                message={alertBox.message}
                onClose={closeCustomAlert}
                confirmbtn={alertBox.confirmbtn}
            />
        </SafeAreaView>
    );
};

export default ApplyInternalTicket;
