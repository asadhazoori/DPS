import { StyleSheet } from 'react-native';
import { COLORS } from '../../../theme/colors';

export const styles = StyleSheet.create({
    container: {
        // paddingBottom: 16,
        // paddingHorizontal: 16,
        // gap: 12,

        marginHorizontal: 16,
        padding: 16,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        gap: 12,
    },

    bottomView: {
        marginTop: 12,
        marginHorizontal: 4,
    },
    alertIconView: {
        backgroundColor: '#F73C3C3D',
        height: 48,
        width: 48,
        borderRadius: 48,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
