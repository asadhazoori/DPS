import {
  View,
  SafeAreaView,
  StyleSheet,
  FlatList,
  Text,
  RefreshControl,
  Dimensions,
  Platform,
  I18nManager,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import GeneralHeader from '../../components/Headers/GeneralHeader';
import Theme from '../../theme/theme';
import ApplyLoan from './ApplyLoan/ApplyLoan';
import { useTranslation } from 'react-i18next';
import SmartButtonTrans from '../../components/Buttons/SmartButtonTrans';
import { Icons } from '../../assets/SvgIcons/Icons';
import { COLORS } from '../../theme/colors';
import { FontStyle } from '../../theme/FontStyle';
import LoanBalanceCard from '../../components/Cards/LoansBalanceCard';
import { getAllLoans } from '../../redux/loans/actions/getAllLoans';
import { useDispatch, useSelector } from 'react-redux';
import { getLoanTypes } from '../../redux/loans/actions/getLoanTypes';
import Carousel from 'react-native-reanimated-carousel';
import { getEmployeeLoans } from '../../redux/loans/actions/getEmployeeLoans';
import LoanRequestCard from '../../components/Cards/LoanRequestCard';

export const window = Dimensions.get('window');

const PAGE_WIDTH = window.width;

const baseOptions = {
  vertical: false,
  width: PAGE_WIDTH,
  height: null,
};

const Loans = ({ navigation }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { isManager, loanCards, employeeWiseLoanCounts } = useSelector(
    state => state?.employeeProfile,
  );
  const { empWiseLoanReqs, mangrWiseLoanReqs } = useSelector(
    state => state?.loans,
  );
  const [filteredData, setFilteredData] = useState(empWiseLoanReqs);
  const [selectedCategoryId, setSelectedCategoryId] = useState(1);
  const [modalVisible1, setModalVisible1] = useState(false);
  const [managersData, setManagersData] = useState(false);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    if (selectedCategoryId == 1) {
      setFilteredData(empWiseLoanReqs);
    } else {
      setFilteredData(mangrWiseLoanReqs);
    }
  }, [empWiseLoanReqs, mangrWiseLoanReqs]);

  useEffect(() => {
    getData('emp');
  }, []);

  const getData = async category => {
    try {
      setRefresh(true);

      await dispatch(getLoanTypes({ navigation }));

      if (category === 'emp') {
        await dispatch(getAllLoans({ navigation }));
      } else if (category === 'man') {
        setManagersData(true);
        await dispatch(getEmployeeLoans({ navigation }));
      } else {
        if (isManager) {
          await dispatch(getEmployeeLoans({ navigation }));
        }
        await dispatch(getAllLoans({ navigation }));
      }

      setRefresh(false);
    } catch (error) {
      setRefresh(false);
      console.log('Error while calling getAllEmployeeLoans', error);
    }
  };

  return (
    <SafeAreaView style={Theme.SafeArea}>
      <GeneralHeader title={t('loans-advances')} navigation={navigation} />
      <View style={styles.container}>
        <View
          style={{
            alignItems: 'flex-end',
            marginVertical: 16,
            marginBottom: 16,
            marginRight: 16,
          }}>
          <SmartButtonTrans
            title={t('add-new')}
            rightIcon={Icons.plus}
            onPress={() => setModalVisible1(true)}
          />
        </View>

        {isManager ? (
          <View
            style={{
              height: Platform.OS == 'ios' && I18nManager.isRTL ? 150 : 120,
            }}>
            <Carousel
              loop={false}
              {...baseOptions}
              style={{
                width: PAGE_WIDTH,
              }}
              mode="parallax"
              modeConfig={{
                parallaxScrollingScale: 0.9,
                parallaxScrollingOffset: 110,
              }}
              data={loanCards}
              onSnapToItem={async index => {
                const selected = loanCards[index];
                if (selected?.id == 1) {
                  setSelectedCategoryId(selected.id);
                  setFilteredData(empWiseLoanReqs);
                } else if (selected.id == 2) {
                  setSelectedCategoryId(selected.id);
                  setFilteredData(mangrWiseLoanReqs);
                  if (!managersData) {
                    await getData('man');
                  }
                }
              }}
              renderItem={({ item, index }) => {
                return (
                  <LoanBalanceCard
                    item={item}
                    selectedCategoryId={selectedCategoryId}
                  />
                );
              }}
            />
          </View>
        ) : (
          <View style={{ paddingHorizontal: 16 }}>
            <LoanBalanceCard item={employeeWiseLoanCounts} />
          </View>
        )}

        <View style={styles.body}>
          <FlatList
            data={filteredData}
            removeClippedSubviews={false}
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={() => {
                  if (selectedCategoryId == 1) {
                    getData('emp');
                  } else {
                    getData('man');
                  }
                }}
                tintColor={COLORS.primaryColor}
                colors={[COLORS.primaryColor]}
              />
            }
            contentContainerStyle={{
              paddingBottom: 16,
              paddingTop: isManager ? 0 : 16,
              flexGrow: 1,
            }}
            renderItem={({ item, index }) => {
              return (
                <LoanRequestCard
                  data={item}
                  employeeLoan={selectedCategoryId == 1 ? false : true}
                  navigation={navigation}
                  getEmployeeLoans={getData}
                />
              );
            }}
            keyExtractor={(item, index) => index}
            ItemSeparatorComponent={() => <View style={{ height: 12 }} />}
            ListEmptyComponent={() => (
              <>
                {filteredData?.length < 1 && !refresh && (
                  <Text style={[styles.dateText, { alignSelf: 'center' }]}>
                    {t('no-record-found')}
                  </Text>
                )}
              </>
            )}
          />
        </View>

        <ApplyLoan
          modalVisible1={modalVisible1}
          setModalVisible1={setModalVisible1}
          navigation={navigation}
          getLoans={getData}
        />
      </View>
    </SafeAreaView>
  );
};

export default Loans;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    flex: 1,
    marginHorizontal: 16,
  },
  dateText: {
    ...FontStyle.Regular12,
    fontWeight: '500',
    color: COLORS.grey5,
  },
});
