import React, { useState } from 'react';
import { View } from 'react-native';
import inputValidation from '../../../utilities/Validations/YupValidate';
import { commonApi } from '../../../utilities/api/apiController';
import { LoanRequestSchema } from '../../../utilities/Validations';
import { useDispatch, useSelector } from 'react-redux';
import { styles } from './styles';
import { NextButton, TextInputField } from '../../../components/Inputs';
import { useTranslation } from 'react-i18next';
import { COLORS } from '../../../theme/colors';
import { FontStyle } from '../../../theme/FontStyle';
import { getFormattedDate } from '../../../utilities/helpers/CurretDate';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../../redux/users/user.actions';
import { AlertModal } from '../../../components/Modals';
import { SvgXml } from 'react-native-svg';
import { DrawerIcons } from '../../../assets/SvgIcons/DrawerIcons';
import GenericModal from '../../../components/Modals/GenericModal';
import DropdownComponent from '../../../components/Inputs/ElementDropdown/ElementDropdown';
import DatePickerNew from '../../../components/DateTimePicker/DatePickerNew';

const ApplyLoan = ({ navigation, modalVisible1, setModalVisible1, getLoans }) => {
  const { employeeID, loanTypes } = useSelector(state => state?.employeeProfile);
  const [loading, setLoading] = useState(false);
  const [showStartDatePicker, setShowStartDatePicker] = useState(false);
  const [showEndDatePicker, setShowEndDatePicker] = useState(false);
  const [numberOfDays, setNumberOfDays] = useState(0);
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [inputs, setInputs] = useState({
    amount: null,
    type: null,
    startDate: null,
    endDate: null,
    numberOfInstallment: "0",
    reason: null,
    errors: null,
  });

  const [alertBox, setAlertBox] = useState({
    showBox: false,
    title: null,
    message: null,
    confirmbtn: null,
    okbtn: null,
  });

  const handleInputChange = (field, value) => {
    setInputs({
      ...inputs,
      [field]: value,
      errors: {
        ...inputs.errors,
        [field]: false,
      },
    });
  };

  const closeCustomAlert = () => {
    setTimeout(() => {
      setAlertBox({ showBox: false });
    }, 300);
  };

  const handleDateChange = async (selectedDate, field) => {
    const dateObject = new Date(selectedDate);
    const formattedDate = dateObject.toISOString().split('T')[0];

    if (field === 'startDate') {
      setShowStartDatePicker(false);

    } else if (field === 'endDate') {
      setShowEndDatePicker(false);
    }

    await handleInputChange(field, formattedDate);

    setInputs(prevInputs => {
      const startDate = field === 'startDate' ? formattedDate : prevInputs.startDate;
      const endDate = field === 'endDate' ? formattedDate : prevInputs.endDate;

      if (startDate && endDate) {
        const totalMonths = countMonths(startDate, endDate);
        return {
          ...prevInputs,
          numberOfInstallment: totalMonths > 0 ? String(totalMonths) : "0",
        };
      }
      return prevInputs;
    });

  };

  function countMonths(startDate, endDate) {

    const start = new Date(startDate);
    const end = new Date(endDate);

    const yearDiff = end.getFullYear() - start.getFullYear();
    const monthDiff = end.getMonth() - start.getMonth();

    let totalMonths = yearDiff * 12 + monthDiff;

    if (end.getDate() < start.getDate()) {
      totalMonths -= 1;
    }

    return totalMonths;
  }


  const validate = async () => {
    const schema = LoanRequestSchema(t);
    const result = await inputValidation(schema, inputs);

    if (result.isValidate) {
      handleSubmit();
    } else {
      setInputs(prev => ({
        ...prev,
        errors: result?.err,
      }));
    }
  };

  const handleSubmit = async () => {
    setLoading(true);

    const dateObject = new Date(inputs.startDate);
    const formattedStartDate = dateObject.toISOString().split('T')[0];

    const dateObject1 = new Date(inputs.endDate);
    const formattedEndDate = dateObject1.toISOString().split('T')[0];

    try {
      const body = {
        params: {
          model: 'loan.advance.wags',
          method: 'create_loan_advance_request',
          args: [
            {
              employee_id: employeeID,
              date: new Date(),
              start_date: formattedStartDate,
              end_date: formattedEndDate,
              plan_type: 'loan', // for advance enter "advance"
              loan_type_id: inputs.type,
              amount: inputs?.amount,
              // number_of_installment: inputs?.numberOfInstallment,
              reason: inputs?.reason,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });
      setLoading(false);

      if (response?.data?.result) {
        if (response?.data?.result) {
          setLoading(false);
          setModalVisible1(false);
          setInputs({
            amount: null,
            type: null,
            startDate: null,
            endDate: null,
            numberOfInstallment: "0",
            reason: null,
            errors: null,
          });
          getLoans();
          setTimeout(() => { Toast.show(`${t('loan-request-submitted-successfully')} !`); }, 300);
        } else if (response?.data?.result?.error) {
          setLoading(false);
        }
      } else {
        setLoading(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              setAlertBox({
                showBox: true,
                title: `${'Validation Error'}`,
                message: `${response?.data?.error?.data?.message}`,
              });
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <GenericModal
      modalVisible={modalVisible1}
      setModalVisible={setModalVisible1}
      heading={t('apply-loan')}>
      <View style={styles.container}>
        <View style={{ zIndex: 9999 }}>
          <DropdownComponent
            label={t('loan-type')}
            data={loanTypes}
            value={inputs?.type}
            placeholder={t('select-loan-type')}
            error={inputs?.errors?.type}
            onChange={selectedType => handleInputChange('type', selectedType)}
          />
        </View>

        {/* <View style={{ zIndex: 9999 }}>
                    <YearPicker
                        label={t('loan-type')}
                        data={loanTypes}
                        value={inputs?.type}
                        placeholder={t('select-loan-type')}
                        error={inputs?.errors?.type}
                        onChange={(selectedType) => handleInputChange('type', selectedType)} />
                </View> */}

        <TextInputField
          label={t('amount')}
          placeholder={t('amount-plcholder')}
          value={inputs.amount}
          editable={true}
          error={inputs?.errors?.amount}
          keyboardType={'number-pad'}
          onChangeText={text => handleInputChange('amount', text)}
          labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
        />

        {/* <TextInputField
          label={t('date')}
          value={getFormattedDate(inputs.date)}
          editable={false}
          labelStyle={[FontStyle.Regular14, {color: COLORS.darkBlack}]}
        /> */}

        <DatePickerNew
          mode="date"
          date={inputs.startDate ? new Date(inputs.startDate) : new Date()}
          value={inputs.startDate && getFormattedDate(inputs.startDate)}
          label={t('start-date')}
          onChange={selectedDate => handleDateChange(selectedDate, 'startDate')}
          showDatePicker={showStartDatePicker}
          setShowDatePicker={setShowStartDatePicker}
          placeholder={t('start-date')}
          error={inputs?.errors?.startDate}
          minimumDate={new Date()}
        />

        <DatePickerNew
          mode="date"
          date={inputs.endDate ? new Date(inputs.endDate) : new Date(inputs?.startDate)}
          label={t('end-date')}
          value={inputs?.endDate && getFormattedDate(inputs.endDate)}
          onChange={selectedDate => handleDateChange(selectedDate, 'endDate')}
          showDatePicker={showEndDatePicker}
          setShowDatePicker={setShowEndDatePicker}
          placeholder={t('end-date')}
          error={inputs?.errors?.endDate}
          minimumDate={new Date()}
        />

        <TextInputField
          label={t("number-of-installment")}
          editable={false}
          value={inputs.numberOfInstallment}
          labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
        />

        <TextInputField
          label={t('reason')}
          editable={true}
          placeholder={t('reason-plcholder')}
          value={inputs.reason}
          error={inputs?.errors?.reason}
          // multiline={true}
          // height={75}
          onChangeText={text => handleInputChange('reason', text)}
          labelStyle={[FontStyle.Regular14, { color: COLORS.darkBlack }]}
        />

        <View style={styles.bottomView}>
          <NextButton title={t('submit')} onPress={validate} loader={loading} />
        </View>
      </View>

      <AlertModal
        visible={alertBox.showBox}
        icon={
          <View style={styles.alertIconView}>
            <SvgXml xml={DrawerIcons.loans} />
          </View>
        }
        title={alertBox.title}
        message={alertBox.message}
        onClose={closeCustomAlert}
        confirmbtn={alertBox.confirmbtn}
      />
    </GenericModal>
  );
};

export default ApplyLoan;
