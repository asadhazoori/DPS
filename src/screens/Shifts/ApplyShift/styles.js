import { StyleSheet } from 'react-native'
import { COLORS } from '../../../theme/colors';
import { FontStyle } from '../../../theme/FontStyle';

export const styles = StyleSheet.create({

    container: {
        margin: 16,
        // borderWidth: 1,
    },

    header: {
        ...FontStyle.Regular16_500,
        fontWeight: '700',
        color: COLORS.black,
        flex: 1,

    },

    bottomView: {
        // marginVertical: 24,
        marginTop: 16,
        marginHorizontal: 4,
    },



    container1: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        justifyContent: 'center',
        alignItems: 'center',

    },

    innerContainer: {
        // padding: 18,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        width: '85%',

    },

    textView: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 12,
    },


});
