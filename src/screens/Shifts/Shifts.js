import { View, SafeAreaView, FlatList, Text, StyleSheet } from 'react-native'
import React, { useEffect, useState } from 'react'
import GeneralHeader from '../../components/Headers/GeneralHeader'
import Theme from '../../theme/theme'
import ApplyShift from './ApplyShift/ApplyShift'
import { useTranslation } from 'react-i18next'
import SmartButtonTrans from '../../components/Buttons/SmartButtonTrans'
import { COLORS } from '../../theme/colors'
import { Icons } from '../../assets/SvgIcons/Icons'
import { FontStyle } from '../../theme/FontStyle'
import ShiftBalanceCard from '../../components/Cards/ShiftBalanceCard '
import ShiftRequestsCard from '../../components/Cards/ShiftRequestsCard'

const Shifts = ({ navigation, route }) => {

    const [modalVisible1, setModalVisible1] = useState(false);
    const { t } = useTranslation();

    const dummyData = [{
        "type": "Overtime",
        "date": "2022-03-13",
        "startTime": "8:30 PM",
        "endTime": "11:00 PM",
        "hours": "2.5",
        "state": "Approved",
    },
    {
        "type": "Overtime",
        "date": "2023-04-24",
        "startTime": "7:30 PM",
        "endTime": "11:30 PM",
        "hours": "4",
        "state": "Approved",
    },
    {
        "type": "Overtime",
        "date": "2023-04-08",
        "startTime": "7:30 PM",
        "endTime": "11:30 PM",
        "hours": "4",
        "state": "Approved",
    },
    {
        "type": "Overtime",
        "date": "2024-04-16",
        "startTime": "7:30 PM",
        "endTime": "11:30 PM",
        "hours": "4",
        "state": "Approved",
    }];



    return (
        <SafeAreaView style={Theme.SafeArea}>
            <GeneralHeader title={t('shifts')} navigation={navigation} />


            <View style={styles.container}>


                <View style={{ alignItems: 'flex-end', marginVertical: 24, marginRight: 16 }}>
                    <SmartButtonTrans title={t('add-new')} rightIcon={Icons.plus} onPress={() => setModalVisible1(true)} />

                </View>
                <View style={styles.body}>


                    <ShiftBalanceCard />

                    <FlatList
                        data={dummyData}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ paddingVertical: 16, flexGrow: 1 }}
                        renderItem={({ item, index }) => (<ShiftRequestsCard key={index} data={item} />)}
                        keyExtractor={(item, index) => index}
                        ItemSeparatorComponent={() => <View style={{ height: 12 }} />}
                        ListEmptyComponent={() => (<Text style={[styles.dateText, { alignSelf: 'center' }]}>{t('no-record-found')}</Text>)}


                    />
                </View>

                <ApplyShift modalVisible1={modalVisible1} setModalVisible1={setModalVisible1} />




            </View>


        </SafeAreaView>
    )
}

export default Shifts


const styles = StyleSheet.create({
    container: {
        // borderWidth: 1,
        flex: 1
    },
    body: {
        // borderWidth: 1,
        flex: 1,
        marginHorizontal: 16,

    },
    dateText: {
        ...FontStyle.Regular12,
        fontWeight: '500',
        color: COLORS.grey5

    },
})
