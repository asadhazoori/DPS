import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {FontStyle} from '../../theme/FontStyle';
import RequestsCard from '../Cards/RequestsCard';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';

const EmployeeWarnings = ({navigation}) => {
  const {t} = useTranslation();

  const {employeeWarning} = useSelector(state => state?.request);

  return (
    <>
      {employeeWarning?.length > 0 && (
        <View style={styles.container}>
          <View style={styles.haedingContainer}>
            <Text style={styles.heading}>{t('warnings')}</Text>
          </View>

          <FlatList
            data={employeeWarning}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.contentContainerStyle}
            scrollEnabled={false}
            renderItem={({item, index}) => (
              <RequestsCard key={index} data={item} navigation={navigation} />
            )}
            keyExtractor={(item, index) => index}
          />
        </View>
      )}
    </>
  );
};

export default EmployeeWarnings;

const styles = StyleSheet.create({
  container: {
    gap: 8,
    marginBottom: 8,
    paddingHorizontal: 16,
  },

  haedingContainer: {
    padding: 8,
    borderRadius: 8,
    backgroundColor: '#FFC1C3',
  },

  heading: {
    ...FontStyle.Regular14_500M,
    flex: 1,
    marginLeft: 8,
    textAlign: 'left',
  },

  contentContainerStyle: {
    gap: 12,
    paddingBottom: 16,
    flexGrow: 1,
  },
});
