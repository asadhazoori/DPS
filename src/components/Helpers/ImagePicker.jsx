import React from 'react';
import {
  View,
  PermissionsAndroid,
  Modal,
  StyleSheet,
  TouchableOpacity,
  Text,
  Platform,
  Alert,
  Linking,
} from 'react-native';
import { launchImageLibrary, launchCamera } from 'react-native-image-picker';
import { COLORS } from '../../theme/colors';
import ReactNativeVI from './ReactNativeVI';
import { FontStyle } from '../../theme/FontStyle';
import { useTranslation } from 'react-i18next';

const ImagePicker = ({ modalVisible, setModalVisible, onImageSelected }) => {
  const { t } = useTranslation();

  const openImagePicker = async source => {
    setModalVisible(false);

    const getCameraImage = async () => {
      const options = {
        mediaType: 'photo',
        includeBase64: true,
        // maxHeight: 200,
        // maxWidth: 2000,
      };

      try {
        launchCamera(options, response => {
          if (response.didCancel) {
            console.log('User cancelled camera');
          } else if (
            response.error ||
            response.errorCode ||
            response.errorMessage
          ) {
            console.log('Camera error: ', response.error, response.errorCode);
          } else {
            let imageBase64 = response.assets?.[0]?.base64;
            onImageSelected(imageBase64);
          }
        });
      } catch (error) {
        console.log(error, 'error');
      }
    };

    setTimeout(async () => {
      try {
        if (source === 'camera') {
          if (Platform.OS === 'android') {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.CAMERA,
              {
                title: 'Permission Camera',
                message: 'We need access to your camera to take a photo.',
                buttonNeutral: 'Ask Me Later',
                buttonNegative: 'Cancel',
                buttonPositive: 'OK',
              },
            );

            if (granted == PermissionsAndroid.RESULTS.GRANTED) {
              getCameraImage();
            } else {
              // Linking.openSettings();
              setTimeout(() => {
                Alert.alert(
                  'Camera Permission Denied',
                  'Allow Camera Permission for Wags HR App from settings.',
                  [
                    {
                      text: 'Open Settings',
                      onPress: () => Linking.openSettings(),
                    },
                    {
                      text: 'Cancel',
                    },
                  ],
                );
              }, 300);
            }
          } else {
            console.log('else');
            getCameraImage();
          }
        } else if (source === 'gallery') {
          const options = {
            mediaType: 'photo',
            includeBase64: true,
            // maxHeight: 2000,
            // maxWidth: 2000,
          };

          launchImageLibrary(options, response => {
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('Image picker error: ', response.error);
            } else {
              let imageBase64 = response.assets?.[0]?.base64;
              onImageSelected(imageBase64);
            }
          });
        }
      } catch (err) {
        console.warn(err);
      }
    }, 500);
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(false);
      }}>
      <View style={styles.modalContainer}>
        <View style={styles.modalContent}>
          <TouchableOpacity
            style={styles.crossIcon}
            onPress={() => setModalVisible(false)}>
            <ReactNativeVI
              Lib={'Ionicons'}
              color={COLORS.black}
              name={'close'}
              size={28}
            />
          </TouchableOpacity>

          <View style={styles.rowView}>
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => openImagePicker('camera')}
              style={styles.padding}>
              <ReactNativeVI
                Lib={'FontAwesome'}
                color={COLORS.primaryColor}
                name={'camera'}
                size={48}
              />
              <Text style={styles.label}>{t('camera')}</Text>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.5}
              style={styles.padding}
              onPress={() => openImagePicker('gallery')}>
              <ReactNativeVI
                Lib={'Ionicons'}
                color={COLORS.primaryColor}
                name={'images'}
                size={48}
              />
              <Text style={styles.label}>{t('gallery')}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: '#F3F3F3',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 20,
    paddingVertical: 30,
  },

  crossIcon: { position: 'absolute', padding: 10, right: 0 },

  rowView: {
    marginTop: 8,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 0,
  },

  label: {
    ...FontStyle.Regular14,
    fontWeight: '700',
    color: COLORS.primaryColor,
    marginTop: 8,
  },

  padding: { padding: 16 },
});

export default ImagePicker;
