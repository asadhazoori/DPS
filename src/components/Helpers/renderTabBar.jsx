import { TabBar } from "react-native-tab-view";
import { TabViewStyles } from "../../theme/TabViewStyles";
import { COLORS } from "../../theme/colors";
import { Text, View } from "react-native";
import { t } from "i18next";

export const renderTabBar = propss => (
    <View style={TabViewStyles.tabBar}>

        <TabBar
            {...propss}
            indicatorStyle={TabViewStyles.TabViewIndicator}
            style={TabViewStyles.TabViewCreateContainer}
            renderLabel={({ route, focused }) => (
                <View >
                    <Text style={[TabViewStyles.titleText, { color: focused ? COLORS.primaryColor : COLORS.grey3, }]}>{t(route.title)}</Text>
                </View>
            )}
        />

    </View>
);
