import { AppState, Dimensions, StyleSheet, Text, View } from 'react-native'
import React, { memo, useEffect, useState } from 'react'
import moment from 'moment';
import { COLORS } from '../../theme/colors';
import * as Progress from 'react-native-progress';
import { FontStyle } from '../../theme/FontStyle';
const TimeDifference = ({ ProgressBarwidth, height, attendanceRecords, activeAttendance, workingHours, punchStatus }) => {


    const width = Dimensions.get('screen').width * ProgressBarwidth;
    const totalHours = workingHours !== 0.0 ? workingHours : 8;
    const [duration, setDuration] = useState({
        minutes: 0,
        hours: 0,
        percentageWorked: 0
    });
    const calculateWorkedTime = (checkin, checkout) => {

        const punchInMoment = moment(checkin, "YYYY-MM-DD HH:mm:ss");
        const punchOutMoment = moment(checkout, "YYYY-MM-DD HH:mm:ss");
        const duration = moment.duration(punchOutMoment.diff(punchInMoment));
        const hours = Math.floor(duration.asHours());
        const minutes = Math.floor(duration.asMinutes()) % 60;

        return { rightHours: hours, rightMinutes: minutes }
    }

    const calculateRunTime = (checkin) => {

        const punchInMoment = moment.utc(checkin, "YYYY-MM-DD HH:mm:ss").utc();
        const duration = moment.duration(moment().diff(punchInMoment));
        const hours = Math.floor(duration.asHours());
        const minutes = Math.floor(duration.asMinutes()) % 60;
        return { rightHours: hours, rightMinutes: minutes }
    }


    const calculateTotalTime = () => {

        let totalDuration = { hours: 0, minutes: 0 };

        attendanceRecords?.forEach((record, index) => {

            if (record.final_checkout) {
                const { rightHours, rightMinutes } = calculateWorkedTime(record?.final_checkin, record?.final_checkout);
                totalDuration.hours += rightHours;
                totalDuration.minutes += rightMinutes;
            } else {
                const { rightHours, rightMinutes } = calculateRunTime(record?.final_checkin);
                totalDuration.hours += rightHours;
                totalDuration.minutes += rightMinutes;
            }

        });

        totalDuration.hours += Math.floor(totalDuration.minutes / 60);
        totalDuration.minutes %= 60;

        const totalMinutesWorked = (totalDuration.hours * 60) + totalDuration.minutes;
        const totalMinutesInDay = totalHours * 60;
        const percetage = Math.floor((totalMinutesWorked / totalMinutesInDay) * 100);

        setDuration({
            hours: totalDuration.hours,
            minutes: totalDuration.minutes,
            percentageWorked: percetage
        })
    }


    const geWorkingTime = () => {

        calculateTotalTime();
        if (punchStatus == "Punch-Out") {
            const intervalId = setInterval(() => {
                if (AppState.currentState == 'active') {
                calculateTotalTime();
                }
            }, 60000);
            return () => clearInterval(intervalId);

        }
        // else {
        //     calculateTotalTime();
        // }

    }

    useEffect(() => {

        geWorkingTime();
    }, [punchStatus, duration.minutes, attendanceRecords]);

    return (
        <View style={{ flexDirection: 'row', marginTop: 4, }}>

            <View style={{ justifyContent: 'center', }}>

                <Progress.Bar
                    // progress={0.9}
                    // progress={activeAttendance?.total_time / 9}
                    progress={duration.percentageWorked > 100 ? 100 / 100 : duration.percentageWorked / 100}
                    color={COLORS.primaryColor}
                    width={width}
                    unfilledColor={COLORS.secondaryColor}
                    // style={{flex:1}}
                    // unfilledColor={"#f0dbe6"}
                    // borderWidth={1}
                    // borderColor={"transparent"} 
                    // borderRadius={8}  
                    height={height}

                />
            </View>
            <Text style={[FontStyle.Regular14_500, { marginLeft: 8 }]}>{`${duration.percentageWorked}%`}</Text>
        </View>
    )
}

// export default TimeDifference;
export default memo(TimeDifference);

const styles = StyleSheet.create({})