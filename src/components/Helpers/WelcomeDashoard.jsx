import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'
import { useTranslation } from 'react-i18next'

const WelcomeDashoard = ({ name }) => {

    const { t } = useTranslation();

    return (
        <View style={{ marginTop: 10, paddingHorizontal: 16 }}>
            <Text style={[FontStyle.Regular14, { fontWeight: '500', color: COLORS.grey5, textAlign: 'left' }]}>{t('welcome-back')}</Text>
            <View style={{ flexDirection: 'row', marginTop: 4, }}>
                <View style={{ flex: 1, }}>
                    <Text style={[FontStyle.Regular16_500M, { textAlign: 'left' }]}>{t('hello')} {name}</Text>
                </View>
                {/* <View style={{ flex: 1, justifyContent: 'center' }}>
            <SvgXml xml={Icons.hand} style={{ marginHorizontal: 6, alignSelf: 'center' }} />
        </View> */}

            </View>
        </View>
    )
}

export default WelcomeDashoard

const styles = StyleSheet.create({})