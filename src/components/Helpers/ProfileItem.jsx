import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {FontStyle} from '../../theme/FontStyle';
import {COLORS} from '../../theme/colors';
import {SvgXml} from 'react-native-svg';
import ReactNativeVI from './ReactNativeVI';

const ProfileItem = ({
  icon,
  title,
  caption,
  onPress,
  rightIcon,
  container,
  logout,
  vectorIcon,
}) => {
  return (
    <TouchableOpacity
      style={[styles.container, container]}
      activeOpacity={0.5}
      onPress={onPress}>
      <View style={styles.row}>
        <View style={[styles.iconView, !logout && {marginTop: 4}]}>
          {vectorIcon ? (
            <ReactNativeVI
              Lib="FontAwesome"
              name={icon}
              color={COLORS.primaryColor}
              size={17}
            />
          ) : (
            <SvgXml xml={icon} />
          )}
        </View>

        <View style={styles.textView}>
          <Text
            style={[
              styles.mainText,
              {
                color: logout ? COLORS.logout : COLORS.darkBlack,
              },
            ]}>
            {title}
          </Text>
        </View>
      </View>

      {caption && (
        <View style={styles.captionView}>
          <Text style={[styles.captionText]}>{caption}</Text>
        </View>
      )}

      {rightIcon && (
        <View style={styles.rightIconView}>
          <SvgXml xml={rightIcon} />
        </View>
      )}
    </TouchableOpacity>
  );
};

export default ProfileItem;

const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  iconView: {
    marginTop: 4,
  },
  textView: {
    marginLeft: 10,
    flex: 1,
  },
  captionView: {
    marginTop: 4,
    marginLeft: 26,
  },
  mainText: {
    ...FontStyle.Regular16_500,
    fontWeight: '700',
    textAlign: 'left',
  },
  captionText: {
    ...FontStyle.Regular14_500,
    textAlign: 'left',
    color: COLORS.darkBlack,
  },
  rightIconView: {
    marginHorizontal: 16,
    justifyContent: 'center',
  },
});
