import { I18nManager, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Avatar, Drawer, } from 'react-native-paper'
import { useDispatch, useSelector } from 'react-redux'
import { siginOut } from '../../redux/users/actions/signOut';
import { COLORS } from '../../theme/colors';
import { SvgXml } from 'react-native-svg';
import { DrawerIcons } from '../../assets/SvgIcons/DrawerIcons';
import { FontStyle } from '../../theme/FontStyle';
import CustomDrawerItem from '../Drawer/DrawerItem';
import Theme from '../../theme/theme';
import { Icons } from '../../assets/SvgIcons/Icons';
import { logout_user } from '../../redux/users/user.actions';
import { useTranslation } from 'react-i18next';
import FontAwesome6 from 'react-native-vector-icons/FontAwesome6'
import { LangModal } from '../Modals';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RNRestart from 'react-native-restart';
import i18next from '../../i18n/services/i18next';

const CustomDrawer1 = (props) => {

    const dispatch = useDispatch();
    const { t } = useTranslation();
    const profileData = useSelector((state) => state.employeeProfile.data);
    const [lngModalVisible, setLngModalVisible] = useState(false);
    const uid = useSelector((state) => state.signin.uid);
    const [selectedScreen, selectScreen] = useState();
    const Menus = {
        attendace: [
            {
                id: 1,
                name: "Attendance",
                navigate: "Attendance"
            },
            {
                id: 2,
                name: "History",
                navigate: "Attendance"
            },
            // {
            //     id: 3,
            //     name: "Location",
            //     navigate: "Attendance"
            // },
        ],
        leaves: [
            {
                id: 6,
                name: t('leave-balance'),
                navigate: "Leaves",
                route: 0,
            },
            {
                id: 7,
                name: t('leaves-detail'),
                navigate: "Leaves",
                route: 1,
            },
            {
                id: 8,
                name: t('apply-leave'),
                navigate: "Leaves",
                route: 2,
            },
        ],
        loans: [
            {
                id: 11,
                name: t('loans-status'),
                navigate: "Loans",
                route: 0,
            },
            {
                id: 12,
                name: t('apply-loan'),
                navigate: "Loans",
                route: 1
            },
            // {
            //     id: 7,
            //     name: "Approved",
            //     navigate: "Loans"
            // },

        ],
        medicalClaims: [
            {
                id: 16,
                name: t('claims-status'),
                navigate: "MedicalClaim",
                route: 0
            },
            {
                id: 17,
                name: t('apply-medical-claims'),
                navigate: "MedicalClaim",
                route: 1
            },

        ],
        reports: [
            {
                id: 21,
                name: t('payslip'),
                navigate: "Reports"
            },
            {
                id: 22,
                name: t('tax-certificate'),
                navigate: "Reports"
            },
            {
                id: 23,
                name: t('tardiness-report'),
                navigate: "Reports"
            },

        ],
        shifts: [
            {
                id: 26,
                name: t('shifts-status'),
                navigate: "Shifts",
                route: 0
            },
            {
                id: 27,
                name: t('change-shift'),
                navigate: "Shifts",
                route: 1
            },
        ],
        overtimeTracking: [
            {
                id: 31,
                name: t('overtime-requests-status'),
                navigate: "OvertimeTracking",
                route: 0
            },
            {
                id: 32,
                name: t('request-overtime'),
                navigate: "OvertimeTracking",
                route: 1
            },
        ],
        timesheet: [
            {
                id: 36,
                name: t('timesheet-status'),
                navigate: "Timesheet",
                route: 0,
            },
            {
                id: 37,
                name: t('add-tasks'),
                navigate: "Timesheet",
                route: 1,
            },
        ],

    }

    const changeLng = async (rtl) => {

        const lng = rtl ? 'ar' : 'en';
        AsyncStorage.setItem('language', lng)
        I18nManager.forceRTL(rtl);
        i18next.changeLanguage(lng);
        RNRestart.Restart();

    }



    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
            <DrawerContentScrollView  {...props} showsVerticalScrollIndicator={false} style={{

            }}>

                <View style={styles.drawerContent}>


                    <View style={{ marginVertical: 24, flexDirection: 'row' }}>
                        <View style={{ alignItems: 'flex-start', flex: 1 }}>

                            <View style={{}}>
                                {profileData?.image_1920 ?

                                    <Avatar.Image
                                        source={{ uri: `data:image/jpeg;base64,${profileData?.image_1920}` }}
                                        size={60}
                                        style={[Theme.ImageShadow]}
                                    />
                                    :
                                    <View style={[Theme.ImageShadow, { backgroundColor: '#efeff2', height: 60, width: 60, borderRadius: 30, alignItems: 'center', justifyContent: 'center' }]}>
                                        <Icon name="account" size={40} color="#a9adc1" />
                                    </View>
                                    // {/* // <SvgXml xml={Icons.personIcon} style={Theme.ImageShadow} /> */}
                                }
                                {/* <View style={{ position: 'absolute', bottom: 0, right: 0 }}>
                                    <SvgXml xml={DrawerIcons.camera} />
                                </View> */}
                            </View>


                            <View style={{ marginTop: 12, marginLeft: 4 }}>
                                <Text style={[FontStyle.Regular14, { color: '#0D1224' }]}>{profileData?.name}</Text>
                            </View>

                            {profileData?.job_id[1] &&
                                <View style={{ marginTop: 4, marginLeft: 4 }}>
                                    <Text style={[FontStyle.Regular12, { color: '#B2BBBB' }]}>{profileData?.job_id[1]}</Text>
                                </View>
                            }
                        </View>

                        <TouchableOpacity activeOpacity={0.5} onPress={() => setLngModalVisible(true)}>
                            <FontAwesome6
                                name="language"
                                size={24}
                                style={{ color: COLORS.primaryColor }} />
                        </TouchableOpacity>

                    </View>

                    <View style={styles.drawerSection}>

                        <CustomDrawerItem
                            title={t('dashboard')}
                            navigate={() => props.navigation.navigate('BottomTab')}
                            icon={DrawerIcons.dashboard}
                        />

                        <CustomDrawerItem
                            title={t('profile')}
                            navigate={() => props.navigation.navigate('Profile')}
                            icon={DrawerIcons.personal}
                        />

                        <CustomDrawerItem
                            title={t('attendance')}
                            navigate={() => props.navigation.navigate('Attendance')}
                            icon={DrawerIcons.attendance}
                        />

                        {/* <CustomDrawerItem
                            title={'Attendance'}
                            navigate={(screen) => props.navigation.navigate(screen)}
                            icon={DrawerIcons.attendance}
                            extandable={true}
                            data={Menus.attendace}
                            onSelect={selectScreen}
                            selectedScreen={selectedScreen}
                        />
*/}
                        {/* <CustomDrawerItem
                            title={t('leaves')}
                            navigate={(screen, route) => props.navigation.navigate(screen, { route: route })}
                            icon={DrawerIcons.leaves}
                            extandable={true}
                            data={Menus.leaves}
                            onSelect={selectScreen}
                            selectedScreen={selectedScreen}
                        />

                        <CustomDrawerItem
                            title={t('loans-advances')}
                            navigate={(screen, route) => props.navigation.navigate(screen, { route: route })}
                            icon={DrawerIcons.loans}
                            extandable={true}
                            data={Menus.loans}
                            onSelect={selectScreen}
                            selectedScreen={selectedScreen}

                        />



                        <CustomDrawerItem
                            title={t('medical-claims')}
                            navigate={(screen, route) => props.navigation.navigate(screen, { route: route })}
                            icon={DrawerIcons.medical}
                            extandable={true}
                            data={Menus.medicalClaims}
                            onSelect={selectScreen}
                            selectedScreen={selectedScreen}
                        /> */}

                        {/* <CustomDrawerItem
                            title={'Shifts'}
                            navigate={() => props.navigation.navigate('Shifts')}
                            icon={DrawerIcons.shifts}
                        /> */}

                        {/* <CustomDrawerItem
                            title={t('shifts')}
                            navigate={(screen, route) => props.navigation.navigate(screen, { route: route })}
                            icon={DrawerIcons.shifts}
                            extandable={true}
                            data={Menus.shifts}
                            onSelect={selectScreen}
                            selectedScreen={selectedScreen}
                        /> */}

                        {/* <CustomDrawerItem
                            title={'Reports'}
                            navigate={() => props.navigation.navigate('Reports')}
                            icon={DrawerIcons.leaves}
                        /> */}

                        {/* <CustomDrawerItem
                            title={t('reports')}
                            navigate={(screen) => props.navigation.navigate(screen)}
                            icon={DrawerIcons.leaves}
                            extandable={true}
                            data={Menus.reports}
                            onSelect={selectScreen}
                            selectedScreen={selectedScreen}
                        /> */}



                        {/* <CustomDrawerItem
                            title={'Payslip'}
                            navigate={() => props.navigation.navigate('Reports')}
                            icon={DrawerIcons.payslip}
                        /> */}

                        {/* <CustomDrawerItem
                            title={'Overtime Tracking'}
                            navigate={() => props.navigation.navigate('OvertimeTracking')}
                            icon={DrawerIcons.overTime}
                        /> */}

                        {/* <CustomDrawerItem
                            title={t('overtime-tracking')}
                            navigate={(screen, route) => props.navigation.navigate(screen, { route: route })}
                            icon={DrawerIcons.overTime}
                            extandable={true}
                            data={Menus.overtimeTracking}
                            onSelect={selectScreen}
                            selectedScreen={selectedScreen}
                        /> */}

                        {/* <CustomDrawerItem
                            title={'Timesheet Management'}
                            navigate={() => props.navigation.navigate('Timesheet')}
                            icon={DrawerIcons.timeSheet}
                        /> */}

                        {/* <CustomDrawerItem
                            title={t('timesheet-management')}
                            navigate={(screen, route) => props.navigation.navigate(screen, { route: route })}
                            icon={DrawerIcons.timeSheet}
                            extandable={true}
                            data={Menus.timesheet}
                            onSelect={selectScreen}
                            selectedScreen={selectedScreen}
                        /> */}

                    </View>

                </View>
            </DrawerContentScrollView>


            <View style={styles.bottomDrawerSection}>

                <CustomDrawerItem
                    title={t('logout')}
                    navigate={() => {
                        const navigation = props.navigation;
                        dispatch(logout_user(false))
                        navigation.replace('Login')
                        // siginOut({ uid, navigation })
                    }}
                    icon={DrawerIcons.logout}
                    style={{ color: COLORS.logout, fontWeight: '700', }}
                // marginTop={107}
                />
            </View>
            <LangModal
                modalVisible={lngModalVisible}
                setModalVisible={setLngModalVisible}
                onChangeSelection={changeLng}
            />




        </SafeAreaView>
    )
}

export default CustomDrawer1

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },

    drawerContent: {
        flex: 1,
        marginHorizontal: 16,
        // borderWidth:1,
    },

    drawerSection: {
        flex: 1,
        // borderWidth: 1,

    },

    userInfoSection: {
        marginLeft: 14,

        position: 'absolute',
        bottom: 10,
    },

    iconView: {
        position: 'absolute',
        right: 0,
        marginRight: 10,
        marginTop: 10

    },

    iconColor: {
        color: COLORS.black
    },

    labelColor: {
        color: COLORS.black
    },

    title: {
        fontSize: 14,
        marginTop: 6,
        fontWeight: 'bold',
        color: COLORS.black,
    },
    caption: {
        marginTop: 2,
        fontSize: 13,
    },

    bottomDrawerSection: {
        // borderWidth: 1,
        marginTop: 6,
        marginHorizontal: 16,
    },
})