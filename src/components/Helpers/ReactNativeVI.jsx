import React from "react";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Octicons from 'react-native-vector-icons/Octicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import { Text } from "react-native";

const ReactNativeVI = ({ size, name, color, Lib, ...props }) => {
    return (

        Lib == 'Ionicons' ? <Ionicons  {...props} name={name} size={size} color={color} /> :
            Lib == 'AntDesign' ? <AntDesign {...props} name={name} size={size} color={color} /> :
                Lib == 'MaterialCommunityIcons' ? <MaterialCommunityIcons {...props} name={name} size={size} color={color} /> :
                    Lib == 'EvilIcons' ? <EvilIcons {...props} name={name} size={size} color={color} /> :
                        Lib == 'Entypo' ? <Entypo {...props} name={name} size={size} color={color} /> :
                            Lib == 'SimpleLineIcons' ? <SimpleLineIcons {...props} name={name} size={size} color={color} /> :
                                Lib == 'MaterialIcons' ? <MaterialIcons {...props} name={name} size={size} color={color} /> :
                                    Lib == 'Fontisto' ? <Fontisto {...props} name={name} size={size} color={color} /> :
                                        Lib == 'FontAwesome5' ? <FontAwesome5 {...props} name={name} size={size} color={color} /> :
                                            Lib == 'Feather' ? <Feather {...props} name={name} size={size} color={color} /> :
                                                Lib == 'Octicons' ? <Octicons {...props} name={name} size={size} color={color} /> :
                                                    Lib == 'FontAwesome' ? <FontAwesome {...props} name={name} size={size} color={color} /> : <Text>~</Text>
    )

}

export default ReactNativeVI;