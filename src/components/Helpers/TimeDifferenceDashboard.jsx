import { AppState, Dimensions, StyleSheet, Text, View } from 'react-native'
import React, { memo, useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import moment from 'moment';
import { COLORS } from '../../theme/colors';
import * as Progress from 'react-native-progress';
import { FontStyle } from '../../theme/FontStyle';
import { useTranslation } from 'react-i18next';
const TimeDifferenceDashboard = ({ ProgressBarwidth, height, attendanceRecords, activeAttendance }) => {

    const { t } = useTranslation();

    const width = Dimensions.get('screen').width * ProgressBarwidth;
    const { data } = useSelector((state) => state?.employeeProfile);
    // const { attendanceRecords, activeAttendance } = useSelector((state) => state?.attendance);
    const punchStatus = activeAttendance?.last_checkout ? 'Punch-In' : !activeAttendance?.checkin ? 'Punch-In' : 'Punch-Out'
    const totalHours = data?.working_hours !== 0.0 ? data?.working_hours : 8;
    const [duration, setDuration] = useState({
        minutes: 0,
        hours: 0,
        percentageWorked: 0
    });

    const calculateWorkedTime = (checkin, checkout) => {

        const punchInMoment = moment(checkin, "YYYY-MM-DD HH:mm:ss");
        const punchOutMoment = moment(checkout, "YYYY-MM-DD HH:mm:ss");
        const duration = moment.duration(punchOutMoment.diff(punchInMoment));
        const hours = Math.floor(duration.asHours());
        const minutes = Math.floor(duration.asMinutes()) % 60;

        return { rightHours: hours, rightMinutes: minutes }
    }

    const calculateRunTime = (checkin) => {

        const punchInMoment = moment.utc(checkin, "YYYY-MM-DD HH:mm:ss").utc();
        const duration = moment.duration(moment().diff(punchInMoment));
        const hours = Math.floor(duration.asHours());
        const minutes = Math.floor(duration.asMinutes()) % 60;
        return { rightHours: hours, rightMinutes: minutes }
    }


    const calculateTotalTime = () => {

        let totalDuration = { hours: 0, minutes: 0 };

        attendanceRecords?.forEach((record, index) => {

            if (record.final_checkout) {
                const { rightHours, rightMinutes } = calculateWorkedTime(record?.final_checkin, record?.final_checkout);
                totalDuration.hours += rightHours;
                totalDuration.minutes += rightMinutes;
            } else {
                const { rightHours, rightMinutes } = calculateRunTime(record?.final_checkin);
                totalDuration.hours += rightHours;
                totalDuration.minutes += rightMinutes;
            }

        });

        totalDuration.hours += Math.floor(totalDuration.minutes / 60);
        totalDuration.minutes %= 60;

        const totalMinutesWorked = (totalDuration.hours * 60) + totalDuration.minutes;
        const totalMinutesInDay = totalHours * 60;
        const percetage = Math.floor((totalMinutesWorked / totalMinutesInDay) * 100);

        setDuration({
            hours: totalDuration.hours,
            minutes: totalDuration.minutes,
            percentageWorked: percetage
        })
    }


    const geWorkingTime = () => {

        calculateTotalTime();
        if (punchStatus == "Punch-Out") {
            const intervalId = setInterval(() => {
                if (AppState.currentState == 'active') {
                    calculateTotalTime();
                }
            }, 60000);
            return () => clearInterval(intervalId);

        }

    }

    useEffect(() => {

        geWorkingTime();
    }, [punchStatus, duration.minutes, attendanceRecords]);

    return (
        <View style={{ marginTop: 8, }}>

            <View style={{ justifyContent: 'center', }}>
                <View style={{ marginBottom: 4 }}>
                    <Text style={[FontStyle.Regular14_500, { color: COLORS.primaryColor, fontWeight: '500', textAlign: 'left' }]}>{`${duration.percentageWorked}% ${t('completed')} / `}
                        <Text style={{ color: COLORS.grey5 }}>100%</Text></Text>

                </View>

                <Progress.Bar
                    progress={duration.percentageWorked > 100 ? 100 / 100 : duration.percentageWorked / 100}
                    color={COLORS.primaryColor}
                    width={width}
                    unfilledColor={COLORS.secondaryColor}
                    // style={{flex:1}}
                    // unfilledColor={"#f0dbe6"}
                    // borderWidth={1}
                    // borderColor={"transparent"} 
                    // borderRadius={8}  
                    height={height}

                />
            </View>
        </View>
    )
}

export default memo(TimeDifferenceDashboard);

const styles = StyleSheet.create({})