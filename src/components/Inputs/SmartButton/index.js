import { ActivityIndicator, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { COLORS } from '../../../theme/colors'
import { FontStyle } from '../../../theme/FontStyle'
import Theme from '../../../theme/theme'

const SmartButton = ({ title, handlePress, disabled, textStyle, width, loader }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            disabled={disabled}
            style={[
                Theme.Shadow,
                {
                    backgroundColor: !disabled ? COLORS.primaryColor : COLORS.grey2,
                    paddingHorizontal: 16,
                    paddingVertical: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderWidth: 0,
                    width: width,
                }]}
            onPress={handlePress}>
            {
                !loader &&
                <Text style={[FontStyle.Regular14, textStyle, { color: COLORS.white }]}>{title}</Text>
            }
            {loader &&
                <ActivityIndicator size={24} color={'white'} />
            }

        </TouchableOpacity>
    )
}

export default SmartButton

const styles = StyleSheet.create({})
