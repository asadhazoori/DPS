import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native'
import React from 'react'
import { FontStyle } from '../../../theme/FontStyle'
import { styles } from './styles'
import { COLORS } from '../../../theme/colors'

const PrimaryButton = ({
    title,
    onPress,
    marginTop,
    container,
    loader
}) => {

    return (
        <TouchableOpacity
            onPress={onPress}
            activeOpacity={0.9}
            style={[styles.main, container, { marginTop: marginTop, backgroundColor: loader ? '#a94577' : COLORS.primaryColor }]}
            disabled={loader}
        >
            {
                !loader &&
                <Text style={[FontStyle.Regular16, { color: COLORS.white }]}>{title}</Text>
            }
            {loader &&
                <ActivityIndicator size={24} color={'white'} />
            }
        </TouchableOpacity>
    )
}

export default PrimaryButton