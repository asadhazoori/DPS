import { I18nManager, StyleSheet, Text, TextInput, View } from 'react-native'
import React, { useState } from 'react'
import { FontStyle } from '../../../theme/FontStyle'
import { COLORS } from '../../../theme/colors'
import Theme from '../../../theme/theme'
import DropDownPicker from 'react-native-dropdown-picker'

const YearPicker = ({
    label,
    placeholder,
    onChange,
    error,
    editable = true,
    keyboardType,
    marginBottom,
    data, value,
}) => {

    const isRTL = I18nManager.isRTL;
    const [open, setOpen] = useState(false);
    const [items, setItems] = useState(data);

    const handleValueChange = (newValue) => {
        // setValue(newValue);
        const selectedValue = newValue();
        console.log(selectedValue)
        onChange(selectedValue);
    };


    return (
        <View style={[styles.container]}>
            {/* <View style={[styles.container, { marginBottom: marginBottom ? marginBottom : 12 }]}> */}
            <View>
                <Text style={[FontStyle.Regular14, { color: COLORS.darkBlack }]}>{label}</Text>
            </View>

            <DropDownPicker
                open={open}
                placeholderStyle={[FontStyle.Regular14_500, { color: COLORS.grey3 }]}
                value={value}
                props={{ activeOpacity: 0.7 }}
                // items={items}
                // items={items?.map((item) => ({
                //     ...item,
                //     label: isRTL ? item.arabicLabel : item.label,
                // }))}


                items={data?.map((item, index) => ({
                    label: item.name,
                    value: item.id
                }))}
                setOpen={setOpen}
                setValue={handleValueChange}
                setItems={setItems}
                placeholder={placeholder}
                // autoScroll={false}
                // scrollViewProps={false}

                // badgeProps={{
                //     activeOpacity: 0.5
                // }}

                style={[
                    {
                        ...Theme.Shadow,
                        borderColor: error ? COLORS.red : "#BEBEBE",
                        marginTop: 4,
                        paddingHorizontal: 8,
                        height: 45,
                        flexDirection: 'row', alignItems: 'center'
                    }]}

                labelStyle={[FontStyle.Regular14_500, {
                    textAlign: 'left',
                    color: COLORS.darkBlack,
                }]}

                dropDownContainerStyle={[{
                    borderRadius: 12,
                    backgroundColor: COLORS.white,
                    borderWidth: 1,
                    borderColor: 'rgba(0, 0, 0, 0.10)',
                    paddingHorizontal: 8,
                    // ...Theme.Shadow,
                    // borderTopWidth: 0,
                    // zIndex: 990
                    // position: 'absolute'
                    paddingTop: 4,
                    paddingBottom: 4,

                }]}

                showTickIcon={false}
                listItemLabelStyle={[
                    FontStyle.Regular12_500, {
                        color: '#455A64',
                        fontWeight: '700',
                        textAlign: 'left',
                        // marginBottom: 4,

                    }]}

                selectedItemLabelStyle={{ color: COLORS.darkBlack }}
                selectedItemContainerStyle={{
                    backgroundColor: '#F0F5FF',
                    borderRadius: 8,
                }}

            />

            {error &&
                < Text style={[styles.label, { color: COLORS.red, marginTop: 6, fontSize: 12, }]}>{error}</Text>
            }
        </View>
    )
}

export default YearPicker

const styles = StyleSheet.create({
    container: {
        // borderWidth: 1,
        marginHorizontal: 4,

    },

    label: {
        ...FontStyle.Regular14_500,
        fontWeight: '400',
        paddingHorizontal: 1,
        letterSpacing: 0.16,
        marginLeft: 2
    },
})