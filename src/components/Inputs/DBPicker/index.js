import {
  ActivityIndicator,
  I18nManager,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {FontStyle} from '../../../theme/FontStyle';
import {COLORS} from '../../../theme/colors';
import Theme from '../../../theme/theme';
import DropDownPicker from 'react-native-dropdown-picker';
import {useTranslation} from 'react-i18next';

const DBPicker = ({
  label,
  placeholder,
  onChange,
  error,
  marginBottom,
  data,
  value1,
  openDropdown,
  setOpenDropdown,
  loading,
}) => {
  const {t} = useTranslation();
  const handleValueChange = newValue => {
    const selectedValue = newValue();
    onChange(selectedValue);
  };

  return (
    <View
      style={[
        styles.container,
        // { marginBottom: marginBottom ? marginBottom : 12 }
        // { zIndex: 9999 }
      ]}>
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.label}>{label}</Text>
        {loading && (
          <ActivityIndicator
            color={COLORS.primaryColor}
            size={16}
            style={{marginLeft: 6}}
          />
        )}
      </View>

      <DropDownPicker
        open={openDropdown}
        props={{activeOpacity: 0.8}}
        placeholderStyle={[
          FontStyle.Regular14_500,
          {color: COLORS.grey3, textAlign: 'left'},
        ]}
        value={value1}
        items={data?.map((item, index) => ({
          label: item,
          value: item,
        }))}
        setOpen={setOpenDropdown}
        setValue={handleValueChange}
        placeholder={placeholder}
        style={[
          {
            ...Theme.Shadow,
            borderColor: error ? COLORS.red : '#BEBEBE',
            // borderColor: error ? COLORS.red : openDropdown ? COLORS.primaryColor : '#BEBEBE',
            marginTop: 4,
          },
        ]}
        labelStyle={[
          FontStyle.Regular14_500,
          {
            textAlign: 'left',
            color: COLORS.darkBlack,
          },
        ]}
        dropDownContainerStyle={[
          {
            borderRadius: 12,
            backgroundColor: COLORS.white,
            borderColor: 'rgba(0, 0, 0, 0.10)',
            paddingHorizontal: 8,
            paddingTop: 4,
            paddingBottom: 4,
            ...Theme.Shadow,

            // maxHeight: 200, // Adjust this value as needed
            // overflow: 'scroll',

            position: 'relative',
            top: 0,
          },
        ]}
        showTickIcon={false}
        listItemLabelStyle={[
          FontStyle.Regular14_500,
          {
            color: '#455A64',
            // fontWeight: '500',
            textAlign: 'left',
            // marginBottom: 4,
          },
        ]}
        selectedItemLabelStyle={{color: COLORS.darkBlack}}
        selectedItemContainerStyle={{
          backgroundColor: '#F0F5FF',
          borderRadius: 8,
        }}
        ListEmptyComponent={({
          listMessageContainerStyle,
          listMessageTextStyle,
          ActivityIndicatorComponent,
          loading,
          message,
        }) => (
          <View style={listMessageContainerStyle}>
            {loading ? (
              <ActivityIndicatorComponent />
            ) : (
              <Text
                style={[
                  FontStyle.Regular12,
                  {
                    color: '#455A64',
                    fontWeight: '500',
                    textAlign: 'left',
                    // marginBottom: 4,
                  },
                ]}>
                {t('db-not-exist')}
              </Text>
            )}
          </View>
        )}
        listMode="SCROLLVIEW"
        scrollViewProps={{
          nestedScrollEnabled: true,
        }}
      />
      {error && (
        // < Text style={[styles.label, { color: COLORS.red, marginLeft: 2, fontSize: 12, }]}>{error}</Text>
        <Text
          style={[
            styles.label,
            FontStyle.Regular12_500,
            {color: COLORS.red, marginTop: 2},
          ]}>
          {error}
        </Text>
      )}
    </View>
  );
};

export default DBPicker;

const styles = StyleSheet.create({
  container: {},

  inputView: {
    ...Theme.Shadow,
    marginTop: 8,
    borderRadius: 8,
    paddingHorizontal: 8,
  },
  label: {
    textAlign: 'left',
    ...FontStyle.Regular16_500M,
    marginLeft: 2,
  },
});
