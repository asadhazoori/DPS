import { View, Text, TextInput } from 'react-native'
import React, { useRef, useState } from 'react'
import { FontStyle } from '../../../theme/FontStyle'
import { styles } from './styles'
import { COLORS } from '../../../theme/colors'
import { SvgXml } from 'react-native-svg'
import { Icons } from '../../../assets/SvgIcons/Icons'
import ReactNativeVI from '../../Helpers/ReactNativeVI'

const TextInputAuth = React.forwardRef(({
    label,
    placeholder,
    value,
    onChangeText,
    marginTop,
    error,
    password,
    rightIcon,
    editable = true,
    icon,
    keyboardType,
    onChangeEditable,
    returnKeyType,
    // ref,
    onSubmitEditing,

}, ref) => {

    const [hidePassword, setHidePassword] = useState(password);
    const [isFocused, setIsFocused] = useState(false);

    const handleFocusButtonClick = () => {


        onChangeEditable(!editable);
    };


    return (
        <View style={[styles.container, {
            marginTop: marginTop,
        }]}>

            <Text style={styles.label}>
                {label}
            </Text>

            <View style={[styles.inputView, {
                backgroundColor: editable ? COLORS.white : COLORS.backgroundInput,
                borderColor: error ? COLORS.red : isFocused ? COLORS.primaryColor : '#BEBEBE',
                flexDirection: 'row',

            }]}>

                {icon && (
                    <View style={{ justifyContent: 'center', marginRight: 8 }}>
                        <SvgXml xml={icon} />
                    </View>
                )}

                <TextInput
                    ref={ref}
                    placeholder={placeholder}
                    onChangeText={onChangeText}
                    placeholderTextColor={COLORS.grey3}
                    value={value}
                    style={styles.textInput}
                    autoCapitalize='none'
                    secureTextEntry={hidePassword}
                    onFocus={(focus) => {
                        setIsFocused(true);
                    }}
                    onBlur={() => setIsFocused(false)}
                    editable={editable}
                    cursorColor={COLORS.primaryColor}
                    keyboardType={keyboardType}
                    onSubmitEditing={onSubmitEditing}
                    returnKeyType={returnKeyType}

                />

                {password && (
                    <View style={styles.iconView}>

                        <ReactNativeVI
                            Lib={'MaterialCommunityIcons'}
                            name={hidePassword ? 'eye-outline' : 'eye-off-outline'}
                            color={COLORS.primaryColor} size={20}
                            onPress={() => setHidePassword(!hidePassword)}
                        />
                    </View>
                )}

                {rightIcon && (
                    <View style={styles.iconView}>
                        {!editable ?

                            <ReactNativeVI
                                Lib={'MaterialIcons'}
                                name={'edit'}
                                // name={editable ? 'check-circle-outline' : 'edit'}
                                color={COLORS.primaryColor} size={20}
                                onPress={handleFocusButtonClick}
                            />
                            :
                            <ReactNativeVI
                                Lib={'MaterialIcons'}
                                name={'check-circle-outline'}
                                color={COLORS.primaryColor} size={20}
                                onPress={onSubmitEditing}
                            />
                        }
                    </View>
                )}
            </View>

            {error &&
                < Text style={[styles.label, FontStyle.Regular12_500, { color: COLORS.red, marginTop: 2, }]}>{error}</Text>
            }
        </View >
    )
})

export default TextInputAuth