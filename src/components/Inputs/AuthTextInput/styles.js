import { StyleSheet } from "react-native";
import { FontStyle } from "../../../theme/FontStyle";
import { COLORS } from "../../../theme/colors";
import Theme from "../../../theme/theme";

export const styles = StyleSheet.create({
    container: {

    },

    label: {
        alignSelf: 'flex-start',
        ...FontStyle.Regular16_500M,
        marginLeft: 2,
    },

    inputView: {
        marginTop: 4,
        ...Theme.Shadow,
        paddingHorizontal: 12,
    },

    textInput: {
        flex: 1,
        padding: 0,
        margin: 0,
        height: 45,
        ...FontStyle.Regular14_500,
        color: COLORS.darkBlack,
    },

    iconView: {
        justifyContent: 'center',
        marginLeft: 8
    },


})