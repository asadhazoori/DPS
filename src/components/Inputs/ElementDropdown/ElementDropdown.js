import React, {useState} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {Dropdown} from 'react-native-element-dropdown';
import {FontStyle} from '../../../theme/FontStyle';
import {COLORS} from '../../../theme/colors';
import Theme from '../../../theme/theme';
import ReactNativeVI from '../../Helpers/ReactNativeVI';
// import AntDesign from '@expo/vector-icons/AntDesign';

const DropdownComponent = ({
  label,
  placeholder,
  onChange,
  error,
  data,
  value,
  search,
  fullItemReq,
  labelField
}) => {
  const [isFocus, setIsFocus] = useState(false);
  return (
    <View style={[styles.container]}>
      <View>
        <Text
          style={[
            FontStyle.Regular14,
            {color: COLORS.darkBlack, textAlign: 'left'},
          ]}>
          {label}
        </Text>
      </View>

      <Dropdown
        style={[
          {
            ...Theme.Shadow,
            borderBottomStartRadius: isFocus ? 0 : 8,
            borderBottomEndRadius: isFocus ? 0 : 8,
            borderColor: error ? COLORS.red : '#BEBEBE',
            marginTop: 4,
            paddingHorizontal: 8,
            height: 45,
            flexDirection: 'row',
            alignItems: 'center',
          },
          // isFocus && { borderColor: COLORS.primaryColor },
        ]}
        inputSearchStyle={[
          FontStyle.Regular14_500,
          styles.textinput,
          {color: COLORS.darkBlack},
        ]}
        searchPlaceholder="search"
        containerStyle={[
          {
            borderRadius: 12,
            backgroundColor: COLORS.white,
            borderWidth: 1,
            borderColor: 'rgba(0, 0, 0, 0.10)',
            paddingHorizontal: 8,
            paddingTop: 2,
            paddingBottom: 2,
            borderTopLeftRadius: 0,
            borderTopRightRadius: 0,
          },
        ]}
        placeholderStyle={[FontStyle.Regular14_500, {color: COLORS.grey3}]}
        value={value}
        onChange={item => {
          if (fullItemReq) {
            onChange(item);
          } else {
            onChange(item.id);
          }
          setIsFocus(false);
        }}
        data={data}
        placeholder={placeholder}
        search={search}
        selectedTextStyle={styles.selectedTextStyle}
        // iconStyle={styles.iconStyle}
        maxHeight={220}
        labelField={labelField ? labelField : 'name'}
        valueField="id"
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
        itemTextStyle={[
          FontStyle.Regular12_500,
          {
            color: '#455A64',
            fontWeight: '700',
            textAlign: 'left',
          },
        ]}
        // renderLeftIcon={() => (
        //     <ReactNativeVI
        //         Lib={'AntDesign'}
        //         color={isFocus ? 'blue' : 'black'}
        //         name="Safety"
        //         size={20}
        //     />
        // )}

        iconColor={COLORS.darkBlack}
        activeColor="#F0F5FF"
      />

      {error && (
        <Text
          style={[
            styles.label,
            {color: COLORS.red, marginTop: 6, fontSize: 12, textAlign: 'left'},
          ]}>
          {error}
        </Text>
      )}
    </View>
  );
};

export default DropdownComponent;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 4,
  },
  dropdown: {
    height: 50,
    borderColor: 'gray',
  },
  icon: {
    marginRight: 5,
  },
  placeholderStyle: {
    fontSize: 16,
  },
  selectedTextStyle: {
    ...FontStyle.Regular14_500,
    textAlign: 'left',
    color: COLORS.darkBlack,
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 16,
  },

  label: {
    ...FontStyle.Regular14_500,
    fontWeight: '400',
    paddingHorizontal: 1,
    letterSpacing: 0.16,
    marginLeft: 2,
  },
});
