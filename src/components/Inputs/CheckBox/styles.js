import { FontStyle } from "../../../theme/FontStyle";
import { COLORS } from "../../../theme/colors";
import { fonts } from "../../../theme/fonts";

const { StyleSheet } = require("react-native");

export const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: COLORS.black,
        marginTop: 10
    },

    title: {
        ...FontStyle.Regular16_500M,
        marginRight: 12,
    },
    cheeckBox: {
        borderWidth: 2,
        height: 20,
        width: 20,
        borderRadius: 4,
        borderColor: COLORS.primaryColor,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
