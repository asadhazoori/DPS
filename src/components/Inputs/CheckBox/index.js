import { Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { COLORS } from '../../../theme/colors'
import { useTranslation } from 'react-i18next';
import { styles } from './styles';
import ReactNativeVI from '../../Helpers/ReactNativeVI';

const CheckBox = ({
    status,
    onPress,
    title = 'keep-me-logged',
    container
}) => {

    const { t } = useTranslation();
    return (
        <View style={[styles.container, container]}>
            <Text style={styles.title}>{t(title)}</Text>

            <TouchableOpacity style={[styles.cheeckBox, {
                backgroundColor: status ? COLORS.primaryColor : 'transparent',
            }]}
                onPress={onPress}
            >
                {status &&
                    <ReactNativeVI Lib={'FontAwesome'} name={'check'} color={COLORS.white} size={16} />
                }


            </TouchableOpacity>

        </View>
    )
}

export default CheckBox
