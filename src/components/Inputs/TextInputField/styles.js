import {StyleSheet} from 'react-native';
import Theme from '../../../theme/theme';
import {COLORS} from '../../../theme/colors';
import {FontStyle} from '../../../theme/FontStyle';

export default styles = StyleSheet.create({
  container: {
    marginHorizontal: 4,
    borderColor: 'blue',
  },

  inputView: {
    ...Theme.Shadow,
    marginTop: 4,
    borderRadius: 8,
    paddingHorizontal: 10,
  },

  textinput: {
    color: COLORS.darkBlack,
    flex: 1,
    padding: 0,
    margin: 0,
  },
  label: {
    ...FontStyle.Regular14_500,
    fontWeight: '400',
    paddingHorizontal: 1,
    letterSpacing: 0.16,
    marginLeft: 2,
  },
});
