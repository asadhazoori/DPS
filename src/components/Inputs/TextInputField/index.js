import {I18nManager, StyleSheet, Text, TextInput, View} from 'react-native';
import React, {useState} from 'react';
import {FontStyle} from '../../../theme/FontStyle';
import {COLORS} from '../../../theme/colors';
import styles from './styles';

const TextInputField = ({
  label,
  placeholder,
  value,
  onChangeText,
  error,
  editable = true,
  keyboardType,
  marginBottom,
  height,
  multiline,
  labelStyle,
}) => {
  const [isFocused, setIsFocused] = useState(false);

  return (
    <View style={[styles.container]}>
      <View>
        <Text
          style={[
            FontStyle.Regular16_500M,
            labelStyle,
            {color: COLORS.darkBlack, textAlign: 'left', marginLeft: 2},
          ]}>
          {label}
        </Text>
      </View>

      <View
        style={[
          styles.inputView,
          {
            backgroundColor: !editable ? COLORS.backgroundInput : COLORS.white,
            borderColor: error
              ? COLORS.red
              : isFocused
              ? COLORS.primaryColor
              : '#BEBEBE',
            height: height ? height : 50,
          },
        ]}>
        <TextInput
          placeholder={placeholder}
          placeholderTextColor={COLORS.grey3}
          value={value}
          numberOfLines={multiline && 5}
          multiline={multiline && true}
          editable={editable}
          keyboardType={keyboardType}
          onFocus={() => {
            setIsFocused(true);
          }}
          onBlur={() => setIsFocused(false)}
          onChangeText={onChangeText}
          style={[
            FontStyle.Regular14_500M,
            {textAlign: I18nManager.isRTL ? 'right' : 'left'},
            styles.textinput,
            multiline && {textAlignVertical: 'top', paddingTop: 8},
          ]}
          cursorColor={COLORS.primaryColor}
        />
      </View>
      {error && (
        <Text
          style={[
            styles.label,
            {color: COLORS.red, marginTop: 2, fontSize: 12, textAlign: 'left'},
          ]}>
          {error}
        </Text>
      )}
    </View>
  );
};

export default TextInputField;
