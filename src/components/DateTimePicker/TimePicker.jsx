import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import {COLORS} from '../../theme/colors';
import {FontStyle} from '../../theme/FontStyle';
import Theme from '../../theme/theme';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {SvgXml} from 'react-native-svg';
import {Icons} from '../../assets/SvgIcons/Icons';

const TimePicker = ({
  label,
  placeholder,
  value,
  initialValue,
  onChange,
  error,
  showTimePicker,
  setShowTimePicker,
  disabled,
  color,
  containerStyle,
  labelStyle,
  inputStyle,
  icon,
  editable = true,

  locale,
}) => {
  const handleTimeChange = selectedTime => {
    onChange(selectedTime);
    setShowTimePicker(false);
  };

  const cancel = () => {
    setShowTimePicker(false);
  };

  let borderColor = COLORS.black;

  if (error) {
    borderColor = COLORS.red;
  } else if (value) {
    borderColor = COLORS.black;
  }
  return (
    <View style={[{marginTop: 4}, containerStyle]}>
      <Text
        style={[
          FontStyle.Regular14_500,
          labelStyle,
          {color: COLORS.darkBlack, flex: 1, textAlign: 'left'},
        ]}>{`${label}`}</Text>

      <TouchableOpacity
        disabled={disabled}
        activeOpacity={0.8}
        onPress={() => setShowTimePicker(true)}
        style={[
          Theme.Shadow,
          {
            justifyContent: 'space-between',
            backgroundColor: disabled ? COLORS.backgroundInput : COLORS.white,
            borderColor: error ? COLORS.red : '#BEBEBE',
            paddingVertical: 8,
            flex: 1,
          },
          inputStyle,
        ]}>
        <Text
          style={[
            FontStyle.Regular14_500,
            {color: !disabled ? color : COLORS.grey3, textAlign: 'center'},
          ]}>
          {value ? value : placeholder}
        </Text>
        {icon && <SvgXml xml={Icons.clock} style={{}} />}
      </TouchableOpacity>
      {error && (
        <Text
          style={[
            styles.label,
            {color: COLORS.red, marginTop: 2, fontSize: 12, textAlign: 'left'},
          ]}>
          {error}
        </Text>
      )}
      {showTimePicker && (
        <DateTimePickerModal
          isVisible={showTimePicker}
          mode="time"
          date={initialValue}
          onConfirm={handleTimeChange}
          onCancel={cancel}
          locale="en_GB"
        />
      )}
    </View>
  );
};

export default TimePicker;

const styles = StyleSheet.create({
  dobView: {
    marginTop: 16,
    padding: 15,
    height: 50,
    backgroundColor: COLORS.white,
    borderWidth: 1,
    borderRadius: 10,
  },
  inputView: {
    ...Theme.Shadow,
    marginTop: 8,
    paddingHorizontal: 8,
    paddingVertical: 14,
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    ...FontStyle.Regular14_500,
    fontWeight: '400',
    paddingHorizontal: 1,
    letterSpacing: 0.16,
    marginLeft: 2,
  },
});
