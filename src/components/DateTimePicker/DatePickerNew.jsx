import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import {COLORS} from '../../theme/colors';
import {FontStyle} from '../../theme/FontStyle';
import Theme from '../../theme/theme';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {SvgXml} from 'react-native-svg';
import {Icons} from '../../assets/SvgIcons/Icons';

const DatePickerNew = ({
  value,
  date,
  label,
  placeholder,
  onChange,
  showDatePicker,
  minimumDate,
  setShowDatePicker,
  error,
  marginBottom,
  editable = true,
  mode = 'date',
}) => {
  let borderColor = COLORS.black;

  if (error) {
    borderColor = COLORS.red;
  } else if (value) {
    borderColor = COLORS.black;
  }

  const OnChangeDate = selectedTime => {
    onChange(selectedTime);
    setShowDatePicker(false);
  };

  const cancel = () => {
    setShowDatePicker(false);
  };

  return (
    <View style={[styles.container]}>
      <View>
        <Text
          style={[
            FontStyle.Regular14,
            {color: COLORS.darkBlack, textAlign: 'left'},
          ]}>
          {label}
        </Text>
      </View>

      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => setShowDatePicker(true)}
        disabled={!editable}
        style={[
          styles.inputView,
          {
            backgroundColor: !editable ? COLORS.backgroundInput : COLORS.white,
            // backgroundColor: COLORS.white,
            borderColor: error ? COLORS.red : '#BEBEBE',
          },
        ]}>
        <Text
          style={[
            FontStyle.Regular14_500,
            {
              color: value ? COLORS.darkBlack : COLORS.grey3,
              flex: 1,
              textAlign: 'left',
            },
          ]}>
          {value ? value : placeholder}
        </Text>
        <SvgXml
          xml={mode == 'time' ? Icons.clock : Icons.calender}
          style={{marginRight: 4, alignSelf: 'center'}}
        />
      </TouchableOpacity>
      {showDatePicker && (
        <DateTimePickerModal
          isVisible={showDatePicker}
          mode={mode}
          date={date}
          onConfirm={OnChangeDate}
          onCancel={cancel}
          minimumDate={minimumDate}
        />
      )}
      {error && (
        <Text
          style={[
            styles.label,
            {color: COLORS.red, marginTop: 2, fontSize: 12, textAlign: 'left'},
          ]}>
          {error}
        </Text>
      )}
    </View>
  );
};

export default DatePickerNew;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 4,
  },

  inputView: {
    ...Theme.Shadow,
    marginTop: 4,
    paddingHorizontal: 8,
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    ...FontStyle.Regular14_500,
    fontWeight: '400',
    paddingHorizontal: 1,
    letterSpacing: 0.16,
    marginLeft: 2,
  },
});
