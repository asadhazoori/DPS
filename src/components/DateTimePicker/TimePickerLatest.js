import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import {COLORS} from '../../theme/colors';
import {FontStyle} from '../../theme/FontStyle';
import Theme from '../../theme/theme';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {SvgXml} from 'react-native-svg';
import {Icons} from '../../assets/SvgIcons/Icons';

const TimePickerLatest = ({
  label,
  placeholder,
  value,
  initialValue,
  onChange,
  error,
  showTimePicker,
  setShowTimePicker,
  disabled,
  color,
  containerStyle,
  labelStyle,
  inputStyle,
  icon,
  height,
  editable = true,

  locale,
}) => {
  const handleTimeChange = selectedTime => {
    onChange(selectedTime);
    setShowTimePicker(false);
  };

  const cancel = () => {
    setShowTimePicker(false);
  };

  let borderColor = COLORS.black;

  if (error) {
    borderColor = COLORS.red;
  } else if (value) {
    borderColor = COLORS.black;
  }
  return (
    <View style={[styles.container]}>
      {/* <Text
        style={[
          FontStyle.Regular14_500,
          labelStyle,
          {color: COLORS.darkBlack, flex: 1, textAlign: 'left'},
        ]}>{`${label}`}</Text> */}

      <View>
        <Text
          style={[
            FontStyle.Regular16_500M,
            labelStyle,
            {color: COLORS.darkBlack, textAlign: 'left', marginLeft: 2},
          ]}>
          {`${label}`}
        </Text>
      </View>

      <TouchableOpacity
        disabled={disabled}
        activeOpacity={0.8}
        onPress={() => setShowTimePicker(true)}
        style={[
          styles.inputView,
          {
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: disabled ? COLORS.backgroundInput : COLORS.white,
            borderColor: error ? COLORS.red : '#BEBEBE',
            height: height ? height : 50,
          },
          inputStyle,
        ]}>
        <Text
          style={[
            FontStyle.Regular14_500,
            {color: !disabled ? color : COLORS.grey3},
          ]}>
          {value ? value : placeholder}
        </Text>
        {icon && <SvgXml xml={Icons.clock} style={{}} />}
      </TouchableOpacity>
      {error && (
        <Text
          style={[
            styles.label,
            {color: COLORS.red, marginTop: 2, fontSize: 12, textAlign: 'left'},
          ]}>
          {error}
        </Text>
      )}
      {showTimePicker && (
        <DateTimePickerModal
          isVisible={showTimePicker}
          mode="time"
          date={initialValue}
          onConfirm={handleTimeChange}
          onCancel={cancel}
          locale="en_GB"
        />
      )}
    </View>
  );
};

export default TimePickerLatest;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 4,
    borderColor: 'blue',
  },

  inputView: {
    ...Theme.Shadow,
    marginTop: 4, //8
    borderRadius: 8,
    paddingHorizontal: 10,
  },

  label: {
    ...FontStyle.Regular14_500,
    fontWeight: '400',
    paddingHorizontal: 1,
    letterSpacing: 0.16,
    marginLeft: 2,
  },
});
