import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import Theme from '../../theme/theme';
import {COLORS} from '../../theme/colors';
import {SvgXml} from 'react-native-svg';
import {BottomTabIcons} from '../../assets/SvgIcons/BottomTabIcons';

const PunchFloatingButton = ({onPress}) => {
  return (
    <View style={styles.container} pointerEvents="box-none">
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.9}
        style={styles.innerView}>
        <SvgXml xml={BottomTabIcons.location} />
      </TouchableOpacity>
    </View>
  );
};

export default PunchFloatingButton;

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    width: 70,
    alignItems: 'center',
  },
  innerView: {
    ...Theme.BottomTabButtonShadow,
    backgroundColor: COLORS.primaryColor,
    height: 46,
    width: 46,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: -16,
  },
});
