import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import Theme from '../../theme/theme';
import {FontStyle} from '../../theme/FontStyle';
import {COLORS} from '../../theme/colors';
import {SvgXml} from 'react-native-svg';

const SmartButtonTrans = ({title, onPress, rightIcon, color}) => {
  return (
    <TouchableOpacity
      style={[styles.container, Theme.Shadow]}
      onPress={onPress}
      activeOpacity={0.5}>
      <Text
        style={[
          FontStyle.Regular14_500M,
          {color: color ? color : COLORS.primaryColor,},
        ]}>
        {title}
      </Text>
      {rightIcon && (
        <View style={{marginLeft: 8}}>
          <SvgXml xml={rightIcon} />
        </View>
      )}
    </TouchableOpacity>
  );
};

export default SmartButtonTrans;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 18,
    minWidth: 87,
    paddingVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
