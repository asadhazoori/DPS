import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'

const TouchableButton = ({
    onPress,
    title,
    container
}) => {
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            style={[styles.container, container]}
            onPress={onPress}>
            <Text style={[FontStyle.Regular16_500M]}>{title}</Text>
        </TouchableOpacity>
    )
}

export default TouchableButton

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 8,
        paddingVertical: 4,
    },
})