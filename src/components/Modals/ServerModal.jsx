import { View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next';
import { DBPicker, NextButton, TextInputAuth } from '../Inputs';
import inputValidation from '../../utilities/Validations/YupValidate';
import { ServerSchema, ServerSchema1 } from '../../utilities/Validations/AuthValidations/ServerSchema';
import { setDatabase } from '../../redux/server/server.actions';
import { useDispatch, useSelector } from 'react-redux';
import { getDatabases } from '../../redux/server/actions/getDatabases';
import { getCompanyDetails } from '../../redux/settings/actions/get_company_details';
import GenericModal from './GenericModal';

const ServerModal = ({ modalVisible, setModalVisible, onChangeSelection, navigation }) => {

    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { IP, selectedDB, DBS } = useSelector((state) => state.server);
    const data = useSelector((state) => state.server);
    const [isIPEditable, setIPEditable] = useState(IP ? false : true);
    const [loading, setLoading] = useState(false);
    const [databases, setDBS] = useState(DBS);
    const [openDropdown, setOpenDropdown] = useState(false);

    const [inputs, setInputs] = useState({
        ip: IP,
        database: selectedDB,
        errors: null,
    });


    const validate = async () => {

        const schema = ServerSchema1(t);
        const result = await inputValidation(schema, inputs)

        if (result.isValidate) {
            setLoading(true)
            await dispatch(setDatabase(inputs.database));
            await dispatch(getCompanyDetails({ navigation }));
            setLoading(false);
            setModalVisible(false);

        } else {
            setInputs(prev => ({
                ...prev,
                errors: result?.err
            }))
        }

    }
    const handleOnChange = (field, value) => {
        if (field == "ip") {
            setDBS([])
            setInputs({
                ...inputs,
                [field]: value,
                ['database']: null,
                errors: {
                    ...inputs.errors,
                    [field]: false
                }
            })
        }
        if (field == "database") {

            setInputs({
                ...inputs,
                [field]: value,
                errors: {
                    ...inputs.errors,
                    [field]: false
                }
            })
        }
    }


    const getDatabasesList = async () => {

        const schema = ServerSchema(t);
        const result = await inputValidation(schema, inputs)
        if (result.isValidate) {
            setLoading(true);
            const databases = await dispatch(getDatabases({ IP: inputs.ip }))
            setLoading(false);
            if (databases) {
                setDBS(databases);
                setOpenDropdown(true);
            }
            else {
                setDBS([]);

            }

        } else {
            setInputs(prev => ({
                ...prev,
                errors: result?.err
            }))
        }


    }

    useEffect(() => {
        if (modalVisible == true) {
            setInputs({
                ip: IP,
                database: selectedDB
            })
            setIPEditable(IP ? false : true);
            setDBS(DBS)

        }

    }, [modalVisible])


    return (
        <GenericModal
            modalVisible={modalVisible}
            setModalVisible={setModalVisible}
            heading={t('server-details')}
            container={{ width: '85%' }}
        >

            <View style={{ marginBottom: 18, paddingHorizontal: 16 }}>
                <TextInputAuth
                    label={t('server-ip')}
                    placeholder={t('ip-placeholder')}
                    value={isIPEditable ? inputs.ip : IP}
                    error={inputs?.errors?.ip}
                    onChangeText={(text) => handleOnChange('ip', text)}
                    editable={isIPEditable}
                    rightIcon={inputs.ip ? true : false}
                    onChangeEditable={(newEditable) => {setIPEditable(newEditable) }}
                    onSubmitEditing={getDatabasesList}
                />

                <View style={{ marginTop: 8 }}>


                    <DBPicker
                        label={t('database')}
                        data={databases}
                        value1={inputs.database}
                        openDropdown={openDropdown}
                        setOpenDropdown={setOpenDropdown}
                        placeholder={t('select-db')}
                        onChange={(db) => handleOnChange('database', db)}
                        error={inputs?.errors?.database}
                    />
                </View>

                <View style={{ marginTop: 18, zIndex: -9999 }}>

                    <NextButton container={{ zIndex: -1, }} title={inputs.ip && inputs.database ? t('connect') : t('get-dbs')} loader={loading} onPress={() => inputs.ip && inputs.database ? validate() : getDatabasesList()} />

                </View>

            </View>
        </GenericModal>
    )
}

export default ServerModal
