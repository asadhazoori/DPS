import {
  StyleSheet,
  Text,
  View,
  Modal,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Alert,
} from 'react-native';
import React, { useState } from 'react';
import Icons from 'react-native-vector-icons/Ionicons';
import { useTranslation } from 'react-i18next';
import { FontStyle } from '../../theme/FontStyle';
import { getFormattedDate, getFormattedTime } from '../../utilities/helpers/CurretDate';
import { useDispatch, useSelector } from 'react-redux';
import { SmartButton, TextInputField } from '../Inputs';
import SmartButtonTrans from '../Buttons/SmartButtonTrans';
import { commonApi } from '../../utilities/api/apiController';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../redux/users/user.actions';
import { COLORS } from '../../theme/colors';
import ReactNativeVI from '../Helpers/ReactNativeVI';
import GenericModal from './GenericModal';

const ManualAttendanceDetailsModal = ({
  modalVisible,
  setModalVisible,
  data,
  employeeLoan,
  navigation,
  color,
  status,
  getEmployeeLoans,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [loading1, setLoading1] = useState(false);

  const updateState = async state => {
    try {
      const body = {
        "params": {
          "model": "manual.attendance.wags",
          "method": "update_attendance_status",
          "args": [
            {
              "record_id": data?.id,
              "status": state // for reject use "rejected" manager_approved
            }
          ],
          "kwargs": {}
        }
      }


      const response = await commonApi({ body, navigation });

      if (response?.data?.result?.status == 'success') {
        setLoading(false);
        setLoading1(false);
        setModalVisible(false);
        setTimeout(() => { Toast.show(`${t('request-updated-successfully')} !`); }, 300);
        getEmployeeLoans();
      } else {
        setLoading(false);
        setLoading1(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: update_attendance_status\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      setLoading1(false);
      console.log(error);
      setLoading(false);
    }
  };

  return (
    <GenericModal
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      heading={employeeLoan ? data?.employee_id?.[1] : data?.attendance_work_type_id?.[1]}>
      <View style={{ paddingHorizontal: 16, marginBottom: 12 }}>
        <View style={{ gap: 8 }}>
          {employeeLoan && (
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('type')}: </Text>
              <Text style={styles.title}>{data?.attendance_work_type_id?.[1]}</Text>
            </View>
          )}

          <View style={styles.rowView}>
            <Text style={styles.heading}>{t('date')}:</Text>
            <Text style={styles.title}>{getFormattedDate(data?.date)}</Text>
          </View>
          {data?.checkin &&
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('punch-in')}:</Text>
              <Text style={styles.title}>{getFormattedTime(data?.checkin)}</Text>
            </View>
          }
          {data?.checkout &&
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('punch-out')}:</Text>
              <Text style={styles.title}>{getFormattedTime(data?.checkout)}</Text>
            </View>
          }

          <View style={styles.rowView}>
            <Text style={styles.heading}>
              {t('reason')}:<Text style={styles.title}> {data?.reason}</Text>
            </Text>
          </View>

          {!employeeLoan ? (
            <View style={{ alignItems: 'flex-end' }}>
              <Text style={[FontStyle.Regular16_500, { color: color ? color : COLORS.primaryColor, fontWeight: '700', marginRight: 4, },]}>
                {status}
              </Text>
            </View>
          ) : data?.state !== 'Submit' ? (
            <View style={{ alignItems: 'flex-end' }}>
              <Text style={[FontStyle.Regular16_500, { color: color ? color : COLORS.primaryColor, fontWeight: '700', marginRight: 4, },]}>
                {status}
              </Text>
            </View>
          ) : null}
        </View>
      </View>
      {employeeLoan && data?.state == 'Submit' && (
        <View style={{ paddingHorizontal: 12, marginTop: -4, marginBottom: 12 }}>

          <View
            style={[
              styles.rowView,
              {
                paddingVertical: 4,
                justifyContent: 'space-around',
                marginTop: 10,
              },
            ]}>
            <SmartButton
              title={t('approve')}
              width={100}
              disabled={loading || loading1}
              loader={loading}
              handlePress={() => {
                setLoading(true);
                updateState('manager_approved');
              }}
              color={COLORS.green}
            />
            <SmartButton
              title={t('reject')}
              width={100}
              disabled={loading1 || loading}
              loader={loading1}
              handlePress={() => {
                setLoading1(true);
                updateState('rejected');

              }}
              color={COLORS.red}
            />
          </View>
        </View>
      )}
    </GenericModal>
  );
};

export default ManualAttendanceDetailsModal;

const styles = StyleSheet.create({
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 4,

  },

  heading: {
    ...FontStyle.Regular14_500,
    color: 'black',
    fontWeight: '700',
  },

  title: {
    flex: 1,
    ...FontStyle.Regular14_500,
    color: 'black',
    textAlign: 'left',
  },
});
