import { ActivityIndicator, Linking, Modal, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { FontStyle } from '../../theme/FontStyle'
import { SvgXml } from 'react-native-svg'
import { useTranslation } from 'react-i18next'
import { COLORS } from '../../theme/colors'

const AlertModal = ({ visible, icon, title, message, onClose, confirmbtn, okButton = true, retry, updateBtn, updateAction, loading }) => {
    const { t } = useTranslation();
    return (
        <Modal
            animationType='fade'
            transparent={true}
            visible={visible}
        >
            <View style={styles.centeredView}>
                <View style={styles.modalView}>

                    <View style={{ alignItems: 'center' }}>
                        {icon}

                        <Text style={[FontStyle.Regular22, { fontSize: 16, marginTop: 14 }]}>{title}</Text>
                        <Text style={[FontStyle.Regular14_500, { marginTop: 4, textAlign: 'center', color: COLORS.black }]}>{message}</Text>
                    </View>

                    <View style={styles.buttonView}>
                        {
                            okButton &&
                            <TouchableOpacity
                                onPress={() => { onClose() }}
                                style={{ padding: 6, marginRight: 6, }}
                                activeOpacity={0.6}
                                disabled={loading}
                            >
                                {loading ? <ActivityIndicator size={24} color={'#3BCA78'} /> :
                                    <Text style={[FontStyle.Regular16_500M, { color: confirmbtn ? '#F73C3C' : '#3BCA78' }]}>{confirmbtn ? `${t("cancel")}` : okButton && retry ? `${t('check-again')}` : `${t("ok")}`}</Text>
                                }</TouchableOpacity>
                        }


                        {updateBtn &&
                            <TouchableOpacity
                                onPress={updateAction}
                                style={{ padding: 6, marginRight: 6 }}
                                activeOpacity={0.6}
                            >
                                <Text style={[FontStyle.Regular16_500M, { color: confirmbtn ? '#F73C3C' : '#3BCA78' }]}>{`${t("update")}`}</Text>
                            </TouchableOpacity>
                        }

                        {confirmbtn &&
                            <TouchableOpacity
                                activeOpacity={0.6}
                                onPress={() => {
                                    if (Platform.OS === 'ios') {
                                        Linking.openURL('app-settings:');

                                    } else {
                                        Linking.openSettings();
                                    }
                                    onClose()
                                }
                                }
                                style={{ padding: 6, marginLeft: 8, }}>
                                <Text style={[FontStyle.Regular16_500M, { color: '#3BCA78' }]}>{t('allow')}</Text>
                            </TouchableOpacity>
                        }
                    </View>

                </View>
            </View>
        </Modal>
    )
}

export default AlertModal

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.60)',
        justifyContent: 'center',
        alignItems: 'center'

    },

    modalView: {
        // margin: 40,
        backgroundColor: 'white',
        borderRadius: 8,
        padding: 14,
        width: 300,

    },

    iconView: {
        backgroundColor: "#F73C3C3D",
        height: 48,
        width: 48,
        borderRadius: 48,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalTitle: {
        fontSize: 20,
        marginBottom: 15,
        fontWeight: 'bold',
    },
    modalText: {
        fontSize: 16,
        marginBottom: 20,
        textAlign: 'center',
    },

    buttonView: {
        flexDirection: 'row',
        marginTop: 8,
        justifyContent: 'flex-end',
    }
})