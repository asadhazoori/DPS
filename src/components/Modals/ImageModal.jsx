import {Image, Modal, StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {COLORS} from '../../theme/colors';
import ReactNativeVI from '../Helpers/ReactNativeVI';
import {FontStyle} from '../../theme/FontStyle';

const ImageModal = ({modalVisible, setModalVisible, selectedImage}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(false);
      }}>
      <View style={styles.container}>
        <View style={[styles.innerContainer]}>
          <Image
            source={{
              uri: `data:image/jpeg;base64,${selectedImage}`,
            }}
            resizeMode="contain"
            style={{flex: 1}}
          />
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => setModalVisible(false)}
            style={styles.iconView}>
            <ReactNativeVI
              Lib={'Ionicons'}
              name={'close'}
              color={COLORS.white}
              size={26}
            />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default ImageModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'center',
  },

  innerContainer: {
    backgroundColor: COLORS.white,
    borderRadius: 8,
    width: '87%',
    height: '50%',
  },

  textView: {
    marginTop: 16,
    marginHorizontal: 16,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginBottom: 12,
  },

  header: {
    ...FontStyle.Regular16_500,
    fontWeight: '700',
    color: COLORS.black,
    borderColor: 'red',
  },

  iconView: {
    position: 'absolute',
    right: -10,
    top: -10,
    padding: 6,
    backgroundColor: COLORS.grey,
    borderRadius: 100,
  },
});
