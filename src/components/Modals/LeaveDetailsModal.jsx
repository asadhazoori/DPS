import { Alert, StyleSheet, Text, View } from 'react-native';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FontStyle } from '../../theme/FontStyle';
import {
  floatToTimeString,
  getFormattedDate,
} from '../../utilities/helpers/CurretDate';
import { useDispatch } from 'react-redux';
import { SmartButton, TextInputField } from '../Inputs';
import { commonApi } from '../../utilities/api/apiController';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../redux/users/user.actions';
import { COLORS } from '../../theme/colors';
import GenericModal from './GenericModal';

const LeaveDetailsModal = ({
  modalVisible,
  setModalVisible,
  data,
  employeeLeaves,
  navigation,
  color,
  status,
  getEmployeeLeaves,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [comment, setComment] = useState(null);
  const [loading1, setLoading1] = useState(false);
  const [error, setError] = useState(null);

  const updateState = async state => {
    try {
      const body = {
        params: {
          model: 'hr.holidays.wags',
          method: 'update_leave_status',
          args: [
            {
              leave_id: data.leave_id,
              state: state,
              note: comment,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        setLoading(false);
        setLoading1(false);
        setModalVisible(false);
        getEmployeeLeaves();
        setTimeout(() => { Toast.show(`${t('leave-updated-successfully')} !`); }, 300);
      } else {
        setLoading(false);
        setLoading1(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: update_leave_status\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      setLoading1(false);
      console.log(error);
      setLoading(false);
    }
  };

  return (
    <GenericModal
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      heading={employeeLeaves ? data?.employee_name : data?.leave_type_name}>
      <View style={{ paddingHorizontal: 18, marginBottom: 12 }}>
        <View style={{ gap: 8 }}>
          {employeeLeaves && (
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('leave-type')}: </Text>
              <Text style={styles.title}>{data?.leave_type_name}</Text>
            </View>
          )}
          <View style={styles.rowView}>
            <Text style={styles.heading}>
              {data?.is_short_leave ? t('date') : t('from')}:{' '}
            </Text>
            <Text style={styles.title}>
              {getFormattedDate(data?.date_from)}
            </Text>
          </View>

          {!data?.is_short_leave && (
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('to')}: </Text>
              <Text style={styles.title}>
                {getFormattedDate(data?.date_to)}
              </Text>
            </View>
          )}
          {!data?.is_short_leave && (
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('no-of-days')}: </Text>
              <Text style={styles.title}>{data?.number_of_days}</Text>
            </View>
          )}
          {data?.is_short_leave && (
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('leave-time')}: </Text>
              <Text style={styles.title}>
                {floatToTimeString(data?.outtime)}
              </Text>
            </View>
          )}
          {data?.is_short_leave && data.intime !== 0.0 && (
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('return-time')}: </Text>
              <Text style={styles.title}>
                {floatToTimeString(data?.intime)}
              </Text>
            </View>
          )}

          <View style={styles.rowView}>
            <Text style={styles.heading}>{t('purpose')}: </Text>
            <Text style={styles.title}>{data?.comments}</Text>
          </View>

          {
            data?.is_advance_salary &&
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('is-advance-salary')}: </Text>
              <Text style={styles.title}>{t("yes")}</Text>
            </View>}

          {
            data?.is_flight_ticket &&
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('is-exit-re-entry')}: </Text>
              <Text style={styles.title}>{t("yes")}</Text>
            </View>}

          {
            data?.is_exit_re_entry &&
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('is-flight-ticket')}: </Text>
              <Text style={styles.title}>{t("yes")}</Text>
            </View>}

          {data?.manager_remarks && (
            <View style={styles.rowView}>
              <Text style={[styles.heading, { color: COLORS.primaryColor }]}>
                {t('manager-comments')}:{' '}
              </Text>
              <Text style={styles.title}>{data?.manager_remarks}</Text>
            </View>
          )}
          {!employeeLeaves ? (
            <View style={{ alignItems: 'flex-end' }}>
              <Text
                style={[
                  FontStyle.Regular16_500,
                  {
                    color: color ? color : COLORS.primaryColor,
                    fontWeight: '700',
                    marginRight: 4,
                  },
                ]}>
                {status}
              </Text>
            </View>
          ) : data?.state == 'approved_by_manager' ||
            data?.state == 'refuse' ||
            data?.state == 'approved' ? (
            <View style={{ alignItems: 'flex-end' }}>
              <Text
                style={[
                  FontStyle.Regular16_500,
                  {
                    color: color ? color : COLORS.primaryColor,
                    fontWeight: '700',
                    marginRight: 4,
                  },
                ]}>
                {status}
              </Text>
            </View>
          ) : null}
        </View>
      </View>
      {employeeLeaves &&
        data?.state !== 'approved_by_manager' &&
        data?.state !== 'refuse' &&
        data?.state !== 'approved' && (
          <View
            style={{ paddingHorizontal: 12, marginTop: -4, marginBottom: 12 }}>
            <TextInputField
              label={t('comments')}
              placeholder={t('enter-comments')}
              labelStyle={FontStyle.Regular14}
              value={comment}
              error={error}
              onChangeText={text => setComment(text)}
            />
            <View
              style={[
                styles.rowView,
                {
                  paddingVertical: 4,
                  justifyContent: 'space-around',
                  marginTop: 10,
                },
              ]}>
              <SmartButton
                title={t('approve')}
                width={100}
                disabled={loading || loading1}
                loader={loading}
                handlePress={() => {
                  setLoading(true);
                  updateState('approved_by_manager');
                }}
                color={COLORS.green}
              />
              <SmartButton
                title={t('reject')}
                width={100}
                disabled={loading1 || loading}
                loader={loading1}
                handlePress={() => {
                  if (comment?.trim() == '' || comment?.trim() == null) {
                    setError(t('comment-is-required'));
                  } else {
                    setLoading1(true);
                    updateState('refuse');
                  }
                }}
                color={COLORS.red}
              />
            </View>
          </View>
        )}
    </GenericModal>
  );
};

export default LeaveDetailsModal;

const styles = StyleSheet.create({
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 4,
  },

  heading: {
    ...FontStyle.Regular14_500,
    color: 'black',
    fontWeight: '700',
  },

  title: {
    flex: 1,
    ...FontStyle.Regular14_500,
    color: 'black',
    textAlign: 'left',
  },
});
