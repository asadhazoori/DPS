import { Alert, StyleSheet, Text, View } from 'react-native';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FontStyle } from '../../theme/FontStyle';
import { getFormattedDate } from '../../utilities/helpers/CurretDate';
import { useDispatch } from 'react-redux';
import { SmartButton, TextInputField } from '../Inputs';
import { commonApi } from '../../utilities/api/apiController';
import Toast from 'react-native-simple-toast';
import { logout_user } from '../../redux/users/user.actions';
import { COLORS } from '../../theme/colors';
import GenericModal from './GenericModal';

const OvertimeDetailsModal = ({
  modalVisible,
  setModalVisible,
  data,
  employeeOvertime,
  navigation,
  color,
  status,
  getEmployeeOvertimes,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [comment, setComment] = useState(null);
  const [loading1, setLoading1] = useState(false);
  const [error, setError] = useState(null);
  const updateState = async state => {
    try {
      const body = {
        params: {
          model: 'hr.overtime.wags',
          method: 'update_overtime_request',
          args: [
            {
              overtime_id: data.record_id,
              status: state,
              manager_comments: comment,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        setLoading(false);
        setLoading1(false);
        setModalVisible(false);
        setTimeout(() => { Toast.show(`${t('overtime-request-updated-successfully')} !`); }, 300);
        getEmployeeOvertimes();
      } else {
        setLoading(false);
        setLoading1(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: update_overtime_request\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      setLoading1(false);
      console.log(error);
      setLoading(false);
    }
  };

  return (
    <GenericModal
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      heading={
        employeeOvertime
          ? data?.employee_name
          : `${t('overtime')}: ${data?.overtime} ${t('hours')}`
      }>
      <View style={{ paddingHorizontal: 16, marginBottom: 12 }}>
        <View style={{ gap: 8 }}>
          {employeeOvertime && (
            <View style={styles.rowView}>
              <Text style={styles.heading}>{t('overtime')}: </Text>
              <Text style={styles.title}>
                {data?.overtime} {t('hours')}
              </Text>
            </View>
          )}
          <View style={styles.rowView}>
            <Text style={styles.heading}>{t('date')}: </Text>
            <Text style={styles.title}>
              {getFormattedDate(data?.overtime_date)}
            </Text>
          </View>
          <View style={styles.rowView}>
            <Text style={styles.heading}>{t('no-of-days')}: </Text>
            <Text style={styles.title}>{data?.overtime_days}</Text>
          </View>
          {data?.approved_amount !== undefined &&
            data?.approved_amount !== null && (
              <View style={styles.rowView}>
                <Text style={styles.heading}>{t('approved-amount')}: </Text>
                <Text style={styles.title}>{`${data?.approved_amount}`}</Text>
              </View>
            )}
          <View style={styles.rowView}>
            <Text style={styles.heading}>
              {t('purpose')}: <Text style={styles.title}>{data?.reason}</Text>
            </Text>
          </View>

          {data?.manager_comments && (
            <View style={styles.rowView}>
              <Text style={[styles.heading, { color: COLORS.primaryColor }]}>
                {t('manager-comments')}:{' '}
                <Text style={styles.title}>{data?.manager_comments}</Text>
              </Text>
            </View>
          )}

          {!employeeOvertime ? (
            <View style={{ alignItems: 'flex-end' }}>
              <Text
                style={[
                  FontStyle.Regular16_500,
                  {
                    color: color ? color : COLORS.primaryColor,
                    fontWeight: '700',
                    marginRight: 4,
                  },
                ]}>
                {status}
              </Text>
            </View>
          ) : data?.status !== 'draft' ? (
            <View style={{ alignItems: 'flex-end' }}>
              <Text
                style={[
                  FontStyle.Regular16_500,
                  {
                    color: color ? color : COLORS.primaryColor,
                    fontWeight: '700',
                    marginRight: 4,
                  },
                ]}>
                {status}
              </Text>
            </View>
          ) : null}
        </View>
      </View>
      {employeeOvertime && data?.status == 'draft' && (
        <View style={{ paddingHorizontal: 12, marginTop: -4, marginBottom: 12 }}>
          <TextInputField
            label={t('comments')}
            placeholder={t('enter-comments')}
            labelStyle={FontStyle.Regular14}
            value={comment}
            error={error}
            onChangeText={text => setComment(text)}
          />
          <View
            style={[
              styles.rowView,
              {
                paddingVertical: 4,
                justifyContent: 'space-around',
                marginTop: 10,
              },
            ]}>
            <SmartButton
              title={t('approve')}
              width={100}
              disabled={loading || loading1}
              loader={loading}
              handlePress={() => {
                setLoading(true);
                updateState('validate');
              }}
              color={COLORS.green}
            />
            <SmartButton
              title={t('reject')}
              width={100}
              disabled={loading1 || loading}
              loader={loading1}
              handlePress={() => {
                if (comment?.trim() == '' || comment?.trim() == null) {
                  setError(t('comment-is-required'));
                } else {
                  setLoading1(true);
                  updateState('cancel');
                }
              }}
              color={COLORS.red}
            />
          </View>
        </View>
      )}
    </GenericModal>
  );
};

export default OvertimeDetailsModal;

const styles = StyleSheet.create({
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 4,
  },

  heading: {
    ...FontStyle.Regular14_500,
    color: 'black',
    fontWeight: '700',
  },

  title: {
    flex: 1,
    ...FontStyle.Regular14_500,
    color: 'black',
    textAlign: 'left',
  },
});
