import { StyleSheet, Text, View, Modal } from 'react-native'
import React from 'react'
import { COLORS } from '../../theme/colors'
import { ActivityIndicator } from 'react-native-paper'
import Theme from '../../theme/theme'
import { FontStyle } from '../../theme/FontStyle'

const Loader = ({ loading, setLoading, title }) => {
    return (
        <Modal
            animationType='fade'
            transparent={true}
            visible={loading}
            onRequestClose={() => {
                setLoading && setLoading(false);
            }}
        // presentationStyle='overFullScreen'

        >
            <View style={styles.container}>

                <View style={styles.container1}>

                    <ActivityIndicator color={COLORS.primaryColor} />
                    <Text style={styles.text}>{title}</Text>

                </View>
            </View>
        </Modal>
    )
}

export default Loader

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.30)',
        justifyContent: 'center',
        alignItems: 'center'

    },

    container1: {
        paddingVertical: 20,
        paddingHorizontal: 24,
        flexDirection: 'row',
        backgroundColor: COLORS.white,
        borderRadius: 8,
        gap: 18,
        ...Theme.Shadow

    },

    text: {
        ...FontStyle.Regular16_500M,
        color: COLORS.darkBlack

    },

})