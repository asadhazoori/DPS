import { StyleSheet, Text, View, I18nManager } from 'react-native'
import React, { useEffect, useState } from 'react'
import { RadioButton } from 'react-native-paper';
import { useTranslation } from 'react-i18next';
import { COLORS } from '../../theme/colors';
import { NextButton } from '../Inputs';
import { FontStyle } from '../../theme/FontStyle';
import GenericModal from './GenericModal';

const LangModal = ({ modalVisible, setModalVisible, onChangeSelection }) => {

    const [selectedType, setSelectedType] = useState(null);
    const [existingLang, setExistingLang] = useState(null);
    const { t } = useTranslation();

    const data = [
        { id: 1, name: 'English', key: 'en' },
        { id: 2, name: "العربية", key: 'ar' }
    ]

    const handleRadioButtonChange = (itemId) => {
        setSelectedType(itemId);
    };

    const handleOkButtonPress = () => {

        if (selectedType?.key !== existingLang?.key) {
            onChangeSelection(selectedType.key === 'ar');
        }
        setModalVisible(false);
    };

    useEffect(() => {
        const checkLang = async () => {
            const defaultLang = I18nManager.isRTL ? data[1] : data[0];
            setSelectedType(defaultLang);
            setExistingLang(defaultLang);
        }
        checkLang();

    }, [])


    return (
        <GenericModal
            modalVisible={modalVisible}
            setModalVisible={setModalVisible}
            heading={t('change-language')}
            container={{ width: '85%' }}
        >
            <View style={{ paddingBottom: 16, paddingHorizontal: 16 }}>
                {data?.map((item) => (
                    <View key={item.id} style={styles.radioView}>
                        <View>
                            <RadioButton.Android color={COLORS.primaryColor}
                                value={item.id}
                                status={selectedType?.id === item.id ? 'checked' : 'unchecked'}
                                onPress={() => handleRadioButtonChange(item)}
                            />
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={styles.text}>{item.name}</Text>
                        </View>
                    </View>
                ))}
                <View style={styles.bottomView}>
                    <NextButton title={t('ok')} onPress={handleOkButtonPress} />
                </View>
            </View>
        </GenericModal >
    )
}

export default LangModal

const styles = StyleSheet.create({

    radioView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5,
        justifyContent: 'center',
    },

    text: {
        ...FontStyle.Regular14_500,
        marginLeft: 4,
        color: COLORS.black,
        alignSelf: 'flex-start',
        justifyContent: 'flex-start',

    },

    bottomView: {
        marginTop: 8,
    },



})