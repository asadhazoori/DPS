import { KeyboardAvoidingView, Modal, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { COLORS } from '../../theme/colors';
import ReactNativeVI from '../Helpers/ReactNativeVI';
import { FontStyle } from '../../theme/FontStyle';

const GenericModal = ({ modalVisible, setModalVisible, heading, container, children }) => {

    return (
        <Modal
            animationType='fade'
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
                setModalVisible(false);
            }}
        >
            <KeyboardAvoidingView
                style={styles.container}
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            >
                <View style={[styles.innerContainer, container]}>
                    <View>
                        <View style={[styles.textView]}>
                            <Text style={styles.header}>{heading}</Text>
                        </View>
                        <TouchableOpacity onPress={() => setModalVisible(false)} style={styles.iconView}>
                            <ReactNativeVI Lib={'Ionicons'} name={'close'} color={COLORS.black} size={26} />
                        </TouchableOpacity>
                    </View>
                    {children}
                </View>
            </KeyboardAvoidingView>
        </Modal>
    )
}

export default GenericModal

const styles = StyleSheet.create({


    container: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        justifyContent: 'center',
        alignItems: 'center',

    },

    innerContainer: {
        backgroundColor: COLORS.white,
        borderRadius: 8,
        width: '87%',

    },

    textView: {
        marginTop: 16,
        marginHorizontal: 16,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginBottom: 12,
    },

    header: {
        ...FontStyle.Regular16_500,
        fontWeight: '700',
        color: COLORS.black,
        borderColor: 'red'

    },

    iconView: {
        position: 'absolute',
        right: 0,
        padding: 8
    }


})