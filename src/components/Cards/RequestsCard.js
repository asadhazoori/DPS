import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {FontStyle} from '../../theme/FontStyle';
import {COLORS} from '../../theme/colors';
import Theme from '../../theme/theme';
import {useTranslation} from 'react-i18next';
import {
  getFormattedDate,
  getFormattedTime,
} from '../../utilities/helpers/CurretDate';

const RequestsCard = ({data, navigation, manualAttend}) => {
  const {t} = useTranslation();

  let color = '';
  let caption = '';
  let description = '';
  let manualAttendStatus = '';

  switch (data?.request_type) {
    case 'Iqama Renewal':
      caption = `${t('issue_date')}: ${
        data?.iqama_issue_date
          ? getFormattedDate(data?.iqama_issue_date, false)
          : '-'
      }`;
      description = `${t('expiry_date')}: ${
        data?.iqama_expiry_date
          ? getFormattedDate(data?.iqama_expiry_date, false)
          : '-'
      }`;

      switch (data?.state) {
        case 'Draft':
          color = COLORS.yellow;
          break;

        case 'Waiting For Payment':
          color = COLORS.red;
          break;

        case 'Paid':
          color = COLORS.green;
          break;

        default:
          break;
      }

      break;

    case 'Employee Resignation':
      caption = `${t('resignation_date')}: ${
        data?.resignation_date
          ? getFormattedDate(data?.resignation_date, false)
          : '-'
      }`;
      description = `${data?.resignation_reason} `;

      switch (data?.state) {
        case 'Draft':
          color = COLORS.yellow;
          break;

        case 'Manager Approval':
          color = COLORS.green;
          break;

        case 'HR Approval':
          color = COLORS.green;
          break;

        case 'Exit Process':
          color = COLORS.red1;
          break;

        case 'Cancel':
          color = COLORS.red1;
          break;

        case 'Reject':
          color = COLORS.red1;
          break;

        default:
          break;
      }

      break;

    case 'Travel & Ticket':
      caption = `${data?.from_country} - ${data?.to_country} `;
      description = `${data?.travel_purpose} `;

      switch (data?.state) {
        case 'Draft':
          color = COLORS.yellow;
          break;

        case 'Confirm':
          color = COLORS.green;
          break;

        case 'Approved':
          color = COLORS.green;
          break;

        default:
          break;
      }

      break;

    case 'Employment Letter':
      caption = `${data?.identification_letter_type} `;
      description = `${data?.reason} `;

      switch (data?.state) {
        case 'Approved By Manager':
          color = COLORS.yellow;
          break;

        case 'Approved By HR Manager':
          color = COLORS.primaryColor;
          break;

        case 'Approved By CEO':
          color = COLORS.green;
          break;

        case 'Approved':
          color = COLORS.green;
          break;

        default:
          break;
      }

      break;

    case 'Employee Warning':
      caption = `${t('deduction_type')}: ${data?.deduction_type} `;
      description = `${data?.warning_reasons} `;

      break;

    default:
      if (data?.work_type == 'missed_checkin') {
        caption = `${getFormattedTime(data?.checkin)}`;
      } else if (data?.work_type == 'missed_checkout') {
        caption = `${getFormattedTime(data?.checkout)}`;
      } else {
        caption = `${getFormattedTime(data?.checkin)} -> ${getFormattedTime(
          data?.checkout,
        )}`;
      }
      description = `${data?.reason}`;

      switch (data?.state) {
        case 'Submit':
          color = COLORS.yellow;
          manualAttendStatus = t('pending');
          break;

        case 'Rejected':
          color = COLORS.red;
          manualAttendStatus = t('rejected');
          break;

        case 'Validated':
          color = COLORS.green;
          manualAttendStatus = t('approved');
          break;
        case 'Manager Approved':
          color = COLORS.primaryColor;
          manualAttendStatus = t('manager-approved');
          break;

        default:
          break;
      }
  }

  return (
    <TouchableOpacity
      disabled={manualAttend}
      activeOpacity={0.7}
      onPress={() => {
        if (!manualAttend) {
          navigation.navigate('RequestDetails', {
            data: data,
            color: color,
          });
        }
      }}
      style={styles.container}>
      <View style={styles.detailsView}>
        <Text style={styles.typeText}>
          {manualAttend
            ? data?.attendance_work_type_id?.[1]
            : t(data?.request_type)}
        </Text>
        {manualAttend && (
          <Text style={styles.dateText}>{getFormattedDate(data?.date)}</Text>
        )}
        <Text
          style={[
            styles.dateText,
            manualAttend && {color: COLORS.primaryColor},
          ]}>
          {caption}
        </Text>
        <Text style={styles.daysText}>{description}</Text>
      </View>

      {data?.request_type !== 'Employee Warning' && (
        <View style={styles.statusView}>
          <Text
            style={[
              styles.statusText,
              {color: color ? color : COLORS.primaryColor},
            ]}>
            {manualAttend ? manualAttendStatus : data?.state}
          </Text>
        </View>
      )}
    </TouchableOpacity>
  );
};

export default RequestsCard;

const styles = StyleSheet.create({
  container: {
    padding: 8,
    marginHorizontal: 4,
    ...Theme.Shadow,
    borderWidth: 0,
    marginTop: 2,
    flexDirection: 'row',
  },

  detailsView: {
    flex: 2,
    // borderWidth:1,
  },

  statusView: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },

  statusText: {
    ...FontStyle.Regular14_500M,
    fontWeight: '700',
  },

  typeText: {
    ...FontStyle.Regular16,
    fontWeight: '700',
    color: COLORS.darkBlack,
    alignSelf: 'flex-start',
  },

  dateText: {
    ...FontStyle.Regular14,
    color: COLORS.grey5,
    fontWeight: '400',
    marginTop: 4,
    alignSelf: 'flex-start',
  },

  daysText: {
    ...FontStyle.Regular12,
    color: COLORS.grey5,
    fontWeight: '400',
    marginTop: 8,
    alignSelf: 'flex-start',
  },
});
