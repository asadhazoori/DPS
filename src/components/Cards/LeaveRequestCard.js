import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import {FontStyle} from '../../theme/FontStyle';
import {COLORS} from '../../theme/colors';
import Theme from '../../theme/theme';
import {
  floatToTimeString,
  getFormattedDate,
} from '../../utilities/helpers/CurretDate';
import LeaveDetailsModal from '../Modals/LeaveDetailsModal';
import {useTranslation} from 'react-i18next';

const LeaveRequestCard = ({
  data,
  employeeLeaves,
  navigation,
  getEmployeeLeaves,
}) => {
  const [modalVisible, setModalVisible] = useState(false);

  const {t} = useTranslation();

  let state = '';
  let status = '';
  let color = '';

  switch (data?.state) {
    case 'submit':
      status = t('pending');
      state = 'Cancel';
      color = COLORS.yellow;
      break;

    case 'draft':
      status = t('pending');
      state = 'Cancel';
      color = COLORS.yellow;
      break;

    case 'refuse':
      status = t('rejected');
      state = 'Rejected';
      color = COLORS.red1;
      break;

    case 'approved':
      status = t('approved');
      state = 'Approved';
      color = COLORS.green;
      break;

    case 'approved_by_manager':
      status = t('approved_by_manager');
      state = 'Approved';
      color = COLORS.green;
      break;

    default:
      break;
  }

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => {
        setModalVisible(true);
      }}
      style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <View style={styles.detailsView}>
          {employeeLeaves && (
            <Text style={styles.typeText}>{data?.employee_name}</Text>
          )}
          <Text
            style={[
              styles.typeText,
              {
                ...(employeeLeaves && FontStyle.Regular16_500M),
                color: employeeLeaves ? COLORS.grey5 : COLORS.darkBlack,
              },
            ]}>
            {data?.leave_type_name}
          </Text>
          {data?.is_short_leave ? (
            <Text style={styles.dateText}>
              {!data.intime && data.intime == 0 ? `${t('leave-time')}: ` : ''}
              {data?.outtime && floatToTimeString(data?.outtime)}
              {data.intime && data.intime !== 0 && data.intime !== 0.0
                ? ` - ${floatToTimeString(data?.intime)}`
                : ''}
            </Text>
          ) : (
            <Text style={styles.dateText}>
              {t('no-of-days')}: {data?.number_of_days}{' '}
              {data?.number_of_days > 1 ? t('days') : t('day')}
            </Text>
          )}
        </View>

        <View style={styles.statusView}>
          <Text
            style={[
              FontStyle.Regular14_500,
              {color: color ? color : COLORS.primaryColor, fontWeight: '700'},
            ]}>
            {status}
          </Text>
        </View>
      </View>

      {data?.is_short_leave ? (
        <Text style={styles.daysText}>{getFormattedDate(data?.date_from)}</Text>
      ) : (
        <Text style={styles.daysText}>
          {getFormattedDate(data?.date_from)} -{' '}
          {getFormattedDate(data?.date_to)}
        </Text>
      )}

      <LeaveDetailsModal
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        data={data}
        employeeLeaves={employeeLeaves}
        navigation={navigation}
        color={color}
        status={status}
        getEmployeeLeaves={getEmployeeLeaves}
      />
    </TouchableOpacity>
  );
};

export default LeaveRequestCard;

const styles = StyleSheet.create({
  container: {
    padding: 8,
    marginHorizontal: 4,
    ...Theme.Shadow,
    borderWidth: 0,
    marginTop: 2,
  },

  detailsView: {
    flex: 2,
  },

  statusView: {
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  statusView1: {
    ...Theme.Shadow,
    paddingVertical: 8,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },

  statusText: {
    ...FontStyle.Regular12,
    fontWeight: '500',
  },

  typeText: {
    ...FontStyle.Regular16,
    fontWeight: '700',
    color: COLORS.darkBlack,
    alignSelf: 'flex-start',
  },

  dateText: {
    ...FontStyle.Regular14,
    color: COLORS.grey5,
    fontWeight: '400',
    marginTop: 4,
    alignSelf: 'flex-start',
  },

  daysText: {
    ...FontStyle.Regular12,
    color: COLORS.grey5,
    fontWeight: '400',
    marginTop: 8,
    alignSelf: 'flex-start',
  },
});

// ${data?.outtime && floatToTimeString(data?.outtime)}
