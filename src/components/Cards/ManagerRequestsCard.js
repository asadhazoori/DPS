import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'
import Theme from '../../theme/theme'
import SmartButtonTrans from '../Buttons/SmartButtonTrans'

const ManagerRequestsCard = ({ data }) => {

    let state = ''
    let status = ''
    let color = ''


    switch (data?.state) {
        case 'confirm':
            status = 'Pending'
            state = 'Cancel'
            color = COLORS.yellow
            break;

        case 'refuse':
            status = 'Rejected'
            state = 'Rejected'
            color = COLORS.red1
            break;

        case 'validate':
            status = 'Accepted'
            state = 'Approved'
            color = COLORS.green

            break;




        default:
            break;
    }


    return (
        <View style={styles.container}>
            <View style={styles.detailsView}>

                <Text style={styles.typeText}>{data?.leaves_type}</Text>
                <Text style={styles.dateText}>12 Dec 2023 - 22 Dec 2023</Text>
                <Text style={styles.daysText}>{'Rejected due to IGI rejection.'}</Text>
                {/* <Text style={styles.daysText}>{data?.date_from_ecube} - {data?.date_to_ecube}</Text> */}
            </View>

            {data.state == 'confirm' ?

                <View style={styles.statusView}>
                    {/* <SmartButtonTrans title={state} color={color} /> */}

                    {/* <Text style={[FontStyle.Regular12, { color: color ? color : COLORS.primaryColor, fontWeight: '500', marginRight: 8 }]}>{status}</Text> */}
                    {/* <View style={styles.statusView1}>
                        <Text style={[styles.statusText, { color: COLORS.green }]}>{'Approve'}</Text>
                    </View>

                    <View style={styles.statusView1}>
                        <Text style={[styles.statusText, { color: COLORS.red1 }]}>{'Reject'}</Text>
                    </View> */}
                    <SmartButtonTrans title={'Approve'} color={COLORS.green} />
                    <SmartButtonTrans title={'Reject'} color={COLORS.red1} />

                </View>

                :

                <View style={[styles.statusView, { justifyContent: 'center' }]}>
                    {/* <SmartButtonTrans title={state} color={color} /> */}
                    <Text style={[FontStyle.Regular12, { color: color ? color : COLORS.primaryColor, fontWeight: '500', marginRight: 8 }]}>{status}</Text>
                </View>
            }
        </View>

    )
}


export default ManagerRequestsCard

const styles = StyleSheet.create({
    container: {
        padding: 8,
        marginHorizontal: 4,
        ...Theme.Shadow,
        borderWidth: 0,
        marginTop: 2,
        flexDirection: 'row',

    },

    detailsView: {
        flex: 2,
    },

    statusView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'space-between',

    },
    statusView1: {
        ...Theme.Shadow,
        paddingVertical: 8,
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },

    statusText: {
        ...FontStyle.Regular10,
        fontWeight: '500',
    },

    typeText: {
        textAlign: 'left',
        ...FontStyle.Regular14,
        fontWeight: '700',
        color: COLORS.darkBlack,
    },

    dateText: {
        textAlign: 'left',
        ...FontStyle.Regular12,
        color: COLORS.grey4,
        fontWeight: '400',
        marginTop: 4,
    },

    daysText: {
        textAlign: 'left',
        ...FontStyle.Regular12,
        color: COLORS.darkBlack,
        fontWeight: '400',
        marginTop: 12,
    },


})