import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'
import Theme from '../../theme/theme'
import { getFormattedDate } from '../../utilities/helpers/CurretDate'
import { useTranslation } from 'react-i18next'
import OvertimeDetailsModal from '../Modals/OvertimeDetailsModal'

const OvertimeRequestCard = ({
    data,
    employeeOvertime,
    navigation,
    getEmployeeOvertimes }) => {

    const [modalVisible, setModalVisible] = useState(false);
    const { t } = useTranslation()

    let status = ''
    let color = ''

    switch (data?.status) {
        case 'draft':
            color = COLORS.yellow
            status = 'Pending'
            break;

        case 'validate':
            color = COLORS.green
            status = 'Approved'
            break;

        case 'cancel':
            color = COLORS.notify
            status = 'Rejected'

            break;

        default:
            color = 'grey'
            break;
    }


    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => { setModalVisible(true) }}
            style={styles.container}>
            <View style={{ flexDirection: 'row', }}>

                <View style={styles.detailsView}>
                    {employeeOvertime &&
                        <Text style={styles.typeText}>{data?.employee_name}</Text>
                    }
                    <Text style={[styles.typeText,
                    {
                        ...(employeeOvertime && FontStyle.Regular16_500M),
                        color: employeeOvertime ? COLORS.grey5 : COLORS.darkBlack,
                    }]}>{t('overtime')}: {data?.overtime} {t('hours')}</Text>
                    <Text style={styles.dateText}>{getFormattedDate(data?.overtime_date)}</Text>
                    <Text style={styles.daysText}>{t('no-of-days')}: {data?.overtime_days}</Text>
                </View>

                <View style={styles.statusView}>
                    <Text style={[FontStyle.Regular14_500, { color: color ? color : COLORS.primaryColor, fontWeight: '700', }]}>{status}</Text>

                </View>
            </View>

            <OvertimeDetailsModal
                modalVisible={modalVisible}
                setModalVisible={setModalVisible}
                data={data}
                employeeOvertime={employeeOvertime}
                navigation={navigation}
                color={color}
                status={status}
                getEmployeeOvertimes={getEmployeeOvertimes}
            />
        </TouchableOpacity>

    )
}


export default OvertimeRequestCard

const styles = StyleSheet.create({
    container: {
        padding: 8,
        marginHorizontal: 4,
        ...Theme.Shadow,
        borderWidth: 0,
        marginTop: 2,
    },

    detailsView: {
        flex: 2,
    },

    statusView: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        flex: 1,

    },
    statusView1: {
        ...Theme.Shadow,
        paddingVertical: 8,
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },

    statusText: {
        ...FontStyle.Regular12,
        fontWeight: '500',
    },

    typeText: {
        ...FontStyle.Regular16,
        fontWeight: '700',
        color: COLORS.darkBlack,
        alignSelf: 'flex-start'
    },

    dateText: {
        ...FontStyle.Regular14,
        color: COLORS.grey5,
        fontWeight: '400',
        marginTop: 4,
        alignSelf: 'flex-start'
    },

    daysText: {
        ...FontStyle.Regular12,
        color: COLORS.grey5,
        fontWeight: '400',
        marginTop: 4,
        alignSelf: 'flex-start'
    },


})