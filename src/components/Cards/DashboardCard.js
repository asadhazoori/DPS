import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {SvgXml} from 'react-native-svg';
import {FontStyle} from '../../theme/FontStyle';
import {COLORS} from '../../theme/colors';
import Theme from '../../theme/theme';
import TimeDifference from '../Helpers/TimeDifference';
import ReactNativeVI from '../Helpers/ReactNativeVI';

const DashboardCard = ({
  icon,
  title,
  onPress,
  notify,
  progressBar,
  c1,
  c2,
  c3,
  index,
  manualAttendIcon,
}) => {
  const Item = ({data}) => (
    <View
      style={{
        marginTop: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}>
      <Text
        style={[
          FontStyle.Regular12,
          {color: COLORS.darkBlack, fontWeight: '400'},
        ]}>
        {data?.caption}
      </Text>
      <Text
        style={[FontStyle.Regular14, {color: COLORS.grey4, fontWeight: '400'}]}>
        {data?.value}
      </Text>
    </View>
  );

  return (
    <View style={[styles.conatiner, {alignItems: index && 'flex-end'}]}>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={onPress}
        style={[Theme.DashboardCardShadow, styles.buttonView]}>
        {/* <View style={{
                    width: 12, height: 12,
                    backgroundColor: notify ? COLORS.notify : 'transparent', borderRadius: 6, marginTop: 8, marginLeft: 8
                }}></View> */}

        {!manualAttendIcon && (
          <View style={styles.iconView}>
            <SvgXml xml={icon} />
          </View>
        )}
        {manualAttendIcon && (
          <View style={{alignItems: 'flex-start'}}>
            <ReactNativeVI
              name={'calendar-clock-outline'}
              Lib={'MaterialCommunityIcons'}
              size={28}
              color={COLORS.primaryColor}
            />
          </View>
        )}

        <View style={{marginTop: 4}}>
          <Text
            style={[
              FontStyle.Regular14,
              {textAlign: 'left', color: COLORS.darkBlack, fontWeight: '700'},
            ]}>
            {title}
          </Text>
        </View>

        {/* <View style={{ marginTop: 4, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={[FontStyle.Regular12, { color: COLORS.darkBlack, fontWeight: '400' }]}>{'Available Leaves'}</Text>
                    <Text style={[FontStyle.Regular14, { color: COLORS.grey4, fontWeight: '400' }]}>{'10'}</Text>
                </View>
                <View style={{ marginTop: 4, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={[FontStyle.Regular12, { color: COLORS.darkBlack, fontWeight: '400' }]}>{'Approve Leaves'}</Text>
                    <Text style={[FontStyle.Regular14, { color: COLORS.grey4, fontWeight: '400' }]}>{'14'}</Text>
                </View>
                <View style={{ marginTop: 4, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={[FontStyle.Regular12, { color: COLORS.darkBlack, fontWeight: '400' }]}>{'Pending Leaves'}</Text>
                    <Text style={[FontStyle.Regular14, { color: COLORS.grey4, fontWeight: '400' }]}>{'12'}</Text>
                </View> */}

        {progressBar && <TimeDifference ProgressBarwidth={0.27} height={2} />}
        <Item data={c1} />

        {c2 && <Item data={c2} />}
        {/* <Item data={c3} /> */}
      </TouchableOpacity>
    </View>
  );
};

export default DashboardCard;

const styles = StyleSheet.create({
  conatiner: {
    flex: 1,
  },

  buttonView: {
    padding: 8,
    minHeight: 120,
    backgroundColor: COLORS.white,
    width: '100%',
    borderRadius: 8,
    borderWidth: 0,
  },

  iconView: {},
});
