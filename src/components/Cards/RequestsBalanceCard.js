import {
  Dimensions,
  I18nManager,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {FontStyle} from '../../theme/FontStyle';
import {COLORS} from '../../theme/colors';
import Theme from '../../theme/theme';
import {useTranslation} from 'react-i18next';

const RequestsBalanceCard = ({selectedCategoryId, item}) => {
  const {t} = useTranslation();
  return (
    <View
      style={[
        styles.container,
        {
          width: selectedCategoryId && Dimensions.get('window').width * 0.5,
          backgroundColor: selectedCategoryId
            ? selectedCategoryId === item.id
              ? COLORS.primaryColor
              : COLORS.grey3
            : COLORS.primaryColor,
        },
      ]}>
      <Text style={styles.title}>{item?.name}</Text>
    </View>
  );
};

export default RequestsBalanceCard;

const styles = StyleSheet.create({
  container: {
    ...Theme.Shadow,
    borderWidth: 1,
    borderRadius: 8,
    backgroundColor: ' rgba(72, 24, 53, 1)',
    padding: 12,
    paddingVertical: 16,
  },

  title: {
    ...FontStyle.Regular16_500M,
    fontWeight: '700',
    color: COLORS.white,
    textAlign: 'left',
  },
});
