import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import TimeDifference from '../Helpers/TimeDifference'
import { SmartButton } from '../Inputs'
import { FontStyle } from '../../theme/FontStyle'
import Theme from '../../theme/theme'
import { COLORS } from '../../theme/colors'
import { useTranslation } from 'react-i18next'

const TodayAttendanceReport = ({ firstCheckIn, lastCheckIn, lastCheckOut, punchStatus, punchPress, morePress, date, attendanceRecords, activeAttendance, employeeAttendance, workingHours }) => {
    const { t } = useTranslation();

    const { width } = Dimensions.get('window')
    function calculateBarwidth() {
        const a = 15.7895e-5;
        const b = 0.6700;
        return a * width + b;
    }


    return (
        <View style={{}}>
            {date &&
                <View style={{ alignItems: 'flex-start' }}>
                    <Text style={[styles.timeText, { textAlign: 'left', marginTop: 0, color: COLORS.grey5 }]}>{date}</Text>
                </View>}
            <View style={[styles.cardView, { marginTop: 4 }]} >
                <View style={{ marginHorizontal: 4 }}>
                    <TimeDifference
                        workingHours={workingHours}
                        ProgressBarwidth={calculateBarwidth()}
                        height={6}
                        attendanceRecords={attendanceRecords}
                        activeAttendance={activeAttendance}
                        punchStatus={punchStatus}
                    />
                    {firstCheckIn &&
                        <Text style={[FontStyle.Regular14_500, { marginTop: 2, alignSelf: 'flex-start', fontWeight: '700' }]}>{firstCheckIn}</Text>
                    }
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderWidth: 0, marginTop: 8, marginBottom: 16 }}>

                    <View style={{ flex: 1, }}>
                        <Text style={styles.punchText}>{t('last-in')}</Text>
                        <Text style={styles.timeText}>{lastCheckIn}</Text>
                    </View>
                    <View style={{ flex: 1, }}>
                        <Text style={styles.punchText}>{t('last-out')}</Text>
                        <Text style={styles.timeText}>{lastCheckOut}</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    {
                        !employeeAttendance &&
                        <SmartButton title={punchStatus} textStyle={[FontStyle.Regular14_500, { fontWeight: '700' }]} handlePress={punchPress} />
                    }
                    <TouchableOpacity activeOpacity={0.5} style={{ justifyContent: 'flex-end', paddingHorizontal: 6, }} onPress={morePress}>
                        <Text style={FontStyle.Regular16_500}>{t('more')}</Text>
                    </TouchableOpacity>

                </View>

            </View>
        </View>
    )
}

export default TodayAttendanceReport

const styles = StyleSheet.create({
    cardView: {
        ...Theme.card,
        borderWidth: 0,
        padding: 12,
    },

    punchText: {
        ...FontStyle.Regular16_500M,
        textAlign: 'center',
        color: COLORS.primaryColor
    },

    timeText: {
        ...FontStyle.Regular16_500,
        textAlign: 'center',
        fontWeight: '500',
        color: COLORS.grey4,
        marginTop: 8
    },

})