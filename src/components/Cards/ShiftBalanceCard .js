import { I18nManager, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { FontStyle } from "../../theme/FontStyle";
import { COLORS } from "../../theme/colors";
import Theme from "../../theme/theme";
import { useTranslation } from "react-i18next";

const ShiftBalanceCard = ({ item, index, leavesLength, selectedCategoryId, setSelectedCategoryId }) => {
    const { t } = useTranslation();
    return (
        <View style={[styles.container, {

            backgroundColor: COLORS.primaryColor
        }]}

        >

            <Text style={styles.title}>{t('Shift Change Requests')}</Text>


            <View style={{ marginTop: 4, flexDirection: 'row' }}>
                {/* <View>

                    <Text style={[styles.title, { fontSize: 12 }]}>Personal Loan</Text>
                    <Text style={[styles.title, { fontSize: 16, marginTop: 4 }]}>10</Text>
                </View> */}
                <View style={{ flex: 1, alignItems: 'center' }}>
                    {/* <View style={{ marginLeft: 24 }}> */}

                    <Text style={[styles.title, { fontSize: 12 }]}>{t('rejected')}</Text>
                    <View style={{ flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row', alignItems: 'center', }}>
                        <Text style={[styles.title, { fontSize: 16, marginTop: 4, }]}>18</Text>

                    </View>
                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    {/* <View style={{ marginLeft: 24 }}> */}

                    <Text style={[styles.title, { fontSize: 12 }]}>{t('accepted')}</Text>
                    <View style={{ flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row', alignItems: 'center', }}>
                        <Text style={[styles.title, { fontSize: 16, marginTop: 4, }]}>6</Text>

                    </View>
                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    {/* <View style={{ marginLeft: 24 }}> */}

                    <Text style={[styles.title, { fontSize: 12 }]}>{t('pending')}</Text>
                    <View style={{ flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row', alignItems: 'center', }}>
                        <Text style={[styles.title, { fontSize: 16, marginTop: 4, }]}>12</Text>

                    </View>
                </View>
                {/* <View style={{ flex: 1 }}>

                    <Text style={[styles.title, { fontSize: 12 }]}>Advance salary</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[styles.title, { fontSize: 16, marginTop: 4 }]}>1200 / </Text>
                        <Text style={[styles.title, { fontSize: 12, marginTop: 4 }]}>50000</Text>

                    </View>
                </View> */}

            </View>

        </View >
    )
}

export default ShiftBalanceCard


const styles = StyleSheet.create({
    container: {
        ...Theme.Shadow,
        borderWidth: 1,
        // height: 80,
        backgroundColor: ' rgba(72, 24, 53, 1)',
        // marginBottom: 12,
        // alignItems: 'center',
        borderRadius: 8,
        // justifyContent: 'center',
        padding: 12,
    },

    title: {
        ...FontStyle.Regular14_500,
        fontWeight: '700',
        // lineHeight: 24,
        color: COLORS.white,
    }

})