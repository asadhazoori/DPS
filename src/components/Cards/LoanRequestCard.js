import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'
import Theme from '../../theme/theme'
import { getFormattedDate } from '../../utilities/helpers/CurretDate'
import { useTranslation } from 'react-i18next'
import LoanDetailsModal from '../Modals/LoanDetailsModal'

const LoanRequestCard = ({
    data,
    employeeLoan,
    navigation,
    getEmployeeLoans }) => {

    const [modalVisible, setModalVisible] = useState(false);

    const { t } = useTranslation()

    let state = ''
    let status = ''
    let color = ''


    switch (data?.status) {
        case 'draft':
            color = COLORS.yellow
            status = 'Pending'
            break;

            case 'submit':
                color = COLORS.yellow
                status = 'Pending'
                break;

        case '1st_appr':
            color = COLORS.green
            status = 'Approved by Manager'
            break;

        case 'approved':
            color = COLORS.green
            status = 'Approved'
            break;

        case 'rejected':
            color = COLORS.notify
            status = 'Rejected'
            break;

        default:
            color = 'grey'
            break;
    }


    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => { setModalVisible(true) }}
            style={styles.container}>
            <View style={{ flexDirection: 'row', }}>

                <View style={styles.detailsView}>
                    {employeeLoan &&
                        <Text style={styles.typeText}>{data?.employee_name}</Text>
                    }
                    <Text style={[styles.typeText,
                    {
                        ...(employeeLoan && FontStyle.Regular16_500M),
                        color: employeeLoan ? COLORS.grey5 : COLORS.darkBlack,
                    }]}>{data?.loan_type_name}</Text>
                    <Text style={styles.dateText}>{t('requested-amount')}: {data?.requested_amount}</Text>
                    <Text style={styles.daysText}>{getFormattedDate(data?.date)}</Text>
                </View>

                <View style={styles.statusView}>

                    <Text style={[FontStyle.Regular14_500, { color: color ? color : COLORS.primaryColor, fontWeight: '700', }]}>{status}</Text>

                </View>
            </View>

            <LoanDetailsModal
                modalVisible={modalVisible}
                setModalVisible={setModalVisible}
                data={data}
                employeeLoan={employeeLoan}
                navigation={navigation}
                color={color}
                status={status}
                getEmployeeLoans={getEmployeeLoans}
            />
        </TouchableOpacity>

    )
}


export default LoanRequestCard

const styles = StyleSheet.create({
    container: {
        padding: 8,
        marginHorizontal: 4,
        ...Theme.Shadow,
        borderWidth: 0,
        marginTop: 2,
    },

    detailsView: {
        flex: 2,
    },

    statusView: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        flex: 1,

    },
    statusView1: {
        ...Theme.Shadow,
        paddingVertical: 8,
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },

    statusText: {
        ...FontStyle.Regular12,
        fontWeight: '500',
    },

    typeText: {
        ...FontStyle.Regular16,
        fontWeight: '700',
        color: COLORS.darkBlack,
        alignSelf: 'flex-start'
    },

    dateText: {
        ...FontStyle.Regular14,
        color: COLORS.grey5,
        fontWeight: '400',
        marginTop: 4,
        alignSelf: 'flex-start'
    },

    daysText: {
        ...FontStyle.Regular12,
        color: COLORS.grey5,
        fontWeight: '400',
        marginTop: 8,
        alignSelf: 'flex-start'
    },


})