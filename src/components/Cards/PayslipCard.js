import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { COLORS } from '../../theme/colors'
import { FontStyle } from '../../theme/FontStyle'
import { SvgXml } from 'react-native-svg'
import { Icons } from '../../assets/SvgIcons/Icons'
import Theme from '../../theme/theme'

const PayslipCard = ({ title, caption, navigate }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            style={styles.container}
            onPress={navigate}
        >
            {/* <View style={styles.iconView}>
                <SvgXml xml={Icons.camera} />

            </View> */}
            <View style={styles.textView}>

                <View style={styles.monthDateView}>
                    <Text style={styles.monthText}>{title}</Text>
                    <Text style={styles.dateText}>{caption}</Text>
                </View>


                {/* <View style={styles.amountView} >
                    <Text style={styles.amountText}>50,000 Rs</Text>

                </View> */}

            </View>
            <View style={{ justifyContent: 'center' }}>

                <SvgXml xml={Icons.dots3} />
            </View>
        </TouchableOpacity>
    )
}

export default PayslipCard

const styles = StyleSheet.create({
    container: {
        padding: 12,
        marginBottom: 12,
        flexDirection: 'row',
        marginHorizontal: 4,
        marginTop: 2,
        ...Theme.Shadow,
    },

    iconView: {
        justifyContent: 'center',

    },

    textView: {
        flex: 1,
        flexDirection: 'row',
    },

    monthDateView: {
        alignItems: 'flex-start',
        flex: 1
    },

    monthText: {
        ...FontStyle.Regular16_500M,
        fontWeight: '500',
        color: COLORS.black,
    },

    dateText: {
        ...FontStyle.Regular14_500,
        fontWeight: '500',
        color: '#9E9EA0',
        marginTop: 2,
    },

    amountView: {
        justifyContent: 'flex-end',
        paddingRight: 12,
        marginLeft: 40,
    },

    amountText: {
        ...FontStyle.Regular12,
        color: COLORS.black,
    },


})