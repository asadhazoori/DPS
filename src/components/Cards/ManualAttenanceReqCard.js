import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'
import Theme from '../../theme/theme'
import { getFormattedDate, getFormattedTime } from '../../utilities/helpers/CurretDate'
import { useTranslation } from 'react-i18next'
import LoanDetailsModal from '../Modals/LoanDetailsModal'
import ManualAttendanceDetailsModal from '../Modals/ManualAttendanceDetailsModal'

const ManualAttenanceReqCard = ({
    data,
    employeeLoan,
    navigation,
    getEmployeeLoans }) => {

    const [modalVisible, setModalVisible] = useState(false);

    const { t } = useTranslation()
    let color = ''
    let manualAttendStatus = '';

    if (data?.work_type == 'missed_checkin') {
        caption = `${getFormattedTime(data?.checkin)}`;
    } else if (data?.work_type == 'missed_checkout') {
        caption = `${getFormattedTime(data?.checkout)}`;
    } else {
        caption = `${getFormattedTime(data?.checkin)} -> ${getFormattedTime(
            data?.checkout,
        )}`;
    }
    description = `${data?.reason}`;
    switch (data?.state) {
        case 'Submit':
            color = COLORS.yellow;
            manualAttendStatus = t('pending');
            break;

        case 'Rejected':
            color = COLORS.red;
            manualAttendStatus = t('rejected');
            break;

        case 'Validated':
            color = COLORS.green;
            manualAttendStatus = t('approved');
            break;
        case 'Manager Approved':
            color = COLORS.primaryColor;
            manualAttendStatus = t('manager-approved');
            break;

        default:
            break;
    }


    return (
        <TouchableOpacity
            // disabled={manualAttend}
            activeOpacity={0.7}
            onPress={() => { setModalVisible(true) }}
            style={styles.container}>
            <View style={styles.detailsView}>
                {employeeLoan &&
                    <Text style={styles.typeText}>{data?.employee_id?.[1]}</Text>
                }
                <Text style={[styles.typeText,
                {
                    ...(employeeLoan && FontStyle.Regular16_500M),
                    color: employeeLoan ? COLORS.grey5 : COLORS.darkBlack, marginTop: employeeLoan ? 4 : 0
                }]}>{data?.attendance_work_type_id?.[1]}</Text>
                <Text style={styles.dateText}>{getFormattedDate(data?.date)}</Text>
                <Text style={[styles.dateText, { color: COLORS.primaryColor },]}>{caption}</Text>
                <Text style={styles.daysText}>{description}</Text>
            </View>

            <View style={styles.statusView}>
                <Text style={[styles.statusText, { color: color ? color : COLORS.primaryColor },]}>{manualAttendStatus} </Text>
            </View>

            <ManualAttendanceDetailsModal
                modalVisible={modalVisible}
                setModalVisible={setModalVisible}
                data={data}
                employeeLoan={employeeLoan}
                navigation={navigation}
                color={color}
                status={manualAttendStatus}
                getEmployeeLoans={getEmployeeLoans}
            />
        </TouchableOpacity>

    )
}


export default ManualAttenanceReqCard

const styles = StyleSheet.create({
    container: {
        padding: 8,
        marginHorizontal: 4,
        ...Theme.Shadow,
        borderWidth: 0,
        marginTop: 2,
        flexDirection: 'row',
    },

    detailsView: {
        flex: 2,
        // borderWidth:1,
    },

    statusView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },

    statusText: {
        ...FontStyle.Regular14_500M,
        fontWeight: '700',
    },

    typeText: {
        ...FontStyle.Regular16,
        fontWeight: '700',
        color: COLORS.darkBlack,
        alignSelf: 'flex-start',
    },

    dateText: {
        ...FontStyle.Regular14,
        color: COLORS.grey5,
        fontWeight: '400',
        marginTop: 4,
        alignSelf: 'flex-start',
    },

    daysText: {
        ...FontStyle.Regular12,
        color: COLORS.grey5,
        fontWeight: '400',
        marginTop: 8,
        alignSelf: 'flex-start',
    },
});
