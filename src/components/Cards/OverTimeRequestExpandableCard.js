import { I18nManager, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import Theme from '../../theme/theme'
import { SvgXml } from 'react-native-svg'
import { Icons } from '../../assets/SvgIcons/Icons'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'
import { useTranslation } from 'react-i18next'

const OverTimeRequestExpandableCard = ({
    data, employeeOvertime
}) => {

    const { t } = useTranslation();
    let color = ''
    let status = ''



    switch (data?.status) {
        case 'draft':
            color = COLORS.yellow
            status = 'Pending'
            break;

        case 'validate':
            color = COLORS.notify
            status = 'Approved'
            break;

        case 'cancel':
            color = COLORS.green
            status = 'Rejected'

            break;

        default:
            color = 'grey'
            break;
    }

    const TextView = ({ title, value }) => (
        <View style={{ flex: 1, alignItems: 'flex-start' }}>
            <Text style={[FontStyle.Regular14_500, { color: COLORS.darkBlack, letterSpacing: 0.2 }]}>{title}</Text>
            <Text style={[styles.headerText, { color: COLORS.grey4, marginTop: 4, letterSpacing: 0.2 }]}>{value}</Text>
        </View>
    )


    const [detail, setOPenDetail] = useState(false);

    return (
        <View style={[styles.outerContainer, {
            backgroundColor: detail ? COLORS.white : 'transparent'
        }]}>

            <TouchableOpacity style={styles.container}
                activeOpacity={0.95}
                onPress={() => setOPenDetail(!detail)}>
                <View style={styles.headerView}>
                    <Text style={styles.headerText}>{data?.overtime_date}</Text>
                    <Text style={styles.dateText}>{data?.overtime_date}</Text>

                </View>
                <View style={styles.iconView} >
                    {I18nManager.isRTL ?
                        <SvgXml xml={detail ? Icons.downArrow : Icons.greaterArrow} style={{ transform: [{ scaleX: -1 }] }} /> :
                        <SvgXml xml={detail ? Icons.downArrow : Icons.greaterArrow} />

                    }
                </View>
            </TouchableOpacity>

            {detail &&
                <View style={styles.detailView}>

                    <View style={{ flexDirection: 'row', }}>
                        <TextView title={t('date')} value={data?.overtime_date} />
                        <TextView title={t('start-time')} value={data?.overtime_days} />
                        {/* <TextView title={t('end-time')} value={data?.endTime} /> */}


                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 12, }}>
                        <TextView title={t('hours')} value={data?.overtime_days} />
                        <TextView title={t('status')} value={data?.state} />
                        <TextView title={''} value={''} />


                    </View>
                    {/* <View style={[styles.statusView, { backgroundColor: color, marginTop: 16 }]}>
                        <Text style={styles.statusText}>{data?.state}</Text>
                    </View> */}

                    <View style={styles.statusView1}>
                        <Text style={[styles.statusText, { color: color }]}>{status}</Text>
                    </View>


                </View>
            }
        </View>
    )
}

export default OverTimeRequestExpandableCard

const styles = StyleSheet.create({

    outerContainer: {
        ...Theme.Shadow,
        // marginBottom: 10,
        // borderRadius: 8,
        // borderWidth: 0,
        marginHorizontal: 4,
        marginTop: 2,
        borderWidth: 0,
        backgroundColor: COLORS.white
        // borderWidth:1
    },

    container: {
        ...Theme.Shadow,
        borderWidth: 0,
        padding: 8,
        flexDirection: 'row'
    },

    headerView: {
        flex: 1,
        // borderWidth: 1,
        alignItems: 'flex-start'
    },

    iconView: {
        justifyContent: 'center'
    },

    headerText: {
        ...FontStyle.Regular12,
        fontWeight: '500',
        color: COLORS.darkBlack
    },

    dateText: {
        marginTop: 4,
        ...FontStyle.Regular10,
        fontWeight: '400',
        color: COLORS.grey4

    },

    detailView: {
        marginTop: 8,
        paddingVertical: 8,
        paddingHorizontal: 20,
        // borderWidth: 1,

    },

    statusView: {
        borderRadius: 8,
        width: 88,
        height: 32,
        alignItems: 'center',
        justifyContent: 'center',
        ...Theme.Shadow,
        borderWidth: 0,

        backgroundColor: COLORS.red1,
        // borderWidth: 1

    },

    statusView1: {
        alignSelf: 'flex-end',
        ...Theme.Shadow,
        width: 87,
        height: 27,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 12
    },

    statusText: {
        ...FontStyle.Regular10,
        fontWeight: '500',
        // color: COLORS.white
    },

})