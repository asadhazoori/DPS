import { I18nManager, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'
import Theme from '../../theme/theme'
import SmartButtonTrans from '../Buttons/SmartButtonTrans'
import { getFormattedDate, getFormattedTime } from '../../utilities/helpers/CurretDate'
import ReactNativeVI from '../Helpers/ReactNativeVI'
import { useTranslation } from 'react-i18next'

const AttendanceRequestsCard = ({ navigation, data, getAllEmployeeRequests }) => {

    const { t } = useTranslation();

    let state = ''
    let status = ''
    let color = ''


    switch (data?.state) {
        case 'change_request':
            status = t('pending')
            state = 'Cancel'
            color = COLORS.yellow
            break;

        case 'rejected':
            status = t('rejected')
            state = 'Rejected'
            color = COLORS.red1
            break;

        case 'approved':
            status = t('approved')
            state = 'Approved'
            color = COLORS.green

            break;




        default:
            break;
    }


    return (
        <TouchableOpacity style={styles.container}
            onPress={() => { navigation.navigate('AttendaceChangeRequest', { date: data?.date, punch: data, canEdit: false, }) }}
            activeOpacity={0.8}
        >
            <View style={styles.detailsView}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ flex: 1, }}>

                        {data.employee_name &&
                            <Text style={styles.typeText}>{data.employee_name}</Text>
                        }
                        <Text style={[styles.typeText, { fontWeight: data.employee_name ? '500' : '700', color: data.employee_name ? COLORS.grey5 : COLORS.darkBlack }]}>{getFormattedDate(data?.date)}</Text>
                    </View>
                    <View style={[styles.statusView,]}>

                        <Text style={[FontStyle.Regular14_500, { color: color ? color : COLORS.primaryColor, fontWeight: '700', }]}>{status}</Text>

                    </View>
                </View>

                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    borderColor: 'green', marginTop: 4,
                    flex: 1,
                }}>
                    <Text style={[styles.dateText, {
                        flex: 1,
                        textAlign: 'center',
                        borderColor: 'green', textAlignVertical: 'center'
                    }]}>{`${getFormattedTime(data?.final_checkin)} - ${data?.final_checkout ? getFormattedTime(data?.final_checkout) : 'missed'}`}</Text>
                    <View style={{

                    }}>
                        <ReactNativeVI Lib={'MaterialCommunityIcons'} name={I18nManager.isRTL ? 'chevron-triple-left' : 'chevron-triple-right'} color={COLORS.red} size={20} />
                    </View>
                    <Text style={[styles.dateText, {
                        flex: 1,
                        borderColor: 'green',
                        textAlign: 'center',
                    }]}>{`${data?.requested_checkin ? getFormattedTime(data?.requested_checkin) : getFormattedTime(data?.final_checkin)} - ${data?.requested_checkout ? getFormattedTime(data?.requested_checkout) : getFormattedTime(data?.checkout)}`}</Text>
                </View>

                <Text style={[styles.daysText, {}]}>{data?.remarks}</Text>
            </View>

        </TouchableOpacity >

    )
}


export default AttendanceRequestsCard

const styles = StyleSheet.create({
    container: {
        padding: 8,
        marginHorizontal: 4,
        ...Theme.Shadow,
        borderWidth: 0,
        marginTop: 2,
        flexDirection: 'row',

    },

    detailsView: {
        flex: 4,
    },

    statusView: {
        marginLeft: 8,
        justifyContent: 'center'
    },
    statusView1: {
        ...Theme.Shadow,
        paddingVertical: 8,
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },

    typeText: {
        flex: 1,
        textAlign: 'left',
        ...FontStyle.Regular16_500M,
        fontWeight: '700',
        color: COLORS.darkBlack,
    },

    dateText: {
        ...FontStyle.Regular14_500,
        color: COLORS.grey4,
        fontWeight: '400',
    },

    daysText: {
        textAlign: 'left',
        ...FontStyle.Regular14_500,
        color: COLORS.darkBlack,
        fontWeight: '400',
        marginTop: 8,
    },


})