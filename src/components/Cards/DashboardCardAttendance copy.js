import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { SvgXml } from 'react-native-svg'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'
import Theme from '../../theme/theme'
import TimeDifference from '../Helpers/TimeDifference'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { CircularProgressBase } from 'react-native-circular-progress-indicator';

const DashboardCardAttendance = ({
    icon,
    title,
    onPress,
    notify,
    progressBar,
    c1, c2, c3
}) => {

    const { t } = useTranslation();
    const { monthlyDatewiseAttendance, pendingReqs, rejectedReqs, approvedReqs, } = useSelector((state) => state?.attendance);

    const Item = ({ data }) => (
        <View style={{ marginTop: 4, flexDirection: 'row', justifyContent: 'space-between', flex: 1, }}>
            <Text style={[FontStyle.Regular12, { color: COLORS.darkBlack, fontWeight: '400' }]}>{data?.caption}</Text>
            <Text style={[FontStyle.Regular14, { color: COLORS.grey4, fontWeight: '400' }]}>{data?.value}</Text>
        </View>
    )

    const checkinData = Object.entries(monthlyDatewiseAttendance).filter(
        ([key, value]) => value.checkin !== false
    );

    const holidayData = Object.entries(monthlyDatewiseAttendance).filter(
        ([key, value]) => value.attendance_status === 'holiday'
    );

    const absentData = Object.entries(monthlyDatewiseAttendance).filter(
        ([key, value]) =>
            value.attendance_status === 'absent' || (value.checkin === false && value.checkout === false && value.attendance_status != 'holiday')
    );

    const l = Object.keys(monthlyDatewiseAttendance).length;
    const l1 = checkinData.length / l;
    const l2 = holidayData.length / l;
    const l3 = absentData.length / l;

    const props = {
        activeStrokeWidth: 10,
        inActiveStrokeWidth: 10,
        inActiveStrokeOpacity: 0.2
    };

    return (
        <View style={styles.conatiner}>
            <TouchableOpacity
                activeOpacity={0.85}
                onPress={onPress}
                style={[Theme.Shadow, styles.buttonView]}
            >

                <View>

                    <View style={{ alignItems: 'center' }}>

                        <View style={styles.iconView}>
                            <SvgXml xml={icon} />
                        </View>

                        <View style={{ marginTop: 4, }}>
                            <Text style={[FontStyle.Regular14, { color: COLORS.darkBlack, fontWeight: '700' }]}>{title}</Text>
                        </View>
                    </View>

                    {progressBar &&
                        <TimeDifference ProgressBarwidth={0.670} height={3} /> //0.598 suitable
                    }
                    <Item data={c1} />

                    <Text style={[FontStyle.Regular12, { color: COLORS.darkBlack, fontWeight: '700', marginTop: 2, textAlign: 'left' }]}>{`${t('requests')}`}</Text>
                    <Item data={{ caption: t('approved'), value: approvedReqs }} />
                    <Item data={{ caption: t('pending'), value: pendingReqs }} />
                    <Item data={{ caption: t('rejected'), value: rejectedReqs }} />

                    <View
                        style={{
                            marginVertical: 12,
                            borderBottomColor: COLORS.primaryColor,
                            borderBottomWidth: StyleSheet.hairlineWidth,
                        }}
                    />

                    <View style={{ flexDirection: 'row', marginVertical: 2 }}>

                        <View style={{ flex: 1, alignItems: 'center' }}>


                            <CircularProgressBase
                                {...props}
                                value={l1 ? l1 * 100 : 0}
                                radius={60}
                                activeStrokeColor={COLORS.primaryColor}
                                inActiveStrokeColor={'#a24272'}
                            >
                                <CircularProgressBase
                                    {...props}
                                    value={l3 ? l3 * 100 : 0}
                                    radius={45}
                                    activeStrokeColor={'#b2497d'}
                                    inActiveStrokeColor={'#bd5d8d'}
                                >
                                    <CircularProgressBase
                                        {...props}
                                        value={l2 ? l2 * 100 : 0}
                                        radius={30}
                                        activeStrokeColor={'#cb80a5'}
                                        inActiveStrokeColor={'#cd84a8'}
                                    />
                                </CircularProgressBase>
                            </CircularProgressBase>

                        </View>
                        <View style={{ alignItems: 'center', flex: 1, }}>

                            <View style={{ flex: 1, }}>
                                <Text style={[FontStyle.Regular12, { color: COLORS.darkBlack, fontWeight: '700' }]}>{`${t('last-30-days')}`}</Text>
                                <View style={{ justifyContent: 'space-around', flex: 1 }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                                        <View style={{ height: 15, width: 15, borderRadius: 100, backgroundColor: COLORS.primaryColor }} >
                                        </View>
                                        <Text style={[FontStyle.Regular12, { marginLeft: 8, color: COLORS.darkBlack, fontWeight: '400' }]}>{`${checkinData?.length} ${t('presents')}`}</Text>

                                    </View>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={{ height: 15, width: 15, borderRadius: 100, backgroundColor: '#b2497d' }} >
                                        </View>
                                        <Text style={[FontStyle.Regular12, { marginLeft: 8, color: COLORS.darkBlack, fontWeight: '400' }]}>{`${absentData?.length} ${t('absents')}`}</Text>

                                    </View>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={{ height: 15, width: 15, borderRadius: 100, backgroundColor: '#cb80a5' }} >
                                        </View>
                                        <Text style={[FontStyle.Regular12, { marginLeft: 8, color: COLORS.darkBlack, fontWeight: '400' }]}>{`${holidayData?.length} ${t('holidays')}`}</Text>

                                    </View>
                                </View>

                            </View>
                        </View>
                    </View>


                </View>

            </TouchableOpacity >
        </View >
    )
}

export default DashboardCardAttendance

const styles = StyleSheet.create({
    conatiner: {
        alignItems: 'center',


    },

    buttonView: {
        padding: 12,
        minHeight: 130, //120
        backgroundColor: COLORS.white,
        width: Dimensions.get('window').width * 0.89,
        borderRadius: 8,
        borderWidth: 0,
        justifyContent: 'center'
    },

    iconView: {
    }
})