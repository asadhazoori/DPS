import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { FontStyle } from "../../theme/FontStyle";
import { COLORS } from "../../theme/colors";
import Theme from "../../theme/theme";

const MedicalClaimBalanceCard = ({ item, index, leavesLength, selectedCategoryId, setSelectedCategoryId }) => {
    const backColors = ['#3bca7840', '#f73c3c40', '#f8b14240', '#9e9ea040'];
    const borderColors = ['#3BCA78', '#F73C3C', '#F8B142', '#9E9EA0'];
    return (
        <View style={[styles.container, {

            backgroundColor: COLORS.primaryColor
        }]}
        >

            <Text style={styles.title}>{'Medical Claim Balance'}</Text>


            <View style={{ marginTop: 4, flexDirection: 'row', justifyContent: 'space-between' }}>
                {/* <View>

                    <Text style={[styles.title, { fontSize: 12 }]}>Personal Loan</Text>
                    <Text style={[styles.title, { fontSize: 16, marginTop: 4 }]}>10</Text>
                </View> */}
                <View style={{}}>
                    {/* <View style={{ marginLeft: 24 }}> */}

                    <Text style={[styles.title, { fontSize: 12, alignSelf: 'center' }]}>Claimed</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[styles.title, { fontSize: 16, marginTop: 4 }]}>12,000</Text>
                        {/* <Text style={[styles.title, { fontSize: 12, marginTop: 4 }]}>12,000</Text> */}

                    </View>
                </View>
                <View style={{}}>

                    <Text style={[styles.title, { fontSize: 12, alignSelf: 'center' }]}>Balance</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[styles.title, { fontSize: 16, marginTop: 4 }]}>50,000</Text>
                        {/* <Text style={[styles.title, { fontSize: 12, marginTop: 4 }]}>50000</Text> */}

                    </View>
                </View>

            </View>

        </View>
    )
}

export default MedicalClaimBalanceCard


const styles = StyleSheet.create({
    container: {
        ...Theme.Shadow,
        borderWidth: 1,
        // height: 80,
        backgroundColor: ' rgba(72, 24, 53, 1)',
        // marginBottom: 12,
        // alignItems: 'center',
        borderRadius: 8,
        // justifyContent: 'center',
        padding: 12,
    },

    title: {
        ...FontStyle.Regular14_500,
        fontWeight: '700',
        // lineHeight: 24,
        color: COLORS.white,
    }

})