import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'
import Theme from '../../theme/theme'
import { getFormattedDate } from '../../utilities/helpers/CurretDate'
import { useTranslation } from 'react-i18next'
import LoanDetailsModal from '../Modals/LoanDetailsModal'

const TicketRequestCard = ({
    data,
    navigation,
    getTickets }) => {

    const { t } = useTranslation()

    let status = ''
    let color = ''


    switch (data?.state) {
        case 'Draft':
            color = COLORS.yellow
            status = 'Pending'
            break;

        case 'Approved':
            color = COLORS.primaryColor
            status = 'Approved'
            break;

        case 'Assigned':
            color = COLORS.primaryColor
            status = 'Assigned'
            break;

        case 'Complete':
            color = COLORS.green
            status = 'Complete'
            break;

        case 'Closed':
            color = COLORS.green
            status = 'Closed'
            break;

        case 'Re-opened':
            color = COLORS.red
            status = 'Re-Opened'
            break;

        default:
            color = 'grey'
            break;
    }


    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.navigate("InternalTicketDetail", { data: data })}
            style={styles.container}>
            <View style={{ flexDirection: 'row', }}>

                <View style={styles.detailsView}>

                    <Text style={styles.typeText}>{data?.subject_name}</Text>

                    <Text numberOfLines={1}
                        ellipsizeMode="tail"
                        style={styles.dateText}>{data?.description?.trim()}</Text>

                    <Text style={styles.daysText}>{getFormattedDate(data?.create_date)}</Text>

                </View>

                <View style={styles.statusView}>

                    <Text style={[FontStyle.Regular14_500, { color: color ? color : COLORS.primaryColor, fontWeight: '700', }]}>{status}</Text>

                </View>
            </View>
        </TouchableOpacity>

    )
}


export default TicketRequestCard

const styles = StyleSheet.create({
    container: {
        padding: 8,
        marginHorizontal: 4,
        ...Theme.Shadow,
        borderWidth: 0,
        marginTop: 2,
    },

    detailsView: {
        flex: 2,
    },

    statusView: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        flex: 1,

    },
    statusView1: {
        ...Theme.Shadow,
        paddingVertical: 8,
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },

    statusText: {
        ...FontStyle.Regular12,
        fontWeight: '500',
    },

    typeText: {
        ...FontStyle.Regular16,
        fontWeight: '700',
        color: COLORS.darkBlack,
        alignSelf: 'flex-start'
    },

    dateText: {
        ...FontStyle.Regular14,
        color: COLORS.grey5,
        fontWeight: '400',
        marginTop: 4,
        alignSelf: 'flex-start'
    },

    daysText: {
        ...FontStyle.Regular12,
        color: COLORS.grey5,
        fontWeight: '400',
        marginTop: 8,
        alignSelf: 'flex-start'
    },


})