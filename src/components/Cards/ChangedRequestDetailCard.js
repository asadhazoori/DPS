import { Alert, StyleSheet, Text, View } from 'react-native';
import React, { useState } from 'react';
import { COLORS } from '../../theme/colors';
import { FontStyle } from '../../theme/FontStyle';
import { getFormattedTime } from '../../utilities/helpers/CurretDate';
import Theme from '../../theme/theme';
import { useTranslation } from 'react-i18next';
import { SmartButton, TextInputField } from '../Inputs';
import { logout_user } from '../../redux/users/user.actions';
import Toast from 'react-native-simple-toast';
import { useDispatch, useSelector } from 'react-redux';
import { commonApi } from '../../utilities/api/apiController';
import { getAllEmployeeRequests } from '../../redux/attendance/actions/getAllEmployeeRequests';
import { getAllChangeRequests } from '../../redux/attendance/actions/getAllChangeRequests';

const ChangedRequestDetailCard = ({
  navigation,
  punch,
  status,
  state,
  color,
  employeeAttendance,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [comment, setComment] = useState(null);
  const [loading1, setLoading1] = useState(false);
  const [error, setError] = useState(null);
  const { employeeID } = useSelector(state => state?.employeeProfile);

  const updateState = async state => {
    try {
      const body = {
        params: {
          model: 'raw.attendance.wags',
          method: 'update_request_status',
          args: [
            {
              punch_attendance_id: punch.punch_attendance_id,
              state: state,
              manager_remarks: comment,
            },
          ],
          kwargs: {},
        },
      };

      const response = await commonApi({ body, navigation });

      if (response?.data?.result) {
        setLoading(false);
        setLoading1(false);
        navigation.goBack();
        setTimeout(() => { Toast.show(`${t('request-updated-successfully')} !`); }, 300);
        await dispatch(
          getAllEmployeeRequests({
            navigation,
            employeeID,
          }),
        );
        await dispatch(
          getAllChangeRequests({
            navigation,
            employeeID,
          }),
        );
      } else {
        setLoading(false);
        setLoading1(false);
        if (response?.data?.error) {
          if (response?.data?.error?.message == 'Odoo Session Expired') {
            setTimeout(() => { Toast.show(t('session-expired')); }, 500);
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
            });
            dispatch(logout_user(false));
          } else {
            setTimeout(() => {
              Alert.alert(
                response?.data?.error?.message,
                `Method: update_request_status\n${response?.data?.error?.data?.message}`,
              );
            }, 300);
          }
        } else if (
          response == 'AxiosError: Request failed with status code 404'
        ) {
          setTimeout(() => { Toast.show(t('session-expired')); }, 500);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
          dispatch(logout_user(false));
        }
      }
    } catch (error) {
      setLoading1(false);
      console.log(error);
      setLoading(false);
    }
  };

  const renderStatus = () => (
    <View
      style={{
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginTop: 8,
      }}>
      <Text
        style={[
          FontStyle.Regular16,
          {
            color: color ? color : COLORS.primaryColor,
            fontWeight: '700',
            marginRight: 4,
          },
        ]}>
        {status}
      </Text>
    </View>
  );

  return (
    <View style={{ flex: 1 }}>
      <View
        style={[
          Theme.Shadow,
          { marginTop: 24, padding: 12, marginHorizontal: 4, borderWidth: 0 },
        ]}>
        {employeeAttendance ? (
          <View style={{ paddingHorizontal: 16, paddingVertical: 0 }}>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 4,
                justifyContent: 'space-between',
              }}>
              <Text
                style={[styles.daysText, FontStyle.Regular16_500, {}]}>{`${t(
                  'punch-in',
                )}: `}</Text>
              <Text
                style={[
                  styles.daysText,
                  FontStyle.Regular16_500,
                  { color: COLORS.grey4 },
                ]}>
                {getFormattedTime(punch?.final_checkin)}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 8,
                justifyContent: 'space-between',
              }}>
              <Text style={[styles.daysText, FontStyle.Regular16_500]}>{`${t(
                'punch-out',
              )}: `}</Text>
              <Text
                style={[
                  styles.daysText,
                  FontStyle.Regular16_500,
                  { color: punch?.final_checkout ? COLORS.grey4 : COLORS.red1 },
                ]}>
                {punch?.final_checkout
                  ? getFormattedTime(punch?.final_checkout)
                  : 'missed'}
              </Text>
            </View>
          </View>
        ) : (
          <View>
            <View style={{ flexDirection: 'row' }}>
              {state != 'Approved' && (
                <View style={{ borderColor: 'red', flex: 1 }}>
                  <Text
                    style={[
                      styles.header,
                      { color: COLORS.darkBlack, textAlign: 'left' },
                    ]}>
                    {t('before')}
                  </Text>
                  <Text style={[styles.daysText, { marginTop: 4 }]}>
                    {`${t('punch-in')}: `}
                    <Text style={[styles.daysText, { color: COLORS.grey4 }]}>
                      {getFormattedTime(punch?.final_checkin)}
                    </Text>
                  </Text>
                  <Text style={[styles.daysText, { marginTop: 2 }]}>
                    {`${t('punch-out')}: `}
                    <Text
                      style={[
                        styles.daysText,
                        {
                          color: punch?.final_checkout
                            ? COLORS.grey4
                            : COLORS.red1,
                        },
                      ]}>
                      {punch?.final_checkout
                        ? getFormattedTime(punch?.final_checkout)
                        : 'missed'}
                    </Text>
                  </Text>
                </View>
              )}

              <View style={{ borderColor: 'red', flex: 1 }}>
                <Text
                  style={[
                    styles.header,
                    { color: COLORS.darkBlack, textAlign: 'left' },
                  ]}>
                  {state == 'Approved' ? t('updated-record') : t('request')}
                </Text>
                <Text style={[styles.daysText, { marginTop: 4 }]}>
                  {`${t('punch-in')}: `}
                  <Text
                    style={[
                      styles.daysText,
                      {
                        color: punch?.requested_checkin
                          ? COLORS.primaryColor
                          : COLORS.grey4,
                      },
                    ]}>
                    {punch?.requested_checkin
                      ? getFormattedTime(punch?.requested_checkin)
                      : `${getFormattedTime(punch?.final_checkin)}`}
                  </Text>
                </Text>
                <Text style={[styles.daysText, { marginTop: 2 }]}>
                  {`${t('punch-out')}: `}
                  <Text
                    style={[
                      styles.daysText,
                      {
                        color: punch?.requested_checkout
                          ? COLORS.primaryColor
                          : COLORS.grey4,
                      },
                    ]}>
                    {punch?.requested_checkout
                      ? getFormattedTime(punch?.requested_checkout)
                      : `${getFormattedTime(punch?.final_checkout)}`}
                  </Text>
                </Text>
              </View>
            </View>
            <View style={{ flex: 1, marginTop: 6 }}>
              <Text style={[styles.daysText, {}]}>
                {`${t('reason-for-update')}: `}
                <Text style={[styles.daysText, { color: COLORS.grey4 }]}>
                  {' '}
                  {punch?.remarks}
                </Text>
              </Text>
            </View>
            {punch.manager_remarks && (
              <View style={{ flex: 1, marginTop: 6 }}>
                <Text style={[styles.daysText, {}]}>
                  {`${t('manager-comments')}: `}
                  <Text style={[styles.daysText, { color: COLORS.primaryColor }]}>
                    {' '}
                    {punch?.manager_remarks}
                  </Text>
                </Text>
              </View>
            )}
            {punch.punch_attendance_id ? (
              punch.state == 'change_request' ? (
                <View
                  style={{ paddingHorizontal: 0, marginVertical: 4, gap: 12 }}>
                  <TextInputField
                    label={t('comments')}
                    placeholder={t('enter-comments')}
                    labelStyle={FontStyle.Regular14}
                    value={comment}
                    error={error}
                    onChangeText={text => setComment(text)}
                  />
                  <View
                    style={[
                      {
                        justifyContent: 'space-around',
                        flexDirection: 'row',
                        marginBottom: 4,
                      },
                    ]}>
                    <SmartButton
                      title={t('approve')}
                      width={110}
                      disabled={loading || loading1}
                      loader={loading}
                      handlePress={() => {
                        setLoading(true);
                        updateState('approved');
                      }}
                      color={COLORS.green}
                    />
                    <SmartButton
                      title={t('reject')}
                      width={110}
                      disabled={loading1 || loading}
                      loader={loading1}
                      handlePress={() => {
                        if (comment?.trim() == '' || comment?.trim() == null) {
                          setError(t('comment-is-required'));
                        } else {
                          setLoading1(true);
                          updateState('rejected');
                        }
                      }}
                      color={COLORS.red}
                    />
                  </View>
                </View>
              ) : (
                renderStatus()
              )
            ) : (
              renderStatus()
            )}
          </View>
        )}
      </View>
    </View>
  );
};

export default ChangedRequestDetailCard;

const styles = StyleSheet.create({
  header: {
    ...FontStyle.Regular14_500,
    color: COLORS.darkBlack,
    fontWeight: '700',
    textAlign: 'left',
  },
  daysText: {
    ...FontStyle.Regular14_500,
    color: COLORS.darkBlack,
    fontWeight: '400',
    textAlign: 'left',
  },
});
