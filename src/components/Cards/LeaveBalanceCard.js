import { Dimensions, I18nManager, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { FontStyle } from "../../theme/FontStyle";
import { COLORS } from "../../theme/colors";
import Theme from "../../theme/theme";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";

const LeaveBalanceCard = ({ item, selectedCategoryId, setSelectedCategoryId }) => {

    const { t } = useTranslation();
    const isManager = item?.isManager ? true : false
    const isTotal = item?.leave_type_id == 11111 ? true : false
    const isUserManager = useSelector(state => state?.employeeProfile?.isManager);

    return (
        <TouchableOpacity style={[styles.container, {
            backgroundColor: selectedCategoryId === item.leave_type_id ? COLORS.primaryColor : COLORS.grey3
        }]}
            activeOpacity={0.6}
            onPress={() => setSelectedCategoryId(item)}
        >

            <Text style={[styles.title]}>{isManager ? t('team-requests') : isTotal ? `${isUserManager ? t('personal-requests') : t('all-requests')}${(item?.total) > 0 ? ` (${item?.total})` : ''}` : item?.leave_type_name}</Text>
            {isTotal ?
                <View style={{ marginTop: 4, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ flex: 1, alignItems: 'center' }}>

                        <Text style={[styles.title1]}>{t('pending')}</Text>
                        <View style={{ alignItems: 'center' }}>

                            <Text style={[styles.heading, { marginTop: 4, alignSelf: 'auto' }]}>{item?.pending}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>

                        <Text style={[styles.title1]}>{t('approved')}</Text>
                        <View style={{ alignItems: 'center' }}>

                            <Text style={[styles.heading, { marginTop: 4, alignSelf: 'auto' }]}>{item?.approved + item?.approved_by_manager}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>

                        <Text style={[styles.title1]}>{t('rejected')}</Text>
                        <View style={{ alignItems: 'center' }}>

                            <Text style={[styles.heading, { marginTop: 4, alignSelf: 'auto' }]}>{item?.rejected}</Text>
                        </View>
                    </View>

                </View>
                :
                <View style={{ marginTop: 4, flexDirection: 'row' }}>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <Text style={[styles.title1, { alignSelf: 'auto' }]}>{t('requests')}</Text>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={[styles.heading, { marginTop: 4, alignSelf: 'auto' }]}>{item?.total}</Text>
                        </View>
                    </View>
                    <View style={{ marginLeft: 2, flex: 2, alignItems: 'center' }}>

                        <Text style={[styles.title1, { alignSelf: 'auto' }]}>{isManager ? t('pending') : t('balance-in-days')}</Text>
                        {isManager ?
                            <View style={{ alignItems: 'center' }}>
                                <Text style={[styles.heading, { marginTop: 4, alignSelf: 'auto' }]}>{item?.pending}</Text>
                            </View>
                            :
                            <View style={{ flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row', marginTop: 4, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={[styles.heading, { alignSelf: 'auto' }]}>{Math.floor(item?.pending_leaves)} / </Text>
                                <Text style={[styles.title1, { alignSelf: 'auto' }]}>{item?.total_leaves}</Text>

                            </View>
                        }
                    </View>

                </View>
            }

        </TouchableOpacity >
    )
}

export default LeaveBalanceCard


const styles = StyleSheet.create({
    container: {
        ...Theme.Shadow,
        borderWidth: 1,
        borderRadius: 8,
        backgroundColor: ' rgba(72, 24, 53, 1)',
        padding: 12,
        width: Dimensions.get('window').width * 0.80,
    },

    heading: {
        ...FontStyle.Regular18,
        color: COLORS.white,
        alignSelf: 'flex-start'
    },
    title: {
        ...FontStyle.Regular16_500,
        fontWeight: '700',
        color: COLORS.white,
        alignSelf: 'flex-start',
        textAlign: 'left'
    },

    title1: {
        ...FontStyle.Regular14,
        color: COLORS.white,
    }

})