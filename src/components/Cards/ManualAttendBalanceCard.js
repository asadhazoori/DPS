import { Dimensions, StyleSheet, Text, View } from 'react-native';
import { FontStyle } from '../../theme/FontStyle';
import { COLORS } from '../../theme/colors';
import Theme from '../../theme/theme';
import { useTranslation } from 'react-i18next';

const ManualAttendBalanceCard = ({ item, selectedCategoryId }) => {
  const { t } = useTranslation();

  return (
    <View style={[styles.container, {
      width: selectedCategoryId && Dimensions.get('window').width * 0.80,
      backgroundColor: selectedCategoryId === item?.id ? COLORS.primaryColor : COLORS.grey3
    }]}
    >
      <Text style={styles.title}>{selectedCategoryId ? item.id == 1 ? t('personal-requests') : t('team-requests') : t('requests')}</Text>

      <View style={{ marginTop: 4, flexDirection: 'row' }}>
        <View style={{ flex: 1, alignItems: 'center' }}>
          <Text style={[styles.title, { fontSize: 14 }]}>{t('pending')}</Text>
          <Text style={[styles.heading, { marginTop: 4 }]}>{item?.submit}</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center' }}>
          <Text style={[styles.title, { fontSize: 14 }]}>{t('approved')}</Text>
          <Text style={[styles.heading, { marginTop: 4 }]}>
            {item?.manager_approved + item?.validate}
          </Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center' }}>
          <Text style={[styles.title, { fontSize: 14 }]}>{t('rejected')}</Text>
          <Text style={[styles.heading, { marginTop: 4 }]}>{item?.rejected}</Text>
        </View>
      </View>
    </View>
  );
};

export default ManualAttendBalanceCard;

const styles = StyleSheet.create({
  container: {
    ...Theme.Shadow,
    borderWidth: 1,
    backgroundColor: COLORS.primaryColor,
    borderRadius: 8,
    padding: 12,
  },

  title: {
    ...FontStyle.Regular16_500,
    fontWeight: '700',
    color: COLORS.white,
    textAlign: 'left',
  },
  heading: {
    ...FontStyle.Regular18,
    color: COLORS.white,
  },
});
