import {
  Dimensions,
  I18nManager,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {FontStyle} from '../../theme/FontStyle';
import {COLORS} from '../../theme/colors';
import Theme from '../../theme/theme';
import {useTranslation} from 'react-i18next';

const AttendanceRequestsBalanceCard = ({
  pending,
  approved,
  rejected,
  selectedCategoryId,
  item,
}) => {
  const {t} = useTranslation();
  return (
    <View
      style={[
        styles.container,
        {
          width: selectedCategoryId && Dimensions.get('window').width * 0.8,
          backgroundColor: selectedCategoryId
            ? selectedCategoryId === item.id
              ? COLORS.primaryColor
              : COLORS.grey3
            : COLORS.primaryColor,
        },
      ]}>
      <Text style={styles.title}>
        {selectedCategoryId
          ? item.id == 1
            ? t('personal-requests')
            : t('team-requests')
          : t('Status')}
      </Text>

      <View style={{marginTop: 4, flexDirection: 'row'}}>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Text style={[styles.title, {fontSize: 14}]}>{t('pending')}</Text>
          <Text style={[styles.title, {marginTop: 4}]}>{pending}</Text>
        </View>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Text style={[styles.title, {fontSize: 14}]}>{t('approved')}</Text>
          <Text style={[styles.title, {marginTop: 4}]}>{approved}</Text>
        </View>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Text style={[styles.title, {fontSize: 14}]}>{t('rejected')}</Text>
          <Text style={[styles.title, {marginTop: 4}]}>{rejected}</Text>
        </View>
        {/* <View style={{ flex: 1 }}>

                    <Text style={[styles.title, { fontSize: 12 }]}>Advance salary</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[styles.title, { fontSize: 16, marginTop: 4 }]}>1200 / </Text>
                        <Text style={[styles.title, { fontSize: 12, marginTop: 4 }]}>50000</Text>

                    </View>
                </View> */}
      </View>
    </View>
  );
};

export default AttendanceRequestsBalanceCard;

const styles = StyleSheet.create({
  container: {
    ...Theme.Shadow,
    borderWidth: 1,
    borderRadius: 8,
    backgroundColor: ' rgba(72, 24, 53, 1)',
    padding: 12,
  },

  title: {
    textAlign: 'left',
    ...FontStyle.Regular16_500M,
    fontWeight: '700',
    color: COLORS.white,
  },
});
