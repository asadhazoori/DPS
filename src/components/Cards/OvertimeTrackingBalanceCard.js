import { Dimensions, I18nManager, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { FontStyle } from "../../theme/FontStyle";
import { COLORS } from "../../theme/colors";
import Theme from "../../theme/theme";
import { useTranslation } from "react-i18next";

const OvertimeTrackingBalanceCard = ({ item, selectedCategoryId }) => {

    const { t } = useTranslation();

    const renderBalance = ({ item1 }) => (
        <View style={{ marginTop: 4, flexDirection: 'row' }}>
            <View style={{ flex: 1, alignItems: 'center' }}>
                <Text style={[styles.title, { fontSize: 13 }]}>{t('worked-hours')}</Text>
                <View style={{ flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row', alignItems: 'center', }}>
                    <Text style={[styles.heading, { marginTop: 4, }]}>{item1?.approved_overtime ? item1?.approved_overtime?.toFixed(1) : '0.0'}</Text>

                </View>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
                <Text style={[styles.title, { fontSize: 13 }]}>{t('requested-hours')}</Text>
                <View style={{ flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row', alignItems: 'center', }}>
                    <Text style={[styles.heading, { marginTop: 4, }]}>{item1?.pending_overtime ? item1?.pending_overtime?.toFixed(1) : `0.0`}</Text>

                </View>
            </View>

        </View>
    )

    return (
        <View style={[styles.container, {
            width: selectedCategoryId && Dimensions.get('window').width * 0.80,
            backgroundColor: selectedCategoryId === item?.id ? COLORS.primaryColor : COLORS.grey3
        }]}
        >

            <Text style={styles.title}>{selectedCategoryId ? item?.id == 1 ? t('overtime-balance') : t('team-requests') : t('overtime-balance')}</Text>

            {
                selectedCategoryId ?

                    item?.id == 1 ?
                        renderBalance({ item1: item })
                        :
                        <View style={{ marginTop: 4, flexDirection: 'row' }}>
                            <View style={{ flex: 1, alignItems: 'center' }}>

                                <Text style={[styles.title, { fontSize: 14 }]}>{t('pending')}</Text>
                                <Text style={[styles.heading, { marginTop: 4, }]}>{item?.pending}</Text>

                            </View>
                            <View style={{ flex: 1, alignItems: 'center' }}>

                                <Text style={[styles.title, { fontSize: 14 }]}>{t('approved')}</Text>
                                <Text style={[styles.heading, { marginTop: 4, }]}>{item?.approved}</Text>

                            </View>
                            <View style={{ flex: 1, alignItems: 'center' }}>

                                <Text style={[styles.title, { fontSize: 14 }]}>{t('rejected')}</Text>
                                <Text style={[styles.heading, { marginTop: 4, }]}>{item?.rejected}</Text>

                            </View>
                        </View>
                    :
                    renderBalance({ item1: item })
            }
        </View >
    )
}

export default OvertimeTrackingBalanceCard


const styles = StyleSheet.create({
    container: {
        ...Theme.Shadow,
        borderWidth: 1,
        backgroundColor: ' rgba(72, 24, 53, 1)',
        borderRadius: 8,
        padding: 12,
    },

    title: {
        ...FontStyle.Regular16_500,
        fontWeight: '700',
        color: COLORS.white,
        textAlign: 'left'
    },
    heading: {
        ...FontStyle.Regular18,
        color: COLORS.white,
    },

})