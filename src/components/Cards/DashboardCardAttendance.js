import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {FontStyle} from '../../theme/FontStyle';
import {COLORS} from '../../theme/colors';
import Theme from '../../theme/theme';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {CircularProgressBase} from 'react-native-circular-progress-indicator';
import TimeDifferenceDashboard from '../Helpers/TimeDifferenceDashboard';

const DashboardCardAttendance = ({
  icon,
  title,
  onPress,
  notify,
  progressBar,
  c1,
  c2,
  c3,
  punchStatus,
  onPressMiddle,
}) => {
  const {width} = Dimensions.get('window');
  const {t} = useTranslation();
  const {
    monthlyDatewiseAttendance,
    pendingReqs,
    rejectedReqs,
    approvedReqs,
    activeAttendance,
    attendanceRecords,
    yesterdayAttendance,
    yesterdayAttendanceRecords,
  } = useSelector(state => state?.attendance);

  const Item = ({data}) => (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        alignItems: 'center',
      }}>
      <Text style={[styles.caption]}>{data?.caption}</Text>
      <Text style={styles.caption}>{data?.value}</Text>
    </View>
  );

  const checkinData = Object.entries(monthlyDatewiseAttendance).filter(
    ([key, value]) => value.checkin !== false,
  );

  const holidayData = Object.entries(monthlyDatewiseAttendance).filter(
    ([key, value]) => value.attendance_status === 'holiday',
  );

  const absentData = Object.entries(monthlyDatewiseAttendance).filter(
    ([key, value]) =>
      value.attendance_status === 'absent' ||
      (value.checkin === false &&
        value.checkout === false &&
        value.attendance_status != 'holiday'),
  );

  const l = Object.keys(monthlyDatewiseAttendance).length;
  const l1 = checkinData.length / l;
  const l2 = holidayData.length / l;
  const l3 = absentData.length / l;

  const props = {
    activeStrokeWidth: 10,
    inActiveStrokeWidth: 10,
    inActiveStrokeOpacity: 0.2,
  };

  function calculateBarwidth() {
    const a = 10.7895e-5;
    const b = 0.781;
    return a * width + b;
  }

  return (
    <View style={styles.conatiner}>
      <TouchableOpacity
        activeOpacity={0.85}
        onPress={onPress}
        style={[Theme.DashboardCardShadow, styles.buttonView]}>
        <View style={{}}>
          {/* <Text style={styles.header}>{activeAttendance?.overlapping ? t("yesterday-attendance") : t("today-attendance")}</Text> */}
          <Text style={styles.header}>{t('today-attendance')}</Text>

          {
            progressBar && (
              // <TimeDifferenceDashboard ProgressBarwidth={calculateBarwidth()} height={3} activeAttendance={activeAttendance?.overlapping ? yesterdayAttendance : activeAttendance} attendanceRecords={activeAttendance?.overlapping ? yesterdayAttendanceRecords : attendanceRecords} /> //0.598 suitable
              // <TimeDifferenceDashboard ProgressBarwidth={calculateBarwidth()} height={3} activeAttendance={activeAttendance?.checkin ? activeAttendance : yesterdayAttendance} attendanceRecords={activeAttendance?.checkin ? attendanceRecords : yesterdayAttendanceRecords} /> //0.598 suitable
              <TimeDifferenceDashboard
                ProgressBarwidth={calculateBarwidth()}
                height={3}
                activeAttendance={activeAttendance}
                attendanceRecords={attendanceRecords}
              />
            ) //0.598 suitable
          }
          <View style={{alignItems: 'center', marginTop: 6}}>
            <Text
              style={[
                FontStyle.Regular14_500,
                {
                  color:
                    punchStatus == 'punched-in'
                      ? COLORS.green
                      : punchStatus == 'punched-out'
                      ? COLORS.orange
                      : COLORS.red1,
                  fontWeight: '700',
                },
              ]}>
              {t(punchStatus)}
            </Text>
          </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity
        activeOpacity={0.85}
        onPress={onPressMiddle}
        style={[Theme.DashboardCardShadow, styles.buttonView]}>
        <View style={{}}>
          <Text style={styles.header}>{`${t(
            'attendance-change-requests',
          )}`}</Text>
        </View>

        <View style={{marginTop: 8, gap: 4}}>
          <Item data={{caption: t('approved'), value: approvedReqs}} />
          <Item data={{caption: t('pending'), value: pendingReqs}} />
          <Item data={{caption: t('rejected'), value: rejectedReqs}} />
        </View>
      </TouchableOpacity>

      <TouchableOpacity
        activeOpacity={0.85}
        onPress={onPress}
        style={[Theme.DashboardCardShadow, styles.buttonView]}>
        <View>
          <Text style={styles.header}>{`${t('last-30-days')}`}</Text>
        </View>

        <View style={{flexDirection: 'row', marginTop: 8}}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <CircularProgressBase
              {...props}
              value={l1 ? l1 * 100 : 0}
              radius={60}
              activeStrokeColor={COLORS.primaryColor}
              inActiveStrokeColor={'#a24272'}>
              <CircularProgressBase
                {...props}
                value={l3 ? l3 * 100 : 0}
                radius={45}
                activeStrokeColor={'#b2497d'}
                inActiveStrokeColor={'#bd5d8d'}>
                <CircularProgressBase
                  {...props}
                  value={l2 ? l2 * 100 : 0}
                  radius={30}
                  activeStrokeColor={'#cb80a5'}
                  inActiveStrokeColor={'#cd84a8'}
                />
              </CircularProgressBase>
            </CircularProgressBase>
          </View>
          <View style={{alignItems: 'center', flex: 1}}>
            <View style={{justifyContent: 'space-around', flex: 1}}>
              <View style={styles.dotContainer}>
                <View
                  style={[
                    styles.dotView,
                    {backgroundColor: COLORS.primaryColor},
                  ]}></View>
                <Text style={styles.dotText}>
                  {`${checkinData?.length} ${t('presents')}`}
                </Text>
              </View>
              <View style={styles.dotContainer}>
                <View
                  style={[styles.dotView, {backgroundColor: '#b2497d'}]}></View>
                <Text style={styles.dotText}>
                  {`${absentData?.length} ${t('absents')}`}
                </Text>
              </View>
              <View style={styles.dotContainer}>
                <View
                  style={[styles.dotView, {backgroundColor: '#cb80a5'}]}></View>
                <Text style={styles.dotText}>
                  {`${holidayData?.length} ${t('holidays')}`}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default DashboardCardAttendance;

const styles = StyleSheet.create({
  conatiner: {
    gap: 16,
  },

  buttonView: {
    padding: 12,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    borderWidth: 0,
    justifyContent: 'center',
  },

  iconView: {},

  header: {
    ...FontStyle.Regular16_500,
    color: COLORS.darkBlack,
    fontWeight: '700',
    textAlign: 'left',
  },
  caption: {
    ...FontStyle.Regular14_500,
    color: COLORS.darkBlack,
    fontWeight: '400',
  },
  dotContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dotView: {
    height: 15,
    width: 15,
    borderRadius: 100,
  },
  dotText: {
    ...FontStyle.Regular14_500,
    color: COLORS.darkBlack,
    fontWeight: '400',
    marginLeft: 8,
  },
});
