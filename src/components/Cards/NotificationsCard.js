import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { FontStyle } from '../../theme/FontStyle'
import { COLORS } from '../../theme/colors'
import Theme from '../../theme/theme'
import SmartButtonTrans from '../Buttons/SmartButtonTrans'

const NotifiationCard = ({ data }) => {

    let state = ''
    let status = ''
    let color = ''


    switch (data?.state) {
        case 'confirm':
            status = 'Pending'
            state = 'Cancel'
            color = COLORS.secondaryColor
            break;

        case 'validate':
            status = 'Accepted'
            state = 'Approved'
            color = COLORS.white

            break;




        default:
            break;
    }


    return (
        <View style={[styles.container, {
            backgroundColor: color
        }]}>
            <View style={styles.detailsView}>

                <Text style={styles.typeText}>{data?.leaves_type}</Text>
                <Text style={styles.dateText}>{data?.caption}</Text>
                <Text style={styles.daysText}>{data?.remarks}</Text>
                {/* <Text style={styles.daysText}>{data?.date_from_ecube} - {data?.date_to_ecube}</Text> */}
            </View>


        </View>

    )
}


export default NotifiationCard

const styles = StyleSheet.create({
    container: {
        padding: 8,
        // marginBottom: 10,
        marginHorizontal: 4,
        ...Theme.Shadow,
        borderWidth: 0,
        marginTop: 2,
        flexDirection: 'row',

    },

    detailsView: {
        flex: 1,
        // borderWidth: 1
    },

    statusView: {
        // borderWidth: 1,
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    statusView1: {
        ...Theme.Shadow,
        // width: 87,
        // height: 27,
        paddingVertical: 8,
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },

    statusText: {
        ...FontStyle.Regular10,
        fontWeight: '500',
        // color: COLORS.white
    },

    typeText: {
        textAlign: 'left',
        ...FontStyle.Regular14,
        fontWeight: '700',
        color: COLORS.darkBlack,
    },

    dateText: {
        textAlign: 'left',
        ...FontStyle.Regular12,
        color: COLORS.grey4,
        fontWeight: '400',
        marginTop: 4,
    },

    daysText: {
        textAlign: 'left',
        ...FontStyle.Regular12,
        color: COLORS.darkBlack,//COLORS.blue,
        fontWeight: '400',
        marginTop: 12,
    },


})