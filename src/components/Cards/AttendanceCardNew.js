import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import { FontStyle } from '../../theme/FontStyle';
import { COLORS } from '../../theme/colors';
import { useTranslation } from 'react-i18next';
import { SvgXml } from 'react-native-svg';
import { Icons } from '../../assets/SvgIcons/Icons';
import { fonts } from '../../theme/fonts';

const AttendanceCardNew = ({
  date,
  color,
  punchIn,
  punchOut,
  time,
  onPress,
  disabled,
  attendance_status,
  item
}) => {
  const { t } = useTranslation();
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      activeOpacity={0.5}
      style={{}}>
      {date && (
        <View style={{ alignItems: 'flex-start' }}>
          <Text
            style={[
              styles.timeText,
              { textAlign: 'left', marginTop: 0, color: COLORS.grey5 },
            ]}>
            {date}
          </Text>
        </View>
      )}
      <View
        style={[
          styles.cardView,
          {
            borderColor:
              attendance_status == 'holiday' || attendance_status == 'national_holiday' ? '#189f2d'
                : attendance_status == 'leave' ? '#d08208'
                  : attendance_status == 'absent' || (!punchIn && !punchOut) ? '#ff1206'
                    : COLORS.primaryColor,
            backgroundColor:
              attendance_status == 'holiday' || attendance_status == 'national_holiday' ? 'rgba(209, 248, 215, 0.8)'
                : attendance_status == 'leave' ? COLORS.lightYellow
                  : attendance_status == 'absent' || (!punchIn && !punchOut) ? '#ffe0de'
                    : COLORS.white,
          },
        ]}>
        {attendance_status == 'holiday' ? (
          <View style={styles.exceptionalCard}>
            <Text style={[styles.punchText, { marginVertical: 8 }]}>{t('holiday')}</Text>
          </View>
        ) :
          attendance_status == 'leave' ? (
            <View style={styles.exceptionalCard}>
              <Text style={[styles.punchText, { marginVertical: 8 }]}>{item?.leave_type?.[1] || item.attendance_status_str}</Text>
            </View>
          ) : attendance_status == 'national_holiday' ? (
            <View style={styles.exceptionalCard}>
              <Text style={[styles.punchText, { marginVertical: 8 }]}>{item?.national_holiday?.[1] || item?.attendance_status_str}</Text>
            </View>
          ) : attendance_status == 'absent' || (!punchIn && !punchOut) ? (
            <View style={styles.exceptionalCard}>
              <Text style={[styles.punchText, { marginVertical: 8 }]}>{t('absent')}</Text>
            </View>
          ) : (
            <View style={styles.itemRow}>
              <View style={styles.punchView}>
                <Text style={styles.punchText}>{t('punch-in')}</Text>
                <Text style={styles.timeText}>{punchIn ? punchIn : '----'}</Text>
              </View>

              <View style={styles.punchOutView}>
                <Text style={styles.punchText}>{t('punch-out')}</Text>
                <Text style={styles.timeText}>{punchOut ? punchOut : '----'}</Text>
              </View>

              {time && (
                <View style={styles.timeView}>
                  <SvgXml xml={Icons.clock} />
                  <Text style={[styles.punchText, styles.timeAdditionalStyle]}>{time ? `${time}` : '----'}
                  </Text>
                </View>
              )}
            </View>
          )}
      </View>
    </TouchableOpacity>
  );
};

export default AttendanceCardNew;

const styles = StyleSheet.create({
  cardView: {
    marginTop: 4,
    borderRadius: 8,
    borderWidth: 0.4,
    padding: 10,
  },

  itemRow: {
    flexDirection: 'row',
  },

  punchView: {
    flex: 1,
    borderColor: COLORS.grey4,
    flex: 1,
    justifyContent: 'center',
  },

  punchText: {
    ...FontStyle.Regular16_500M,
    textAlign: 'center',
    fontWeight: '500',
    color: COLORS.black,
  },

  timeText: {
    ...FontStyle.Regular16_500,
    textAlign: 'center',
    fontWeight: '500',
    color: COLORS.grey4,
    marginTop: 10,
  },

  exceptionalCard: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: 12,
  },
  punchOutView: {
    flex: 1, justifyContent: 'center'
  },
  timeView: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  timeAdditionalStyle: {
    marginTop: 4, fontFamily: fonts.regular
  }
});
