import { Dimensions, I18nManager, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { FontStyle } from "../../theme/FontStyle";
import { COLORS } from "../../theme/colors";
import Theme from "../../theme/theme";
import { useTranslation } from "react-i18next";

const TimeSheetBalanceCard = ({ item, index, leavesLength, selectedCategoryId, setSelectedCategoryId }) => {
    const backColors = ['#3bca7840', '#f73c3c40', '#f8b14240', '#9e9ea040'];
    const borderColors = ['#3BCA78', '#F73C3C', '#F8B142', '#9E9EA0'];

    const { t } = useTranslation();
    return (
        <TouchableOpacity style={[styles.container, {

            backgroundColor: selectedCategoryId === item.id ? COLORS.primaryColor : COLORS.grey3
        }]}

            activeOpacity={0.6}
            onPress={() => setSelectedCategoryId(item)}
        >

            <Text style={[styles.title, { fontSize: 16, }]}>{item?.leaves_type}</Text>


            <View style={{ marginTop: 8, flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>

                    <Text style={[styles.title, { fontSize: 14, alignSelf: 'center' }]}>{t('Total Days')}</Text>
                    <View style={{ alignItems: 'center' }}>

                        <Text style={[styles.title, { fontSize: 18, marginTop: 4, alignSelf: 'auto' }]}>10</Text>
                    </View>
                </View>
                <View style={{ flex: 1 }}>

                    <Text style={[styles.title, { fontSize: 14, alignSelf: 'center' }]}>{t('Total Hours')}</Text>
                    <View style={{ alignItems: 'center' }}>

                        <Text style={[styles.title, { fontSize: 18, marginTop: 4, alignSelf: 'auto' }]}>06</Text>
                    </View>
                </View>
                <View style={{ flex: 1 }}>

                    <Text style={[styles.title, { fontSize: 14, alignSelf: 'center' }]}>{t('Working Hours')}</Text>
                    <View style={{ alignItems: 'center' }}>

                        <Text style={[styles.title, { fontSize: 18, marginTop: 4, alignSelf: 'auto' }]}>08</Text>
                    </View>
                </View>

            </View>

        </TouchableOpacity >
    )
}

export default TimeSheetBalanceCard


const styles = StyleSheet.create({
    container: {
        ...Theme.Shadow,
        borderWidth: 1,
        // height: 80,
        backgroundColor: ' rgba(72, 24, 53, 1)',
        // marginBottom: 12,
        // alignItems: 'center',
        borderRadius: 8,
        // justifyContent: 'center',
        padding: 12,

        width: Dimensions.get('window').width * 0.86,
    },

    title: {
        ...FontStyle.Regular14_500,
        fontWeight: '700',
        // lineHeight: 24,
        color: COLORS.white,
        // borderWidth: 1,
        // fontSize: '16',
        alignSelf: 'flex-start'
    }

})