import { Dimensions, StyleSheet, Text, View } from "react-native";
import { FontStyle } from "../../theme/FontStyle";
import { COLORS } from "../../theme/colors";
import Theme from "../../theme/theme";
import { useTranslation } from "react-i18next";

const TicketBalanceCard = ({ item, selectedCategoryId }) => {

    const { t } = useTranslation();

    return (
        <View style={[styles.container, {
            width: selectedCategoryId && Dimensions.get('window').width * 0.80,
            backgroundColor: COLORS.primaryColor 
        }]}
        >
            <Text style={styles.title}>{t('requests') }</Text>


            {/* <View style={{ marginTop: 4, flexDirection: 'row' }}>
                <View>

                    <Text style={[styles.title, { fontSize: 12 }]}>Personal Loan</Text>
                    <Text style={[styles.title, { fontSize: 16, marginTop: 4 }]}>10</Text>
                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>

                    <Text style={[styles.title, { fontSize: 12 }]}>{t('personal-loan')}</Text>
                    <View style={{ flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row', alignItems: 'center', }}>
                        <Text style={[styles.title, { fontSize: 16, marginTop: 4, }]}>1200 / </Text>
                        <Text style={[styles.title, { fontSize: 12, marginTop: 4, }]}>50000</Text>

                    </View>
                </View>
                <View style={{ flex: 1 }}>

                    <Text style={[styles.title, { fontSize: 12 }]}>Advance salary</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[styles.title, { fontSize: 16, marginTop: 4 }]}>1200 / </Text>
                        <Text style={[styles.title, { fontSize: 12, marginTop: 4 }]}>50000</Text>

                    </View>
                </View>

            </View> */}

            <View style={{ marginTop: 4, flexDirection: 'row' }}>
                <View style={{ flex: 1, alignItems: 'center' }}>

                    <Text style={[styles.title, { fontSize: 14 }]}>{t('pending')}</Text>
                    <Text style={[styles.heading, { marginTop: 4, }]}>{item?.pending}</Text>

                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>

                    <Text style={[styles.title, { fontSize: 14 }]}>{t('approved')}</Text>
                    <Text style={[styles.heading, { marginTop: 4, }]}>{item?.assigned + item?.approved}</Text>

                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>

                    <Text style={[styles.title, { fontSize: 14 }]}>{t("closed")}</Text>
                    <Text style={[styles.heading, { marginTop: 4, }]}>{item?.closed}</Text>

                </View>
            </View>

        </View >
    )
}

export default TicketBalanceCard


const styles = StyleSheet.create({
    container: {
        ...Theme.Shadow,
        borderWidth: 1,
        backgroundColor: ' rgba(72, 24, 53, 1)',
        borderRadius: 8,
        padding: 12,
    },

    title: {
        ...FontStyle.Regular16_500,
        fontWeight: '700',
        color: COLORS.white,
        textAlign: 'left'
    },
    heading: {
        ...FontStyle.Regular18,
        color: COLORS.white,
    },

})