import {
  I18nManager,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import Theme from '../../theme/theme';
import {SvgXml} from 'react-native-svg';
import {Icons} from '../../assets/SvgIcons/Icons';
import {FontStyle} from '../../theme/FontStyle';
import {COLORS} from '../../theme/colors';
import {useTranslation} from 'react-i18next';
import {getFormattedDate} from '../../utilities/helpers/CurretDate';

const LoanCard = ({data, employeeLoan}) => {
  const [detail, setOPenDetail] = useState(false);
  const {t} = useTranslation();
  let color = '';
  let status = '';

  switch (data?.status) {
    case 'draft':
      color = COLORS.yellow;
      status = 'Pending';
      break;

    case '1st_appr':
      color = COLORS.green;
      status = 'Approved by Manager';
      break;

    case 'approved':
      color = COLORS.green;
      status = 'Approved';
      break;

    case 'rejected':
      color = COLORS.notify;
      status = 'Rejected';
      break;

    default:
      color = 'grey';
      break;
  }

  const TextView = ({title, value}) => (
    <View style={{flex: 1, alignItems: 'flex-start'}}>
      <Text style={[styles.headerText, {color: COLORS.darkBlack}]}>
        {title}
      </Text>
      <Text
        style={[
          FontStyle.Regular14_500,
          {marginTop: 4, color: COLORS.darkBlack},
        ]}>
        {value}
      </Text>
    </View>
  );

  return (
    <View
      style={[
        styles.outerContainer,
        {
          backgroundColor: detail ? COLORS.white : 'transparent',
        },
      ]}>
      <TouchableOpacity
        style={styles.container}
        activeOpacity={0.95}
        onPress={() => setOPenDetail(!detail)}>
        <View style={styles.headerView}>
          {employeeLoan && (
            <Text style={styles.headerText}>{data?.employee_name}</Text>
          )}
          <Text
            style={[
              styles.headerText,
              {
                ...(employeeLoan && FontStyle.Regular14_500M),
                color: employeeLoan ? COLORS.grey5 : COLORS.darkBlack,
                marginTop: 4,
              },
            ]}>
            {data?.loan_type_name}
          </Text>
          <Text style={styles.dateText}>
            {getFormattedDate(data?.date, false)}
          </Text>
        </View>
        <View style={styles.iconView}>
          <Text
            style={[
              FontStyle.Regular14_500,
              {color: color ? color : COLORS.primaryColor, fontWeight: '700'},
            ]}>
            {status}
          </Text>
          <View style={{alignItems: 'flex-end'}}>
            {I18nManager.isRTL ? (
              <SvgXml
                xml={detail ? Icons.downArrow : Icons.greaterArrow}
                style={{transform: [{scaleX: -1}]}}
              />
            ) : (
              <SvgXml xml={detail ? Icons.downArrow : Icons.greaterArrow} />
            )}
          </View>
        </View>
      </TouchableOpacity>

      {detail && (
        <View style={styles.detailView}>
          <View style={{flexDirection: 'row'}}>
            <TextView title={t('Requested')} value={data?.requested_amount} />
            <TextView title={t('Approved')} value={data?.approved_amount} />
            <TextView title={'Remaining'} value={data?.remaining_amount} />
          </View>
          <View style={{marginTop: 12}}>
            <TextView title={t('Reason')} value={data?.reason} />
          </View>
        </View>
      )}
    </View>
  );
};

export default LoanCard;

const styles = StyleSheet.create({
  outerContainer: {
    ...Theme.Shadow,
    marginHorizontal: 4,
    marginTop: 2,
    borderWidth: 0,
    backgroundColor: COLORS.white,
  },

  container: {
    ...Theme.Shadow,
    borderWidth: 0,
    padding: 8,
    flexDirection: 'row',
  },

  headerView: {
    flex: 1,
    alignItems: 'flex-start',
  },

  iconView: {
    justifyContent: 'space-between',
  },

  headerText: {
    ...FontStyle.Regular14_500,
    fontWeight: '700',
    color: COLORS.darkBlack,
    alignSelf: 'flex-start',
  },

  dateText: {
    ...FontStyle.Regular14,
    color: COLORS.grey5,
    fontWeight: '400',
    marginTop: 4,
    alignSelf: 'flex-start',
  },

  detailView: {
    marginTop: 8,
    paddingVertical: 8,
    paddingHorizontal: 20,
  },

  statusView: {
    borderRadius: 8,
    width: 88,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    ...Theme.Shadow,
    borderWidth: 0,

    backgroundColor: COLORS.red1,
  },

  statusView1: {
    alignSelf: 'flex-end',
    ...Theme.Shadow,
    width: 87,
    height: 27,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 12,
  },

  statusText: {
    ...FontStyle.Regular10,
    fontWeight: '500',
  },
});
