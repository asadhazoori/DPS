import {
  I18nManager,
  Image,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import { FontStyle } from '../../theme/FontStyle';
import { SvgXml } from 'react-native-svg';
import { COLORS } from '../../theme/colors';
import { useSelector } from 'react-redux';
import { DashboardIcons } from '../../assets/SvgIcons/DashboardIcons';
import { ProfileIcons } from '../../assets/SvgIcons/ProfileIcons';
import Theme from '../../theme/theme';
import ReactNativeVI from '../Helpers/ReactNativeVI';

const GeneralHeader = ({
  title,
  navigation,
  profileIcon = true,
  backIcon = true,
  notificationIcon = true,
  rightIcon = false,
  manualAttendance = false,
}) => {
  const isRTL = I18nManager.isRTL;
  const name = useSelector(state => state.employeeProfile.name);
  const pendingReqs = useSelector(state => state?.attendance?.pendingReqs);
  const profileData = useSelector(state => state.employeeProfile.data);
  const totalUnreadCount = useSelector(state => state?.chat?.totalUnreadCount);

  return (
    <View style={[styles.container, { paddingLeft: backIcon ? 4 : 16 }]}>
      {backIcon && (
        <Pressable
          style={({ pressed }) => [
            styles.backIconView,
            pressed && styles.pressedView,
          ]}
          onPress={() => navigation.goBack()}>
          <View style={styles.alignCenter}>
            <ReactNativeVI
              Lib={'MaterialIcons'}
              name={isRTL ? 'arrow-forward-ios' : 'arrow-back-ios-new'}
              size={24}
              color={'black'}
            />
          </View>
        </Pressable>
      )}

      <View style={styles.textView}>
        <Text style={FontStyle.Regular18}>{title}</Text>
      </View>

      {/* <TouchableOpacity style={styles.iconView}
                activeOpacity={0.5}
                onPress={() => { navigation.openDrawer() }}>
                <SvgXml xml={HeaderIcons.burger} />
            </TouchableOpacity> */}
      {manualAttendance && profileData?.enable_manual_attendance && (
        <TouchableOpacity
          style={styles.iconView}
          activeOpacity={0.5}
          onPress={() => {
            navigation.navigate('ManualAttendanceReqs');
          }}>
          {/* <SvgXml xml={DashboardIcons.timesheet} /> */}
          <ReactNativeVI
            name={'calendar-clock-outline'}
            Lib={'MaterialCommunityIcons'}
            size={30}
            color={COLORS.primaryColor}
          />
        </TouchableOpacity>
      )}

      {rightIcon && (
        <TouchableOpacity
          style={styles.reqIconView}
          activeOpacity={0.5}
          onPress={() => navigation.navigate('AttendanceRequests')}>
          <View style={{ position: 'absolute' }}>
            <SvgXml xml={DashboardIcons.requests2} />
          </View>
          <View
            style={[styles.reqIcon, { marginLeft: I18nManager.isRTL ? -4 : 4 }]}>
            {pendingReqs > 0 ? (
              <Text style={styles.reqNoText}>{pendingReqs}</Text>
            ) : (
              <SvgXml xml={ProfileIcons.person1} />
            )}
          </View>
        </TouchableOpacity>
      )}
      {profileData?.enable_notification && notificationIcon && (
        <TouchableOpacity
          style={styles.notifIcon}
          activeOpacity={0.5}
          onPress={() => {
            navigation.navigate('HomeChats');
          }}>
          <View style={styles.outerDotView}>
            <View style={{ position: 'absolute' }}>
              <ReactNativeVI
                color={COLORS.primaryColor}
                size={30}
                Lib={'Ionicons'}
                name={'notifications'}
              />
            </View>
            {totalUnreadCount > 0 && <View style={styles.dotView}></View>}
          </View>
        </TouchableOpacity>
      )}
      {profileIcon && (
        <TouchableOpacity
          style={styles.imageBack}
          activeOpacity={0.85}
          onPress={() => navigation.navigate('Profile')}>
          {profileData?.image_1920 ? (
            <View style={{ flex: 1 }}>
              <Image
                source={{
                  uri: `data:image/jpeg;base64,${profileData?.image_1920}`,
                }}
                style={styles.imgView}
              />
            </View>
          ) : (
            <View style={styles.profileIconView}>
              <ReactNativeVI
                Lib={'MaterialCommunityIcons'}
                name={'account'}
                color={'#a9adc1'}
                size={20}
              />
            </View>
          )}
        </TouchableOpacity>
      )}
    </View>
  );
};

export default GeneralHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 14,
    paddingRight: 16,
    paddingLeft: 4,
    marginBottom: 3,
    alignItems: 'center',
  },
  backIconView: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    borderRadius: 100,
    width: 40,
    height: 40,
  },
  pressedView: {
    opacity: 0.8,
    backgroundColor: COLORS.grey4,
  },
  alignCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  textView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  reqIconView: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: 12,
    width: 26,
    alignItems: 'center',
  },
  reqIcon: {
    height: 16,
    width: 16,
    borderRadius: 100,
    backgroundColor: COLORS.primaryColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
  reqNoText: {
    ...FontStyle.Regular10,
    color: COLORS.white,
    fontWeight: '700',
    fontSize: 8,
    textAlign: 'center',
  },
  notifIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 12,
  },
  outerDotView: {
    height: 30,
    width: 30,
    alignItems: 'flex-end',
  },
  dotView: {
    height: 11,
    width: 11,
    borderRadius: 100,
    backgroundColor: COLORS.red,
    marginTop: 4,
    marginRight: 2,
  },

  iconView: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingHorizontal: 6,
    marginRight: 6,
  },

  imageBack: {
    ...Theme.ImageShadow,
    height: 40,
    width: 40,
    borderRadius: 9999,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgView: {
    height: 40,
    width: 40,
  },
  profileIconView: {
    ...Theme.ImageShadow,
    backgroundColor: '#fafafb',
    height: 40,
    width: 40,
    flex: 1,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
