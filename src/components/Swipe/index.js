import Swipe from "./Swipe"
import SwipeReverse from "./SwipeReverse"
import SwipeReverseRTL from "./SwipeReverseRTL"
import SwipeRTL from "./SwipeRtl"

export {
    Swipe,
    SwipeRTL,
    SwipeReverse,
    SwipeReverseRTL
}