import React, { createContext, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import inputValidation from '../utilities/Validations/YupValidate';
import { LoginSchema } from '../utilities/Validations';
import { siginin } from '../redux/users/actions/signin';
import AsyncStorage from '@react-native-async-storage/async-storage';
import i18next from '../i18n/services/i18next';
import { useTranslation } from 'react-i18next';
import RNRestart from 'react-native-restart';
import { I18nManager, Platform, View } from 'react-native';
import { matchServerVerions } from '../utilities/helpers/matchServerVersion';
import { getEmployeeProfile } from '../redux/profile/actions/getEmployeeProfile';
import { AppVersion } from '../utilities/constant/version';
import { compareVersions } from '../utilities/helpers/checkLowerVersion';
import ReactNativeVI from '../components/Helpers/ReactNativeVI';
import { useEffect } from 'react';

const AuthContext = createContext();

const AuthProvider = ({ children, navigation, status }) => {
  const input1Ref = useRef(null);
  const input2Ref = useRef(null);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [lngModalVisible, setLngModalVisible] = useState(false);
  const [serverModal, setServerModal] = useState(false);
  const { IP, selectedDB } = useSelector(state => state.server);

  const [inputs, setInputs] = useState({
    username: null,
    password: null,
    loggedIn: true,
    errors: null,
  });

  const [alertBox, setAlertBox] = useState({
    showBox: false,
    title: null,
    message: null,
    confirmbtn: null,
    retry: null,
    icon: null,
    updateBtn: null,
    okButton: false,
  });

  useEffect(() => {
    if (status?.message) {
      setTimeout(() => {
        setAlertBox({
          showBox: true,
          okButton: true,
          title: t("already-logged-in"),
          message: `${t("already-logged-in-msg")}: ${status.deviceeName}`,
          icon: (
            <View style={iconView}>
              <ReactNativeVI
                Lib={'Entypo'}
                name={'mobile'}
                color={'#F73C3C'}
                size={26}
              />
            </View>
          ),
        });
      }, 500)
    }
  }, [status?.message])

  const handleOnChange = (field, value) => {
    setInputs({
      ...inputs,
      [field]: value,
      errors: {
        ...inputs.errors,
        [field]: false,
      },
    });
  };

  const iconView = {
    backgroundColor: '#F73C3C3D',
    height: 48,
    width: 48,
    borderRadius: 48,
    justifyContent: 'center',
    alignItems: 'center',
  };

  const validate = async () => {
    const schema = LoginSchema(t);
    const result = await inputValidation(schema, inputs);

    if (result.isValidate) {
      if (IP && selectedDB) {
        handleLogin();
      } else {
        setServerModal(true);
      }
    } else {
      setInputs(prev => ({
        ...prev,
        errors: result?.err,
      }));
    }
  };

  const handleLogin = async () => {
    try {
      setLoading(true);

      const userID = await dispatch(
        siginin({ navigation, inputs, DB: selectedDB, setLoading }),
      );

      if (userID) {
        const versionMatched = await matchServerVerions({ navigation, dispatch });
        if (versionMatched == 'matched') {
          const result = await dispatch(
            getEmployeeProfile({
              uid: userID,
              navigation,
              setLoading,
            }),
          );

          if (result?.message === "Already Logged In") {
            setTimeout(() => {
              setAlertBox({
                showBox: true,
                okButton: true,
                title: t("already-logged-in"),
                message: `${t("already-logged-in-msg")}: ${result.deviceeName}`,
                icon: (
                  <View style={iconView}>
                    <ReactNativeVI
                      Lib={'Entypo'}
                      name={'mobile'}
                      color={'#F73C3C'}
                      size={26}
                    />
                  </View>
                ),
              });
            }, 300);
          }
        } else if (versionMatched == 'Null') {
          setLoading(false)
          setTimeout(() => {
            setAlertBox({
              showBox: true,
              confirmbtn: false,
              okButton: true,
              retry: 'retry',
              title: 'Server Version Not Configured',
              message: `${t(
                'server-update-msg',
              )}\nApp Version: ${AppVersion}\nServer Version: ${versionMatched}`,
              icon: (
                <View style={iconView}>
                  <ReactNativeVI
                    Lib={'FontAwesome5'}
                    name={'server'}
                    color={'#F73C3C'}
                    size={26}
                  />
                </View>
              ),
            });
          }, 300);
        } else if (versionMatched !== 'matched' && versionMatched !== 'session_expired') {
          const lowerVersion = compareVersions(AppVersion, versionMatched);
          setLoading(false)
          setTimeout(() => {
            setAlertBox({
              showBox: true,
              confirmbtn: false,
              okButton: true,
              retry: 'retry',
              updateBtn:
                Platform.OS == 'android' && lowerVersion == 'updateApp'
                  ? true
                  : false,
              title:
                lowerVersion == 'updateServer'
                  ? t('server-update-req')
                  : t('app-update-req'),
              message: `${lowerVersion == 'updateServer'
                ? t('server-update-msg')
                : t('app-update-msg')
                }\nApp Version: ${AppVersion}\nServer Version: ${versionMatched}`,
              icon: (
                <View style={iconView}>
                  <ReactNativeVI
                    Lib={
                      lowerVersion == 'updateServer'
                        ? 'FontAwesome5'
                        : 'MaterialIcons'
                    }
                    name={
                      lowerVersion == 'updateServer'
                        ? 'server'
                        : 'system-update'
                    }
                    color={'#F73C3C'}
                    size={26}
                  />
                </View>
              ),
            });
          }, 300);
        } else {
          setLoading(false);
          setTimeout(() => {
            setAlertBox({
              showBox: true,
              confirmbtn: false,
              okButton: true,
              title: 'Allowed Companies Not Found',
              message: `${t('Add company in users')}`,
              icon: (
                <View style={iconView}>
                  <ReactNativeVI
                    Lib={'FontAwesome5'}
                    name={'server'}
                    color={'#F73C3C'}
                    size={26}
                  />
                </View>
              ),
            });
          }, 300);
        }

      }
      // setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log('handleLogin in Wrapper', error);
    }
  };

  const changeLng = async rtl => {
    const lng = rtl ? 'ar' : 'en';
    AsyncStorage.setItem('language', lng);
    I18nManager.forceRTL(rtl);
    i18next.changeLanguage(lng);
    RNRestart.Restart();
  };

  const handleInput1Forward = () => {
    input2Ref.current.focus();
  };

  const closeCustomAlert = () => {
    setTimeout(() => {
      setAlertBox({ showBox: false });
    }, 300);
  };

  return (
    <AuthContext.Provider
      value={{
        loading,
        inputs,
        validate,
        handleOnChange,
        setInputs,
        t,
        changeLng,
        lngModalVisible,
        setLngModalVisible,
        serverModal,
        setServerModal,
        input1Ref,
        input2Ref,
        handleInput1Forward,
        alertBox,
        closeCustomAlert,
      }}>
      {children}
    </AuthContext.Provider>
  );
};

export { AuthContext, AuthProvider };
