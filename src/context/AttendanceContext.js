import React, {createContext, useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {getDailyAttendance} from '../redux/attendance/actions/getDailyAttendance';
import {getChatGroups} from '../screens/chat/redux/apis/getChatGroups';
import {useNavigation} from '@react-navigation/native';

const AttendanceContext = createContext();

const AttendanceProvider = ({children}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {employeeID, data} = useSelector(state => state.employeeProfile);
  const {firstLogin, loggedIn} = useSelector(state => state.signin);

  const getData = () => {
    dispatch(getDailyAttendance({navigation, employeeID: employeeID}));
  };

  useEffect(() => {
    if (firstLogin && loggedIn) {
      const now = new Date();
      const midnight = new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate() + 1,
        0,
        0,
        0,
      );
      const timeUntilMidnight = midnight - now;

      const firstTimeout = setTimeout(() => {
        getData();
      }, timeUntilMidnight);

      return () => {
        clearTimeout(firstTimeout);
      };
    }
  }, [firstLogin, loggedIn, employeeID]);

  // useEffect(() => {
  //   if (firstLogin && loggedIn & data?.enable_notification) {
  //     const delay = 5 * 60 * 1000;

  //     const timer = setTimeout(() => {
  //       getChatGroups({navigation, dispatch});
  //     }, delay);

  //     return () => clearTimeout(timer);
  //   }
  // }, [firstLogin, loggedIn, data?.enable_notification]);

  return (
    <AttendanceContext.Provider value={{}}>
      {children}
    </AttendanceContext.Provider>
  );
};

export {AttendanceContext, AttendanceProvider};
