import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import store from './src/redux/store';
import {Provider} from 'react-redux';
import {I18nextProvider} from 'react-i18next';
import i18next from './src/i18n/services/i18next';
import messaging from '@react-native-firebase/messaging';
// AppRegistry.registerComponent(appName, () => App);

// messaging().setBackgroundMessageHandler(async remoteMessage => {
//     console.log('Message handled in the background!', remoteMessage);
// });

const AppRedux = () => (
  <Provider store={store}>
    <I18nextProvider i18n={i18next}>
      <App />
    </I18nextProvider>
  </Provider>
);

AppRegistry.registerComponent(appName, () => AppRedux);
