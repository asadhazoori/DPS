import React, { useEffect, useRef } from 'react';
import Navigation from './src/navigation';
import { Provider, useDispatch } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import store, { persistedStore } from './src/redux/store';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';
import handleLocalNotification from './src/utilities/helpers/handleLocalNotification';
import { NavigationContainer } from '@react-navigation/native';
import { Platform } from 'react-native';
import { getChatGroups } from './src/screens/chat/redux/apis/getChatGroups';

const App = () => {
  const navigationRef = useRef();
  let lastNotificationId = useRef(null);
  const dispatch = useDispatch();

  PushNotification.configure({
    onNotification: function (notification) {
      if (notification?.userInteraction) {
        navigationRef.current.navigate("HomeChats");
      }

    },
    // requestPermissions: Platform.OS === 'ios',
    requestPermissions: true,
  });

  const handleNotificationTap = (notification) => {
    if (navigationRef?.current?.isReady()) {
      navigationRef.current.navigate("HomeChats");
    }
  };


  useEffect(() => {
    const unsubscribeForeground = messaging().onMessage(async (remoteMessage) => {
      if (remoteMessage?.messageId && lastNotificationId.current !== remoteMessage.messageId) {
        lastNotificationId.current = remoteMessage.messageId;
        getChatGroups({ navigation: navigationRef, dispatch });
        handleLocalNotification({ remoteMessage });
      }
    });

    messaging().setBackgroundMessageHandler(async (remoteMessage) => {
      if (remoteMessage) {
        console.log('Background message received:', remoteMessage)
        // handleLocalNotification({ remoteMessage });
      };
    });


    const unsubscribeBackground = messaging().onNotificationOpenedApp(async (remoteMessage) => {
      handleNotificationTap(remoteMessage);
    });

    return () => {
      unsubscribeForeground();
      if (unsubscribeBackground) unsubscribeBackground();
    };
  }, []);


  useEffect(() => {
    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        if (remoteMessage) {
          if (navigationRef.current?.isReady()) {
            navigationRef.current.navigate("HomeChats");
          }
        }
      })
      .catch((error) => console.error("Error fetching initial notification:", error));
  }, []);


  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistedStore}>
          <NavigationContainer ref={navigationRef}>
            <Navigation />
          </NavigationContainer>
        </PersistGate>
      </Provider>
    </GestureHandlerRootView>
  );
};

export default App;
